const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const nodemailer = require("nodemailer");

const ref = admin.database().ref();

 // Create and Deploy Your First Cloud Functions
 // https://firebase.google.com/docs/functions/write-firebase-functions

exports.calculateEstimatedAreasAndPanels = functions.database.ref('/components/{id}').onWrite(( snap ) => {

  console.log(JSON.stringify(snap.after));
  
  const component = snap.after.val();
  const projectRef = component.project;
 
  return calculateEstimatedAreasAndPanels(projectRef)
    .then(() => {
      return console.log("Estimates calculated!");
    });
     
});

exports.updateProductionPlan = functions.https.onCall((data, context) => {
  
  var plan = data.plan;
 
  return buildAreasPlan(plan)
          .then(() => {
            return Promise.resolve('Update Production Plan Complete.');
           });

});

exports.panelsPlanned = functions.https.onRequest((request, response) => {
  return getAheadPlans()
    .then(function (res) {
      console.log(res);
      console.log('panel planned counter updated.');
      return response.send(res);
     }, function (error) {
      console.log(error);
      return response.send(error);
     });
});

exports.dailyCountUpStats = functions.https.onRequest((request, response) => {
    return dailyCountUpStats()
    .then(function (res) {
      console.log(res);
      console.log('panel daily count up updated.');
      return response.send(res);
     }, function (error) {
      console.log(error);
      return response.send(error);
     });
});
 
exports.panelsDeliveredToday = functions.https.onRequest((request, response) => {
  return countPanelsDeliveredToday()
    .then(function (res) {
      console.log(res);
      console.log('panel delivered counter updated.');
      return response.send(res);
     }, function (error) {
      console.log(error);
      return response.send(error);
     });
});

exports.panelComplete = functions.database.ref('/panels/{id}/qa/completed').onWrite(( change ) => {
  var collectionRef;
  var panel;
  
  var productLine;
  
  if (change.after.exists() && !change.before.exists()) {
    collectionRef = change.after.ref.parent.parent;
  } else if (!change.after.exists() && change.before.exists()) {
    collectionRef = change.before.ref.parent.parent;
  } else {
    return null;
  }
  
  return collectionRef.once('value').then((snapshot) => {
    panel = snapshot.val();
  })
  .then(() => {
    return getProductTypes()
      .then((productTypeProductionLine) => {
        productLine = productTypeProductionLine[panel.type];
      });
  })
  .then(() => {
      let increment;
      let date;
      let add;
      let count;
      if (change.after.exists() && !change.before.exists()) {
        increment = 1;
        add = true;
        date = new Date(change.after.val());
        date = date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
      } else if (!change.after.exists() && change.before.exists()) {
        increment = -1;
        add = false;
        date = new Date(change.before.val());
        date = date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
      } else {
        return null;
      }
      
      const countRef = ref.child("stats").child(productLine).child(date).child("completedCount");
      const m2Ref = ref.child("stats").child(productLine).child(date).child("completedM2");

      // Return the promise from countRef.transaction() so our function
      // waits for this async event to complete before it exits.
      return countRef.transaction(( current ) => {
        count = (current || 0) + increment;
        return count;
      })
      .then(() => {
        return m2Ref.transaction(( current ) => {
          if(add){ 
            return (current || 0) + Math.round(panel.dimensions.area*10)/10;
          }else{
            if(count <= 0){
              return 0;
            }else{
              return (current || 0) - Math.round(panel.dimensions.area*10)/10;
            }   
          }
        });
      }).then(() => {
        return console.log("panel complete counter updated.");
      });
  });
});

exports.panelStart = functions.database.ref('/panels/{id}/qa/started').onWrite(( change ) => {
  var collectionRef;
  var panel;
  
  var productLine;
  
  if (change.after.exists() && !change.before.exists()) {
    collectionRef = change.after.ref.parent.parent;
  } else if (!change.after.exists() && change.before.exists()) {
    collectionRef = change.before.ref.parent.parent;
  } else {
    return null;
  }
  
  return collectionRef.once('value').then((snapshot) => {
    panel = snapshot.val();
  })
  .then(() => {
    return getProductTypes()
      .then((productTypeProductionLine) => {
        productLine = productTypeProductionLine[panel.type];
      });
  })
  .then(() => {
      let increment;
      let date;
      let add;
      let count;
      if (change.after.exists() && !change.before.exists()) {
        increment = 1;
        add = true;
        date = new Date(change.after.val());
        date = date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
      } else if (!change.after.exists() && change.before.exists()) {
        increment = -1;
        add = false;
        date = new Date(change.before.val());
        date = date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
      } else {
        return null;
      }
      
      const countRef = ref.child("stats").child(productLine).child(date).child("startedCount");
      const m2Ref = ref.child("stats").child(productLine).child(date).child("startedM2");
      
      // Return the promise from countRef.transaction() so our function
      // waits for this async event to complete before it exits.
      return countRef.transaction(( current ) => {
        count = (current || 0) + increment;
        return count;
      })
      .then(() => {
        return m2Ref.transaction(( current ) => {
          if(add){ 
            return (current || 0) + Math.round(panel.dimensions.area*10)/10;
          }else{
            if(count <= 0){
              return 0;
            }else{
              return (current || 0) - Math.round(panel.dimensions.area*10)/10;
            }  
          }
        });
      }).then(() => {
        return console.log("panel start counter updated.");
      });
  });
});

function getProductTypes() {
  var productTypeProductionLine = {};
  var productTypes = ProductTypes();
  return productTypes
    .then(function ( snapshot ) {
      var productTypes = snapshot.val();
      productTypeProductionLine = buildMap(productTypes, "productionLine");
      return productTypeProductionLine;
    });
}

function getReferenceData(key) {
    return ref.child("srd").child(key).once('value');
} 

function ProductTypes() {
  return getReferenceData("productTypes");
}

function buildMap(items, property) {
  var map = {};
  for (var key in items) {
    var item = items[key];
      map[key] = item[property];
  }
  return map;
}

function panelsForArea(areaKey) {
  return ref.child('panels').orderByChild('area').equalTo(areaKey).once('value');
}

function getAheadPlans(){
  var today = new Date();
  today = today.getFullYear()+"-" + ("0" + (today.getMonth()+1)).slice(-2) + "-"+ ("0" + today.getDate()).slice(-2);
  
  var weekAhead = plusDays(today, 7);
  var dayAhead = plusDays(today, 1);

  var productTypeProductionLine = {};


  var promise = new Promise(function(resolve, reject) {
    
    getProductTypes()
    .then((productionLine) => {
      productTypeProductionLine = productionLine;
      return getAllPlans();
    })
    .then(function (snap) {
      return processPlan(snap);
    })
    .then(function () {
      return resolve("Sucsess: Resolved. Planned week today count");
    }, function (error) {
      return reject("Error: Rejected. Planned week today count | - " + error);
    }); 
  });
  
  return promise;
  
  function processPlan(snap) {
    var promises = [];
    snap.forEach(childSnap => {
      var areaPlan = childSnap.val();
      var areaKey = childSnap.key;
      if (areaPlan.buildDays !== undefined && areaPlan.buildDays[weekAhead] !== undefined) {
        var buildDay = areaPlan.buildDays[weekAhead];
        promises.push(updateAreaWeekAhead(areaKey, buildDay));
      }
      if (areaPlan.buildDays !== undefined && areaPlan.buildDays[dayAhead] !== undefined) {
        var buildDay = areaPlan.buildDays[dayAhead];
        promises.push(updateAreaDayAhead(areaKey, buildDay));
      }
    });

    return Promise.all(promises);
  }
 

  function updateAreaWeekAhead(areaKey, buildOnDay) {
    return getAreaFromRef(areaKey)
            .then(( areaSnap ) => {
              var area = areaSnap.val();
              if (area !== null && area.supplier === "Innovaré" && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
                return ref.child("additionalData/areas/" + areaKey + "/buildPlannedDays/" + weekAhead).set(buildOnDay);
              }
            });
  }
  
  function updateAreaDayAhead(areaKey, buildOnDay) {
    return getAreaFromRef(areaKey)
            .then(( areaSnap ) => {
              var area = areaSnap.val();
              if (area !== null && area.supplier === "Innovaré" && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
                return ref.child("additionalData/areas/" + areaKey + "/buildPlannedDays2/" + dayAhead).set(buildOnDay);
              }
            });
  }

}

function countPanelsDeliveredToday(){
  var today = new Date();
  today = today.getFullYear()+"-" + ("0" + (today.getMonth()+1)).slice(-2) + "-"+ ("0" + today.getDate()).slice(-2);

  var matchingAreas = {};
  var matchingPanels = {};
  var areaUnpublishedDate = [];
  var productTypeProductionLine = {};
  
  var promise = new Promise(function(resolve, reject) {
    return getProductTypes()
      .then(getProjects)
      .then(processProjects)
      .then(panelsToBeDeliveredToday)
      .then(passToSave)
      .then(function () {
        return resolve("Sucsess: Resolved. Delivered today count");
      }, function (error) {
        return reject("Error: Rejected. Delivered today count | - " + error);
      }); 
  });
  
  return promise;
  
  function getProductTypes() {
    var productTypes = ProductTypes();
    return productTypes
      .then(function ( snapshot ) {
        var productTypes = snapshot.val();
        productTypeProductionLine = buildMap(productTypes, "productionLine");
      });
  }

  function processProjects(snap) {
    var promises = [];
    snap.forEach(childSnap => {
      var project = childSnap.val();
      var projectKey = childSnap.key;
      if (project.deliverySchedule.published !== undefined) {
        // get areas with published delivery date within specified range
        promises.push(findAreasWithMatchingDeliveryDate(projectKey, project));
      }
    });
    return Promise.all(promises);
  }

  function findAreasWithMatchingDeliveryDate(projectKey, project) {
    return getProjectAreas(projectKey)
      .then(function (snap) {
        snap.forEach(childSnap => {
          var area = childSnap.val();
          var areaKey = childSnap.key;
          if (area.revisions !== undefined && area.revisions[project.deliverySchedule.published] !== undefined) {
            var revisionDate = area.revisions[project.deliverySchedule.published];
            if (revisionDate === today) {
              areaUnpublishedDate[area.$id] = project.deliverySchedule.published;
            }
          }
        });
      });
  }
  
  function panelsToBeDeliveredToday(){
    var promises = [];
    for (var key in matchingAreas) {
        promises.push(
          panelsForArea(key)
            .then(function (snap) {
              snap.forEach(childSnap => {
                var panel = childSnap.val();
                var productionLine = productTypeProductionLine[panel.type];
                if(matchingPanels[productionLine] === undefined && productionLine !== undefined){
                  matchingPanels[productionLine] = {
                    count: 0,
                    m2: 0
                  };
                }
                if(productionLine !== undefined){
                  matchingPanels[productionLine].count++;
                  matchingPanels[productionLine].m2 += Math.round(panel.dimensions.area*10)/10;
                }
              });
            })
          );
    }
    return Promise.all(promises);
  }
  
  function passToSave(){
    var promises = [];
    for (var key in matchingPanels) {
      if(matchingPanels[key].count <= 0){
        matchingPanels[key].m2 = 0;
        matchingPanels[key].count = 0;
      }
      promises.push(saveToStats(key, matchingPanels[key].count, matchingPanels[key].m2));
    }
    return Promise.all(promises);
  }
  
  function saveToStats(productionLine, count, m2){
    const countRef = ref.child("stats").child(productionLine).child(today).child("deliveredCount");
    const m2Ref = ref.child("stats").child(productionLine).child(today).child("deliveredM2");
    return countRef.transaction(() => {
      return count;
    })
    .then(() => {
        return m2Ref.transaction(() => {
          return m2;
        });
    }).then(() => {
      return console.log("panel delivery counter updated.");
    });
  }
}

exports.panelsCompletedTodayEmail = functions.https.onRequest((request, response) => {
  
  var gmailEmail = functions.config().gmail.email;
  var gmailPassword = functions.config().gmail.password;
  var mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword
    }
  });

  var panData = {};

  var todayDate = new Date();
  var start = todayDate.setHours(0,0,0,0);
  var end = todayDate.setHours(23,59,59,999);

  return getProductTypes()
          .then(function (productTypeProductionLine) {
            return getPanels(productTypeProductionLine, start, end);
          })
          .then(function (panData) {
            sendMail(panData);
          })
          .then(function () {
            return response.send("Panels complete emails sent");
          }, function (error) {
            return response.send(error);
          });

  function getPanels(productTypeProductionLine, start, end){
    var panels = ref.child("panels").orderByChild("qa/completed").startAt(start).endAt(end);
    return panels.once("value", function (snap) {
      snap.forEach(childSnap => {
        var panel = childSnap.val();
        if(panData[productTypeProductionLine[panel.type]] === undefined){
          panData[productTypeProductionLine[panel.type]] = [];
        }
        panData[productTypeProductionLine[panel.type]].push(panel);
      });
    });

  }

  function getProductTypes() {
    var productTypeProductionLine = {};
    var productTypes = ProductTypes();
    return productTypes
      .then(function ( snapshot ) {
        var productTypes = snapshot.val();
        productTypeProductionLine = buildMap(productTypes, "productionLine");
        return productTypeProductionLine;
      });
  }

  function getReferenceData(key) {
      return ref.child("srd").child(key).once('value');
  } 

  function ProductTypes() {
    return getReferenceData("productTypes");
  }

  function buildMap(items, property) {
    var map = {};
    for (var key in items) {
      var item = items[key];
        map[key] = item[property];
    }
    return map;
  }

  function sendMail(){

    var today = new Date();
    today = ("0" + today.getDate()).slice(-2)+"/" + ("0" + (today.getMonth()+1)).slice(-2) + "/"+ today.getFullYear();

    var html = "";

    html += "The following panels were completed today ("+today+"): ";
    html += "<br>";
    html += "<br>";
    html += "<table style='padding: 5px;border: 1px solid black;border-collapse: collapse;width:100%;font-family: Verdana, Geneva, sans-serif;'>";
    html += "<tr>";
    html += "<th style='padding: 5px;border: 1px solid black;text-align: center;font-family: Verdana, Geneva, sans-serif;'>Production Line</th>";
    html += "<td style='font-weight: bold;padding: 5px;border: 1px solid black;text-align: center;font-family: Verdana, Geneva, sans-serif;'>Panels</td>";
    html += "</tr>";
    for (var productionLine in panData){
      var panels = panData[productionLine];
      html += "<tr>";
      html += "<th style='padding: 5px;border: 1px solid black;text-align: center;font-family: Verdana, Geneva, sans-serif;' rowspan='"+panels.length+"'>"+productionLine+"</th>";
      html += "<td style='padding: 5px;border: 1px solid black;text-align: center;font-family: Verdana, Geneva, sans-serif;'>"+panels[0].id+"</td>";
      html += "</tr>";
      for (var i = 1; i < panels.length; i++){
        var panel = panels[i];
        html += "<tr>";
        html += "<td style='padding: 5px;border: 1px solid black;text-align: center;font-family: Verdana, Geneva, sans-serif;'>"+panel.id+"</td>";
        html += "</tr>";
      }
    }
    html += "</table>";
    html += "<br>";
    html += "Innview";

    return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "danny.boyce@innovaresystems.co.uk", subject:  "Panels completed today - " + today, html: html})
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Stephen.Lynas@innovaresystems.co.uk", subject:  "Panels completed today - " + today, html: html});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Daniel.Phillips@innovaresystems.co.uk", subject: "Panels completed today - " + today, html: html});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "stephen.cromey@innovaresystems.co.uk", subject: "Panels completed today - " + today, html: html});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Neil.Sisk@innovaresystems.co.uk", subject: "Panels completed today - " + today, html: html});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "jason.powell@innovaresystems.co.uk", subject: "Panels completed today - " + today, html: html});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "James.Nicholls@innovaresystems.co.uk", subject: "Panels completed today - " + today, html: html});
      });

    }
  
});
 
exports.createMaterialRequest = functions.database.ref("/materialrequests/{id}").onCreate((snapshot, context) => {

  var gmailEmail = functions.config().gmail.email;
  var gmailPassword = functions.config().gmail.password;
  var mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword
    }
  });

  var data = snapshot.val();
  var key = context.params.id;
  var dateRequired = new Date(data.dateRequired);
  dateRequired = ("0" + dateRequired.getDate()).slice(-2) + "/" + ("" + (dateRequired.getMonth()+1)).slice(-2) + "/" + dateRequired.getFullYear();

  var html = "Dear " + data.person;
  html += "<br>";
  html += "<br>";
  html += "The following request has been submitted:";
  html += "<br>";
  html += "<br>";
  html += data.projectId + " - " + data.projectName;
  html += "<br>";
  html += "<br>";
  html += "<pre>"+data.materialRequired+"</pre>";
  html += "<br>";
  html += "<br>";
  html += "Requested to be delivered on:";
  html += "<br>";
  html += "<br>";
  html += dateRequired;
  html += "<br>";
  html += "<br>";
  html += "Will be delivered to:";
  html += "<br>";
  html += "<br>";
  html += data.projectName;
  html += "<br>";
  html += "<br>";
  html += "Once actioned, you will receive notification of est. delivery date.";
  html += "<br>";
  html += "<br>";
  html += "PLEASE NOTE: Material requests require a 24hr notification period prior to site delivery";

  var html2 = "A material request has been submitted. Please review and action on InnView <a href='https://innview.firebaseapp.com/#/materialrequests/updateMaterialRequest/"+key+"'>here</a>.";
  html2 += "<br>";
  html2 += "<br>";
  html2 += "Requested by: " + data.person;
  html2 += "<br>";
  html2 += "<br>";
  if(data.siteContact !== undefined){
    html2 += "Site Contact: " + data.siteContact;
    html2 += "<br>";
    html2 += "<br>";
  }
  if(data.siteContact !== undefined){
    html2 += "Site Contact No: " + data.siteContactNo;
    html2 += "<br>";
    html2 += "<br>";
  }
  html2 += "Project: " + data.projectId + " - " + data.projectName;
  html2 += "<br>";
  html2 += "<br>";
  html2 += "Requested Delivery Date: " + dateRequired;
  html2 += "<br>";
  html2 += "<br>";
  html2 += "<pre>"+data.materialRequired+"</pre>";
  html2 += "<br>";
  html2 += "<br>";
  html2 += "PLEASE NOTE: Material request should be actioned within 24hrs.";

  return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "danny.boyce@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2})
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "stephen.cromey@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Stephen.Lynas@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "joseph.andrejczuk@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "benjamin.raybould@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "regan.jeffrey@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Neil.Sisk@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "jason.powell@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "James.Nicholls@innovaresystems.co.uk", subject: "New Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: data.email, subject: "Material Request Submitted - " + data.projectId, html: html});
    });

});

exports.updateMaterialRequest = functions.database.ref("/materialrequests/{id}").onUpdate((snapshot, context) => {

  var gmailEmail = functions.config().gmail.email;
  var gmailPassword = functions.config().gmail.password;
  var mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword
    }
  });
  
  var sendUpdate = false;
  var sendAction = false;
  
  var dataBefore = snapshot.before.val();
  var data = snapshot.after.val();
  var dateRequired = new Date(data.dateRequired);
  dateRequired = ("0" + dateRequired.getDate()).slice(-2) + "/" + ("" + (dateRequired.getMonth()+1)).slice(-2) + "/" + dateRequired.getFullYear();
  var deliveryDate = new Date(data.deliveryDate);
  deliveryDate = ("0" + deliveryDate.getDate()).slice(-2) + "/" + ("" + (deliveryDate.getMonth()+1)).slice(-2) + "/" + deliveryDate.getFullYear();

  if (!dataBefore.action && data.action){
    sendAction = true;
  }
  
  if(dataBefore.person !== data.person
    || dataBefore.email !== data.email
    || dataBefore.projectId !== data.projectId
    || dataBefore.projectName !== data.projectName
    || dataBefore.materialRequired !== data.materialRequired
    || dataBefore.dateRequired !== data.dateRequired
    || dataBefore.siteContact !== data.siteContact
    || dataBefore.siteContactNo !== data.siteContactNo
    || dataBefore.mainContractor !== data.mainContractor
    || dataBefore.deliveryDate !== data.deliveryDate
    || dataBefore.note !== data.note){
    sendUpdate = true;
  }
  
  var html = "Dear " + data.person;
  html += "<br>";
  html += "<br>";
  html += "The following request has been updated:";
  html += "<br>";
  html += "<br>";
  html += data.projectId + " - " + data.projectName;
  html += "<br>";
  html += "<br>";
  html += "<pre>"+data.materialRequired+"</pre>";
  html += "<br>";
  html += "<br>";
  html += "Requested to be delivered on:";
  html += "<br>";
  html += "<br>";
  html += dateRequired;
  html += "<br>";
  html += "<br>";
  html += "Will be delivered to:";
  html += "<br>";
  html += "<br>";
  html += data.projectName;
  html += "<br>";
  html += "<br>";
  html += "Once actioned, you will receive notification of est. delivery date.";
  html += "<br>";
  html += "<br>";
  html += "Your request has the following comments:";
  html += "<br>";
  html += "<br>";
  if (data.note !== undefined) {
    html += data.note;
  } else {
    html += "No comments at this time";
  }
  html += "<br>";
  html += "<br>";
  html += "Delivery Date:";
  html += "<br>";
  html += "<br>";
  if (data.deliveryDate !== undefined) {
    html += deliveryDate;
  } else {
    html += "Not yet scheduled";
  }
  html += "<br>";
  html += "<br>";
  html += "PLEASE NOTE: Material requests require a 24hr notification period prior to site delivery";

  var html2 = "A material request has been updated. Please review and action on InnView here.";
  html2 += "<br>";
  html2 += "<br>";
  html2 += "Requested by: " + data.person;
  html2 += "<br>";
  html2 += "<br>";
  if(data.siteContact !== undefined){
    html2 += "Site Contact: " + data.siteContact;
    html2 += "<br>";
    html2 += "<br>";
  }else{
    html2 += "A site contact has not been added";
    html2 += "<br>";
    html2 += "<br>";
  }
  if(data.siteContact !== undefined){
    html2 += "Site Contact No: " + data.siteContactNo;
    html2 += "<br>";
    html2 += "<br>";
  }else{
    html2 += "A site contact number has not been added";
    html2 += "<br>";
    html2 += "<br>";
  }
  html2 += "Project: " + data.projectId + " - " + data.projectName;
  html2 += "<br>";
  html2 += "<br>";
  html2 += "Requested Delivery Date: " + dateRequired;
  html2 += "<br>";
  html2 += "<br>";
  html2 += "<pre>"+data.materialRequired+"</pre>";
  html2 += "<br>";
  html2 += "<br>";
  html2 += "Your request has the following comments:";
  html2 += "<br>";
  html2 += "<br>";
  if (data.note !== undefined) {
    html2 += data.note;
  } else {
    html2 += "No comments at this time";
  }
  html2 += "<br>";
  html2 += "<br>";
  html2 += "Delivery Date:";
  html2 += "<br>";
  html2 += "<br>";
  if (data.deliveryDate !== undefined) {
    html2 += deliveryDate;
  } else {
    html2 += "Not yet scheduled";
  }
  html2 += "<br>";
  html2 += "<br>";
  html2 += "PLEASE NOTE: Material request should be actioned within 24hrs.";
  
  var html3 = "Dear " + data.person;
  html3 += "<br>";
  html3 += "<br>";
  html3 += "The following request has been submitted: ";
  html3 += "<br>";
  html3 += "<br>";
  html3 += data.projectId + " - " + data.projectName;
  html3 += "<br>";
  html3 += "<br>";
  html3 += "<pre>"+data.materialRequired+"</pre>";
  html3 += "<br>";
  html3 += "<br>";
  html3 += "Your request has the following comments:";
  html3 += "<br>";
  html3 += "<br>";
  if (data.note !== undefined) {
    html3 += data.note;
  } else {
    html3 += "No comments at this time";
  }
  html3 += "<br>";
  html3 += "<br>";
  html3 += "Delivery Date:";
  html3 += "<br>";
  html3 += "<br>";
  if (data.deliveryDate !== undefined) {
    html3 += deliveryDate;
  } else {
    html3 += "Not yet scheduled";
  }

  if (sendUpdate && !sendAction) {
    
  return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "danny.boyce@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2})
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "stephen.cromey@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Stephen.Lynas@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "joseph.andrejczuk@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "benjamin.raybould@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "regan.jeffrey@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Neil.Sisk@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "jason.powell@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "James.Nicholls@innovaresystems.co.uk", subject: "Updated Material Request - " + data.projectId, html: html2});
    })
    .then(function () {
      return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: data.email, subject: "Material Request: Updated - " + data.projectId, html: html});
    });

  } else if (!sendUpdate && sendAction) {
    
    return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: data.email, subject: "Material Request Confirmed - " + data.projectId, html: html3});

  } else if (sendUpdate && sendAction) {
    
    return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "danny.boyce@innovaresystems.co.uk", subject: "Confirmed: Material Request - " + data.projectId, html: html2})
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "stephen.cromey@innovaresystems.co.uk", subject: "Confirmed: Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Stephen.Lynas@innovaresystems.co.uk", subject: "Confirmed: Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "joseph.andrejczuk@innovaresystems.co.uk", subject: "Confirmed Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "benjamin.raybould@innovaresystems.co.uk", subject: "Confirmed Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "regan.jeffrey@innovaresystems.co.uk", subject: "Confirmed Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "Neil.Sisk@innovaresystems.co.uk", subject: "Confirmed Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "jason.powell@innovaresystems.co.uk", subject: "Confirmed Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: "James.Nicholls@innovaresystems.co.uk", subject: "Confirmed Material Request - " + data.projectId, html: html2});
      })
      .then(function () {
        return mailTransport.sendMail({from: "'Innview' <" + gmailEmail + ">", to: data.email, subject: "Material Request Confirmed - " + data.projectId, html: html3});
      });
    
  }
});

exports.innviewRevision2 = functions.database.ref('projects/{projectKey}/deliverySchedule/published').onUpdate((change) => {
  
  var gmailEmail = functions.config().gmail.email2;
  var gmailPassword = functions.config().gmail.password2;
  var mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword
    }
  });
  
  var reciepts = [];
  reciepts.push("Neil.Sisk@innovaresystems.co.uk");
  reciepts.push("jason.powell@innovaresystems.co.uk");
  reciepts.push("James.Nicholls@innovaresystems.co.uk");
  reciepts.push("stephen.cromey@innovaresystems.co.uk");
  reciepts.push("Stephen.Lynas@innovaresystems.co.uk");
  reciepts.push("danny.boyce@innovaresystems.co.uk");

  var parentRef = change.after.ref.parent.parent;
  
  var project = {};
  var reciepts = [];

  return parentRef.once('value').then((snapshot) => {
    
    project = snapshot.val();
    reciepts = [];

    var  promises = [];
    if (project.notifyUsers !== undefined){
      for (var userId in project.notifyUsers) {
        promises.push(getUserById(userId).then((snapshot) => {
          var user = snapshot.val();
          reciepts.push(user);
        }));
      }
    }
    return Promise.all(promises);
  })
  .then(() => {
    return countPanelsDeliveredToday();
  })
  .then(() => {
    var textEntry = "";
    textEntry += "<p>Hi,</p>";
    textEntry += "<p>Revision made on project: " + project.id + " - " + project.name + "</p>";
    textEntry += "<p>Innview</p>";
    
    var promises = [];
    for(var i = 0; i < reciepts.length; i++){
      var reciept = reciepts[i];
      var email = reciept["email"];
      console.log("email sent to " + email + " for Innview Revision");
      promises.push(mailTransport.sendMail({from: "'Innview' <sender2.innovaresystems@gmail.com>",to: email,subject: "Innview Revision",html: textEntry}));
    }
    return Promise.all(promises);
  });

});


exports.innviewWeeklyDispatchUpdater2 = functions.database.ref('projects/{projectKey}/deliverySchedule/published').onUpdate((change) => {
  
  var gmailEmail = functions.config().gmail.email2;
  var gmailPassword = functions.config().gmail.password2;
  var mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword
    }
  });
  
  var reciepts = [];
  reciepts.push("Neil.Sisk@innovaresystems.co.uk");
  reciepts.push("jason.powell@innovaresystems.co.uk");
  reciepts.push("James.Nicholls@innovaresystems.co.uk");
  reciepts.push("hugh.templeton@innovaresystems.co.uk");
  reciepts.push("alex.banks@innovaresystems.co.uk");
  reciepts.push("Stephen.Lynas@innovaresystems.co.uk");
  reciepts.push("danny.boyce@innovaresystems.co.uk");
  reciepts.push("Daniel.Phillips@innovaresystems.co.uk");
  reciepts.push("stephen.cromey@innovaresystems.co.uk");

  var todayDate = new Date();
  var todayDay = todayDate.getDay();
  
  var reSendThisWeeklyUpdate = false;
  var reSendNextWeeklyUpdate = false;
  var checkSendWeeklyThisWeek = false;
  var checkSendWeeklyNextWeek = false;
  
  var nextWeek = plusDays(todayDate, 7);
  
  if(todayDay === 4){
    checkSendWeeklyThisWeek = true;
    checkSendWeeklyNextWeek = true;
  }else if(todayDay === 5){
    checkSendWeeklyThisWeek = true;
    checkSendWeeklyNextWeek = true;
  }else if(todayDay >= 1 && todayDay <= 3){
    checkSendWeeklyThisWeek = true;
  }
  else if(todayDay === 6){
    checkSendWeeklyThisWeek = true;
    checkSendWeeklyNextWeek = true;
  }
  else if(todayDay === 0){
    checkSendWeeklyThisWeek = true;
  }
  
  var thisWeekStart = weekStart(todayDate);
  var thisWeekEnd = plusDays(thisWeekStart, 4);

  var nextWeekStart = weekStart(nextWeek);
  var nextWeekEnd = plusDays(nextWeekStart, 4);

  thisWeekStart = new Date(thisWeekStart);
  thisWeekEnd = new Date(thisWeekEnd);
  nextWeekStart = new Date(nextWeekStart);
  nextWeekEnd = new Date(nextWeekEnd);
  var parentRef = change.after.ref.parent.parent;
  
  var project = {};
  var projectKey;
  
  return parentRef.once('value').then((snapshot) => {
    
    project = snapshot.val();
    projectKey = snapshot.key;

    return getAreasDeliveryDate(projectKey, project);
    
  })
  .then(() => {
    if(reSendThisWeeklyUpdate){
      return queryAndSendWeeklyNotifications(thisWeekStart, thisWeekEnd);
    }else{
      return Promise.resolve("No updates on weekly notification for this week");
    }
  })
  .then(() => {
    if(reSendNextWeeklyUpdate){
      return queryAndSendWeeklyNotifications(nextWeekStart, nextWeekEnd);
    }else{
      return Promise.resolve("No updates on weekly notification for next week");
    }
  });

  function getAreasDeliveryDate(projectKey, project) {

    return getProjectAreas(projectKey)
      .then(snap => {

        snap.forEach(childSnap => {
          
          var area = childSnap.val();

          if (project.deliverySchedule !== undefined && project.deliverySchedule.published !== undefined && project.deliverySchedule.unpublished !== undefined) {
            
            var revisionKey = project.deliverySchedule.published;
            var unpublished = project.deliverySchedule.unpublished;
            
            if (area.revisions !== undefined && area.revisions[revisionKey] !== undefined) {

              var revisionsArr = [];
              var revision = area.revisions[revisionKey];
              var revisions = area.revisions;

              for (var key in revisions) {
                if (key !== revisionKey && key !== unpublished) {
                  revisionsArr.push(revisions[key]);
                }
              }

              var prevRevision = revisionsArr[revisionsArr.length - 1];
              var newRevision = revision;

              revision = new Date(revision);

              if (checkSendWeeklyThisWeek) {
                if (revision >= thisWeekStart && revision <= thisWeekEnd) {
                  if (prevRevision !== newRevision) {
                    reSendThisWeeklyUpdate = true;
                  }
                }
              }
              if (checkSendWeeklyNextWeek) {
                if (revision >= nextWeekStart && revision <= nextWeekEnd) {
                  if (prevRevision !== newRevision) {
                    reSendNextWeeklyUpdate = true;
                  }
                }
              }
            }
          }

        });

      });
  }
  
  function queryAndSendWeeklyNotifications(weekStart, weekEnd) {

    var textEntry = "";
    var areaData = {};
    var areaPlan = {};
    var areasAlerts = 0;
    var projects = {};

    var weekStartHuman = toHumanDate(weekStart);

    weekStart = new Date(weekStart);
    weekEnd = new Date(weekEnd);
    
    textEntry += "<p>Hi,</p>";
    textEntry += "<p>Please see below updated areas due dispatch for week start " + weekStartHuman + ":</p>";   

    return processProjects()
      .then(processPlans)
      .then(processAreasDeliveryDate)
      .then(buildTable)
      .then(sendMail);


    function processProjects() {
      return getProjects()
        .then(snapProjects => {
          snapProjects.forEach(snapProject => {
            var project = snapProject.val();
            var projectKey = snapProject.key;
            if (project.active && project.deliverySchedule && project.deliverySchedule.published) {
              if (projects[projectKey] === undefined) {
                projects[projectKey] = project;
              }
            }
          });
        });
    }

    function processPlans(){
      var promises = [];
      for(var projectKey in projects){
        promises.push(getAreasPlans(projectKey));
      }
      return Promise.all(promises);
    }

    function processAreasDeliveryDate(){
      var promises = [];
      for(var projectKey in projects){
        promises.push(getAreasDeliveryDate(projectKey, projects[projectKey]));
      }
      return Promise.all(promises);
    }

    function getAreasPlans(projectKey) {
      return getPlans(projectKey)
        .then(snap => {
          snap.forEach(childSnap => {
            var area = childSnap.val();
            var areaKey = childSnap.key;
            if (areaPlan[areaKey] === undefined) {
              areaPlan[areaKey] = area;
            }
          });
        });
    }

    function getAreasDeliveryDate(projectKey, project) {
      return getProjectAreas(projectKey)
        .then(snap => {
          snap.forEach(childSnap => {
            var area = childSnap.val();
            var areaKey = childSnap.key;
            if (project.deliverySchedule !== undefined && project.deliverySchedule.published !== undefined) {
              var revisionKey = project.deliverySchedule.published;
              if (area.revisions !== undefined && area.revisions[revisionKey] !== undefined && area.supplier === "Innovaré") {
                var revision = area.revisions[revisionKey];
                revision = subtractWorkingDays(revision, 1);
                revision = new Date(revision);
                var percentage = getCompletedPanelsPercent(area);
                var areaRiskDays = "";
                if (areaPlan[areaKey] !== undefined) {
                  var riskDays = daysBetweenWeekDaysOnly(areaPlan[areaKey].plannedFinish, areaPlan[areaKey].riskDate);
                  if (riskDays < 0) {
                    areaRiskDays = "Risk: " + riskDays + " days";
                  } else {
                    areaRiskDays = "Not at risk";
                  }
                } else {
                  areaRiskDays = "Not at risk";
                }
                if (revision >= weekStart && revision <= weekEnd) {
                  var revisionHumanDate = toHumanDate(revision);
                  if (areaData[revisionHumanDate] === undefined) {
                    areaData[revisionHumanDate] = {};
                  }
                  if (areaData[revisionHumanDate][project.name] === undefined) {
                    areaData[revisionHumanDate][project.name] = [];
                  }
                  var areaId = project.id + "-" + area.phase + "-" + area.floor + "-" + area.type;
                  var area = {
                      "date": revisionHumanDate,
                      "project" : project.name,
                      "area": areaId,
                      "percent": percentage,
                      "risk": areaRiskDays
                    };
                  areaData[revisionHumanDate][project.name].push(area);
                  areasAlerts++;
                }
              }
            }
          });
        });
    }

    function buildTable() {

      areaData = sortDates(areaData);

      var dateCount = 0;

      for (var date in areaData) {
        var projectArray = areaData[date];
        textEntry += "<table style='border: 1px solid black;width:100%'>";
        textEntry += "<tr>";
        textEntry += "<td style='text-align:center;width:15%;'>"+date+"</td>";
        textEntry += "<td style='width:85%;'>";

        for (var project in projectArray) {
          var areasArray = projectArray[project];
          dateCount = 0;
          textEntry += "<table style='table-layout: fixed;border: 1px solid black;width:100%'>";
          textEntry += "<tr>";
          textEntry += "<th style='border: 1px solid #dddddd;'>Project:</th>";
          textEntry += "<td style='border: 1px solid #dddddd;'>Area</td>";
          textEntry += "<td style='border: 1px solid #dddddd;'>Precent Complete</td>";
          textEntry += "<td style='border: 1px solid #dddddd;'>Risk</td>";
          textEntry += "</tr>";
          for (var i = 0; i < areasArray.length; i++) {
            dateCount++;
            var area = areasArray[i];
            textEntry += "<tr>";
            if (dateCount === 1) {
              textEntry += "<th style='width: 33%;border: 1px solid #dddddd;' rowspan='" + areasArray.length + "'>" + project + "</th>";
            }
            textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.area + "</td>";
            textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.percent + "</td>";
            textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.risk + "</td>";
            textEntry += "</tr>";
          }
          textEntry += "</table>";
        }

        textEntry += "</td>";
        textEntry += "</tr>";
        textEntry += "</table>";
        textEntry += "</table>";
      }

    }

    function sendMail() {

      textEntry += "<p>Innview</p>";

      if (areasAlerts) {
        var promises = [];
        for (var i = 0; i < reciepts.length; i++) {
          console.log("email sent to " + reciepts[i] + "for Updated - Innview Weekly Dispatch");
          promises.push(mailTransport.sendMail({from: "'Innview' <sender2.innovaresystems@gmail.com>", to: reciepts[i], subject: "Updated - Innview Weekly Dispatch", html: textEntry}));
        }
        return Promise.all(promises);
      } else {
        return Promise.resolve("None");
      }

    }

  }
  
});




exports.innviewNotice2 = functions.https.onRequest((request, response) => {
  
var gmailEmail = functions.config().gmail.email2;
var gmailPassword = functions.config().gmail.password2;
var mailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
});

var reciepts = [];
reciepts.push("Neil.Sisk@innovaresystems.co.uk");
reciepts.push("jason.powell@innovaresystems.co.uk");
reciepts.push("James.Nicholls@innovaresystems.co.uk");
reciepts.push("ben.plant@innovaresystems.co.uk");
reciepts.push("David.Rowbottom@Innovaresystems.co.uk");
reciepts.push("christopher.smith@innovaresystems.co.uk");
reciepts.push("graeme.parkin@innovaresystems.co.uk");
reciepts.push("nicholas.clarke@innovaresystems.co.uk");
reciepts.push("alex.banks@innovaresystems.co.uk");
reciepts.push("kevin.bland@innovaresystems.co.uk");
reciepts.push("stuart.spaven@innovaresystems.co.uk");
reciepts.push("danny.boyce@innovaresystems.co.uk");
reciepts.push("Stephen.Lynas@innovaresystems.co.uk");
reciepts.push("stephen.cromey@innovaresystems.co.uk");
   
return queryAndSendNotifications();


function queryAndSendNotifications() {

  var currentTime = new Date().getTime();
  var nextWeek = currentTime + 604800000;
  var nextDate = new Date(nextWeek);
  var isoDateNext = toIsoDate(nextDate);
  var humanDateNext = toHumanDate(nextDate);

  var textEntry = "";
  var areasAlerts = 0;
  var areaPlan = {};
  var projects = {};

  textEntry += "<p>Hi,</p>";
  textEntry += "<p>Please see below areas due for dispatch one week today on " + humanDateNext + ":</p>";
  textEntry += "<ul>";

  return processProjects()
    .then(processPlans)
    .then(processAreasDeliveryDate)
    .then(sendMail)
    .then(function (res) {
      if (res === "None") {
        return response.send("No deliverys found for next week, no emails sent");
      } else {
        return response.send("Email Sent");
      }
    }, function (error) {
      return response.send(error);
    });

  function processProjects() {
    return getProjects()
      .then(snapProjects => {
        snapProjects.forEach(snapProject => {
          var project = snapProject.val();
          var projectKey = snapProject.key;
          if (project.active && project.deliverySchedule && project.deliverySchedule.published) {
            if (projects[projectKey] === undefined) {
              projects[projectKey] = project;
            }
          }
        });
      });
  }
  
  function processPlans(){
    var promises = [];
    for(var projectKey in projects){
      promises.push(getAreasPlans(projectKey));
    }
    return Promise.all(promises);
  }
  
  function processAreasDeliveryDate(){
    var promises = [];
    for(var projectKey in projects){
      promises.push(getAreasDeliveryDate(projectKey, projects[projectKey]));
    }
    return Promise.all(promises);
  }

  function getAreasPlans(projectKey) {
    return getPlans(projectKey)
      .then(snap => {
        snap.forEach(childSnap => {
          var area = childSnap.val();
          var areaKey = childSnap.key;
          if (areaPlan[areaKey] === undefined) {
            areaPlan[areaKey] = area;
          }
        });
      });
  }

  function getAreasDeliveryDate(projectKey, project) {
    return getProjectAreas(projectKey)
      .then(snap => {
        snap.forEach(childSnap => {
          var area = childSnap.val();
          var areaKey = childSnap.key;
          if (project.deliverySchedule !== undefined && project.deliverySchedule.published !== undefined) {
            var revisionKey = project.deliverySchedule.published;
            if (area.revisions !== undefined && area.revisions[revisionKey] !== undefined && area.supplier === "Innovaré") {
              var revision = area.revisions[revisionKey];
              revision = subtractWorkingDays(revision, 1);
              var percentage = getCompletedPanelsPercent(area);
              if (areaPlan[areaKey] !== undefined) {
                var riskDays = daysBetweenWeekDaysOnly(areaPlan[areaKey].plannedFinish, areaPlan[areaKey].riskDate);
                if (riskDays < 0) {
                  areaRiskDays = "Risk: " + riskDays + " days.";
                } else {
                  areaRiskDays = "Not at risk.";
                }
              } else {
                areaRiskDays = "Not at risk.";
              }
              if (revision === isoDateNext) {
                textEntry += "<li>" + project.id + "-" + area.phase + "-" + area.floor + "-" + area.type + " with " + percentage + "% complete. " + areaRiskDays + "</li>";
                areasAlerts++;
              }
            }
          }
        });
      });
  }

  function sendMail() {

    textEntry += "</ul>";
    textEntry += "<p>Innview</p>";

    console.log(textEntry);

    if (areasAlerts) {
      var promises = [];
      for (var i = 0; i < reciepts.length; i++) {
        console.log("email sent to " + reciepts[i] + "for Innview Notice");
        promises.push(mailTransport.sendMail({from: "'Innview' <sender2.innovaresystems@gmail.com>", to: reciepts[i], subject: "Innview Notice", html: textEntry}));
      }
      return Promise.all(promises);
    } else {
      return Promise.resolve("None");
    }

  }
}
   
});

exports.innviewWeeklyDispatch2 = functions.https.onRequest((request, response) => {
  
  
  var gmailEmail = functions.config().gmail.email2;
  var gmailPassword = functions.config().gmail.password2;
  var mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword
    }
  });

  var reciepts = [];
  reciepts.push("Neil.Sisk@innovaresystems.co.uk");
  reciepts.push("jason.powell@innovaresystems.co.uk");
  reciepts.push("James.Nicholls@innovaresystems.co.uk");
  reciepts.push("alex.banks@innovaresystems.co.uk");
  reciepts.push("Stephen.Lynas@innovaresystems.co.uk");
  reciepts.push("danny.boyce@innovaresystems.co.uk");
  reciepts.push("Daniel.Phillips@innovaresystems.co.uk");
  reciepts.push("stephen.cromey@innovaresystems.co.uk");

  return queryAndSendWeeklyNotifications(response);


  function queryAndSendWeeklyNotifications(response) {

    var todayDate = new Date();
    var todayDay = todayDate.getDay();
    var textEntry = "";
    var areaData = {};
    var areaPlan = {};
    var areasAlerts = 0;
    var projects = {};

    var nextWeekStart = plusDays(todayDate, 4);
    var nextWeekEnd = plusDays(todayDate, 8);
    var nextWeekStartHuman = toHumanDate(nextWeekStart);

    nextWeekStart = new Date(nextWeekStart);
    nextWeekEnd = new Date(nextWeekEnd);

    if (todayDay === 4) {

      textEntry += "<p>Hi,</p>";
      textEntry += "<p>Please see below areas due for dispatch week start " + nextWeekStartHuman + ":</p>";

      return processProjects()
        .then(processPlans)
        .then(processAreasDeliveryDate)
        .then(buildTable)
        .then(sendMail)
        .then(function (res) {
          if (res === "None") {
            return response.send("No deliverys found for next week, no emails sent");
          } else {
            return response.send("Email Sent");
          }
        }, function (error) {
          return response.send(error);
        });

    } else {

      return response.send("Emails only send on a Thursday");

    }

    function processProjects() {
      return getProjects()
        .then(snapProjects => {
          snapProjects.forEach(snapProject => {
            var project = snapProject.val();
            var projectKey = snapProject.key;
            if (project.active && project.deliverySchedule && project.deliverySchedule.published) {
              if (projects[projectKey] === undefined) {
                projects[projectKey] = project;
              }
            }
          });
        });
    }

    function processPlans(){
      var promises = [];
      for(var projectKey in projects){
        promises.push(getAreasPlans(projectKey));
      }
      Promise.all(promises);
    }

    function processAreasDeliveryDate(){
      var promises = [];
      for(var projectKey in projects){
        promises.push(getAreasDeliveryDate(projectKey, projects[projectKey]));
      }
      return Promise.all(promises);
    }

    function getAreasPlans(projectKey) {
      return getPlans(projectKey)
        .then(snap => {
          snap.forEach(childSnap => {
            var area = childSnap.val();
            var areaKey = childSnap.key;
            if (areaPlan[areaKey] === undefined) {
              areaPlan[areaKey] = area;
            }
          });
        });
    }

    function getAreasDeliveryDate(projectKey, project) {
      return getProjectAreas(projectKey)
        .then(snap => {
          snap.forEach(childSnap => {
            var area = childSnap.val();
            var areaKey = childSnap.key;
            if (project.deliverySchedule !== undefined && project.deliverySchedule.published !== undefined) {
              var revisionKey = project.deliverySchedule.published;
              if (area.revisions !== undefined && area.revisions[revisionKey] !== undefined && area.supplier === "Innovaré") {
                var revision = area.revisions[revisionKey];
                revision = subtractWorkingDays(revision, 1);
                revision = new Date(revision);
                var percentage = getCompletedPanelsPercent(area);
                var areaRiskDays = "";
                if (areaPlan[areaKey] !== undefined) {
                  var riskDays = daysBetweenWeekDaysOnly(areaPlan[areaKey].plannedFinish, areaPlan[areaKey].riskDate);
                  if (riskDays < 0) {
                    areaRiskDays = "Risk: " + riskDays + " days";
                  } else {
                    areaRiskDays = "Not at risk";
                  }
                } else {
                  areaRiskDays = "Not at risk";
                }
                if (revision >= nextWeekStart && revision <= nextWeekEnd) {
                  var revisionHumanDate = toHumanDate(revision);
                  if (areaData[revisionHumanDate] === undefined) {
                    areaData[revisionHumanDate] = {};
                  }
                  if (areaData[revisionHumanDate][project.name] === undefined) {
                    areaData[revisionHumanDate][project.name] = [];
                  }
                  var areaId = project.id + "-" + area.phase + "-" + area.floor + "-" + area.type;
                  var area = {
                      "date": revisionHumanDate,
                      "project" : project.name,
                      "area": areaId,
                      "percent": percentage,
                      "risk": areaRiskDays
                    };
                  areaData[revisionHumanDate][project.name].push(area);
                  areasAlerts++;
                }
              }
            }
          });
        });
    }

    function buildTable() {

      areaData = sortDates(areaData);

      var dateCount = 0;

      for (var date in areaData) {
        var projectArray = areaData[date];
        textEntry += "<table style='border: 1px solid black;width:100%'>";
        textEntry += "<tr>";
        textEntry += "<td style='text-align:center;width:15%;'>"+date+"</td>";
        textEntry += "<td style='width:85%;'>";

        for (var project in projectArray) {
          var areasArray = projectArray[project];
          dateCount = 0;
          textEntry += "<table style='table-layout: fixed;border: 1px solid black;width:100%'>";
          textEntry += "<tr>";
          textEntry += "<th style='border: 1px solid #dddddd;'>Project:</th>";
          textEntry += "<td style='border: 1px solid #dddddd;'>Area</td>";
          textEntry += "<td style='border: 1px solid #dddddd;'>Precent Complete</td>";
          textEntry += "<td style='border: 1px solid #dddddd;'>Risk</td>";
          textEntry += "</tr>";
          for (var i = 0; i < areasArray.length; i++) {
            dateCount++;
            var area = areasArray[i];
            textEntry += "<tr>";
            if (dateCount === 1) {
              textEntry += "<th style='width: 33%;border: 1px solid #dddddd;' rowspan='" + areasArray.length + "'>" + project + "</th>";
            }
            textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.area + "</td>";
            textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.percent + "</td>";
            textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.risk + "</td>";
            textEntry += "</tr>";
          }
          textEntry += "</table>";
        }

        textEntry += "</td>";
        textEntry += "</tr>";
        textEntry += "</table>";
        textEntry += "</table>";
      }

    }

    function sendMail() {

      textEntry += "<p>Innview</p>";

      console.log(textEntry);

      if (areasAlerts) {
        var promises = [];
        for (var i = 0; i < reciepts.length; i++) {
          console.log("email sent to " + reciepts[i] + "for Innview Weekly Dispatch");
          promises.push(mailTransport.sendMail({from: "'Innview' <sender2.innovaresystems@gmail.com>", to: reciepts[i], subject: "Innview Weekly Dispatch", html: textEntry}));
        }
        return Promise.all(promises);
      } else {
        return Promise.resolve("None");
      }

    }

  }

});

function calculateEstimatedAreasAndPanels(projectRef) {
  
  var datumData;
  var productTypeProductionLine;
  var project;

  return getProject(projectRef)
    .then((proj) => {
      project = proj.val();
      return getDatums();
    })
    .then((datums) => {   
      datums.forEach(childSnap => {
        var datum = childSnap.val();     
        if(datum.name === project.datumType){
          datumData = datum;
        }
      });
      return getProductTypes();
    })
    .then((productionLine) => {
      productTypeProductionLine = productionLine;
      return getProjectAreas(projectRef);
    })
    .then((areas) => {
      
      var estimatesPerGroup = {};
      var areaCount = {};
      var updates = {};

      Object.keys(project.estimatedAreas).forEach((group) => {
        let totalArea = project.estimatedAreas[group];
        estimatesPerGroup[group] = totalArea;
      });

      areas.forEach(childSnap => {
        var area = childSnap.val();
        if (areaCount[area.type] === undefined) {
          areaCount[area.type] = 0;
        }
        areaCount[area.type]++;
      });

      areas.forEach(childSnap => {
        var area = childSnap.val();
        var areaKey = childSnap.key;
        var productionLineKey = productTypeProductionLine[area.type];
        if (estimatesPerGroup[area.type] !== undefined && area.supplier === "Innovaré" && (area.revitUpload === undefined || !area.revitUpload)) {
          area.estimatedArea = Math.round(estimatesPerGroup[area.type] / areaCount[area.type]);
          if (productionLineKey !== undefined && datumData !== undefined && datumData.productionLines[productionLineKey] !== undefined) {
            var panelAvgOverride = datumData.productionLines[productionLineKey].panelAvgOverride;
            var panelAvg = datumData.productionLines[productionLineKey].panelAvg;
            if (panelAvgOverride && panelAvgOverride > 0) {
              area.estimatedPanels = Math.round(area.estimatedArea / panelAvgOverride) | 0;
            } else {
              area.estimatedPanels = Math.round(area.estimatedArea / panelAvg) | 0;
            }
          }
          
          console.log(areaKey);
          console.log(JSON.stringify(area));
          
          updates['/areas/' + areaKey] = area;

        }
      });

      return ref.update(updates);

    });
}

function getProductTypes() {
  var productTypeProductionLine = {};
  var productTypes = ProductTypes();
  return productTypes
    .then(function ( snapshot ) {
      var productTypes = snapshot.val();
      productTypeProductionLine = buildMap(productTypes, "productionLine");
      return productTypeProductionLine;
    });
}

function getReferenceData(key) {
    return ref.child("srd").child(key).once('value');
} 

function ProductTypes() {
  return getReferenceData("productTypes");
}

function buildMap(items, property) {
  var map = {};
  for (var key in items) {
    var item = items[key];
      map[key] = item[property];
  }
  return map;
}

function panelsForArea(areaKey) {
  return ref.child('panels').orderByChild('area').equalTo(areaKey).once('value');
}

function countPanelsDeliveredToday(){
  var today = new Date();
  today = today.getFullYear()+"-" + ("0" + (today.getMonth()+1)).slice(-2) + "-"+ ("0" + today.getDate()).slice(-2);

  var matchingAreas = {};
  var matchingPanels = {};
  var areaUnpublishedDate = [];
  var productTypeProductionLine = {};
  
  var promise = new Promise(function(resolve, reject) {
    return getProductTypes()
      .then(getProjects)
      .then(processProjects)
      .then(panelsToBeDeliveredToday)
      .then(passToSave)
      .then(function () {
        return resolve("Sucsess: Resolved. Delivered today count");
      }, function (error) {
        return reject("Error: Rejected. Delivered today count | - " + error);
      }); 
  });
  
  return promise;
  
  function getProductTypes() {
    var productTypes = ProductTypes();
    return productTypes
      .then(function ( snapshot ) {
        var productTypes = snapshot.val();
        productTypeProductionLine = buildMap(productTypes, "productionLine");
      });
  }

  function processProjects(snap) {
    var promises = [];
    snap.forEach(childSnap => {
      var project = childSnap.val();
      var projectKey = childSnap.key;
      if (project.deliverySchedule.published !== undefined) {
        // get areas with published delivery date within specified range
        promises.push(findAreasWithMatchingDeliveryDate(projectKey, project));
      }
    });
    return Promise.all(promises);
  }

  function findAreasWithMatchingDeliveryDate(projectKey, project) {
    return getProjectAreas(projectKey)
      .then(function (snap) {
        snap.forEach(childSnap => {
          var area = childSnap.val();
          var areaKey = childSnap.key;
          if (area.revisions !== undefined && area.revisions[project.deliverySchedule.published] !== undefined) {
            var revisionDate = area.revisions[project.deliverySchedule.published];
            if (revisionDate === today) {
              matchingAreas[areaKey] = area;
              areaUnpublishedDate[area.$id] = project.deliverySchedule.published;
            }
          }
        });
      });
  }
  
  function panelsToBeDeliveredToday(){
    var promises = [];
    for (var key in matchingAreas) {
        promises.push(
          panelsForArea(key)
            .then(function (snap) {
              snap.forEach(childSnap => {
                var panel = childSnap.val();
                var productionLine = productTypeProductionLine[panel.type];
                if(matchingPanels[productionLine] === undefined && productionLine !== undefined){
                  matchingPanels[productionLine] = {
                    count: 0,
                    m2: 0
                  };
                }
                if(productionLine !== undefined){
                  matchingPanels[productionLine].count++;
                  matchingPanels[productionLine].m2 += panel.dimensions.area;
                }
              });
            })
          );
    }
    return Promise.all(promises);
  }
  
  function passToSave(){
    var promises = [];
    for (var key in matchingPanels) {
      promises.push(saveToStats(key, matchingPanels[key].count, matchingPanels[key].m2));
    }
    return Promise.all(promises);
  }
  
  function saveToStats(productionLine, count, m2){
    console.log("panelCount: " + count);
    console.log("productionLine: " + productionLine);
    const countRef = ref.child("stats").child(productionLine).child(today).child("deliveredCount");
    const m2Ref = ref.child("stats").child(productionLine).child(today).child("deliveredM2");
    return countRef.transaction(() => {
      return count;
    })
    .then(() => {
        return m2Ref.transaction(() => {
          return m2;
        });
    }).then(() => {
      return console.log("panel delivery counter updated.");
    });
  }
}

function dailyCountUpStats(){
  
  var today = new Date();
  var todayISO = toIsoDate(today);
  var firstDay = today.setHours(0,0,0,0);
  var lastDay = today.setHours(23,59,59,999);

  return Promise.all([
    ProductTypes(),
    panelsByCompleted(firstDay, lastDay), 
    panelsByStarted(firstDay, lastDay), 
    panelsByCreated(firstDay, lastDay)
  ]).then((values) => {
    var matchingPanels = {};
    var updates = {};
    var productTypes = values[0].val();
    var productTypeProductionLine = buildMap(productTypes, "productionLine");
    matchingPanels = countData(values[1], todayISO, productTypeProductionLine, "completed", matchingPanels);
    matchingPanels = countData(values[2], todayISO, productTypeProductionLine, "started", matchingPanels);
    matchingPanels = countData(values[3], todayISO, productTypeProductionLine, "designed", matchingPanels);
    Object.keys(matchingPanels).forEach(proLines => {
      Object.keys(matchingPanels[proLines]).forEach(date => {
        Object.keys(matchingPanels[proLines][date]).forEach(type => {
          updates["stats/"+proLines+"/"+date+"/"+type] = matchingPanels[proLines][date][type];
        });
      });        
    });
    console.log(JSON.stringify(updates));
    return ref.update(updates);
  });
  
  function countData(snap, todayISO, productTypeProductionLine, type, matchingPanels){
    
        snap.forEach(childSnap => {
        var panel = childSnap.val();
        var productionLine = productTypeProductionLine[panel.type];
        if (matchingPanels[productionLine] === undefined && productionLine !== undefined) {
            matchingPanels[productionLine] = {};
        }
        if (matchingPanels[productionLine][todayISO] === undefined && productionLine !== undefined) {
            matchingPanels[productionLine][todayISO] = {};
        }
        if (matchingPanels[productionLine][todayISO][type+"Count"] === undefined && productionLine !== undefined) {
            matchingPanels[productionLine][todayISO][type+"Count"] = 0;
        }
        if (matchingPanels[productionLine][todayISO][type+"M2"] === undefined && productionLine !== undefined) {
            matchingPanels[productionLine][todayISO][type+"M2"] = 0;
        }
        if (productionLine !== undefined) {
            matchingPanels[productionLine][todayISO][type+"Count"]++;
            matchingPanels[productionLine][todayISO][type+"M2"] += panel.dimensions.area;
        }
        });
        return matchingPanels;
  }
  
}

function buildAreasPlan(plan){
  var updates = {};
  var today = new Date();
  today = toIsoDate(today);

  var promiseArr = [];
  plan.areas.map((area) => {
    promiseArr.push(refreshAreaProductionPlan(area, plan));   
  });

  return Promise.all(promiseArr)
          .then((_)=>{
            var updateCollection ={};
            Object.keys(updates).forEach((areaKeys)=>{
              updateCollection["productionPlans/areas/" + areaKeys] = updates[areaKeys];
            });
            return ref.update(updateCollection);
          });


    function refreshAreaProductionPlan(area, plan) {

      return ref.child("productionPlans/areas/" + area.$key).once('value')
            .then(( snap ) => {
              var buildDays = {};
              if (area._productionPlan !== undefined) {
                area._productionPlan.map(( panels, key ) => {
                  var date = plusDays(plan.viewStart, key);
                  //date greater than today because plan is effected now by panels complete on day
                  if (panels["count"] !== null && date > today) {
                    buildDays[date] = panels["count"];
                  }
                });

                var areaPlan = {
                  timestamps: {
                    created: {'.sv': 'timestamp'},
                    modified: {'.sv': 'timestamp'}
                  },
                  project: area.project,
                  plannedStart: area._productionStartDate,
                  plannedFinish: area._productionEndDate,
                  userChange: area._userChange,
                  riskDate: area._riskDate,
                  buildDays: buildDays
                };

                var daysChanged = false;

                let existing = snap.val();
                if (existing !== null) {
                  areaPlan.timestamps.created = existing.timestamps.created;
                  if (existing.buildDays !== undefined) {
                    Object.keys(existing.buildDays).map(( buildDate ) => {
                      let amount = existing.buildDays[buildDate];
                      if (buildDays[buildDate] !== amount) {
                        daysChanged = true;
                      }
                    });
                  }
                }
                if (existing === null
                        || existing.plannedStart !== areaPlan.plannedStart
                        || existing.plannedFinish !== areaPlan.plannedFinish
                        || existing.riskDate !== areaPlan.riskDate
                        || daysChanged) {
                  console.log("save: " + areaPlan);
                  updates[area.$key] = areaPlan;
                }
              }
              return Promise.resolve();
            });
    }
  }

function sortDates(obj) {
  var keys = Object.keys(obj);
  keys.sort(sorterDates);
  var ret = {};
  for (i = 0; i < keys.length; i++) {
    var key = keys[i];
    ret[key] = obj[key];
  };
  return ret;
}

function sorterDates(a, b) {
  var keyA = new Date(a);
  var keyB = new Date(b);
  // Compare the 2 dates
  if (keyA < keyB)
    return -1;
  if (keyA > keyB)
    return 1;
  return 0;
}

function subtractWorkingDays(startDate, days) {
  var date = new Date(startDate);

  while (days > 0) {
    date.setDate(date.getDate()-1);
    if (isWeekDay(date)) {
      days--;
    }
  }
  return toIsoDate(date);
}

function isWeekDay(value) {
  var day = new Date(value).getDay();
  return day >= 1 && day <= 5;
}

function panelsByCompleted(firstDay, lastDay){
  return ref.child("panels").orderByChild("qa/completed").startAt(firstDay).endAt(lastDay).once('value');
}

function panelsByStarted(firstDay, lastDay){
  return ref.child("panels").orderByChild("qa/started").startAt(firstDay).endAt(lastDay).once('value');
}

function panelsByCreated(firstDay, lastDay){
  return ref.child("panels").orderByChild("timestamps/created").startAt(firstDay).endAt(lastDay).once('value');
}

function getUserById(uid) {
  return ref.child("users").child(uid).once('value');
}

function getProjects() {
  return ref.child('projects').orderByChild('active').equalTo(true).once('value');
}

function getDatums() {
  return ref.child('datums').once('value');
}

function getProject(projectRef){
  return ref.child("projects").child(projectRef).once('value');
}

function getProjectAreas(projectKey) {
  return ref.child('areas').orderByChild('project').equalTo(projectKey).once('value');
}

function getPlans(projectKey){
  return ref.child("productionPlans/areas").orderByChild("project").equalTo(projectKey).once('value');
}

function getAreaFromRef(areaRef){
  return ref.child("areas").child(areaRef).once('value');
}

function getAllPlans(){
  return ref.child("productionPlans/areas").once('value');
}

function getPanelsCount(area) {
  return area.actualPanels || area.estimatedPanels || 0;
}

function getCompletedPanelsCount(area) {
  return area.completedPanels || 0;
}

function getCompletedPanelsPercent(area) {
  var count = getPanelsCount(area);
  return count === 0 ? 0 : Math.floor(100 * getCompletedPanelsCount(area) / count);
}

function plusDays(value, days) {
   var date = new Date(value);
   date.setDate(date.getDate() + days);
   return toIsoDate(date);
 }

 function minusDays(value, days) {
   var date = new Date(value);
   date.setDate(date.getDate() - days);
   return toIsoDate(date);
 }

 function toIsoDate(value) {
   var date = new Date(value);
   return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
 }

 function toHumanDate(value) {
   var date = new Date(value);
   return ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
 }
 
function dayOfWeek(value) {
  var date = new Date(value);
  return (date.getDay() + 6) % 7;
}

function weekStart(value) {
  var date = new Date(value);
  date.setDate(date.getDate() - dayOfWeek(date));
  return toIsoDate(date);
}

function daysBetweenWeekDaysOnly(from, to) {
  var toDate = new Date(to);
  var fromDate = new Date(from);
  var minusWeekends = 0;
  if (fromDate < toDate){
    while(fromDate < toDate){
      if (isWeekDay(fromDate)){
        minusWeekends++;
      }
      fromDate = plusDays(fromDate, 1);
      fromDate = new Date(fromDate);
    }
  } else if (fromDate > toDate){
    while(fromDate > toDate){
      if (isWeekDay(fromDate)){
        minusWeekends--;
      }
      fromDate = minusDays(fromDate, 1);
      fromDate = new Date(fromDate);
    }
  }
  return minusWeekends;
}