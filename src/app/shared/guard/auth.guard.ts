
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
    providedIn: 'root',
   })
export class AuthGuard implements CanActivate {

    user: Observable<any>;

    constructor(private af: AngularFireAuth, private router: Router) {
        this.user = af.user;

        af.authState.subscribe(auth => {
            if(auth) {
                this.user = af.user;
                console.log('logged in');
            } else {
                this.user = af.user;
                this.router.navigate(['/login']);
                console.log('not logged in');
            }
          });
    }

    canActivate() {
        if (this.user) {
            return true;
        } else {
            return false;
        }
    }

}
