import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ForwardPlannerService } from '../../services/forward-planner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-project-status-details',
  templateUrl: './project-status-details.component.html',
  styleUrls: ['./project-status-details.component.scss']
})
export class ProjectStatusDetailsComponent implements OnInit {

  @Input() project: object;
  @Output() startChange = new EventEmitter<object>();
  stats: object;
  loaded: boolean;
  _subscription: Subscription;

  constructor(private forwardPlannerService: ForwardPlannerService) { 
    this.loaded = false;
  }

  ngOnInit() {
    this._subscription = this.forwardPlannerService.getProjectStats(this.project).subscribe((stats)=>{
      this.stats = stats;
      let toEmit = {
        project : this.project,
        stats: stats
      }
      this.startChange.emit(toEmit);
      this.loaded = true;
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

}
