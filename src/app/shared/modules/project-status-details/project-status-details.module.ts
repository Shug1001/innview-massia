import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectStatusDetailsComponent } from './project-status-details.component';
import { PipesModule } from '../../pipes/pipes.module';
import { MatGridListModule } from '@angular/material';

@NgModule({
  declarations: [ProjectStatusDetailsComponent],
  imports: [
    CommonModule,
    PipesModule,
    MatGridListModule
  ],
  exports: [ProjectStatusDetailsComponent],
})
export class ProjectStatusDetailsModule { }
