import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectLineComponent } from './project-line.component';
import { ProjectLineDialogComponent } from './project-line-dialog/project-line-dialog.component';
import { MatButtonModule, MatDialogModule } from '@angular/material';

@NgModule({
  declarations: [ProjectLineComponent, ProjectLineDialogComponent],
  exports: [ProjectLineComponent],
  entryComponents: [ProjectLineDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule
  ]
})
export class ProjectLineModule { }
