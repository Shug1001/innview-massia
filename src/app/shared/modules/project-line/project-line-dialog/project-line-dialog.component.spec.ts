import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectLineDialogComponent } from './project-line-dialog.component';

describe('ProjectLineDialogComponent', () => {
  let component: ProjectLineDialogComponent;
  let fixture: ComponentFixture<ProjectLineDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectLineDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectLineDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
