import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { AreasService, ProjectsService } from 'src/app/shared/services';

@Component({
  selector: 'app-project-line-dialog',
  templateUrl: './project-line-dialog.component.html',
  styleUrls: ['./project-line-dialog.component.scss']
})
export class ProjectLineDialogComponent implements OnInit {

  _subscription: Subscription;
  areas: Array<any>;

  constructor(
    public dialogRef: MatDialogRef<ProjectLineDialogComponent>,
    private areasService: AreasService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {

    this._subscription = this.areasService.getAreasForProjectProcessed(this.data.project.$key).subscribe((areas)=>{
      this.areas = areas;
    });
  }

  setLine(line){
    this.areas.forEach(area => {
      area.line = line;        
    });
    return this.areasService.updateAreas(this.areas)
    .then(()=>{
      this.onNoClick();
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
