import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ProjectLineDialogComponent } from './project-line-dialog/project-line-dialog.component';

@Component({
  selector: 'app-project-line',
  templateUrl: './project-line.component.html',
  styleUrls: ['./project-line.component.scss']
})
export class ProjectLineComponent implements OnInit {

  @Input() project: object;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    this.dialog.open(ProjectLineDialogComponent, {
      width: '500px',
      data: {project: this.project}
    });
  }


}
