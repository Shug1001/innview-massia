import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReferenceDataService } from '../../../shared/services/reference-data.service';
import { Observable, Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';
import Srd  from '../../../../assets/srd/srd.json';
import { DateUtilService } from '../../services/date-util.service';

@Component({
  selector: 'app-srd-select',
  templateUrl: './srd-select.component.html',
  styleUrls: ['./srd-select.component.scss']
})
export class SrdSelectComponent implements OnInit {

  @Input() variant: string;
  @Input() required: boolean;
  @Input() name: string;
  @Input() value: string;
  @Input() disableSelect: boolean;
  @Input() ctr: FormGroup;
  @Output() selectionHasChanged = new EventEmitter<string>();

  items: object;
  selectOptions: string;
  _subscription: Subscription;

  constructor(private dateUtilsService: DateUtilService) {}

  ngOnInit() {
    let items = [];
    let itemsHolder = this.getReferenceData(this.variant);
    Object.keys(itemsHolder).map(key => {
      items.push(Object.assign(itemsHolder[key], {$key: key}));
    });
    items.sort(function(a, b){
      return a['seq'] - b['seq'];
    });
    this.items = items;
    console.log(this.variant);

  }

  selectionChange(evt){
    this.selectionHasChanged.emit(evt);
  }

  getReferenceData(variant){
    switch (variant) {
    case "chainOfCustody":
      return Srd.chainOfCustodyTypes;
    case "datumType":
      return Srd.datumTypes;
    case "component":
      return Srd.components;
    case "siteAccess":
      return Srd.vehicleTypes;
    case "phase":
      return Srd.phases;
    case "floor":
      return Srd.floors;
    case "productType":
      return Srd.productTypes;
    case "supplier":
      return Srd.suppliers;
    case "offloadMethod":
      return Srd.offloadMethods;
    case "gateway":
      return Srd.gateways;
    case "team":
      return Srd.teams;
    case "installer":
      return Srd.installers;
    case "engineer2":
      return Srd.engineer2;
    default:
      console.log("Unknown reference data type '" + variant + "'");
    }
  }
  

}
