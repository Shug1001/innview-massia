import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DesignEnteredPopupComponent } from './design-entered-popup/design-entered-popup.component';

@Component({
  selector: 'app-design-entered',
  templateUrl: './design-entered.component.html',
  styleUrls: ['./design-entered.component.scss']
})
export class DesignEnteredComponent {

  @Input() area: object;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(DesignEnteredPopupComponent, {
      width: '500px',
      data: {area: this.area}
    });
  }

}
