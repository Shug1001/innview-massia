import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignEnteredComponent } from './design-entered.component';

describe('DesignEnteredComponent', () => {
  let component: DesignEnteredComponent;
  let fixture: ComponentFixture<DesignEnteredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignEnteredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignEnteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
