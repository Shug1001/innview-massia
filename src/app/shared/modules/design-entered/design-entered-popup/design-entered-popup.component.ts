import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreasService } from 'src/app/shared/services';


@Component({
  selector: 'app-design-entered-popup',
  templateUrl: './design-entered-popup.component.html',
  styleUrls: ['./design-entered-popup.component.scss']
})
export class DesignEnteredPopupComponent {

  constructor(
    public dialogRef: MatDialogRef<DesignEnteredPopupComponent>,
    private areasService: AreasService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  save(){
    return this.areasService.updateDesignEntered(this.data.area)
    .then(()=>{
      this.onNoClick();
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
