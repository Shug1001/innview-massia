import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignEnteredPopupComponent } from './design-entered-popup.component';

describe('DesignEnteredPopupComponent', () => {
  let component: DesignEnteredPopupComponent;
  let fixture: ComponentFixture<DesignEnteredPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignEnteredPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignEnteredPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
