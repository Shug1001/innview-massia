import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatDialogModule, MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { DesignEnteredPopupComponent } from './design-entered-popup/design-entered-popup.component';
import { DesignEnteredComponent } from './design-entered.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [DesignEnteredPopupComponent, DesignEnteredComponent],
  exports: [DesignEnteredComponent],
  entryComponents: [DesignEnteredPopupComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
  ]
})
export class DesignEnteredModule { }
