import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBookingDialogComponent } from './add-booking-dialog/add-booking-dialog.component';
import { AddBookingComponent } from './add-booking.component';
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatChipsModule, MatDialogModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule, MatSelectModule, MatToolbarModule, MatTooltipModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatDatepickerModule,MatNativeDateModule} from '@angular/material';
import { DeletebookingDialogComponent } from './deletebooking-dialog/deletebooking-dialog.component';

@NgModule({
  exports: [AddBookingComponent],
  entryComponents: [AddBookingDialogComponent, DeletebookingDialogComponent],
  declarations: [AddBookingComponent, AddBookingDialogComponent, DeletebookingDialogComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatDialogModule,
    MatSelectModule,
    MatChipsModule,
    FormsModule,
    NgxMaterialTimepickerModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatAutocompleteModule
  ]
})
export class AddBookingModule { }
