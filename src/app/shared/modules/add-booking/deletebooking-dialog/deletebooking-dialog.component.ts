import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DashboardsService } from 'src/app/shared/services/dashboards.service';

@Component({
  selector: 'app-deletebooking-dialog',
  templateUrl: './deletebooking-dialog.component.html',
  styleUrls: ['./deletebooking-dialog.component.scss']
})
export class DeletebookingDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeletebookingDialogComponent>,
    private dashboardService: DashboardsService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  delete() {
    return this.dashboardService.deleteBooking(this.data.booking.$key)
    .then(()=>{
      this.dialogRef.close();
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
