import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletebookingDialogComponent } from './deletebooking-dialog.component';

describe('DeletebookingDialogComponent', () => {
  let component: DeletebookingDialogComponent;
  let fixture: ComponentFixture<DeletebookingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletebookingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletebookingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
