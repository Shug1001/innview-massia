import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Project } from '../../../models/models';
import { Subscription, Observable } from 'rxjs';
import { AreasService, AuthService, ProjectsService } from 'src/app/shared/services';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { DashboardsService } from 'src/app/shared/services/dashboards.service';

@Component({
  selector: 'app-add-booking-dialog',
  templateUrl: './add-booking-dialog.component.html',
  styleUrls: ['./add-booking-dialog.component.scss']
})
export class AddBookingDialogComponent implements OnInit {

  projects: Project[];
  _subscription: Subscription;
  loaded: boolean;
  projectIds: Array<any>;
  filteredOptions: Observable<string[]>;
  bookingControl: FormGroup;
  submitted: boolean;
  project: object;
  onInit: boolean;
  _subscription2: Subscription;
  projectsHolder: object;
  areaHolder: object;
  toppings = new FormControl();
  areas: object[] = [];
  areasHolder: object;

  constructor(
    public dialogRef: MatDialogRef<AddBookingDialogComponent>,
    private projectsService: ProjectsService,
    private areaService: AreasService,
    private dashboardsService: DashboardsService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onSelectionChanged(evt){

    this.areasHolder = {};

    this._subscription2 = this.areaService.getAreasForProjectProcessed(this.projectsHolder[evt.option.value]).subscribe((areas) => {
      areas.forEach((area)=>{ 
        if(this.areasHolder[area.$key] === undefined){
          this.areasHolder[area.$key] = evt.option.value + "-" + area.phase + "-" + area.floor + "-" + area.type;
        }
        this.areas.push({
          id: evt.option.value + "-" + area.phase + "-" + area.floor + "-" + area.type,
          key: area.$key
        });
      });
    });
  }

  setUpForm(){
    if(this.data.type === "edit"){
      this.bookingControl = this.fb.group({
        projectId: new FormControl(this.data.project.id, [Validators.required]),
        projectName: new FormControl({ value: this.data.project.name, disabled: true }, [Validators.required]),
        project: new FormControl(this.data.booking.project, [Validators.required]),
        notes: new FormControl(this.data.booking.notes),
        loadDesc: new FormControl(this.data.booking.loadDesc),
        poNumber: new FormControl(this.data.booking.poNumber),
        delNumber: new FormControl(this.data.booking.delNumber),
        m2: new FormControl(this.data.booking.m2),
        vehicleType: new FormControl(this.data.booking.vehicleType),
        haulierType: new FormControl(this.data.booking.haulierType)
      });
    }else{
      this.bookingControl = this.fb.group({
        projectId: new FormControl(null, [Validators.required]),
        projectName: new FormControl({ value: null, disabled: true }, [Validators.required]),
        project: new FormControl(null, [Validators.required]),
        notes: new FormControl(null),
        loadDesc: new FormControl(null),
        poNumber: new FormControl(null),
        delNumber: new FormControl(null),
        m2: new FormControl(null),
        vehicleType: new FormControl(null),
        haulierType: new FormControl(null)
      });
    }
  }


  onSubmits(): void{
    this.submitted = true;
    let updateObj = this.bookingControl.getRawValue();
    let areas = this.toppings.value;
    let areasHolder = {};
    areas.forEach((area)=>{ 
      if(areasHolder[area] === undefined){
        areasHolder[area] = true;
      }
    });
    updateObj.areas = areasHolder;
    delete updateObj.projectId;
    delete updateObj.projectName;
    if(this.data.type === "edit"){
      this.dashboardsService.updateBooking(this.data.booking.$key, updateObj)
      .then(()=>{
        this.dialogRef.close();
      });
    }else{
      let bookedTime = new Date(this.data.selectDate).getTime();
      updateObj.bookedTime = bookedTime;
      this.dashboardsService.createBooking(updateObj)
      .then(()=>{
        this.dialogRef.close();
      });
    }

  }


  private _filter(value: string): string[] {
    return this.projectIds.filter(option => option.indexOf(value) === 0);
  }

  ngOnInit(){

    this.areas = [];

    this.onInit = false;

    this.setUpForm();

    if(this.data.type === "edit"){

      this._subscription = this.projectsService.getActiveProjectsList().subscribe((projects) => {
        this.projectIds = [];
        this.projectsHolder = {};

        projects.forEach((project)=>{
          this.projectIds.push(project.id);
          if(this.projectsHolder[project.id] === undefined){
            this.projectsHolder[project.id] = project.$key;
          }
        });

        this.projects = projects;

        this._subscription2 = this.areaService.getAreasForProjectProcessed(this.projectsHolder[this.data.project.id]).subscribe((areas) => {
          this.areasHolder = {};
          areas.forEach((area)=>{ 
            if(this.areasHolder[area.$key] === undefined){
              this.areasHolder[area.$key] = this.data.project.id + "-" + area.phase + "-" + area.floor + "-" + area.type;
            }
            this.areas.push({
              id: this.data.project.id + "-" + area.phase + "-" + area.floor + "-" + area.type,
              key: area.$key
            });
          });
          let values = [];
          Object.keys(this.data.booking.areas).forEach((areaKey)=>{ 
            values.push(areaKey);
          });
          this.toppings.setValue(values);
        });

        this.filteredOptions = this.bookingControl.get('projectId').valueChanges.pipe(
          startWith(''),
          map((value) => {
            if(value === ""){
              this.bookingControl.get('projectName').setValue(this.data.project.name);
            }
            const filteredArray = this._filter(value);
            if(filteredArray.length === 1){
              this.projects.forEach((project)=>{
                if(project.id === filteredArray[0]){
                  this.bookingControl.get('projectName').setValue(project.name);
                  this.bookingControl.get('project').setValue(project.$key);
                }
              });
            }else{
              this.bookingControl.get('projectName').setValue(this.data.project.name);
              this.bookingControl.get('project').setValue(this.data.project.$key);
            }
            return filteredArray;
          })
        );



        this.loaded = true;
      });

    }else{

      this._subscription = this.projectsService.getActiveProjectsList().subscribe((projects) => {
        this.projectIds = [];
        this.projectsHolder = {};

        projects.forEach((project)=>{
          this.projectIds.push(project.id);
          if(this.projectsHolder[project.id] === undefined){
            this.projectsHolder[project.id] = project.$key;
          }
        });

        this.projects = projects;

        this.filteredOptions = this.bookingControl.get('projectId').valueChanges.pipe(
          startWith(''),
          map((value) => {
            if(value === ""){
              this.bookingControl.get('projectName').setValue(null);
            }
            const filteredArray = this._filter(value);
            if(filteredArray.length === 1){
              this.projects.forEach((project)=>{
                if(project.id === filteredArray[0]){
                  this.bookingControl.get('projectName').setValue(project.name);
                  this.bookingControl.get('project').setValue(project.$key);
                }
              });
            }else{
              this.bookingControl.get('projectName').setValue(null);
              this.bookingControl.get('project').setValue(null);
            }
            return filteredArray;
          })
        );
        this.loaded = true;
      });
      
    }
  }

  ngOnDestroy(){
    if(this._subscription){
      this._subscription.unsubscribe();
    }   
    if(this._subscription2){
      this._subscription2.unsubscribe();
    }   
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
