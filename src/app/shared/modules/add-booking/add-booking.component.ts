import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddBookingDialogComponent } from './add-booking-dialog/add-booking-dialog.component';
import { DeletebookingDialogComponent } from './deletebooking-dialog/deletebooking-dialog.component';
import { Subscription } from 'rxjs';
import { ProjectsService } from '../../services';

@Component({
  selector: 'app-add-booking',
  templateUrl: './add-booking.component.html',
  styleUrls: ['./add-booking.component.scss'],
})
export class AddBookingComponent implements OnInit {

  @Input() booking: any;
  @Input() type: string;
  @Input() selectDate: string;
  _subscription: Subscription;
  project: object;

  constructor(public dialog: MatDialog, private projectsService: ProjectsService) {}

  ngOnInit() {
    if(this.type === "edit"){
      this._subscription = this.projectsService.getprojectWithId(this.booking.project).subscribe((project) => {
        this.project = project;
      });
    }else{
      this.project = null;
    }
  }

  openDialog(): void {
    this.dialog.open(AddBookingDialogComponent, {
      width: '1000px',
      data: {
        booking: this.booking,
        type: this.type,
        project: this.project,
        selectDate: this.selectDate
      }
    });
  }

  openDeleteDialog(){
    this.dialog.open(DeletebookingDialogComponent, {
      width: '1000px',
      data: {
        booking: this.booking
      }
    });
  }

  ngOnDestroy(){
    if(this._subscription){
      this._subscription.unsubscribe();
    }   
  }

}
