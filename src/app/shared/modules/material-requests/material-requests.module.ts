import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialRequestsRoutingModule } from './material-requests-routing.module';
import { MaterialRequestsComponent } from './material-requests.component';
import { MaterialRequestPopupComponent } from './material-request-popup/material-request-popup.component';

@NgModule({
  exports: [MaterialRequestsComponent],
  entryComponents: [MaterialRequestPopupComponent],
  declarations: [MaterialRequestsComponent, MaterialRequestPopupComponent],
  imports: [
    CommonModule,
    MaterialRequestsRoutingModule
  ]
})
export class MaterialRequestsModule { }
