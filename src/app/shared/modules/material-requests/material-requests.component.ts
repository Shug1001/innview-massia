import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ExtendedArea } from '../../models/models';
import { MaterialRequestPopupComponent } from './material-request-popup/material-request-popup.component';

@Component({
  selector: 'app-material-requests',
  templateUrl: './material-requests.component.html',
  styleUrls: ['./material-requests.component.scss']
})
export class MaterialRequestsComponent implements OnInit {

  @Input() area: ExtendedArea

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    this.dialog.open(MaterialRequestPopupComponent, {
      width: '500px',
      data: {area: this.area}
    });
  }

}
