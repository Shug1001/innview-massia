import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { MaterialRequestService } from 'src/app/shared/services/material-request.service';

@Component({
  selector: 'app-material-request-popup',
  templateUrl: './material-request-popup.component.html',
  styleUrls: ['./material-request-popup.component.scss']
})
export class MaterialRequestPopupComponent implements OnInit {

  _subscription: Subscription;
  materialRequests: Array<any>

  constructor(
    public dialogRef: MatDialogRef<MaterialRequestPopupComponent>,
    private materialRequestService: MaterialRequestService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {

    this._subscription = this.materialRequestService.getMaterialRequestsByArea(this.data.area.$key).subscribe((materialRequests)=>{
      this.materialRequests = materialRequests;
    });
  }

}
