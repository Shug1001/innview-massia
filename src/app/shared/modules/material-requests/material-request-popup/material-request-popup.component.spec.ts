import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialRequestPopupComponent } from './material-request-popup.component';

describe('MaterialRequestPopupComponent', () => {
  let component: MaterialRequestPopupComponent;
  let fixture: ComponentFixture<MaterialRequestPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialRequestPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialRequestPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
