import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectEstimatesComponent } from './project-estimates.component';

describe('ProjectEstimatesComponent', () => {
  let component: ProjectEstimatesComponent;
  let fixture: ComponentFixture<ProjectEstimatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectEstimatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectEstimatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
