import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectsService } from '../../services/projects.service';
import { DateUtilService } from '../../services/date-util.service';
import { Subscription, combineLatest } from 'rxjs';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-project-estimates',
  templateUrl: './project-estimates.component.html',
  styleUrls: ['./project-estimates.component.scss']
})
export class ProjectEstimatesComponent implements OnInit {

  _subscription: Subscription;
  quotes: Array<any>;
  quoteHolder: {};
  estimates: Array<any>;
  loaded: boolean;
  quotesList: Array<any>;
  subTypeObj: {};
  areaEstimates: [];
  toggleEstimates: boolean;
  @Input() projectKey : string;
  @Output() returnData = new EventEmitter<object>();

  constructor(public dialog: MatDialog, private projectsService: ProjectsService, private dateUtilsService: DateUtilService) {
    this.loaded = false;
    this.toggleEstimates = true;
    this.quotes = [];
    this.subTypeObj= {
      "DAT_Walls": function(type) {
        let subType = "No Type";
        if (type.indexOf("iSIP") !== -1 || type.indexOf("iFAST") !== -1) {
          subType = "External Walls";
        } else if (type.indexOf("TF") !== -1) {
          subType = "Internal Walls";
        }
        return subType;
      },
      "DAT_Floors": function(type) {
        let subType = "No Type";
        if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1) {
          subType = "Floors";
        } else if (type.indexOf("Concrete")  !== -1) {
          subType = "Floors";
        }
        return subType;
      },
      "DAT_Roofs": function(type) {
        let subType = "No Type";
        if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1 || type.indexOf("Cassette") !== -1) {
          subType = "Roofs";
        } else if (type.indexOf("Cassette") === -1) {
          subType = "Roofs";
        }
        return subType;
      }
    };
   }

  ngOnInit() {

    this._subscription = combineLatest(this.projectsService.getQuotesList(), this.projectsService.getQuoteDataByProject(this.projectKey)).subscribe(([quotesList, quotes]) => {
      this.quotesList = quotesList;
      this.quoteHolder = {};
      this.estimates = [];
      let areaEstimates = [];
      this.quotes = quotes;
      let createdQuotes = {};
      let groupQuotes = {};
      if(this.quotes.length){
        this.toggleEstimates = false;
        this.quotes.forEach(quote => {
          if(createdQuotes[quote.created] === undefined){
            createdQuotes[quote.created] = [];
          }
          createdQuotes[quote.created].push(quote);
        });
        let createdQuotesArray = Object.keys(createdQuotes).sort();
        createdQuotes[createdQuotesArray[createdQuotesArray.length-1]].forEach(quote => {
          if(this.subTypeObj[quote.sheetName] !== undefined){
            quote.subType = this.subTypeObj[quote.sheetName](quote.type);
            if(quote.subType !== "No Type"){
              if(groupQuotes[quote.type] === undefined){
                groupQuotes[quote.type] = {};
              }
              if(groupQuotes[quote.type][quote.level] === undefined){
                groupQuotes[quote.type][quote.level] = {};
              }
              if(groupQuotes[quote.type][quote.level][quote.phase] === undefined){
                groupQuotes[quote.type][quote.level][quote.phase] = {
                  type: "",  
                  phase: "",
                  level: "",
                  gross:0
                };
              }
              groupQuotes[quote.type][quote.level][quote.phase].type = quote.type;
              groupQuotes[quote.type][quote.level][quote.phase].phase = quote.phase;
              groupQuotes[quote.type][quote.level][quote.phase].level = quote.level;
              groupQuotes[quote.type][quote.level][quote.phase].gross += quote.areaGross;
              if(this.quoteHolder[quote.subType] === undefined){
                this.quoteHolder[quote.subType] = {
                  gross:0,
                  name: quote.subType
                }
              }
              this.quoteHolder[quote.subType].gross += quote.areaGross;
            }
          }
        });
        for (let sheetName in this.quoteHolder) {
          this.quoteHolder[sheetName].gross = this.dateUtilsService.round2(this.quoteHolder[sheetName].gross);
          this.estimates.push(this.quoteHolder[sheetName]);
        }
        for (let type in groupQuotes) {
          for (let level in groupQuotes[type]) {
            for (let phase in groupQuotes[type][level]) {
              let quote = groupQuotes[type][level][phase];
              quote.gross = this.dateUtilsService.round2(groupQuotes[type][level][phase].gross)
              areaEstimates.push(quote);
            }
          }
        }
      }
      this.returnData.emit({
        areaEstimates: areaEstimates,
        quotesList: quotesList
      });
      this.loaded = true;
    });
  }

  getAvg(data) {
    const total = data.reduce((acc, c) => acc + c, 0);
    return total / data.length;
  }

  getObjectKeys(data){
    return Object.keys(data);
  }

}
