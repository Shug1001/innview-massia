import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatFormFieldModule, MatDialogModule, MatCardModule } from '@angular/material';
import { ProjectEstimatesComponent } from './project-estimates.component';

@NgModule({
  declarations: [ProjectEstimatesComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatDialogModule,
    MatCardModule
  ],
  exports: [ProjectEstimatesComponent]
})
export class ProjectEstimatesModule { }
