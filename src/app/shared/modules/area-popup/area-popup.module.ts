import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaPopupComponent } from './area-popup.component';
import { AreaPopupDialogComponent } from './area-popup-dialog/area-popup-dialog.component';
import { AreaDeletePopupDialogComponent } from './area-delete-popup-dialog/area-delete-popup-dialog.component';
import { MatIconModule, MatFormFieldModule, MatInputModule, MatToolbarModule, MatCardModule, MatNativeDateModule, MatDatepickerModule, MatDialogModule, MatButtonModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SrdSelectModule } from '../srd-select/srd-select.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [AreaPopupComponent, AreaPopupDialogComponent, AreaDeletePopupDialogComponent],
  imports: [
    CommonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    SrdSelectModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [AreaPopupComponent],
  entryComponents: [AreaPopupDialogComponent, AreaDeletePopupDialogComponent]
})
export class AreaPopupModule { }
