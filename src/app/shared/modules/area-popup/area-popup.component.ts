import { Component, OnInit, Input } from '@angular/core';
import { AreaPopupDialogComponent } from './area-popup-dialog/area-popup-dialog.component';
import { AreaDeletePopupDialogComponent } from './area-delete-popup-dialog/area-delete-popup-dialog.component';
import { MatDialog } from '@angular/material';
import { area, Project } from 'src/app/shared/models/models';

@Component({
  selector: 'app-area-popup',
  templateUrl: './area-popup.component.html',
  styleUrls: ['./area-popup.component.scss']
})
export class AreaPopupComponent implements OnInit {

  @Input() area: area;
  @Input() project: Project;
  @Input() type: string;
  @Input() projectKey: string;

  status: string;

  constructor(public dialog: MatDialog) {}

  ngOnInit(){
    if(this.area !== undefined){
      if(this.area.completedPanels !== undefined && this.area.completedPanels > 0){
        this.status = "COMPLETE";
      }else if(this.area.actualPanels !== undefined && this.area.actualPanels > 0){
        this.status = "UPLOADED";
      }else{
        this.status = "NOPANELS";
      }
    }
  }

  openDeleteDialog(): void {

    const dialogRef2 = this.dialog.open(AreaDeletePopupDialogComponent, {
      width: '800px',
      data: {
        area: this.area,
        status: this.status,
        project: this.project,
      }
    });

    dialogRef2.componentInstance.data = {
      area: this.area,
      status: this.status,
      project: this.project,
    };

  }

  openDialog(): void {

    const dialogRef = this.dialog.open(AreaPopupDialogComponent, {
      width: '800px',
      data: {
        area: this.area,
        project: this.project,
        type: this.type,
        projectKey: this.projectKey,
      }
    });

    dialogRef.componentInstance.data = {
      area: this.area,
      project: this.project,
      type: this.type,
      projectKey: this.projectKey,
    };

  }

}
