import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpectedDateComponentComponent } from './expected-date-component.component';

@NgModule({
  exports: [ExpectedDateComponentComponent],
  declarations: [ExpectedDateComponentComponent],
  imports: [
    CommonModule
  ]
})
export class ExpectedDateComponentModule { }
