import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpectedDateComponentComponent } from './expected-date-component.component';

describe('ExpectedDateComponentComponent', () => {
  let component: ExpectedDateComponentComponent;
  let fixture: ComponentFixture<ExpectedDateComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpectedDateComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpectedDateComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
