import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AreaExt } from '../../models/models';
import { ForwardPlannerService } from '../../services';
import { DateUtilService } from '../../services/date-util.service';

@Component({
  selector: 'app-expected-date-component',
  templateUrl: './expected-date-component.component.html',
  styleUrls: ['./expected-date-component.component.scss']
})
export class ExpectedDateComponentComponent implements OnInit {

  @Input() areasHolder: AreaExt[];
  @Input() deliveryDate: string;
  _subscription: Subscription;
  ready: boolean;
  expectedDates: Array<any>;
  expectedDate: string;

  constructor(private dateUtilService: DateUtilService, private forwardPlannerService: ForwardPlannerService) { }

  ngOnInit() {

    let promises = [];
    this.expectedDates = [];
    this.expectedDate = "";

    this.areasHolder.forEach(area => {
      promises.push(this.getPlans(area));     
    });

    Promise.all(promises).then(() => {
      if(this.expectedDates.length){
        this.expectedDate = this.dateUtilService.toUserSlachDate(new Date(Math.max.apply(null,this.expectedDates)));
      }
      this.ready = true;
    });
    
  }

  getPlans(area){
    return this.forwardPlannerService.getAreaProductionPlansForAreaPromise(area.$key).then((snap)=>{
      let plan: any = snap.payload.val();
      this.expectedDate = "";
      if(plan !== null && plan.plannedFinish){
        let riskDate = this.dateUtilService.subtractWorkingDays(this.deliveryDate, 1);
        let riskDay = this.dateUtilService.daysBetweenWeekDaysOnly(plan.plannedFinish, riskDate);
        if(riskDay <= 0){
          let expectedDate = new Date(this.dateUtilService.addWorkingDays(plan.plannedFinish,1));
          this.expectedDates.push(expectedDate);
        }    
      }
    });
  }

}
