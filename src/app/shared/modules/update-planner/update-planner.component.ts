import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UpdatePlannerDialogComponent } from './update-planner-dialog/update-planner-dialog.component';

@Component({
  selector: 'app-update-planner',
  templateUrl: './update-planner.component.html',
  styleUrls: ['./update-planner.component.scss']
})
export class UpdatePlannerComponent {
  
  @Input() text: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(UpdatePlannerDialogComponent, {
      width: '500px',
      data: {text: this.text}
    });
  }

}