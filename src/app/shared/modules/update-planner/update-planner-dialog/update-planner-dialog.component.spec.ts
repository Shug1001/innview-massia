import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlannerDialogComponent } from './update-planner-dialog.component';

xdescribe('TextDialogPopupComponent', () => {
  let component: UpdatePlannerDialogComponent;
  let fixture: ComponentFixture<UpdatePlannerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePlannerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlannerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
