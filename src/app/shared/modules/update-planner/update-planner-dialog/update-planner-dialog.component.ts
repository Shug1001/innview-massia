import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreasService, ForwardPlannerService } from 'src/app/shared/services';
import { Blotter, Plan } from 'src/app/shared/models/models';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import Srd  from '../../../../../assets/srd/srd.json';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-update-planner-dialog',
  templateUrl: './update-planner-dialog.component.html',
  styleUrls: ['./update-planner-dialog.component.scss']
})
export class UpdatePlannerDialogComponent implements OnInit {

  minDate = new Date();
  areasMap: object;
  newAreas: Array<any>;
  productionLines: object;
  defaultLineCapacity: object;
  lineCapacities: object;
  productTypes: object;
  productTypeProductionLine: object;
  blotter: Blotter;
  plan: Plan;
  areasLen: number;
  sortKey: number;
  columnsToDisplay: Array<any>;
  dayHeadings: Array<any>;
  days: Array<any>;
  productionLineFilter: object;
  toggleCount: object;
  toggleType: object;
  m2Type: string;
  areas: Array<any>;
  viewStart: string;
  skipWeeks: number;
  areasSelection: object;
  fieldFilters: Array<any>;
  loaded: boolean;
  panelsCompleteToday: Array<any>;
  plannerSort: Array<any>;
  capacities: Array<any>;
  areaPlans: Array<any>;
  mappedPanelsComplete: object;
  areaPlansMapped: object;
  save: boolean;
  startDateCounter: number;
  _subscription: Subscription;
  updateText: string;

  constructor(
    public dialogRef: MatDialogRef<UpdatePlannerDialogComponent>,
    public areasService: AreasService,
    private dateUtilService: DateUtilService,
    private forwardPlannerService: ForwardPlannerService,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.areasMap = [];
      this.newAreas = [];
  
      this.m2Type = "count";
  
      this.loaded = false;
      this.save = true;
      this.updateText = "Retrieving data...";
  
      this.productionLineFilter = { SIP: true, SIP2: true, HSIP: true, IFAST: true, IFAST2: true, TF: true, TF2: true, CASS: true };
      this.toggleType = { actual: true, design: true, est: true };
      this.toggleCount = { action: true };
  
      this.lineCapacities = {};
      this.areasSelection ={};
  
      this.areasLen = 0;
      this.sortKey = 0;
  
      this.days = [];
  
      this.productionLines = Srd.productionSubLines;
      this.productTypes = Srd.productTypes;
      this.defaultLineCapacity = this.dateUtilService.buildMap(this.productionLines, "defaultCapacity");
      this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
    }

  ngOnInit() {
    this.loaded = false;

    this.days = [];

    var today = new Date();
    var start = new Date(today);
    if (start < today) {
      start = today;
    }

    // Plan displays from Monday of selected start date
    this.viewStart = this.dateUtilService.weekStart(start);
    this.skipWeeks = Math.ceil(this.dateUtilService.daysBetween(today, this.viewStart) / 7);

    this.plan = {
      viewStart: this.viewStart,
      current: this.skipWeeks == 0 ? this.dateUtilService.dayOfWeek(today) : -1,
      capacity: {},
      dailyM2Totals: {},
      lineM2Totals: {},
      areas: []
    };

    if(this._subscription){
      this._subscription.unsubscribe();
    }

    this._subscription = this.forwardPlannerService.getActiveAreasAndCapacity(start.setHours(0,0,0,0), start.setHours(23,59,59,999)).subscribe(([areas, capacities, plannerSort, panels, startDateCounter]) => {

      this.panelsCompleteToday = panels;
      this.plannerSort = plannerSort;
      this.capacities = capacities;
      this.startDateCounter = startDateCounter[0];
      this.areaPlans = startDateCounter[1];
      this.areas = areas;

      this.updateText = "Calculating data...";

      this.mapFieldSort(this.plannerSort);
      this.lineCapacities = this.dateUtilService.mapper(this.capacities);
      this.areaPlansMapped = this.dateUtilService.mapper(this.areaPlans);
      this.mapPanelsCompleteToday(this.panelsCompleteToday);

      if(this._subscription){
        this._subscription.unsubscribe();
      }

      this.initiatePlan();
      
    });

  }

  initiatePlan(){

    this.blotter = this.createBlotter(this.plan, this.skipWeeks, 156);
    this.plan.capacity = this.getCapacityFromBlotter(this.skipWeeks);

    this.areas.sort((areaA, areaB) => {
        var a = this.areasSelection[areaA.$key] ? areaA._unpublishedDeliveryDate : areaA._deliveryDate;
        var b = this.areasSelection[areaB.$key] ? areaB._unpublishedDeliveryDate : areaB._deliveryDate;
        var r = a.localeCompare(b);
        if (r === 0) {
          a = areaA._id;
          b = areaB._id;
          r = a.localeCompare(b);
          return r;
        }
        if (r === 0) {
          r = (areaA._sortKey !== undefined && areaA._sortKey !== undefined) ? areaA._sortKey - areaB._sortKey : 0;
        }
        return r;
      }
    );

    this.areas.map((area) => {
      if (this.areasSelection[area.$key] === undefined){
        this.areasSelection[area.$key] = false;
      }
      this.calculateAreaPlan(area, this.areasSelection[area.$key]);
    });

    let blotterAreas = this.getAreasFromBlotter(this.skipWeeks);
    var calculateM2 = this.calculateM2( blotterAreas );
    this.plan.lineM2Totals = calculateM2.line;
    this.plan.dailyM2Totals = calculateM2.total;
    blotterAreas.sort(this.dateUtilService.fieldSorter(this.fieldFilters));
    this.plan.areas = blotterAreas;

    this.updateText = "Saving data...";

    return this.forwardPlannerService.buildAreasPlanForce(this.plan)
            .then((_)=>{
              this.updateText = "Update Complete";
              this.dialogRef.close();
            });
  }

  calculateAreaPlan(area, useUnpublishedDate){

    delete area._productionStartDate;
    let productionLine = area._productionLine;
    let lineBlotter = this.blotter.productionLines[productionLine];
    if (!lineBlotter) {
      // Just drop area
      return false;
    }

    if (lineBlotter.nextStart < 0) {
      // No available capacity
      return false;
    }

    let productionPlan = [];

    for (let i = 0; i < this.blotter.planDays; i++) {
      if(productionPlan[i] === undefined){
        productionPlan[i] = {
          count: null,
          m2: null
        };
      }
    }

    let designHandoverDate = useUnpublishedDate ? area._unpublishedDesignHandoverDate : area._designHandoverDate;

    let buildDay = lineBlotter.nextStart;
    let buildDate = this.dateUtilService.plusDays(this.blotter.planStart, buildDay);  
    area._userChange = false;
        
    //if there is a change of plannedStart by user
    if (this.areaPlansMapped[area.$key] !== undefined && this.areaPlansMapped[area.$key].userChange) {
      var difference = this.dateUtilService.daysBetween(this.blotter.planStart, this.areaPlansMapped[area.$key].plannedStart);
      if (difference > -1){
        buildDay = difference;
        buildDate = this.areaPlansMapped[area.$key].plannedStart;
        area._userChange = true;
      }
    }
    
    if (!area._userChange){
      // Skip forward to design handover date
      while (buildDate < designHandoverDate) {
        buildDate = this.dateUtilService.plusDays(this.blotter.planStart, ++buildDay);
      }
    }

    if (buildDay > this.blotter.planDays) {
      return false;
    }

    area._productionPlan = productionPlan;

    if (area._hasUnpublishedOnly && !useUnpublishedDate) {
      // Don't consume capacity, but potentially include in area list
      return buildDate >= this.blotter.viewStart;
    }

    if(productionPlan[buildDay] === undefined){
      productionPlan[buildDay] = {
        count: null,
        m2: null
      };
    }

    let panels = (area.actualPanels || area.numberOfPanels || area.estimatedPanels || 0) - (area.completedPanels || 0);
    if (panels == 0) {
      productionPlan[buildDay].count = 0;
      productionPlan[buildDay].m2 = 0;
      area._productionStartDate = buildDate;
      area._productionEndDate = null;
    }
    while (panels > 0) {
      // Extend planning capacity
      if (lineBlotter.available.length <= buildDay) {
        let capacity = this.getCapacity(productionLine, buildDate);
        lineBlotter.available[buildDay] = capacity;
      }

      let available = lineBlotter.available[buildDay];
      if (available) {
        if (!area._productionStartDate) {
          area._productionStartDate = buildDate;
        }

        if(this.mappedPanelsComplete[buildDate] && this.mappedPanelsComplete[buildDate][area.$key]){

          if(available - (this.mappedPanelsComplete[buildDate][area.$key] || 0) < 0){
            let toDate = this.dateUtilService.addWorkingDays(buildDate, 1);
            if(this.mappedPanelsComplete[toDate] === undefined){
              this.mappedPanelsComplete[toDate] = {};
            }
            if(this.mappedPanelsComplete[toDate][area.$key] === undefined){
              this.mappedPanelsComplete[toDate][area.$key] = 0;
            }
            this.mappedPanelsComplete[toDate][area.$key] = Math.abs(this.mappedPanelsComplete[buildDate][area.$key] - available);
            available = 0;
          }else{
            available = available - (this.mappedPanelsComplete[buildDate][area.$key] || 0);
            this.mappedPanelsComplete[buildDate][area.$key] = 0;
          }

        }

        let buildCount = Math.min(panels, available);

        panels -= buildCount;
        available -= buildCount;

        lineBlotter.available[buildDay] = available;
          if (buildDay < productionPlan.length) {

            let m2 = this.forwardPlannerService.avgM2(area, buildCount);

            productionPlan[buildDay].m2 = m2;
            productionPlan[buildDay].count = buildCount;
  
          }

        if (panels == 0) {
          area._productionEndDate = buildDate;
        }
      }

      if (!available) {
        while (buildDay < lineBlotter.available.length && !lineBlotter.available[buildDay]) {
          buildDay++;
        }
        buildDate = this.dateUtilService.plusDays(this.blotter.planStart, buildDay);
      }
    }

    lineBlotter.nextStart = buildDay < this.blotter.planDays ? buildDay : -1;

    let toDo = false;

    if (area._productionEndDate) {
      toDo = area._productionEndDate >= this.blotter.viewStart;
    }
    else {
      toDo = area._productionStartDate >= this.blotter.viewStart;
    }
    if (area._deliveryDate !== undefined) {
      if (toDo) {
        //if exists in array overright if not add
        const index = this.blotter.areas.indexOf(area);
        if (index === -1) {
          this.sortKey++;
          this.blotter.areas.push(area);
        } else {
          this.blotter.areas[index] = area;
        }
      }
    }
  }

  createBlotter(plan, skipWeeks, planWeeks){
    var today = new Date();
    var planStart = this.dateUtilService.weekStart(today);
    var planDays = 7 * (skipWeeks + planWeeks);

    this.blotter = {
      planStart: planStart,
      planDays: planDays,
      viewStart: plan.viewStart,
      current: plan.current,
      productionLines: {},
      areas: []
    };

    var nextStart = this.dateUtilService.dayOfWeek(today);
    Object.keys(this.defaultLineCapacity).forEach((productionLine) => {
      var lineBlotter = {
        capacity: [],
        m2:[],
        available: [],
        nextStart: nextStart
      };

      for (var i = 0; i < planDays; i++) {
        var buildDate = this.dateUtilService.plusDays(planStart, i);
        var capacity = this.getCapacity(productionLine, buildDate);
        let available = this.getAvailable(productionLine, capacity, buildDate, today);
        
        lineBlotter.m2.push(0);
        lineBlotter.capacity.push(capacity);
        lineBlotter.available.push(available);
      }

      this.blotter.productionLines[productionLine] = lineBlotter;
    });

    return this.blotter;
  }

  getCapacityFromBlotter(skipWeeks){
    var capacity = {
      count: {},
      m2: {},
      total: {}
    };
    Object.keys(this.blotter.productionLines).forEach(productionLine => {
      let lineBlotter = this.blotter.productionLines[productionLine];
      lineBlotter.capacity.splice(0, 7 * skipWeeks);
      capacity["count"][productionLine] = lineBlotter.capacity;
      capacity["m2"][productionLine] = lineBlotter.m2;
    });
    return capacity;
  }

  getCapacity(productionLine, date) {

    if(this.lineCapacities[productionLine] === undefined){
      return null;
    }
    
    var capacity = this.lineCapacities[productionLine][date];
    if (capacity !== undefined) {
      return capacity;
    }

    if (this.dateUtilService.isWeekDay(date)) {
      return this.defaultLineCapacity[productionLine];
    }
    else {
      return null;
    }
  }

  getAvailable(productionLine, capacity, buildDate, today) {
      
    var available;
    var sum = 0;
    var panelsCompleteOnDay;
      
    panelsCompleteOnDay  = this.mappedPanelsComplete[productionLine];
    
    if(panelsCompleteOnDay === undefined || isNaN(panelsCompleteOnDay)){
      panelsCompleteOnDay = 0;
    }

    if (panelsCompleteOnDay > capacity) {
      sum = capacity;
    } else {
      sum = panelsCompleteOnDay;
    }

    if (this.dateUtilService.isWeekDay(today) && buildDate === this.dateUtilService.toIsoDate(today)) {
      available = capacity - sum;
    } else {
      available = capacity;
    }

    return available;
  }

  getAreasFromBlotter(skipWeeks) {
    Object.keys(this.blotter.areas).forEach((areaKey) => {
      let area = this.blotter.areas[areaKey];
      area._productionPlan.splice(0, 7 * skipWeeks);
    });

    return this.blotter.areas;
  }

  mapFieldSort(plannerSort){
    this.fieldFilters = [];
    const returnPlannerSort = plannerSort.payload.val();
    if(returnPlannerSort !== null){
      Object.keys(returnPlannerSort).map((dataType) => {
        let order = returnPlannerSort[dataType];
        this.fieldFilters[order] = dataType;
      });
    }
  }

  mapPanelsCompleteToday(panels){
    this.mappedPanelsComplete = {};
    panels.map((panel) => {
      if (panel.qa && panel.qa.completed){
        let completed = this.dateUtilService.toIsoDate(panel.qa.completed);
        if(this.mappedPanelsComplete[completed] === undefined){
          this.mappedPanelsComplete[completed] = {};
        }
        if(this.mappedPanelsComplete[completed][panel.area] === undefined){
          this.mappedPanelsComplete[completed][panel.area] = 0;
        }
        this.mappedPanelsComplete[completed][panel.area]++;
      }
    });
  }

  calculateM2( areas ) {

    var calculatedM2 = {
      "line": {},
      "total": {}
    };

    areas.map((area) => {
      area._productionPlan.map((data, buildDays) => {
        if(this.plan.capacity["m2"][area._productionLine] === undefined){
          this.plan.capacity["m2"][area._productionLine] = {};
        }

        if(this.plan.capacity["m2"][area._productionLine][buildDays] === undefined){
          this.plan.capacity["m2"][area._productionLine][buildDays] = 0;
        }

        if(this.plan.capacity["total"][buildDays] === undefined){
          this.plan.capacity["total"][buildDays] = 0;
        }

        this.plan.capacity["m2"][area._productionLine][buildDays] += data.m2;
        this.plan.capacity["total"][buildDays] += data.m2;
      });
    });

    return calculatedM2;
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

}
