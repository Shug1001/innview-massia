import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlannerComponent } from './update-planner.component';

xdescribe('UpdatePlannerComponent', () => {
  let component: UpdatePlannerComponent;
  let fixture: ComponentFixture<UpdatePlannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePlannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
