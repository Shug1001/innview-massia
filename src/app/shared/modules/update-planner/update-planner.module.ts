import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdatePlannerComponent } from './update-planner.component';
import { UpdatePlannerDialogComponent } from './update-planner-dialog/update-planner-dialog.component';
import { MatIconModule, MatDialogModule, MatButtonModule, MatProgressBarModule } from '@angular/material';

@NgModule({
  declarations: [UpdatePlannerComponent, UpdatePlannerDialogComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatProgressBarModule
  ],
  exports: [UpdatePlannerComponent],
  entryComponents: [UpdatePlannerDialogComponent]
})
export class UpdatePlannerModule { }
