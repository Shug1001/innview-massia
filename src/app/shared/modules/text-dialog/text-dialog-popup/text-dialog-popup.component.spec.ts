import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextDialogPopupComponent } from './text-dialog-popup.component';

xdescribe('TextDialogPopupComponent', () => {
  let component: TextDialogPopupComponent;
  let fixture: ComponentFixture<TextDialogPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextDialogPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextDialogPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
