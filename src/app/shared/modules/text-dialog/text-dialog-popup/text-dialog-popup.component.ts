import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-text-dialog-popup',
  templateUrl: './text-dialog-popup.component.html',
  styleUrls: ['./text-dialog-popup.component.scss']
})
export class TextDialogPopupComponent {

  constructor(
    public dialogRef: MatDialogRef<TextDialogPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}