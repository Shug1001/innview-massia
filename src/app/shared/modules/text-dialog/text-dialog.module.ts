import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextDialogComponent } from './text-dialog.component';
import { TextDialogPopupComponent } from './text-dialog-popup/text-dialog-popup.component';
import { MatIconModule, MatDialogModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [TextDialogComponent, TextDialogPopupComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
  ],
  exports: [TextDialogComponent],
  entryComponents: [TextDialogPopupComponent]
})
export class TextDialogModule { }
