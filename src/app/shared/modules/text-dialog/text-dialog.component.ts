import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TextDialogPopupComponent } from './text-dialog-popup/text-dialog-popup.component';

@Component({
  selector: 'app-text-dialog',
  templateUrl: './text-dialog.component.html',
  styleUrls: ['./text-dialog.component.scss']
})
export class TextDialogComponent {
  
  @Input() text: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(TextDialogPopupComponent, {
      width: '500px',
      data: {text: this.text}
    });
  }

}