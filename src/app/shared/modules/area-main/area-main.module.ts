import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaMainComponent } from './area-main.component';
import { MatCardModule, MatInputModule, MatGridListModule, MatIconModule, MatTableModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { AreasService } from '../../services';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        FormsModule, 
        CommonModule, 
        MatCardModule, 
        MatGridListModule, 
        MatInputModule, 
        MatTableModule, 
        MatNativeDateModule, 
        MatIconModule, 
        MatDatepickerModule],
    declarations: [AreaMainComponent],
    exports: [AreaMainComponent],
    providers: [{provide: 'AreasService', useClass: AreasService}],
})
export class AreaMainModule {}
