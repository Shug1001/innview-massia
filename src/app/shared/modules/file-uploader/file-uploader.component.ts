import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

// const URL = '/api/';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})

export class FileUploaderComponent {
  
  @Output() uploadEvent =  new EventEmitter();
  @ViewChild('fileInput') fileInput: any;
  
  public uploader:FileUploader = new FileUploader({
    url: URL, 
    disableMultipart:true
    });
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;

  fileObject: any;


  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e; 
  }
 
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  public onFileSelected(event: EventEmitter<File[]>) {
    const file: File = event[0];

    console.log(file);
    this.uploadEvent.emit(file);
    this.fileInput.nativeElement.value = '';
    // readBase64(file)
    //   .then(function(data) {
    //   console.log(data);
    // })
  }
}