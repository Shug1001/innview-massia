import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../../services';

@Component({
  selector: 'app-project-data-azure',
  templateUrl: './project-data-azure.component.html',
  styleUrls: ['./project-data-azure.component.scss']
})
export class ProjectDataAzureComponent implements OnInit {

  constructor(private projectsService: ProjectsService) { }

  ngOnInit() {
  }

  updateAzure(){

    return this.projectsService.updateProjectDataAzure();

  }

}
