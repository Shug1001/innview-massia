import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDataAzureComponent } from './project-data-azure.component';

describe('ProjectDataAzureComponent', () => {
  let component: ProjectDataAzureComponent;
  let fixture: ComponentFixture<ProjectDataAzureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectDataAzureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDataAzureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
