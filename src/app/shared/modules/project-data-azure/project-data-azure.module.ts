import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectDataAzureComponent } from './project-data-azure.component';
import { MatButtonModule, MatIconModule } from '@angular/material';

@NgModule({
  declarations: [ProjectDataAzureComponent],
  exports: [ProjectDataAzureComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class ProjectDataAzureModule { }
