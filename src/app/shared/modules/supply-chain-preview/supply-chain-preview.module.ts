import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplyChainPreviewComponent } from './supply-chain-preview.component';

@NgModule({
  declarations: [SupplyChainPreviewComponent],
  imports: [
    CommonModule
  ],
  exports: [SupplyChainPreviewComponent]
})
export class SupplyChainPreviewModule { }
