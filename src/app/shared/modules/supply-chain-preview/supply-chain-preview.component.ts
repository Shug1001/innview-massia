import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import {SupplyChainService } from '../../services/supply-chain.service';
import {DateUtilService } from '../../services/date-util.service';
import { SupplierStock } from '../../models/models';

@Component({
  selector: 'app-supply-chain-preview',
  templateUrl: './supply-chain-preview.component.html',
  styleUrls: ['./supply-chain-preview.component.scss']
})
export class SupplyChainPreviewComponent implements OnInit {

  @Input() which: string;
  data: any;
  _subscription: Subscription;
  loaded: boolean;

  constructor(
    private supplyChainService: SupplyChainService,
    private dateUtilService: DateUtilService) { }

  onDestroy() {
    this._subscription.unsubscribe();
  }

  ngOnInit() {

    this.loaded = false;

    if (this.which === 'stockItem') {
      this._subscription = this.supplyChainService.getStockItemsByDate().subscribe((data) => {
        this.data = data;
        let count = 1;
        this.data.forEach((item) => {
          item.name = item.stockName;
          item.no = count;
          item.dtf = this.dateUtilService.toUserSlachDate(item.dateTimestamp);
          count++;
        });
        this.data.sort((areaA, areaB) => {
          const a = areaA.dtf;
          const b = areaB.dtf;
          const r = b.localeCompare(a);
          return r;
        });
        this.loaded = true;
      });
    } else if (this.which === 'suppliers') {
      this._subscription = this.supplyChainService.getSuppliersByDate().subscribe((data) => {
        this.data = data;
        let count = 1;
        this.data.forEach((item) => {
          item.name = item.companyName;
          item.no = count;
          item.dtf = this.dateUtilService.toUserSlachDate(item.dateTimestamp);
          count++;
        });
        this.data.sort((areaA, areaB) => {
          const a = areaA.dtf;
          const b = areaB.dtf;
          const r = b.localeCompare(a);
          return r;
        });
        this.loaded = true;
      });
    }
  }

}
