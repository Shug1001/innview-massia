import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplyChainPreviewComponent } from './supply-chain-preview.component';

describe('SupplyChainPreviewComponent', () => {
  let component: SupplyChainPreviewComponent;
  let fixture: ComponentFixture<SupplyChainPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplyChainPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplyChainPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
