import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFromRefComponent } from './user-from-ref.component';

@NgModule({
  declarations: [UserFromRefComponent],
  imports: [
    CommonModule
  ],
  exports: [UserFromRefComponent]
})
export class UserFromRefModule { }
