import { Component, OnInit, Input } from '@angular/core';
import { ReferenceDataService } from '../../services/reference-data.service';
import { Subscription } from 'rxjs';
import { Users } from '../../models/models';

@Component({
  selector: 'app-user-from-ref',
  templateUrl: './user-from-ref.component.html',
  styleUrls: ['./user-from-ref.component.scss']
})
export class UserFromRefComponent implements OnInit {

  @Input() userRef: string;
  _subscription: Subscription;
  user: string;

  constructor(private referenceDataService: ReferenceDataService) {
    this.user = "loading...";
  }

  ngOnInit() {
    if(this.userRef !== ""){
      this._subscription = this.referenceDataService.getUser(this.userRef).subscribe((user)=>{
        this.user = user.name;
      })
    }
  }

}
