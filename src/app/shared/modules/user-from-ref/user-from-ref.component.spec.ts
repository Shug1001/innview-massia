import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFromRefComponent } from './user-from-ref.component';

xdescribe('UserFromRefComponent', () => {
  let component: UserFromRefComponent;
  let fixture: ComponentFixture<UserFromRefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFromRefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFromRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
