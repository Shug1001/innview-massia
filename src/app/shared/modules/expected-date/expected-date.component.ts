import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ForwardPlannerService } from '../../services';
import { DateUtilService } from '../../services/date-util.service';
import { StatusService } from '../../services/status.service';

@Component({
  selector: 'app-expected-date',
  templateUrl: './expected-date.component.html',
  styleUrls: ['./expected-date.component.scss']
})
export class ExpectedDateComponent implements OnInit {
  
  @Input() areaId: string;
  @Input() deliveryDate: string;
  _subscription: Subscription;
  ready: boolean;
  expectedDate: string;

  constructor(private dateUtilService: DateUtilService, private forwardPlannerService: ForwardPlannerService) { }

  ngOnInit() {
    this._subscription = this.forwardPlannerService.getAreaProductionPlansForArea(this.areaId).subscribe((snap) => {
      let plan: any = snap.payload.val();
      this.expectedDate = "";
      if(plan !== null && plan.plannedFinish){
        let riskDate = this.dateUtilService.subtractWorkingDays(this.deliveryDate, 1);
        let riskDay = this.dateUtilService.daysBetweenWeekDaysOnly(plan.plannedFinish, riskDate);
        if(riskDay <= 0){
          this.expectedDate = this.dateUtilService.toUserSlachDate(this.dateUtilService.addWorkingDays(plan.plannedFinish,1));
        }    
      }
      this.ready = true;
    });
  }

}
