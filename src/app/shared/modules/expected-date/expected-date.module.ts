import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpectedDateRoutingModule } from './expected-date-routing.module';
import { ExpectedDateComponent } from './expected-date.component';

@NgModule({
  exports: [ExpectedDateComponent],
  declarations: [ExpectedDateComponent],
  imports: [
    CommonModule,
    ExpectedDateRoutingModule
  ]
})
export class ExpectedDateModule { }
