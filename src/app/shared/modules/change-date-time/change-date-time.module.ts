import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeDateTimeDialogComponent } from './change-date-time-dialog/change-date-time-dialog.component';
import { ChangeDateTimeComponent } from './change-date-time.component';
import { MatButtonModule, MatDatepickerModule, MatInputModule } from '@angular/material';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { FormsModule } from '@angular/forms';

@NgModule({
  exports: [ChangeDateTimeComponent],
  entryComponents: [ChangeDateTimeDialogComponent],
  declarations: [ChangeDateTimeComponent, ChangeDateTimeDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    NgxMaterialTimepickerModule
  ]
})
export class ChangeDateTimeModule { }
