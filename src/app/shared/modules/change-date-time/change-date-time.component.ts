import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ChangeDateTimeDialogComponent } from './change-date-time-dialog/change-date-time-dialog.component';

@Component({
  selector: 'app-change-date-time',
  templateUrl: './change-date-time.component.html',
  styleUrls: ['./change-date-time.component.scss']
})
export class ChangeDateTimeComponent {

  @Input() date: number;
  @Input() time: string;
  @Input() type: string;
  @Input() bookingRef: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(ChangeDateTimeDialogComponent, {
      width: '500px',
      data: {
        date: this.date,
        time: this.time,
        type: this.type,
        bookingRef: this.bookingRef
      }
    });
  }

}