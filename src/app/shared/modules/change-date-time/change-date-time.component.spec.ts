import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDateTimeComponent } from './change-date-time.component';

describe('ChangeDateTimeComponent', () => {
  let component: ChangeDateTimeComponent;
  let fixture: ComponentFixture<ChangeDateTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeDateTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeDateTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
