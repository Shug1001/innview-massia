import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDateTimeDialogComponent } from './change-date-time-dialog.component';

describe('ChangeDateTimeDialogComponent', () => {
  let component: ChangeDateTimeDialogComponent;
  let fixture: ComponentFixture<ChangeDateTimeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeDateTimeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeDateTimeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
