import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DashboardsService } from 'src/app/shared/services/dashboards.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';

@Component({
  selector: 'app-change-date-time-dialog',
  templateUrl: './change-date-time-dialog.component.html',
  styleUrls: ['./change-date-time-dialog.component.scss']
})
export class ChangeDateTimeDialogComponent {

  formattedDate: string;

  constructor(
    public dialogRef: MatDialogRef<ChangeDateTimeDialogComponent>,
    private dateutilsService: DateUtilService,
    private dashboardService: DashboardsService,
    @Inject(MAT_DIALOG_DATA) public data) {
    }

  save(){
    if(this.data.date && this.data.time){
      if(this.data.type === 'booking'){
        let timestamp = this.data.date.getTime();
        let updateObj = {
          bookedTime: timestamp,
          bookedTimeText: this.data.time
        }
        return this.dashboardService.updateBooking(this.data.bookingRef, updateObj)
        .then(()=>{
          this.dialogRef.close();
        });
      }else{
        let timestamp = this.data.date.getTime();
        let updateObj = {
          deliveryTime: timestamp,
          deliveryTimeText: this.data.time
        }
        return this.dashboardService.updateBooking(this.data.bookingRef, updateObj)
        .then(()=>{
          this.dialogRef.close();
        });
      }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}