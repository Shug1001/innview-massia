import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssociatedUsersSelectComponent } from './associated-users-select.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule, MatFormFieldModule } from '@angular/material';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [AssociatedUsersSelectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSelectModule,
    PipesModule,
    MatFormFieldModule
  ],
  exports: [AssociatedUsersSelectComponent]
})
export class AssociatedUsersSelectModule { }