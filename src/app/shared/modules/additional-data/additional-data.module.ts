import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material';
import { AdditionalDataComponent } from './additional-data.component';

@NgModule({
  exports: [AdditionalDataComponent],
  declarations: [AdditionalDataComponent],
  imports: [
    CommonModule,
    MatCheckboxModule
  ]
})
export class AdditionalDataModule { }
