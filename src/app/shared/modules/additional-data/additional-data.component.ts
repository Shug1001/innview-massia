import { Component, Input, OnInit } from '@angular/core';
import { AreasService } from 'src/app/shared/services/areas.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { ReferenceDataService } from 'src/app/shared/services/reference-data.service';
import { Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireObject, SnapshotAction } from '@angular/fire/database';

@Component({
  selector: 'app-additional-data',
  templateUrl: './additional-data.component.html',
  styleUrls: ['./additional-data.component.scss']
})
export class AdditionalDataComponent implements OnInit {

  @Input() areaRef: String;
  @Input() type: String;
  additionalData: SnapshotAction<any>;
  ready: boolean;
  isChecked: false;
  additionalDataHolder: Object;
  _subscription: Subscription;

  constructor(private referenceDataService: ReferenceDataService, private authService: AuthService, private areasService: AreasService, private dateUtilService: DateUtilService) { }

  ngOnInit() {
    this.additionalDataHolder = {
      "status" : false,
      "timeDate" : null,
      "user" : null
    };
    this.ready = false;
    this._subscription = this.areasService.getAdditionalAreaType(this.areaRef, this.type).subscribe((additionalData: SnapshotAction<any>) => {
      this.additionalData = additionalData.payload.val();
      this.ready = true;
    });
  }

  changeAdditional(evt, data){
    let updateObj = {
      "status" : data,
      "timeDate" : this.authService.serverTime(),
      "user" : null
    };
    return this.referenceDataService.getUserPromise(this.authService.authState.uid)
    .then((user) => {
      updateObj.user = this.dateUtilService.getInitials(user.name);
      return this.areasService.updateProjectAdditional(this.areaRef, this.type, updateObj);
    });  
  }

}