import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSelectModule, MatInputModule, MatIconModule, MatTooltipModule, MatDialogModule, MatButtonModule } from '@angular/material';
import { StockTypesComponent } from './stock-types.component';
import { AddDialogComponent } from './add-dialog/add-dialog.component';

@NgModule({
  declarations: [StockTypesComponent, AddDialogComponent],
  entryComponents: [AddDialogComponent],
  imports: [
    CommonModule,
    MatSelectModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    MatTooltipModule
  ],
  exports: [StockTypesComponent]
})
export class StockTypesModule { }
