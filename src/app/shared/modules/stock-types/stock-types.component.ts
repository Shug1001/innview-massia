import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { SupplyChainService } from '../../services/supply-chain.service';
import { MatDialog } from '@angular/material/dialog';
import { AddDialogComponent } from './add-dialog/add-dialog.component';

@Component({
  selector: 'app-stock-types',
  templateUrl: './stock-types.component.html',
  styleUrls: ['./stock-types.component.scss']
})
export class StockTypesComponent implements OnInit {

  @Input() stockType: string;
  @Input() displayName: string;
  loaded: boolean;

  _subscription: Subscription;
  items: Array <any>;

  constructor(private supplyChainService: SupplyChainService, public dialog: MatDialog) {
    this.loaded = false;
  }

  openDialog(): void {
    this.dialog.open(AddDialogComponent, {
      width: '500px',
      data: {
        stockType: this.stockType,
        displayName: this.displayName,
        items: this.items
      }
    });
  }

  ngOnInit() {

    this._subscription = this.supplyChainService.getStockTypes(this.stockType).subscribe((items) => {
      items.sort(function(a, b) {
        return a['name'] - b['name'];
      });
      this.items = items;
      this.loaded = true;
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}