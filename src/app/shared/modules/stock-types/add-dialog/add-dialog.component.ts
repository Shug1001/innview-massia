import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StockTypesComponent } from '../stock-types.component';
import { SupplyChainService } from '../../../services/supply-chain.service';
import { StockType } from '../../../models/models';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent {

  newObj: StockType;
  disableSave: boolean;
  status: string;

  constructor(
    public dialogRef: MatDialogRef<StockTypesComponent>,
    private supplyChainService: SupplyChainService,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.disableSave = false;
      this.status = '';
      this.newObj = {
        name: '',
        code: ''
      };
      if (this.data.stockType !== 'stockGroup') {
        delete this.newObj.code;
      }
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    saveStockType() {
      this.disableSave = true;
      let found = false;
      this.data.items.forEach(item => {
        if (item.name === item.element) {
          found = true;
        }
      });
      if ( !found ) {
        return this.supplyChainService.createStockTypes(this.data.stockType, this.newObj)
          .then(() => {
            this.status = '';
            this.dialogRef.close();
          });
      } else {
        this.status = 'Already added!';
      }
    }
}
