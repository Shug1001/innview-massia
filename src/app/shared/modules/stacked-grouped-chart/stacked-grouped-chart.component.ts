import { ViewChild, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { ChartsModule, Color, BaseChartDirective } from 'ng4-charts/ng4-charts';
import { DateUtilService } from '../../services/date-util.service';



@Component({
  selector: 'app-stacked-grouped-chart',
  templateUrl: './stacked-grouped-chart.component.html',
  styleUrls: ['./stacked-grouped-chart.component.scss']
})

export class StackedGroupedChartComponent implements OnInit {

  @Input() chartData: object;
  @Input() chartColours: object;
  @Input() title: string;

  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      enabled: true
    },
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Weeks'
        }
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: '(m2)'
       }
      }]
    },
    // We use these empty structures as placeholders for dynamic theming.scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        display: function(context) {
          return context.dataset.data[context.dataIndex] > 15;
        },
        anchor: 'end',
        align: 'end',
        font: {
          size: 10,
        },
        labels: {
          value: {
            color: 'black'
          }
        },
        formatter: Math.round
      }
    }
  };

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;


  public barChartLabels: string[] = [];
  public barChartType: ChartType;
  public barChartLegend: boolean;

  public barChartPlugins = [pluginDataLabels];

  public barChartColors: Color[] = []

  public barChartData: ChartDataSets[] = [];


  loaded: boolean;

  constructor(private dateUtilsService: DateUtilService) {
    this.loaded = false;
  }

  setUpChart(){
    this.barChartType = 'bar';
    this.barChartLegend = true;
    this.barChartColors = [];
    this.barChartData = [];
    this.barChartLabels = [];
    let labels = [];

    Object.keys(this.chartData).forEach((y) => {

      let dataObj = { data: [], label: y};
      let colour = { backgroundColor: this.chartColours[y], borderColor: '#fff' };
      
      Object.keys(this.chartData[y]).forEach((x) => {
        let date = this.dateUtilsService.toUserDate(x)
        if(labels.indexOf(date) === -1){
          labels.push(date);
        }
        let value = this.chartData[y][x];
        dataObj.data.push(Math.round(value));
      });

      this.barChartColors.push(colour);
      this.barChartData.push(dataObj);
    });

    this.barChartLabels.length = 0;
    setTimeout( () => {
      this.barChartLabels.push(...labels);
    });
    
    this.loaded = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if(JSON.stringify(changes.chartData.currentValue) !== JSON.stringify(changes.chartData.previousValue)){
      this.chartData = changes.chartData.currentValue;
      this.setUpChart(); 
    } 
  }

  ngOnInit() {
    this.setUpChart();    
  }

  chartClicked(e: any): void {
      console.log(e);
  }

  chartHovered(e: any): void {
      console.log(e);
  }

}
