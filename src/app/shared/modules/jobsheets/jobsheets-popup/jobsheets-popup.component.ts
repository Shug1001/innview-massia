import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-jobsheets-popup',
  templateUrl: './jobsheets-popup.component.html',
  styleUrls: ['./jobsheets-popup.component.scss']
})
export class JobsheetsPopupComponent {

  constructor(
    public dialogRef: MatDialogRef<JobsheetsPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
