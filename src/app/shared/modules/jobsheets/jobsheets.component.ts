import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { AreasService } from '../../services';
import { JobsheetsPopupComponent } from './jobsheets-popup/jobsheets-popup.component';

@Component({
  selector: 'app-jobsheets',
  templateUrl: './jobsheets.component.html',
  styleUrls: ['./jobsheets.component.scss']
})
export class JobsheetsComponent implements OnInit {

  @Input() areaKey: string;
  _subscription: Subscription;
  jobsheetsCount: number;
  jobsheets: Array<any>;

  constructor(public dialog: MatDialog, private areasService: AreasService) { }

  ngOnInit() {

    this._subscription = this.areasService.getJobsheetsByArea(this.areaKey).subscribe((jobsheets) => {
      this.jobsheetsCount = jobsheets.length;
      this.jobsheets = jobsheets;
    });
  }

  openDialog(): void {
    this.dialog.open(JobsheetsPopupComponent, {
      width: '500px',
      data: {
        jobsheets: this.jobsheets,
        jobsheetsCount: this.jobsheetsCount
      }
    });
  }

}
