import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsheetsRoutingModule } from './jobsheets-routing.module';
import { JobsheetsComponent } from './jobsheets.component';
import { JobsheetsPopupComponent } from './jobsheets-popup/jobsheets-popup.component';

@NgModule({
  exports: [JobsheetsComponent],
  entryComponents: [JobsheetsPopupComponent],
  declarations: [JobsheetsComponent, JobsheetsPopupComponent],
  imports: [
    CommonModule,
    JobsheetsRoutingModule
  ]
})
export class JobsheetsModule { }
