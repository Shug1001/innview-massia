import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DfmBufferComponent } from './dfm-buffer.component';

@NgModule({
  exports: [DfmBufferComponent],
  declarations: [DfmBufferComponent],
  imports: [
    CommonModule
  ]
})
export class DfmBufferModule { }
