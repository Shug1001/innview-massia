import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DfmBufferComponent } from './dfm-buffer.component';

describe('DfmBufferComponent', () => {
  let component: DfmBufferComponent;
  let fixture: ComponentFixture<DfmBufferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfmBufferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfmBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
