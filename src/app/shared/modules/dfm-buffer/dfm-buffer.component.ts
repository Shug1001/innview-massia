import { Component, OnInit, Input } from '@angular/core';
import { DateUtilService } from '../../services/date-util.service';
import { DashboardsService } from '../../services/dashboards.service';
import { PanelsService } from '../../services/panels.service';
import { Subscription } from 'rxjs';
import { DesignIssued } from '../../models/models';

@Component({
  selector: 'app-dfm-buffer',
  templateUrl: './dfm-buffer.component.html',
  styleUrls: ['./dfm-buffer.component.scss']
})
export class DfmBufferComponent implements OnInit {

  @Input() date: String;
  _subscription: Subscription;
  displayData: DesignIssued; 
  ready: boolean;

  constructor(private dateUtilService: DateUtilService, private panelsService: PanelsService, private dashboardsService: DashboardsService) { }

  ngOnInit() {
    this.ready = false;
    let startDate = this.dateUtilService.minusDays(this.date, 42);
    let startTimeStamp = new Date(startDate).getTime();
    let endTimeStamp = new Date(String(this.date)).getTime();
    this._subscription = this.panelsService.getPanelsCreatedForRange(startTimeStamp, endTimeStamp).subscribe((panels) => {
      this.displayData = this.dashboardsService.bufferDFM(panels);
      this.ready = true;
    });
  }

}
