import { NgModule } from '@angular/core';
import { FileUploaderModule } from '../file-uploader/file-uploader.module';
import { CommonModule } from '@angular/common';
import { DimensionHolderComponent } from './dimension-holder.component';
import { DimensionComponent } from './dimension/dimension.component';
import { MatIconModule, MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [DimensionHolderComponent, DimensionComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    FileUploaderModule
  ],
  exports: [
    DimensionHolderComponent
  ],
  entryComponents: [
    DimensionComponent
  ]
})
export class DimensionHolderModule { }
