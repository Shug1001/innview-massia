import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DimensionHolderComponent } from './dimension-holder.component';

describe('DimensionHolderComponent', () => {
  let component: DimensionHolderComponent;
  let fixture: ComponentFixture<DimensionHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DimensionHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DimensionHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
