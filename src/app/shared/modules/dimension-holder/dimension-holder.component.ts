import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DimensionComponent } from './dimension/dimension.component';

@Component({
  selector: 'app-dimension-holder',
  templateUrl: './dimension-holder.component.html',
  styleUrls: ['./dimension-holder.component.scss']
})
export class DimensionHolderComponent {

  constructor(public dialog: MatDialog) { }

  dimensionsPopup(){
    this.dialog.open(DimensionComponent);
  }

}
