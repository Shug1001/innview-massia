import { Component, OnInit, Input } from '@angular/core';
import { Subscription, combineLatest } from 'rxjs';
import { ProjectsService } from '../../services/projects.service';
import { ProjectsAdditional } from '../../models/models';

@Component({
  selector: 'app-is-revit',
  templateUrl: './is-revit.component.html',
  styleUrls: ['./is-revit.component.scss']
})
export class IsRevitComponent implements OnInit {

  additionalProjectData: any;
  _subscription: Subscription;
  loaded: boolean;
  iRevit: boolean;
  badgeColour: string;
  iRevitData: string;
  @Input() projectKey: string;

  constructor(private projectService: ProjectsService) {
    this.iRevit = false;
    this.badgeColour = 'warn';
    this.iRevitData = 'N';
    this.loaded = false;
  }

  ngOnInit() {
    this._subscription = combineLatest([
      this.projectService.getProjectAdditional(this.projectKey),
      this.projectService.getQuoteByProject(this.projectKey)
    ]).subscribe(([
      additionalProjectData,
      quotes
    ]) => {

      this.iRevit = false;
      this.iRevitData = 'N';
      this.badgeColour = 'warn';

      if (quotes.length) {
        this.iRevit = true;
      }

      this.additionalProjectData = additionalProjectData;
      if (this.additionalProjectData !== null && this.additionalProjectData.revitUpload && this.additionalProjectData.revitUpload === true){
        this.iRevitData = 'Y';
        this.badgeColour = 'primary';
      }
      this.loaded = true;

    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
