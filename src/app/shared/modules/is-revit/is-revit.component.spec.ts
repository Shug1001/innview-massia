import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IsRevitComponent } from './is-revit.component';

describe('IsRevitComponent', () => {
  let component: IsRevitComponent;
  let fixture: ComponentFixture<IsRevitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsRevitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IsRevitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
