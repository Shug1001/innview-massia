import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IsRevitComponent } from './is-revit.component';
import { MatBadgeModule } from '@angular/material';

@NgModule({
  declarations: [IsRevitComponent],
  imports: [
    CommonModule,
    MatBadgeModule
  ],
  exports: [IsRevitComponent]
})
export class IsRevitModule { }
