import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteSupplyChainComponent } from './delete-supply-chain.component';
import { DeleteSupplyChainPopupComponent } from './delete-supply-chain-popup/delete-supply-chain-popup.component';
import { MatIconModule, MatDialogModule, MatButtonModule, MatInputModule, MatCardModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [DeleteSupplyChainComponent, DeleteSupplyChainPopupComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [DeleteSupplyChainComponent],
  entryComponents: [DeleteSupplyChainPopupComponent]
})
export class DeleteSupplyChainModule { }
