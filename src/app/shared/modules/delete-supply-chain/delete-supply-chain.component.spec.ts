import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSupplyChainComponent } from './delete-supply-chain.component';

xdescribe('DeleteSupplyChainComponent', () => {
  let component: DeleteSupplyChainComponent;
  let fixture: ComponentFixture<DeleteSupplyChainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSupplyChainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSupplyChainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
