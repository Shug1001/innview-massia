import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DeleteSupplyChainPopupComponent } from './delete-supply-chain-popup/delete-supply-chain-popup.component';

@Component({
  selector: 'app-delete-supply-chain',
  templateUrl: './delete-supply-chain.component.html',
  styleUrls: ['./delete-supply-chain.component.scss']
})
export class DeleteSupplyChainComponent {

  @Input() type: string;
  @Input() supplierKey: string;
  @Input() stockItemKey: string;
  @Input() stockDetailsKey: string;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(DeleteSupplyChainPopupComponent, {
      width: '500px',
      data: {
        type: this.type,
        supplierKey: this.supplierKey,
        stockItemKey: this.stockItemKey,
        stockDetailsKey: this.stockDetailsKey
      }
    });
  }

}