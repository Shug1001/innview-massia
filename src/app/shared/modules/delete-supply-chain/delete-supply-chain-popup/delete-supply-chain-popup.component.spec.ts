import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSupplyChainPopupComponent } from './delete-supply-chain-popup.component';

xdescribe('DeleteSupplyChainPopupComponent', () => {
  let component: DeleteSupplyChainPopupComponent;
  let fixture: ComponentFixture<DeleteSupplyChainPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSupplyChainPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSupplyChainPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
