import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SupplyChainService } from '../../../services/supply-chain.service';

@Component({
  selector: 'app-delete-supply-chain-popup',
  templateUrl: './delete-supply-chain-popup.component.html',
  styleUrls: ['./delete-supply-chain-popup.component.scss']
})
export class DeleteSupplyChainPopupComponent implements OnInit {

  code: Array<any>;
  userCode: Array<any>;

  constructor(
    public supplyChainService: SupplyChainService,
    public dialogRef: MatDialogRef<DeleteSupplyChainPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.genCode();
  }

  genCode() {
    this.code = [];
    this.code.push(Math.round(Math.random() * 10));
    this.code.push(Math.round(Math.random() * 10));
    this.code.push(Math.round(Math.random() * 10));
    this.code.push(Math.round(Math.random() * 10));

    this.userCode = [];
    this.userCode.push(0);
    this.userCode.push(0);
    this.userCode.push(0);
    this.userCode.push(0);
  }

  isDisabled() {
    if (this.code[0] === this.userCode[0] &&
      this.code[1] === this.userCode[1] &&
      this.code[2] === this.userCode[2] &&
      this.code[3] === this.userCode[3]) {
      return false;
    } else {
      return true;
    }
  }

  delete() {

    if (this.data.type === 'stockItem') {
      return this.supplyChainService.deleteStockItem(this.data.stockItemKey).then(() => {
        this.dialogRef.close();
      });
    } else if (this.data.type === 'supplier') {
      return this.supplyChainService.deleteSupplier(this.data.supplierKey).then(() => {
        this.dialogRef.close();
      });
    } else if (this.data.type === 'supplier-stock') {
      return this.supplyChainService.deleteSupplierStock(
        this.data.stockItemKey,
        this.data.supplierKey,
        this.data.stockDetailsKey)
        .then(() => {
          this.dialogRef.close();
        });
    }

  }

}