import { Component, OnInit, Input, SimpleChanges, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ForwardPlannerService } from '../../services';
import { AreaStats, PanelStat, PanelStats } from '../../models/models';
import { MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { PanelsService } from '../../services/panels.service';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import { LinePopupDialogComponent } from './line-popup-dialog/line-popup-dialog.component';
import { AuditsService } from '../../services/audits.service';
import { DateUtilService } from '../../services/date-util.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-panel-status-details',
  templateUrl: './panel-status-details.component.html',
  styleUrls: ['./panel-status-details.component.scss']
})
export class PanelStatusDetailsComponent {

  @ViewChild(MatSort) sort: MatSort;
  @Input() areaStats: AreaStats;
  panelsStats: PanelStats;
  _subscription: Subscription;
  loaded: boolean;
  date = new Date();
  duration: number;
  disableButtons: boolean;
  toggleAll: boolean;
  displayedColumns: string[] = ['select', 'position', 'id', 'uploaded', 'updated', 'line', 'status'];
  dataSource: MatTableDataSource<PanelStat>;
  line: string;
  selection = new SelectionModel<PanelStat>(true, []);

  constructor(
    private dateUtilsService: DateUtilService,
    private db: AngularFireDatabase,
    private forwardPlannerService: ForwardPlannerService,
    private panelsService: PanelsService, 
    public dialog: MatDialog,
    private auditsService: AuditsService,) { 
      this.dataSource = new MatTableDataSource<PanelStat>([]);
      this.dataSource.sort = this.sort;
      this.disableButtons = true;
      this.toggleAll = true;
      this.duration = 0;
    }

  dateChange(dt){
    this.date = dt;
  }

  durationChange(evt){
    this.duration = evt;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.toggleAll = !this.toggleAll;
    this.toggleAll ?
      this.selection.clear() :
      this.dataSource.data.forEach((row) => {
        if(!row.complete){
          this.selection.select(row);
        }
      });
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PanelStat): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  ngOnChanges(changes: SimpleChanges) {

    this.loaded = false;
    this.disableButtons = true;

    if(this._subscription){
      this._subscription.unsubscribe();
    }
    this._subscription = this.forwardPlannerService.getPanelStats(this.areaStats).subscribe((panelsStats) => {
      this.panelsStats = panelsStats;
      this.dataSource = new MatTableDataSource<PanelStat>(this.panelsStats.stats);
      this.dataSource.sort = this.sort;
      this.loaded = true;
      this.disableButtons = false;
    });

  }

  openLineDialog(): void {
    if(this.selection.selected.length){
      const dialogRef = this.dialog.open(LinePopupDialogComponent, {
        width: '250px'
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.line = result;
        this.completeSetected();
      });
    }
  }

  uncompletePanel(panelStat){
    this.disableButtons = true;
    this.dialog.open(ConfirmationPopupDialogComponent, {
      width: '800px',
      data: { title:"Remove panel completion", text: "Are you sure you want to reset panel " + panelStat.id + "? This will remove ALL QA data for this panel!", buttonText:"Yes"}
    })
    .afterClosed()
    .subscribe(response => {
      if(response.buttonClicked){
        this.panelsService.uncompletePanel(this.panelsStats, panelStat.panel)
          .then(()=>{
            let log = "Marked " + panelStat.panel.id + " uncomplete";
            return this.auditsService.log(log, "", this.areaStats.id, "");
          })
          .then(()=>{
            this.disableButtons = false;
            this.selection.clear();
          });
      }else{
        this.disableButtons = false;
      }
    });

  }

  completeSetected(){
    if(this.selection.selected.length && this.line){
      return this.panelsService.completeSetected(this.panelsStats, this.selection, this.date, this.duration, this.line)
      .then(()=>{
        if (this.panelsStats.actualPanels === this.panelsStats.completed){
          let updates = {};
          updates["productionPlans/areas/" + this.areaStats.areaRef + "/plannedFinish"] = this.dateUtilsService.toIsoDate(this.date);
          return this.db.database.ref().update(updates);
        }else{
          return Promise.resolve();
        } 
      })
      .then(()=>{
        this.selection.clear();
        let log = "Marked " + this.selection.selected.length + " panels complete";
        return this.auditsService.log(log, "", this.areaStats.id, "");
      })
    }
  }

  deleteSetected(){
    if(this.selection.selected.length){
      this.disableButtons = true;
      this.dialog.open(ConfirmationPopupDialogComponent, {
        width: '800px',
        data: {title:"Delete Panels", text: "Are you sure you want to delete these panels?", buttonText:"Yes"}
      })
      .afterClosed()
      .subscribe(response => {
        if(response.buttonClicked){
          this.panelsService.deleteSetected(this.panelsStats, this.selection)
          .then(()=>{
            let log = "Deleted " + this.selection.selected.length + " panels";
            return this.auditsService.log(log, "", this.areaStats.id, "");
          })
            .then(()=>{
              this.disableButtons = false;
              this.selection.clear();
            });
        }else{
          this.disableButtons = false;
        }
      });
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
