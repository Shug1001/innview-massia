import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelStatusDetailsComponent } from './panel-status-details.component';
import { MatTableModule, MatCheckboxModule, MatToolbarModule, MatSortModule, MatFormFieldModule, MatPaginatorModule, MatInputModule, MatIconModule, MatCardModule, MatProgressBarModule, MatButtonModule, MatSelectModule, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DatePickerModule } from '../date-picker/date-picker.module';
import { FormsModule } from '@angular/forms';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import { LinePopupDialogComponent } from './line-popup-dialog/line-popup-dialog.component';

@NgModule({
  declarations: [PanelStatusDetailsComponent, ConfirmationPopupDialogComponent, LinePopupDialogComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSortModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatProgressBarModule,
    FormsModule,
    DatePickerModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [
    PanelStatusDetailsComponent
  ],
  entryComponents: [ConfirmationPopupDialogComponent, LinePopupDialogComponent]
})
export class PanelStatusDetailsModule { }
