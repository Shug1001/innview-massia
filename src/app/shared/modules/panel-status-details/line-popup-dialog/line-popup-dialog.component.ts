import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PanelStatusDetailsComponent } from '../panel-status-details.component';

export interface Line {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-line-popup-dialog',
  templateUrl: './line-popup-dialog.component.html',
  styleUrls: ['./line-popup-dialog.component.scss']
})


export class LinePopupDialogComponent {

  lines: Line[] = [
    {value: '1', viewValue: 'Auto Line 1'},
    {value: '2', viewValue: 'Auto Line 2'},
    {value: '3', viewValue: 'Auto Line 3'},
    {value: '4', viewValue: 'Man. Line 1'},
    {value: '5', viewValue: 'Man. Line 2'},
    {value: '6', viewValue: 'Man. Line 3'},
    {value: '7', viewValue: 'Man. Line 4'},
    {value: '8', viewValue: 'Sip Line 1'}
  ];

  line: string;

  constructor(public dialogRef: MatDialogRef<PanelStatusDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }


  lineChange(evt){
    this.line = evt.value;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
