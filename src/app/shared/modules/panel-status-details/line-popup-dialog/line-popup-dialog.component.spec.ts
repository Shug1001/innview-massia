import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinePopupDialogComponent } from './line-popup-dialog.component';

describe('LinePopupDialogComponent', () => {
  let component: LinePopupDialogComponent;
  let fixture: ComponentFixture<LinePopupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinePopupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinePopupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
