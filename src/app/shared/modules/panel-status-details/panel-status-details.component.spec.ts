import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelStatusDetailsComponent } from './panel-status-details.component';

xdescribe('PanelStatusDetailsComponent', () => {
  let component: PanelStatusDetailsComponent;
  let fixture: ComponentFixture<PanelStatusDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelStatusDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelStatusDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
