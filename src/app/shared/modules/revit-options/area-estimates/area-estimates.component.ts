import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateUtilService } from '../../../services/date-util.service';
import { RevitOptionsComponent } from '../revit-options.component';
import { LoadPanelsService } from '../../../services/load-panels.service';
import { Subscription, combineLatest } from 'rxjs';
import { CreatedComponents } from '../../../models/models';
import { ProjectsService } from '../../../services/projects.service';


@Component({
  selector: 'app-area-estimates',
  templateUrl: './area-estimates.component.html',
  styleUrls: ['./area-estimates.component.scss']
})
export class AreaEstimatesComponent implements OnInit {


  quoteHolder: {};
  status: string;
  quotesList: Array<any>;
  subTypeObj: {};
  toggleEstimates: boolean;
  text: string;
  titles: Array<any>;
  titlesTable: Array<any>;
  errorTitles: Array<any>;
  statusData: object;
  ready: CreatedComponents;
  statusSubscription: Subscription;
  _subscription: Subscription;
  quotes: Array<any>;
  dataSource: ComponentsTable[];
  loaded: boolean;
  isQuotes: boolean;


  constructor(private dateUtilService: DateUtilService, private projectsService: ProjectsService, private loadpanelsService: LoadPanelsService, private dateUtilsService: DateUtilService, public dialogRef: MatDialogRef<RevitOptionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.isQuotes = false;
      this.loaded = false;
      this.subTypeObj= {
        "DAT_Walls": function(type) {
          let subType = "No Type";
          if (type.indexOf("iSIP") !== -1) {
            subType = "External";
          } else if (type.indexOf("TF") !== -1) {
            subType = "Internal";
          } else if (type.indexOf("iFAST") !== -1) {
            subType = "IFAST";
          }
          return subType;
        },
        "DAT_Floors": function(type) {
          let subType = "No Type";
          if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1) {
            subType = "Floor";
          } else if (type.indexOf("Concrete")  !== -1) {
            subType = "Floor";
          }
          return subType;
        },
        "DAT_Roofs": function(type) {
          let subType = "No Type";
          if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1 || type.indexOf("Cassette") !== -1) {
            subType = "Roof";
          } else if (type.indexOf("Cassette") === -1) {
            subType = "Roof";
          }
          return subType;
        }
      };
      
    }

    subTypeObj2(type) {
      let subType = "No Product Type";
      if (type.indexOf("iSIP") !== -1) {
        subType = "iSIP";
      } else if (type.indexOf("iFAST") !== -1) {
        subType = "iFAST";
      } else if (type.indexOf("Closed TF") !== -1) {
        subType = "CTF";
      } else if (type.indexOf("TF")  !== -1) {
        subType = "TF";
      } else if (type.indexOf("245D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("300D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("350D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("400D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("450D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1) {
        subType = "Timber Floors";
      } else if (type.indexOf("Concrete")  !== -1) {
        subType = "Concrete Floors";
      } else if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1 || type.indexOf("Cassette") !== -1) {
        subType = "Timber Cassette Roofs";
      } else if (type.indexOf("Cassette") === -1) {
        subType = "Timber Frame Roofs";
      }
      return subType;
    }
    
    ngOnInit(){

      this._subscription = combineLatest(this.projectsService.getQuotesList(), this.projectsService.getQuoteDataByLatest(this.data.project.$key)).subscribe(([quotesList, quotes]) => {
        
        this.quoteHolder = {};
        this.quotesList = quotesList;
        this.quotes = quotes;

        this.getAreaEstimates();

      });

    }

    updateProjectEstimates(){
      let updateEstimates = {
        "estimatedAreas":{
          Ext: this.quoteHolder["External"] ? Math.round(this.quoteHolder["External"].gross) : 0,
          Int: this.quoteHolder["Internal"] ? Math.round(this.quoteHolder["Internal"].gross) : 0,
          Floor: this.quoteHolder["Floor"] ? Math.round(this.quoteHolder["Floor"].gross) : 0,
          Roof: this.quoteHolder["Roof"] ? Math.round(this.quoteHolder["Roof"].gross) : 0,
          ExtF: this.quoteHolder["IFAST"] ? Math.round(this.quoteHolder["IFAST"].gross) : 0,
        }
      };
      return this.projectsService.updateProject(this.data.project.$key, updateEstimates);
    }

    updateEstimates(){
      this.updateProjectEstimates()
      .then((_)=>{
        return this.loadpanelsService.addAreas(this.ready, this.data.project.$key);
      });
      
    }

    getAreaEstimates(){
 
      if(this.quotes.length){
        let createdQuotes = {};
        let groupQuotes = {};
        let response = {
          models:[],
          errors:[]
        };
        this.quotes.forEach(quote => {
          if(this.subTypeObj[quote.sheetName] !== undefined){
            quote.subType = this.subTypeObj[quote.sheetName](quote.type);
            quote.subType2 = this.subTypeObj2(quote.type);
            if(quote.subType !== "No Type"){
              if(groupQuotes[quote.type] === undefined){
                groupQuotes[quote.type] = {};
              }
              if(groupQuotes[quote.type][quote.level] === undefined){
                groupQuotes[quote.type][quote.level] = {};
              }
              if(groupQuotes[quote.type][quote.level][quote.phase] === undefined){
                groupQuotes[quote.type][quote.level][quote.phase] = {
                  type: "",  
                  phase: "",
                  level: "",
                  gross:0
                };
              }
              groupQuotes[quote.type][quote.level][quote.phase].type = quote.type;
              groupQuotes[quote.type][quote.level][quote.phase].phase = quote.phase;
              groupQuotes[quote.type][quote.level][quote.phase].level = quote.level;
              groupQuotes[quote.type][quote.level][quote.phase].gross += quote.areaGross;
              if(this.quoteHolder[quote.subType] === undefined){
                this.quoteHolder[quote.subType] = {
                  gross:0,
                  name: quote.subType,
                  subType: {}
                }
              }
              if(this.quoteHolder[quote.subType].subType[quote.subType2] === undefined){
                this.quoteHolder[quote.subType].subType[quote.subType2] = {
                  gross:0,
                  name: quote.subType2
                }
              }
              this.quoteHolder[quote.subType].gross += quote.areaGross;
              this.quoteHolder[quote.subType].subType[quote.subType2].gross += quote.areaGross;
              if(quote.level === "unleveled" || quote.phase === "unphased"){
                // if(quote.level === "unleveled"){
                //   response.errors.push(quote.phase + "-" + quote.level + "-" + quote.type + " is unleveled!");
                // }
                // if(quote.phase === "unphased"){
                //   response.errors.push(quote.phase + "-" + quote.level + "-" + quote.type + " is unphased!");
                // }        
              }else{              
                let neededData = {
                  phase: quote.phase,
                  floor: quote.level,
                  type: quote.subType,
                  framing: quote.type,
                  estimatedArea: quote.areaGross
                };              
                response.models.push(neededData);
              }
            }
          }
        });

        return this.loadpanelsService.prepQuotesData(this.data.project, response, this.data.areasGrouped).then((model)=>{

          let models: Response = model;

          models.models.sort((areaA, areaB) => {

            var o1 = areaA.status.toLowerCase();
            var o2 = areaB.status.toLowerCase();
          
            var p1 = areaA.phase+"-"+areaA.floor+"-"+areaA.type.toLowerCase();
            var p2 = areaB.phase+"-"+areaB.floor+"-"+areaB.type.toLowerCase();
          
            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
          });

          this.ready = {areasGrouped: this.data.areasGrouped, project: this.data.project, result: models};

          if(this.ready.result.errors.length){
            this.text = "The following errors have been identified: ";
          }else{
            this.dataSource = models.models;
            this.text = "The following components and areas have been identified: ";
          }
          
          this.titlesTable = [
            "Status",
            "Component",
            "Area",
            "Existing Area",
            "Updated Area",
            "Difference"
          ];

          this.statusData;
          this.statusData = {
            loading: false,
            status:"Processing....",
            type: "Delete",
            counter: 0
          }
          
          this.loaded = true;
          this.isQuotes = true;
    
          this.statusSubscription = this.loadpanelsService.getProgress().subscribe((data)=>{
            this.statusData = data;
          });

        });
        
      }else{
        this.loaded = true;
      }
    }

    round2(data){
      return this.dateUtilsService.round2(data);
    }

    getAvg(data) {
      const total = data.reduce((acc, c) => acc + c, 0);
      return total / data.length;
    }

    removeRevit(str){
      if(str.indexOf("Revit ") !== -1){
        return str.replace("Revit ", "");
      }
    }
  
    getObjectKeys(data){
      return Object.keys(data);
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    ngOnDestroy(){
      this._subscription.unsubscribe();
      if(this.statusSubscription){
        this.statusSubscription.unsubscribe();
      }
    }
  

}

export interface Response {
  models:ComponentsTable[];
  errors:Array<any>;
}

export interface ComponentsTable {
  status: string;
  phase: string;
  floor: string;
  type: string;
  framing: object;
  estimatedArea: number;
  component: string;
  project: string;
  deliveryDate: string;
}