import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaEstimatesComponent } from './area-estimates.component';

describe('AreaEstimatesComponent', () => {
  let component: AreaEstimatesComponent;
  let fixture: ComponentFixture<AreaEstimatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaEstimatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaEstimatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
