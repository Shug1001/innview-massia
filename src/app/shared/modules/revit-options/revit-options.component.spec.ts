import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevitOptionsComponent } from './revit-options.component';

describe('RevitOptionsComponent', () => {
  let component: RevitOptionsComponent;
  let fixture: ComponentFixture<RevitOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevitOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevitOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
