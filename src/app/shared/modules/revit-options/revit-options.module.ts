import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RevitOptionsComponent } from './revit-options.component';
import { AreaEstimatesComponent } from './area-estimates/area-estimates.component';
import { MatIconModule, MatTableModule, MatCardModule, MatToolbarModule, MatButtonModule, MatTooltipModule, MatProgressBarModule, MatSpinner, MatGridListModule } from '@angular/material';
import { FileUploaderModule } from '../../modules/file-uploader/file-uploader.module';
import { DataExportModule } from '../../modules/data-export/data-export.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [RevitOptionsComponent, AreaEstimatesComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatCardModule,
    MatToolbarModule,
    MatTableModule,
    MatGridListModule,
    FileUploaderModule,
    DataExportModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [RevitOptionsComponent],
  entryComponents: [AreaEstimatesComponent]
})
export class RevitOptionsModule { }
