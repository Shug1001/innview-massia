import { Component, Input } from '@angular/core';
import { ProjectsService } from '../../services/projects.service';
import { MatDialog } from '@angular/material';
import { AreaEstimatesComponent } from './area-estimates/area-estimates.component';
import { Project } from '../../models/models';

@Component({
  selector: 'app-revit-options',
  templateUrl: './revit-options.component.html',
  styleUrls: ['./revit-options.component.scss']
})
export class RevitOptionsComponent {f

  quoteHolder: {};
  estimates: Array<any>;
  quotesList: Array<any>;
  subTypeObj: {};
  areaEstimates: [];
  toggleEstimates: boolean;
  @Input() project : Project;
  @Input() areasGrouped : object;

  constructor(public dialog: MatDialog, private projectsService: ProjectsService) {
    this.toggleEstimates = true;
  }

  
  areaEstimatesPopup(): void{
    const dialogRef = this.dialog.open(AreaEstimatesComponent, {
      maxWidth: '95vw',
      maxHeight: '95vh',
      height: '95%',
      width: '95%',
      data: {
        project: this.project,
        areasGrouped: this.areasGrouped,
      }
    });
    dialogRef.componentInstance.data = {
      project: this.project,
      areasGrouped: this.areasGrouped,
    };
  }

}
