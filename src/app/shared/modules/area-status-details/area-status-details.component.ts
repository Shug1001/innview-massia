import { Component, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ForwardPlannerService, ProjectsService } from '../../services';
import { Subscription } from 'rxjs';
import { ProjectStats, AreaStats, area } from '../../models/models';
import { MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-area-status-details',
  templateUrl: './area-status-details.component.html',
  styleUrls: ['./area-status-details.component.scss']
})
export class AreaStatusDetailsComponent {

  @Input() projectStats: ProjectStats;
  areasStats: AreaStats;
  _subscription: Subscription;
  loaded: boolean;
  filterValue: string;
  @Output() areaChange = new EventEmitter<object>();
  dataSource: MatTableDataSource<area>;
  disableAzure: boolean;

  constructor(private forwardPlannerService: ForwardPlannerService, private http: HttpClient, private projectsService: ProjectsService) { 
    this.filterValue = "";
    this.dataSource = new MatTableDataSource<area>([]);
    this.loaded = false;
    this.disableAzure = false;
  }

  applyFilter(filterValue: string) {
    this.dataSource.data = this.areasStats.areas;
    filterValue = filterValue.trim().toLowerCase(); // Remove whitespace
    this.filterValue = filterValue;
    this.dataSource.filter = filterValue;
  }

  sendToMainComponent(stats){
    this.areaChange.emit(stats);
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {

    this.loaded = false;

    if(this._subscription){
      this._subscription.unsubscribe();
    }
    this._subscription = this.forwardPlannerService.getAreaStats(this.projectStats.project).subscribe((areasStats) => {
      this.areasStats = areasStats;
      this.dataSource = new MatTableDataSource<area>(this.areasStats.areas);
      this.loaded = true;
    });

  }

  projectPlanLoad(val){
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectPlanLoad',val).toPromise();
  }


  delay(t) {
    return new Promise(function(resolve) { 
        setTimeout(resolve.bind(null), t)
    });
 }

  updateAzure(){

    this.disableAzure = true;

    let promises = [];

    this.areasStats.exportArea.forEach((stat) => {
      promises.push(this.projectsService.updateProjectPlanLoad(stat));
    });

    return Promise.all(promises)
    .then(() => {
      this.disableAzure = false;
    });
    
  }

}
