import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaStatusDetailsComponent } from './area-status-details.component';
import { MatGridListModule, MatToolbarModule, MatSortModule, MatTableModule, MatFormFieldModule, MatPaginatorModule, MatInputModule, MatIconModule, MatProgressBarModule } from '@angular/material';
import { PipesModule } from '../../pipes/pipes.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DataExportModule } from '../data-export/data-export.module';

@NgModule({
  declarations: [AreaStatusDetailsComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    PipesModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    DataExportModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [
    AreaStatusDetailsComponent
  ]
})
export class AreaStatusDetailsModule { }
