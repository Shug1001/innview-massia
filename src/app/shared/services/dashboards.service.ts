import { Injectable } from '@angular/core';
import { ForwardPlannerService } from './forward-planner.service';
import { switchMap, first, map, combineLatest} from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { DateUtilService } from './date-util.service';
import { PanelsService } from '../services/panels.service';
import { ComponentsService } from './components.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardsService {

  constructor(private componentsService: ComponentsService, private panelsService: PanelsService, private db: AngularFireDatabase, private forwardPlannerService: ForwardPlannerService, private dateUtilsService: DateUtilService) { }

  createBooking(obj){
    return this.db.list('newBooking').push(obj);
  }

  getBookingsPromise(selectDate){

    const dayzGone = 21;
    let monday = this.dateUtilsService.weekStart(selectDate);

    let firstMonday = this.dateUtilsService.minusDays(monday, dayzGone);
    let lastMonday = this.dateUtilsService.plusDays(monday, dayzGone);
    
    let lastSun = this.dateUtilsService.plusDays(lastMonday, 6);
    
    let start = new Date(firstMonday).setHours(0,0,0,0);
    let end = new Date(lastSun).setHours(23,59,59,999);

    const path = `${'newBooking'}`;
    return this.db.list(path, capRef => capRef.orderByChild("bookedTime").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  deleteBooking(key: string) {
    return this.db.list('newBooking').remove(key);
  }

  getBookings(selectDate){

    const dayzGone = 21;
    let monday = this.dateUtilsService.weekStart(selectDate);

    let firstMonday = this.dateUtilsService.minusDays(monday, dayzGone);
    let lastMonday = this.dateUtilsService.plusDays(monday, dayzGone);
    
    let lastSun = this.dateUtilsService.plusDays(lastMonday, 6);
    
    let start = new Date(firstMonday).setHours(0,0,0,0);
    let end = new Date(lastSun).setHours(23,59,59,999);

    const path = `${'newBooking'}`;
    return this.db.list(path, capRef => capRef.orderByChild("bookedTime").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getAllBookings(){
    const path = `${'newBooking'}`;
    return this.db.list(path, capRef => capRef.orderByChild("bookedTime")).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  areasForWeek(selectDate, weeks, daysOfWeeks, buildAreas){

    const weekStartReturn = this.dateUtilsService.weekStart(selectDate);

    let areasByDesignHandoverDate = {};
    
    let chartData = {
      "predicted": {},
      "actual": {},
      "designed": {}
    };
    
    Object.keys(buildAreas).forEach(( key ) => {
      const area = buildAreas[key];
      let panelWeek = this.dateUtilsService.weekStart(area._designHandoverDate);
      if (areasByDesignHandoverDate[panelWeek] === undefined) {
        areasByDesignHandoverDate[panelWeek] = [];
      }
      if (areasByDesignHandoverDate[panelWeek][area._designHandoverDate] === undefined) {
        areasByDesignHandoverDate[panelWeek][area._designHandoverDate] = [];
      }
      areasByDesignHandoverDate[panelWeek][area._designHandoverDate].push(area);
    });
    
    Object.keys(weeks.weekHolder).forEach(( week ) => {
      let data = weeks.weekHolder[week];
      let predictedPlanM2 = 0;
      let actualPlanM2 = 0;
      let areaOfPanels = 0;

      if (data.areaOfPanels !== undefined){
        areaOfPanels = data.areaOfPanels;
      }

      if (data.predictedPlanM2 !== undefined){
        predictedPlanM2 = data.predictedPlanM2;
      }
      if (data.actualPlanM2 !== undefined){
        actualPlanM2 = data.actualPlanM2;
      }
  

      if(chartData["designed"][week] === undefined){
        chartData["designed"][week] = 0;
      }
      chartData["designed"][week] = areaOfPanels;

      if(chartData["actual"][week] === undefined){
        chartData["actual"][week] = 0;
      }
      chartData["actual"][week] = actualPlanM2;

      if(chartData["predicted"][week] === undefined){
        chartData["predicted"][week] = 0;
      }
      chartData["predicted"][week] = predictedPlanM2;

    });

    let chartColours = {
      "designed": "#ff0000",
      "actual": "#0000ff",
      "predicted": "#80ff00"
    }
    
    let returnData = {
      weekStartReturn: weekStartReturn,
      chartColours: chartColours,
      daysOfWeeks: daysOfWeeks,
      chartData: chartData,
      weeks: weeks.week,
      areasByDesignHandoverDate: areasByDesignHandoverDate
    };
    
    return returnData;
    
  }
  
  updateBookingCheck(bookingRef, type, updateObj){
    const itemRef = this.db.object(`${"newBooking"}/${bookingRef}/${type}`)
    return itemRef.update(updateObj);
  }

  updateBooking(bookingRef, updateObj){
    const itemRef = this.db.object(`${"newBooking"}/${bookingRef}`)
    return itemRef.update(updateObj);
  }



  areasForWeekBooking(selectDate, weeks, daysOfWeeks, buildAreas, bookings){


    const weekStartReturn = this.dateUtilsService.weekStart(selectDate);

    let areasByDesignHandoverDate = {};
    
    let chartData = {
      "dispatches": {},
      "booked": {}
    };
    
    bookings.forEach(( booking ) => {
      let bookedTime = this.dateUtilsService.toIsoDate(booking.bookedTime);
      let panelWeek = this.dateUtilsService.weekStart(bookedTime);
      if (areasByDesignHandoverDate[panelWeek] === undefined) {
        areasByDesignHandoverDate[panelWeek] = [];
      }
      if (areasByDesignHandoverDate[panelWeek][bookedTime] === undefined) {
        areasByDesignHandoverDate[panelWeek][bookedTime] = [];
      }
      areasByDesignHandoverDate[panelWeek][bookedTime].push(booking);
    });
    
    Object.keys(weeks.weekHolder).forEach(( week ) => {
      let data = weeks.weekHolder[week];
      let actualPlanM2 = 0;
      let booking = 0;

      if (data.actualPlanM2 !== undefined){
        actualPlanM2 = data.actualPlanM2;
      }

      if(chartData["dispatches"][week] === undefined){
        chartData["dispatches"][week] = 0;
      }
      chartData["dispatches"][week] = actualPlanM2;

      if (data.bookingM2 !== undefined){
        booking = data.bookingM2;
      }

      if(chartData["booked"][week] === undefined){
        chartData["booked"][week] = 0;
      }
      chartData["booked"][week] = booking;


    });

    let chartColours = {
      booked: "#0000FF",
      dispatches: "#FF0000"
    };
    
    let returnData = {
      weekStartReturn: weekStartReturn,
      chartColours: chartColours,
      daysOfWeeks: daysOfWeeks,
      chartData: chartData,
      weeks: weeks.week,
      areasByDesignHandoverDate: areasByDesignHandoverDate
    };
    
    return returnData;

  
  }

  bufferDFM(panels){
    let designIssued = {
      "totalCompleteCount": 0,
      "totalDesignedCount": 0,
      "totalCompleteM2": 0,
      "totalDesignedM2": 0
    };
    panels.forEach((panel) => {
      if (panel.qa !== undefined && panel.qa.completed !== undefined) {
        designIssued.totalCompleteM2 += panel.dimensions.area || 0;
        designIssued.totalCompleteCount ++;
      }
      designIssued.totalDesignedM2 += panel.dimensions.area || 0;
      designIssued.totalDesignedCount ++;
    });
      
    return designIssued;
  }

  getWeekDaysBooking(selectDate){
    let week = {};
    let monday = this.dateUtilsService.weekStart(selectDate);
    for(let i = 0; i < 5; i++){
      let day = this.dateUtilsService.plusDays(monday, i);
      if(week[day] === undefined){
        week[day] = {
          deliverys:0,
          booked:0,
          unplanned:0
        };
      }
    }
    return week;
  }

  getWeekDaysLogistics(selectDate){
    let week = {};
    let monday = this.dateUtilsService.weekStart(selectDate);
    for(let i = 0; i < 5; i++){
      let day = this.dateUtilsService.plusDays(monday, i);
      if(week[day] === undefined){
        week[day] = {
          deliverys:0,
          booked:0,
          unplanned:0
        };
      }
    }
    return week;
  }
  
  getWeeksBooking(selectDate, daysOfWeeks, buildAreas, weeksAmount, dayzGone, bookings) {

    let weekHolder = {};
    let week = {};
    let monday = this.dateUtilsService.weekStart(selectDate);
    let firstMonday = this.dateUtilsService.minusDays(monday, dayzGone);
    for (let j = 0; j <= weeksAmount; j++) {
      for (let i = 0; i < 5; i++) {
        let buildDate = this.dateUtilsService.plusDays(firstMonday, i);
        if (week[buildDate] === undefined) {
          week[buildDate] = {
            areaOfPanels: 0,
            numberOfPanels: 0,
            predictedPlanM2: 0,
            predictedPlanPanels: 0,
            actualPlanM2: 0,
            actualPlanPanels: 0,
            bookingM2:0
          };
        }
        if (weekHolder[firstMonday] === undefined) {
          weekHolder[firstMonday] = {
            areaOfPanels: 0,
            numberOfPanels: 0,
            predictedPlanM2: 0,
            predictedPlanPanels: 0,
            actualPlanM2: 0,
            actualPlanPanels: 0,
            bookingM2: 0
          };
        }
        bookings.forEach(( booking: any ) => {
          if(buildDate === this.dateUtilsService.toIsoDate(booking.bookedTime)){
              week[buildDate].bookingM2 += booking.m2;
          }
        });
        buildAreas.forEach(( area ) => {
          if(buildDate === area._dispatchDate){
            
            if (area.actualArea > 0 && area.actualPanels > 0) {
              week[buildDate].actualPlanM2 += area.actualArea;
              week[buildDate].actualPlanPanels += area.actualPanels;
            }else{
              if (area.areaOfPanels > 0 && area.numberOfPanels > 0) {
                week[buildDate].areaOfPanels += area.areaOfPanels;
                week[buildDate].numberOfPanels += area.numberOfPanels;
              } else {
                if (area.estimatedArea > 0 && area.estimatedPanels > 0) {
                  week[buildDate].predictedPlanM2 += area.estimatedArea;
                  week[buildDate].predictedPlanPanels += area.estimatedPanels;
                }
              }
    
            }

          }
        });
      }
      firstMonday = this.dateUtilsService.plusDays(firstMonday, 7);
    }

    Object.keys(week).forEach(( buildDate ) => {
      const dayData = week[buildDate];
      const monday = this.dateUtilsService.weekStart(buildDate);
      weekHolder[monday].numberOfPanels += dayData.numberOfPanels;
      weekHolder[monday].areaOfPanels += dayData.areaOfPanels;
      weekHolder[monday].predictedPlanM2 += dayData.predictedPlanM2;
      weekHolder[monday].predictedPlanPanels += dayData.predictedPlanPanels;
      weekHolder[monday].actualPlanM2 += dayData.actualPlanM2;
      weekHolder[monday].actualPlanPanels += dayData.actualPlanPanels;
      weekHolder[monday].bookingM2 += dayData.bookingM2;
    });

    Object.keys(daysOfWeeks).forEach(( buildDate ) => {
      daysOfWeeks[buildDate].deliverys = week[buildDate].actualPlanM2;
      daysOfWeeks[buildDate].booked = week[buildDate].bookingM2;
    });
    
    let returnData = {
      weekHolder: weekHolder,
      week: week
    };
    
    return this.areasForWeekBooking(selectDate, returnData, daysOfWeeks, buildAreas, bookings);

  }

  areasForWeekLogistics(selectDate, weeks, daysOfWeeks, buildAreas){


    const weekStartReturn = this.dateUtilsService.weekStart(selectDate);

    let areasByDesignHandoverDate = {};
    
    let chartData = {
      "dispatches": {},
      "booked": {}
    };
    
    Object.keys(buildAreas).forEach(( key ) => {
      const area = buildAreas[key];
      let expectedDispatch;
      var areaRiskDays = area.plan && area.plan.plannedFinish ? this.dateUtilsService.daysBetweenWeekDaysOnly(area.plan.plannedFinish, area._deliveryDate) : 0;
      if(areaRiskDays >= 0){
        expectedDispatch = area._dispatchDate;
      }else{
        expectedDispatch = area.plan && area.plan.plannedFinish ? this.dateUtilsService.addWorkingDays(area.plan.plannedFinish, 1) : "";
      }   
      let panelWeek = this.dateUtilsService.weekStart(expectedDispatch);
      if (areasByDesignHandoverDate[panelWeek] === undefined) {
        areasByDesignHandoverDate[panelWeek] = [];
      }
      if (areasByDesignHandoverDate[panelWeek][expectedDispatch] === undefined) {
        areasByDesignHandoverDate[panelWeek][expectedDispatch] = [];
      }
      areasByDesignHandoverDate[panelWeek][expectedDispatch].push(area);
    });
    
    Object.keys(weeks.weekHolder).forEach(( week ) => {
      let data = weeks.weekHolder[week];
      let actualPlanM2 = 0;
      let booking = 0;

      if (data.actualPlanM2 !== undefined){
        actualPlanM2 = data.actualPlanM2;
      }

      if(chartData["dispatches"][week] === undefined){
        chartData["dispatches"][week] = 0;
      }
      chartData["dispatches"][week] = actualPlanM2;

      if (data.bookingM2 !== undefined){
        booking = data.bookingM2;
      }

      if(chartData["booked"][week] === undefined){
        chartData["booked"][week] = 0;
      }
      chartData["booked"][week] = booking;


    });

    let chartColours = {
      booked: "#0000FF",
      dispatches: "#FF0000"
    };
    
    let returnData = {
      weekStartReturn: weekStartReturn,
      chartColours: chartColours,
      daysOfWeeks: daysOfWeeks,
      chartData: chartData,
      weeks: weeks.week,
      areasByDesignHandoverDate: areasByDesignHandoverDate
    };
    
    return returnData;

  
  }

  getWeeksLogistics(selectDate, daysOfWeeks, buildAreas, weeksAmount, dayzGone, bookings) {

    let weekHolder = {};
    let week = {};
    let monday = this.dateUtilsService.weekStart(selectDate);
    let firstMonday = this.dateUtilsService.minusDays(monday, dayzGone);
    for (let j = 0; j <= weeksAmount; j++) {
      for (let i = 0; i < 6; i++) {
        let buildDate = this.dateUtilsService.plusDays(firstMonday, i);
        if (week[buildDate] === undefined) {
          week[buildDate] = {
            areaOfPanels: 0,
            numberOfPanels: 0,
            predictedPlanM2: 0,
            predictedPlanPanels: 0,
            actualPlanM2: 0,
            actualPlanPanels: 0,
            bookingM2:0
          };
        }
        if (weekHolder[firstMonday] === undefined) {
          weekHolder[firstMonday] = {
            areaOfPanels: 0,
            numberOfPanels: 0,
            predictedPlanM2: 0,
            predictedPlanPanels: 0,
            actualPlanM2: 0,
            actualPlanPanels: 0,
            bookingM2: 0
          };
        }
        bookings.forEach(( booking: any ) => {
          if(buildDate === this.dateUtilsService.toIsoDate(booking.bookedTime)){
              week[buildDate].bookingM2 += booking.m2;
          }
        });
        buildAreas.forEach(( area ) => {
          let expectedDispatch;
          var areaRiskDays = area.plan && area.plan.plannedFinish ? this.dateUtilsService.daysBetweenWeekDaysOnly(area.plan.plannedFinish, area._deliveryDate) : 0;
          if(areaRiskDays >= 0){
            expectedDispatch = area._dispatchDate;
          }else{
            expectedDispatch = area.plan && area.plan.plannedFinish ? this.dateUtilsService.addWorkingDays(area.plan.plannedFinish,1) : "";
          }  
          if(buildDate === expectedDispatch){
            
            if (area.actualArea > 0 && area.actualPanels > 0) {
              week[buildDate].actualPlanM2 += area.actualArea;
              week[buildDate].actualPlanPanels += area.actualPanels;
            }else{
              if (area.areaOfPanels > 0 && area.numberOfPanels > 0) {
                week[buildDate].areaOfPanels += area.areaOfPanels;
                week[buildDate].numberOfPanels += area.numberOfPanels;
              } else {
                if (area.estimatedArea > 0 && area.estimatedPanels > 0) {
                  week[buildDate].predictedPlanM2 += area.estimatedArea;
                  week[buildDate].predictedPlanPanels += area.estimatedPanels;
                }
              }
    
            }

          }
        });
      }
      firstMonday = this.dateUtilsService.plusDays(firstMonday, 7);
    }

    Object.keys(week).forEach(( buildDate ) => {
      const dayData = week[buildDate];
      const monday = this.dateUtilsService.weekStart(buildDate);
      weekHolder[monday].numberOfPanels += dayData.numberOfPanels;
      weekHolder[monday].areaOfPanels += dayData.areaOfPanels;
      weekHolder[monday].predictedPlanM2 += dayData.predictedPlanM2;
      weekHolder[monday].predictedPlanPanels += dayData.predictedPlanPanels;
      weekHolder[monday].actualPlanM2 += dayData.actualPlanM2;
      weekHolder[monday].actualPlanPanels += dayData.actualPlanPanels;
      weekHolder[monday].bookingM2 += dayData.bookingM2;
    });
    
    let returnData = {
      weekHolder: weekHolder,
      week: week
    };
    
    return this.areasForWeekLogistics(selectDate, returnData, daysOfWeeks, buildAreas);


  }
  

  getWeekDays(selectDate){
    let weekStart = this.dateUtilsService.weekStart(selectDate);
    let weekPush = [];
    for(let i = 0; i < 5; i++){
      let day = this.dateUtilsService.plusDays(weekStart, i);
      if(!weekPush.includes(day)){
        weekPush.push(day);
      }
    }
    let keys = Object.keys(weekPush);
    let week = [];
    for (let prop of keys) { 
      week.push(weekPush[prop]); 
    }

    return week;
  }

  getWeeks(selectDate, buildAreas, weeksAmount, dayzGone) {
    let weekHolder = {};
    let week = {};
    let monday = this.dateUtilsService.weekStart(selectDate);
    let firstMonday = this.dateUtilsService.minusDays(monday, dayzGone);
    for (let j = 0; j <= weeksAmount; j++) {
      for (let i = 0; i < 6; i++) {
        let buildDate = this.dateUtilsService.plusDays(firstMonday, i);
        if (week[buildDate] === undefined) {
          week[buildDate] = {
            areaOfPanels: 0,
            numberOfPanels: 0,
            predictedPlanM2: 0,
            predictedPlanPanels: 0,
            actualPlanM2: 0,
            actualPlanPanels: 0
          };
        }
        if (weekHolder[firstMonday] === undefined) {
          weekHolder[firstMonday] = {
            areaOfPanels: 0,
            numberOfPanels: 0,
            predictedPlanM2: 0,
            predictedPlanPanels: 0,
            actualPlanM2: 0,
            actualPlanPanels: 0
          };
        }
        buildAreas.forEach(( area ) => {
          if(buildDate === area._designHandoverDate){
            
            if (area.actualArea > 0 && area.actualPanels > 0) {
              week[buildDate].actualPlanM2 += area.actualArea;
              week[buildDate].actualPlanPanels += area.actualPanels;
            }else{
              if (area.areaOfPanels > 0 && area.numberOfPanels > 0) {
                week[buildDate].areaOfPanels += area.areaOfPanels;
                week[buildDate].numberOfPanels += area.numberOfPanels;
              } else {
                if (area.estimatedArea > 0 && area.estimatedPanels > 0) {
                  week[buildDate].predictedPlanM2 += area.estimatedArea;
                  week[buildDate].predictedPlanPanels += area.estimatedPanels;
                }
              }
    
            }

          }
        });
      }
      firstMonday = this.dateUtilsService.plusDays(firstMonday, 7);
    }

    Object.keys(week).forEach(( buildDate ) => {
      const dayData = week[buildDate];
      const monday = this.dateUtilsService.weekStart(buildDate);
      weekHolder[monday].numberOfPanels += dayData.numberOfPanels;
      weekHolder[monday].areaOfPanels += dayData.areaOfPanels;
      weekHolder[monday].predictedPlanM2 += dayData.predictedPlanM2;
      weekHolder[monday].predictedPlanPanels += dayData.predictedPlanPanels;
      weekHolder[monday].actualPlanM2 += dayData.actualPlanM2;
      weekHolder[monday].actualPlanPanels += dayData.actualPlanPanels;
    });
    
    let returnData = {
      weekHolder: weekHolder,
      week: week
    };
    
    return returnData;
  }

    // getLogisticsDataNotUsed(selectDate, weeks, daysOfWeeks, thisAreas){

  //   let buildAreas = {};

  //   thisAreas.forEach(( area ) => {
  //     if(buildAreas[area.$key] === undefined){
  //       buildAreas[area.$key] = area;
  //     }
  //   });

  //   const dayzGone = 21;
  //   let monday = this.dateUtilsService.weekStart(selectDate);
  //   let sunday = this.dateUtilsService.plusDays(monday, 6);

  //   let firstMonday = this.dateUtilsService.minusDays(monday, dayzGone);
  //   let lastMonday = this.dateUtilsService.plusDays(monday, dayzGone);
    
  //   let lastSun = this.dateUtilsService.plusDays(lastMonday, 6);
    
  //   let start = new Date(firstMonday).setHours(0,0,0,0);
  //   let end = new Date(lastSun).setHours(23,59,59,999);

  //   let bookingsArr = [];
  //   let groupByBookingDate = {};
  //   let panelsForBookings = {};
  //   let bookingAreaMap = {};

  //   return this.getBookings(start, end)
  //     .then((bookings) => {
  //       bookingsArr = bookings;    
  //       return getPanelsForBookings(this);
  //     })
  //     .then(() => {
  //       return processBookings(this);
  //     })
  //     .then(()=>{
  //       return this.componentsService.getcomponentsListPromise();
  //     })
  //     .then((components)=>{
  //       return areasForWeek(this, components);
  //     })
  //     .then(function ( data ) {
  //       return Promise.resolve(data);
  //     });


  //   function getPanelsForBookings(that){
  //     panelsForBookings = {};
  //     var deferredList = [];
  //     bookingsArr.forEach(( booking ) => {
  //       var isoMonday = that.dateUtilsService.weekStart(booking.bookedTime);
  //       var date = that.dateUtilsService.toIsoDate(booking.bookedTime);
  //       if (daysOfWeeks[date] !== undefined) {
  //         daysOfWeeks[date].booked++;
  //       }
  //       if (weeks[isoMonday] !== undefined) {
  //         weeks[isoMonday].booked++;
  //       }
  //       Object.keys(booking.areas).forEach(( areaRef ) => {
  //         if (bookingAreaMap[areaRef] === undefined) {
  //           bookingAreaMap[areaRef] = [];
  //         }
  //         bookingAreaMap[areaRef].push(booking);
  //         if (date >= monday && date <= sunday){
  //           deferredList.push(that.panelsService.getPanelsForAreaPromise(areaRef).then(panels => {
  //             panelsForBookings[areaRef] = panels;
  //           }));
  //         }
  //       });
  //     });
  //     return Promise.all(deferredList);
  //   }
    
  //   function processBookings(that){
  //     groupByBookingDate = {};

  //     bookingsArr.forEach(( booking, key ) => {
  //       var date = that.dateUtilsService.toIsoDate(booking.bookedTime);
  //       if (date >= monday && date <= sunday){
  //         if(groupByBookingDate[booking.bookedTime] === undefined){
  //           groupByBookingDate[booking.bookedTime] = {};
  //         }
  //         if(groupByBookingDate[booking.bookedTime][booking.$key] === undefined){
  //           groupByBookingDate[booking.bookedTime][booking.$key] = {
  //             booking: booking,
  //             areas: {}
  //           };
  //         }
  //         Object.keys(booking.areas).forEach(( areaRef ) => {
  //           if (groupByBookingDate[booking.bookedTime][booking.$key].areas[areaRef] === undefined) {
  //             var panelCounter = 0;

  //             buildAreas[areaRef]._bookings = [];
                
  //             if (bookingAreaMap[buildAreas[areaRef].$key] !== undefined){
  //               var bookings = bookingAreaMap[buildAreas[areaRef].$key];
  //               bookings.sort(function (a, b) {
  //                 return Date.parse(a.bookingTime) - Date.parse(b.bookingTime);
  //               });
  //               buildAreas[areaRef]._bookings = bookings;
  //             }
              
  //             var area = buildAreas[areaRef];
  //             if (area !== null) {
  //               var project = area._projectDetails;
  //               panelsForBookings[areaRef].forEach(( panel ) => {
  //                 if (panel.booking !== undefined && panel.booking === booking.$key) {
  //                   panelCounter++;
  //                 }
  //               });
  //               groupByBookingDate[booking.bookedTime][booking.$key].areas[areaRef] = {
  //                 "area": area,
  //                 "bookedPanels": panelCounter
  //               };
  //             }
  //           }
  //         });
  //       }
  //     });   
 
  //   }

  //   function createAreasMap( components ) {
  //     var areasMap = {};
  //     var areasMapCounter = {};

  //     components.forEach(( component ) => {
  //       if (areasMapCounter[component.type] === undefined) {
  //         areasMapCounter[component.type] = 0;
  //       }
  //       areasMapCounter[component.type]++;
  //       Object.keys(component.areas).forEach(( key ) => {
  //         let area = component.areas[key];
  //         var componentName = component.type + areasMapCounter[component.type];
  //         areasMap[area] = {
  //           id: component.$key,
  //           name: componentName,
  //           comments: component.comments
  //         };
  //       });
  //     });

  //     return areasMap;
  //   }

  //   function areasForWeek(that, components) {

  //     var chartData = {
  //       "booked": [],
  //       "deliverys": []
  //     };

  //     var componentsHolder = {};
  //     var riskDays = undefined;
      
  //     var componentsMap = createAreasMap(components);
  //     Object.keys(buildAreas).forEach(( key ) => {
  //       let area = buildAreas[key];
  //       var date = that.dateUtilsService.weekStart(area._dispatchDate);
  //       if (weeks[date] !== undefined){
  //         var project = area._projectDetails;

  //         var componentKey = componentsMap[area.$id];
  //         if (componentKey === undefined) {
  //           var componentKey = {};
  //           componentKey.name = "NONE";
  //           componentKey.id = 0;
  //           componentKey.comments = 0;
  //         }
          
  //         var projectManagerName = "";
  //         var projectManagerContact = "";
          
  //         if (project.deliveryManager !== undefined && project.deliveryManager.name !== undefined) {
  //           projectManagerName = project.deliveryManager.name;
  //         }
  //         if (project.deliveryManager !== undefined && project.deliveryManager.mobile  !== undefined) {
  //           projectManagerContact = project.deliveryManager.mobile;
  //         }

  //         var component = componentsHolder[componentKey.name];
  //         if (component === undefined) {
  //           component = componentsHolder[componentKey.name] = {
  //             componentId: componentKey.id,
  //             comments: componentKey.comments,
  //             projectManagerName: projectManagerName,
  //             projectManagerContact: projectManagerContact,
  //             siteAccess: project.siteAccess,
  //             siteAddress: project.siteAddress,
  //             postCode: project.postCode,
  //             estimatedAreaTotal: 0,
  //             estimatedCountTotal: 0,
  //             actualAreaTotal: 0,
  //             actualCountTotal: 0,
  //             completedAreaTotal: 0,
  //             riskDays: 0,
  //             completedCountTotal: 0,
  //             riskDaysArray: [],
  //             areas: [],
  //             areasDispatchDate: [],
  //             areasDeliveryDate: [],
  //             areasLatestRevision: [],
  //             areasDelivered: [],
  //             delivered: true,
  //             areasActive: [],
  //             active: true,
  //             same: false,
  //             isSame: [],
  //             plannedFinish: new Date("1970-12-30"),
  //             deliveryDate: new Date("2100-12-30"),
  //             dispatchDate: new Date("2100-12-30"),
  //             latestRevision: new Date("2100-12-30")
  //           };
  //         }
          
  //         component.estimatedAreaTotal += getOptionalNumber(area.estimatedArea);
  //         component.estimatedCountTotal += getOptionalNumber(area.estimatedPanels);
  //         component.actualAreaTotal += getOptionalNumber(area.actualArea);
  //         var areaActualCount = getOptionalNumber(area.actualPanels);
  //         component.actualCountTotal += areaActualCount;
  //         component.completedAreaTotal += getOptionalNumber(area.completedArea);
  //         component.completedCountTotal += getOptionalNumber(area.completedPanels);

  //         if (area.plan && area.plan.plannedFinish && area.plan.riskDate) {
  //           var areaRiskDays = that.dateUtilsService.daysBetweenWeekDaysOnly(area.plan.plannedFinish, area.plan.riskDate);
  //           component.riskDaysArray.push(areaRiskDays);
  //           var plannedFinish = new Date(area.plan.plannedFinish);
  //           if (plannedFinish > component.plannedFinish) {
  //             component.plannedFinish = plannedFinish;
  //           }
  //         }
          
  //         if(component.riskDaysArray.length > 0){
  //           component.riskDays = lowestRisk(component.riskDaysArray);
  //         }

  //         var areaDate = new Date(area._dispatchDate);
  //         var areaDateDelivery = new Date(area._deliveryDate);
  //         var revision = new Date(area.revisions[project.deliverySchedule.unpublished]);

  //         if (project.deliverySchedule.published !== undefined) {
  //           revision = new Date(area.revisions[project.deliverySchedule.published]);
  //         }

  //         var isSame = component.isSame[area.$id];
  //         var dispatchDate = component.areasDispatchDate[area.$id];
  //         var deliveryDate = component.areasDeliveryDate[area.$id];
  //         var latestRevision = component.areasLatestRevision[area.$id];
  //         var areaDelivered = component.areasDelivered[area.$id];
  //         var areaActive = component.areasActive[area.$id];

  //         if (deliveryDate === undefined) {
  //           deliveryDate = component.areasDeliveryDate[area.$id] = areaDateDelivery;
  //         }
          
  //         if (dispatchDate === undefined) {
  //           dispatchDate = component.areasDispatchDate[area.$id] = areaDate;
  //         }

  //         if (latestRevision === undefined) {
  //           latestRevision = component.areasLatestRevision[area.$id] = revision;
  //         }

  //         if (isSame === undefined) {
  //           if (that.dateUtilsService.toIsoDate(deliveryDate) === that.dateUtilsService.toIsoDate(latestRevision)) {
  //             isSame = component.isSame[area.$id] = true;
  //           } else {
  //             isSame = component.isSame[area.$id] = false;
  //           }
  //         }

  //         if (areaDelivered === undefined) {
  //           if (area.delivered !== undefined && area.delivered) {
  //             areaDelivered = component.areasDelivered[area.$id] = true;
  //           } else {
  //             areaDelivered = component.areasDelivered[area.$id] = false;
  //           }
  //         }

  //         if (areaActive === undefined) {
  //           if (area.active) {
  //             areaActive = component.areasActive[area.$id] = true;
  //           } else {
  //             areaActive = component.areasActive[area.$id] = false;
  //           }
  //         }

  //         if (!areaDelivered) {
  //           component.delivered = false;
  //         }

  //         if (!areaActive) {
  //           component.active = false;
  //         }

  //         component.areas.push(area);
  //         if (areaDate < component.dispatchDate) {
  //           component.dispatchDate = areaDate;
  //           var isoDispatchDate = that.dateUtilsService.toIsoDate(areaDate);
  //           var isoMondayDispatchDate = that.dateUtilsService.weekStart(areaDate);
  //           if (daysOfWeeks[isoDispatchDate] !== undefined) {
  //             daysOfWeeks[isoDispatchDate].deliverys++;
  //           }
  //           if (weeks[isoMondayDispatchDate] !== undefined) {
  //             weeks[isoMondayDispatchDate].deliverys++;
  //           }
  //         }
  //         if (areaDateDelivery < component.deliveryDate) {
  //           component.deliveryDate = areaDateDelivery;
  //         }
  //         if (latestRevision < component.latestRevision) {
  //           component.latestRevision = latestRevision;
  //         }
  //         if (that.dateUtilsService.toIsoDate(component.deliveryDate) === that.dateUtilsService.toIsoDate(component.latestRevision)) {
  //           component.same = true;
  //         }
  //       }

  //     });
      
  //     var componentsByDay = {};
  //     Object.keys(componentsHolder).forEach(( componentName ) => {
  //       let component = componentsHolder[componentName];
  //       var dispatchDate = that.dateUtilsService.toIsoDate(component.dispatchDate);
  //       if (dispatchDate >= monday && dispatchDate <= sunday){
  //         if(componentsByDay[dispatchDate] === undefined){
  //           componentsByDay[dispatchDate] = {};
  //         }
  //         if(componentsByDay[dispatchDate][componentName] === undefined){
  //           componentsByDay[dispatchDate][componentName] = {};
  //         }
  //         componentsByDay[dispatchDate][componentName] = component;
  //       }
  //     });
  //     Object.keys(weeks).forEach(( week ) => {
  //       let data = weeks[week];
  //       chartData.booked.push({x: week, y: data.booked});
  //       chartData.deliverys.push({x: week, y: data.deliverys});
  //     });

  //     var returnData = {
  //       groupByBookingDate: groupByBookingDate,
  //       chartData: chartData,
  //       daysOfWeeks: daysOfWeeks,
  //       areasByDispatch: componentsByDay
  //     };

  //     return returnData;

  //   }

  //   function getOptionalNumber( value ) {
  //     if (value !== undefined) {
  //       return parseFloat(value);
  //     }
  //     return 0;
  //   }
    
  //   function lowestRisk( entryArr ) {
  //     var lowest = 0;
  //     entryArr.forEach(( element ) => {
  //       if(element < lowest){
  //         lowest = element;
  //       }
  //     });
  //     return lowest;
  //   }

  // }

}
