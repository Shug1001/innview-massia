import * as FileSaver from 'file-saver';
import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { AreasService } from './areas.service';
import { DateUtilService } from './date-util.service';
import { ForwardPlannerService } from './forward-planner.service';
import { PanelsService } from './panels.service';
import { ProjectsService } from './projects.service';
import { ReferenceDataService } from './reference-data.service';
import { P } from '@angular/cdk/keycodes';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})

export class ExportService {

constructor(
  private projectsService: ProjectsService,
  private areasService: AreasService,
  private panelsService: PanelsService,
  private referenceDataService: ReferenceDataService,
  private dateUtilsService: DateUtilService,
  private plannerService: ForwardPlannerService
) { }

  exportAsExcelFile(json: any[], excelFileName: string): void {  
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);  
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };  
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });  
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});   
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  getData(fromDate, toDate) {

    if(this.dateUtilsService.isDateFormat(fromDate)){
      fromDate = this.dateUtilsService.toIsoDate(fromDate);
    }else{
      fromDate = this.dateUtilsService.toIsoDate(fromDate._d);
    }

    if(this.dateUtilsService.isDateFormat(toDate)){
      toDate = this.dateUtilsService.toIsoDate(toDate);
    }else{
      toDate = this.dateUtilsService.toIsoDate(toDate._d);
    }

    let matchingAreas = {};
    let matchingAdditionalAreas = {};
    let matchingPanels = [];
    let areaUnpublishedDate = [];
    let operativeNames = [];
    let productTypeProductionLine = {};
    let projects = [];
    let productionPlans = {};

    return this.projectsService.getProjectsListPromise()
      .then((projectsList) => {
        projects = projectsList;
        return processProjects(this, projects);
      })
      .then(() => {
        return populateOperativeNames(this);
      })
      .then(() => {
        return getProductTypes(this);
      })
      .then(() => {
        return getProPlan(this);
      })
      .then(() => {
        return getAdditionAreaData(this);
      })
      .then(() => {
        return aggregatePanels(this);
      })
      .then(() => {
        return processPanels(this);
      });

    function processProjects(that, projects) {
      let promises = [];
      projects.forEach(project => {   
        if (project.deliverySchedule.published !== undefined) {
          // get areas with published delivery date within specified range
          promises.push(findAreasWithMatchingDeliveryDate(that, project));
        }
      });
      return Promise.all(promises);
    }

    function findAreasWithMatchingDeliveryDate(that, project) {
      return that.areasService.getAreasForProjectPromise(project.$key)
        .then((projectAreas) => {
          projectAreas.forEach(area => {
            if (area.revisions !== undefined && area.revisions[project.deliverySchedule.published] !== undefined) {
              let revisionDate = area.revisions[project.deliverySchedule.published];
              if (revisionDate >= fromDate && revisionDate <= toDate) {
                matchingAreas[area.$key] = area;
                areaUnpublishedDate[area.$key] = project.deliverySchedule.published;
              }
            }
          });
        });
    }

    function getProPlan(that){
      return that.plannerService.getAreaProductionPlansPromise()
      .then((allPlans) => {
        allPlans.forEach(plan => {
          productionPlans[plan.$key] = plan;
        });
      });
    }
    
    function getProductTypes(that){
      return that.referenceDataService.getProductTypesPromise()
        .then((productTypes) => {
          productTypeProductionLine = that.buildMap(productTypes, "productionLine");
        });
    }

    function populateOperativeNames(that){
      return that.referenceDataService.getOperatives()
        .then((operatives) => {
          operatives.forEach(operative => {
            operativeNames[operative.$key] = operative.name;
          });
        });
    }

    function getAdditionAreaData(that) {
      let promises = [];

      Object.keys(matchingAreas).forEach((areaRef) => {
        promises.push(
          that.areasService.getAdditionalAreaPromise(areaRef)
            .then(function (additionalArea) {
              if(matchingAdditionalAreas[areaRef] === undefined){
                matchingAdditionalAreas[areaRef] = additionalArea.payload.val();
              }
            })
        );
      
      });
      return Promise.all(promises);
    }

    function aggregatePanels(that) {
      let promises = [];

      Object.keys(matchingAreas).forEach((areaRef) => {
        let area = matchingAreas[areaRef];
        promises.push(
          that.panelsService.getPanelsForAreaPromise(areaRef)
            .then(function (panels) {
              let deliveryDate = panelsDeliveryDate(that, area, panels);
              panels.forEach((panel) => {
                panel.deliveryDate = deliveryDate;
                matchingPanels.push(panel);
              });
            })
        );
      
      });
      return Promise.all(promises);
    }
    
    function panelsDeliveryDate(that, area, panels) {  
      
      let x = 0;
      let earliestBuildStart;
      let buildStartDates = [];
      let daysDifference;
      let daysDifferenceArray = [];
      let days = [];
      let daysRemovedNegatives = [];
      let closestNumber;
      let deliveryDate = "";

      if(panels.length){
      
        Object.keys(area.revisions).forEach((revisionRef)=> {

          let revisionDate = area.revisions[revisionRef];
          
          if (areaUnpublishedDate[area.$key] !== revisionRef) {

            panels.forEach((panel) => {

              let buildStartDate;
              let completedStatus = that.determineCompletedStatus(panel.qa);
              if (completedStatus === "In Progress") {
                buildStartDate = that.dateUtilsService.toIsoDate(that.getQAValue(panel.qa, "started"));
                buildStartDates.push(buildStartDate);
              }
            });

            if (buildStartDates.length) {

              buildStartDates.sort(function (a, b) {
                return Date.parse(a) - Date.parse(b);
              });

              earliestBuildStart = buildStartDates[0];

              daysDifference = that.dateUtilsService.daysBetween(earliestBuildStart, revisionDate);

              daysDifferenceArray.push({
                revisionDate: revisionDate,
                daysDifference: daysDifference
              });
              
              days.push(daysDifference);

            }
            
          }

        });
        
        if (days.length) {
          days = that.unique(days);
          closestNumber = that.getClosestNum(0, days);
          daysDifferenceArray.forEach((value) => {
            if (value.daysDifference === closestNumber) {
              deliveryDate = value.revisionDate;
            }
          });
        }

        return deliveryDate;        
      }else{
        return deliveryDate;
      }

    }

    function processPanels(that) {
      let promises = [];
      let allPanelsData = [];
      matchingPanels.forEach((panel) => {
        promises.push(
          extractPanelData(that,
            projects.find(function (item) {
              return item.$key === panel.project;
            }),
            matchingAreas[panel.area],
            matchingAdditionalAreas[panel.area],
            panel
          )
        );
      });

      return Promise.all(promises);
    }

    function extractPanelData(that, project, area, additionalArea, panel) {

      let panelData = {};

      let idParts = panel.id.split("-");
      let panelType = idParts[4];
      let panelNumber = idParts[5];

      panelData["dateCreated"] = matchingAreas[panel.area].timestamps.created ? that.dateUtilsService.toIsoDate(matchingAreas[panel.area].timestamps.created) : "";
      panelData["project"] = project.id;
      panelData["panelNumber"] = panel.id;
      panelData["projectName"] = project.name;
      panelData["jobsheetReference"] = panel.jobSheetNo;
      panelData["phase"] = area.phase;
      panelData["productionLine"] = productTypeProductionLine[panel.type];
      panelData["floor"] = area.floor;
      panelData["number"] = panelNumber;
      panelData["designHeight"] = panel.dimensions.height;
      panelData["designLength"] = panel.dimensions.length;
      panelData["designArea"] = panel.dimensions.area;
      panelData["designWeight"] = panel.dimensions.weight;
      panelData["designWidth"] = panel.dimensions.width !== undefined ? panel.dimensions.width : panel.dimensions.depth;
      panelData["designMP"] = "";
      panelData["jobsheets"] = area.phase + "-" + area.floor + "-" + panelType;
      panelData["dateCompleted"] = that.getQAValue(panel.qa, "completed") ? that.dateUtilsService.toIsoDate(that.getQAValue(panel.qa, "completed")) : "";
      panelData["actualHeight"] = that.getQAValue(panel.qa, "midHeight");
      let midLength = that.getQAValue(panel.qa, "midLength");
      panelData["actualLength"] = midLength !== "" ? midLength : that.getQAValue(panel.qa, "midWidth");
      panelData["actualLRDiag"] = that.getQAValue(panel.qa, "diagonalWidth");
      panelData["actualRLDiag"] = that.getQAValue(panel.qa, "diagonalHeight");
      panelData["operatorSignoff"] = "";
      panelData["supervisorSignoff"] = "";
      panelData["completedStatus"] = that.determineCompletedStatus(panel.qa);

      panelData["deliveryDate"] = panel.deliveryDate;
      panelData["areaType"] = area.type;
      panelData["panelType"] = panelType;
      panelData["uploadDate"] = panel.timestamps.created ? that.dateUtilsService.toIsoDate(panel.timestamps.created) : "";
      let productionPlan = productionPlans[panel.area];
      panelData["areaPlannedStart"] = productionPlan && productionPlans[panel.area] && productionPlans[panel.area].plannedStart ? that.dateUtilsService.toIsoDate(productionPlans[panel.area].plannedStart) : "";
      panelData["areaPlannedFinish"] = productionPlan && productionPlans[panel.area] && productionPlans[panel.area].plannedFinish ? that.dateUtilsService.toIsoDate(productionPlans[panel.area].plannedFinish) : "";
      let startedDate = that.getQAValue(panel.qa, "started");
      let finishAssembly = that.getQAValue(panel.qa, "finishAssembly");
      let startLayup = that.getQAValue(panel.qa, "startLayup");
      let finishDate = that.getQAValue(panel.qa, "completed");
      panelData["buildStartDate"] = startedDate ? that.dateUtilsService.toIsoDate(startedDate) : "";
      panelData["buildStartTime"] = startedDate ? that.dateUtilsService.toTime(startedDate) : "";
      panelData["buildFinishDate"] = finishDate ? that.dateUtilsService.toIsoDate(finishDate) : "";
      panelData["buildFinishTime"] = finishDate ? that.dateUtilsService.toTime(finishDate) : "";
      let buildDuration = getDuration(finishDate, startedDate);
      let wipDuration = getDuration(startLayup, finishAssembly);
      let assemblyDuration = getDuration(finishAssembly, startedDate);
      let LayupDuration = getDuration(finishDate, startLayup);
      panelData["assemblyDuration"] = assemblyDuration > 0 ? assemblyDuration : "";
      panelData["wipDuration"] = wipDuration > 0 ? wipDuration : "";
      panelData["LayupDuration"] = LayupDuration > 0 ? LayupDuration : "";
      panelData["buildDuration"] = buildDuration > 0 ? buildDuration : "";
      panelData["projectDatum"] = project.datumType;
      panelData["line"] = area.line !== undefined ? area.line : 1;
      panelData["operatives"] = getOperatives(panel.qa, "operatives");
      panelData["startedOperatives"] = getOperatives(panel.qa, "startedOperatives");
      panelData["finishAssemblyOperatives"] = getOperatives(panel.qa, "finishAssemblyOperatives");
      panelData["startLayupOperatives"] = getOperatives(panel.qa, "startLayupOperatives");
      panelData["completedOperatives"] = getOperatives(panel.qa, "completedOperatives");
      panelData["framingStyle"] = "";
      if(panel.additionalInfo !== undefined && panel.additionalInfo.framingStyle !== undefined){
        panelData["framingStyle"] = panel.additionalInfo.framingStyle;
      }
      panelData["pmv"] = additionalArea && additionalArea.addons ? additionalArea.addons : "";
      panelData[">4m"] = isGreaterThanFourMeters(that.getQAValue(panel.qa, "midHeight"));
      return panelData;

    }

    function isGreaterThanFourMeters(height){
      let isGreater = "false";
      if(height > 4000){
        isGreater = "true";
      }
      return isGreater;
    }
    
    function getDuration(finishDate, startedDate){
      if (finishDate !== "" && startedDate !== ""){
        return (finishDate - startedDate) / 1000;
      }else{
        return "";
      }
    }
    
    function getOperatives(qa, type) {
      let operatives = [];
      if (qa !== undefined && qa[type] !== undefined) {
        Object.keys(qa[type]).forEach((operativeKey) => {
          let opName = operativeNames[operativeKey];
          operatives.push(opName);
        });
        return operatives.join(", ");
      } else {
        return "";
      }
    }
  
  }
  
  getQAValue(qa, key) {
    return qa !== undefined && qa[key] ? qa[key] : "";
  }

  determineCompletedStatus(qa) {

    if (qa !== undefined) {
      if (qa["completed"] !== undefined) {
        return "Completed";
      } else if (qa["started"] !== undefined) {
        return "In Progress";
      }
    } else {
      return "Planned";
    }
  }
  
  unique(ary) {
    // concat() with no args is a way to clone an array
    let u = ary.concat().sort();
    for (let i = 1; i < u.length; ) {
      if (u[i - 1] === u[i])
        u.splice(i, 1);
      else
        i++;
    }
    return u;
  }
  
  getDublicates(arr) {
    let cache = {};
    let results = [];
    for (let i = 0, len = arr.length; i < len; i++) {
      if (cache[arr[i]] === true) {
        results.push(arr[i]);
      } else {
        cache[arr[i]] = true;
      }

    }
    return arr;
  }
  
  buildMap(items, property) {
    let map = {};
    items.forEach(item => {
      map[item.$key] = item[property];
    });
    return map;
  }
  
  removeNegatives(x) {
    let temp;
    for (let i = x.length - 1; i >= 0; i--) {
      if (x[i] < 0) {
        if (i !== x.length - 1) {
          // swap current value with last value
          temp = x[i];
          x[i] = x[x.length - 1];
          x[x.length - 1] = temp;
        }
        x.pop();
      }
    }
    return x;
  }
  
  getNegatives(x) {
    let temp = [];
    for (let i = x.length - 1; i >= 0; i--) {
      if (x[i] < 0) {
        temp.push(x[i]);
      }
    }
    return temp;
  }
  
  negativesToPositives(x) {
    let temp = [];
    for (let i = x.length - 1; i >= 0; i--) {
      if (x[i] < 0) {
        temp.push(Math.abs(x[i]));
      }
    }
    return temp;
  }
  
  getClosestNum(num, ar)
  {
    let i = 0;
    let closest;
    let closestDiff;
    let currentDiff;
    let dublicates = 0;
    let closestToPositive;

    if (ar.length)
    {
      closest = ar[0];
      for (i; i < ar.length; i++)
      {
        closestDiff = Math.abs(num - closest);
        currentDiff = Math.abs(num - ar[i]);
        if (currentDiff < closestDiff)
        {
          closest = ar[i];
        }
        closestDiff = null;
        currentDiff = null;
      }

      closestToPositive = Math.abs(closest);

      for (i = 0; i < ar.length-1; i++) {
        if (ar[i] === closestToPositive) {
          dublicates++;
        }
      }

      if(dublicates > 0){
        return closestToPositive;
      }
      
      //returns first element that is closest to number
      return closest;
    }
    //no length
    return false;
  }
}