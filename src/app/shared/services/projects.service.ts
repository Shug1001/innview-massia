import {map, first, switchMap, combineAll} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable, combineLatest } from 'rxjs';
import { ComponentsService } from './components.service';
import { AuthService } from './auth.service';
import { DateUtilService } from './date-util.service';
import { ProjectsAdditional, Quote } from '../models/models';
import { HttpClient } from '@angular/common/http';
import { CombineLatestSubscriber } from 'rxjs/internal/observable/combineLatest';

@Injectable({
  providedIn: 'root'
})

export class ProjectsService {

  private basePath = '/projects';
  private basePathAdditional = '/additionalData/projects';
  projectsRef: AngularFireList<any>;
  quotesRef: AngularFireList<any>;
  quoteRef: AngularFireObject<any>;
  activeProjectsRef: AngularFireList<any>;
  projectRef: AngularFireObject<any>;

  projects: Observable<any>;
  project: Observable<any>;

  constructor(private http: HttpClient, private db: AngularFireDatabase, private dateUtilService: DateUtilService, private componentsService: ComponentsService, private authService: AuthService) {
    this.projectsRef = this.db.list(this.basePath);
    this.quotesRef = this.db.list("/quotes");
    this.activeProjectsRef = this.db.list(this.basePath, ref => ref.orderByChild('active').equalTo(true));
  }

  getMaterialRequests(): Observable<any>{
    return this.db.list("materialrequests").snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getProjectAndComponents(id){
    return combineLatest([this.getproject(id), this.componentsService.getcomponentsforproject(id)]);
  }

  getActiveProjectsList() {
    return this.activeProjectsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  updateQuoteProject(quoteKey: string, projectKey: any) {
    console.log('key',quoteKey,'value',projectKey);
    this.quotesRef.update(quoteKey, {project: projectKey});
  }

  getQuoteWithIdObj(key: string): Observable<any> { 
    const path = `${"/quotes"}/${key}`;
    return this.db.object(path).snapshotChanges().pipe(map(obj => {
      if(obj.key){
        return Object.assign(obj.payload.val(), {$key: obj.key});
      }
    }));
  }

  getQuoteWithId(key: string): Observable<any> { 
    const path = `${"/quotes"}/${key}`;
    return this.db.object(path).snapshotChanges().pipe(map(obj => {
      if(obj.key){
        return Object.assign(obj.payload.val(), {$key: obj.key});
      }
    }));
  }

  getQuote(key: string){
    return this.getQuoteWithId(key).pipe(switchMap(quote => {
      if(quote){
        return this.db.list("/quoteData", ref => ref.orderByChild('created').equalTo(quote.latest)).snapshotChanges().pipe(map(arr => {
          return arr.map((snap) => {
            let data: any = snap.payload.val();
            if(data.quote === quote.$key){
              return Object.assign(data, {$key: snap.key});
            }
          }); 
        }));
      }else{
        return Promise.resolve([]);
      }
    }));
  }

  getQuoteWithIdPromise(key: string){ 
    const path = `${"/quotes"}/${key}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  getQuotesList() {
    return this.quotesRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getQuoteByQuoteId(id) {
    return this.db.list("/quotes", ref => ref.orderByChild('qid').equalTo(id)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getQuoteByProject(id) {
    return this.db.list('/quotes', ref => ref.orderByChild('project').equalTo(id)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getQuoteByQuoteIdPromise(id) {
    return this.db.list("/quotes", ref => ref.orderByChild('qid').equalTo(id)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getQuoteDataByQuote(id) {
    let quoteByRef: AngularFireList<any>;
    quoteByRef = this.db.list("/quoteData", ref => ref.orderByChild('quote').equalTo(id));
    return quoteByRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getProjectLoadDataByRefPromise(stat) {
    let projectLoadDataByRef: AngularFireList<any>;
    projectLoadDataByRef = this.db.list("/projectPlanLoad", ref => ref.orderByChild('AreaKey').equalTo(stat.AreaKey));
    return projectLoadDataByRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  updateProjectPlanLoad(stat){
    let updates = {};
    updates['/projectPlanLoad/' + stat.AreaKey] = stat;
    return this.db.database.ref().update(updates);
  }

  getProjectDataByRefPromise(stat) {
    let projectLoadDataByRef: AngularFireList<any>;
    projectLoadDataByRef = this.db.list("/projectData", ref => ref.orderByChild('fb_id').equalTo(stat.fb_id));
    return projectLoadDataByRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  updateProjectData(stat){
    let updates = {};
    updates['/projectData/' + stat.fb_id] = stat;
    return this.db.database.ref().update(updates);
  }

  getQuoteDataByQuotePromise(id) {
    let quoteByRef: AngularFireList<any>;
    quoteByRef = this.db.list("/quoteData", ref => ref.orderByChild('quote').equalTo(id));
    return quoteByRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getQuoteByLastest(id) {
    let quoteByRef: AngularFireList<any>;
    quoteByRef = this.db.list("/quoteData", ref => ref.orderByChild('created').equalTo(id));
    return quoteByRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getQuoteDataByLastest(id) {
    let quoteByRef: AngularFireList<any>;
    quoteByRef = this.db.list("/quoteData", ref => ref.orderByChild('created').equalTo(id));
    return quoteByRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getQuoteDataByProject(id) {
    return this.db.list("/quotes", ref => ref.orderByChild('project').equalTo(id)).snapshotChanges()
    .pipe(switchMap(arr => {
      if(arr.length){
        return this.db.list("/quoteData", ref => ref.orderByChild('quote').equalTo(arr[0].key)).snapshotChanges().pipe(map(arr => {
          return arr.map((snap) => {
            let data: any = snap.payload.val();
            if(data.project === id){
              return Object.assign(snap.payload.val(), {$key: snap.key});
            }
          }); 
        }));
      }else{
        return Promise.resolve([]);
      }
    }));
  }

  getQuoteDataByLatest(id) {
    return this.db.list("/quotes", ref => ref.orderByChild('project').equalTo(id)).snapshotChanges()
    .pipe(switchMap(quoteSnap => {
      if(quoteSnap.length){
        let quote: any = quoteSnap[0].payload.val();
        let quoteKey: any = quoteSnap[0].key;
        return this.db.list("/quoteData", ref => ref.orderByChild('created').equalTo(quote.latest)).snapshotChanges().pipe(map(arr => {
          return arr.map((snap) => {
            let data: any = snap.payload.val();
            if(data.quote === quoteKey){
              return Object.assign(data, {$key: snap.key});
            }
          }); 
        }));
      }else{
        return Promise.resolve([]);
      }
    }));
  }

  deleteQuotesInactive(quoteKey){

    let updates = {};

    return this.getQuoteDataByQuotePromise(quoteKey)
      .then((quotesData) => {

        quotesData.map(quoteData => {
          updates['/quoteData/' + quoteData.$key] = {};
        });

        updates['/quotes/' + quoteKey] = {};

        return this.db.database.ref().update(updates);

      });
  }

  doesExist(project, projects){
    let exists = false;
    projects.array.forEach(proj => {
      if(project.id === proj.id){
        exists = true;
      }
    });
    return exists;
  }

  deleteGateway(project, gateway){
    let updates = {};
    updates['/projects/' + project.$key + '/gatewayDates/' + gateway] = null;
    return this.db.database.ref().update(updates);
  }

  updateProject(key, updateObj){
    if(updateObj.$key !== undefined){
      delete updateObj.$key;
    }

    const path = `${this.basePath}`;
    const itemRef = this.db.list(path);
    console.log("Key: " + key);
    console.log("Update Data: " + JSON.stringify(updateObj));
    return itemRef.update(key, updateObj);
  }

  updateQuote(projectKey, quoteKey){
    return this.quotesRef.update(quoteKey, {"project": projectKey});
  }

  updateQuoteObj(obj, quoteKey){
    return this.quotesRef.update(quoteKey, obj);
  }

  createQuote(obj){
    return this.quotesRef.push(obj);
  }

  updateProjectAdditional(key, updateObj){
    if(updateObj.$key !== undefined){
      delete updateObj.$key;
    }

    const path = `${this.basePathAdditional}`;
    const itemRef = this.db.list(path);
    return itemRef.update(key, updateObj);
  }

  getProjectsList() {
    return this.projectsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getProjectsListPromise() {
    return this.projectsRef.snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  projectData(val){
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectData',val).toPromise();
  }

  updateProjectDataAzure(){
    return this.getProjectsListPromise()
      .then((projects) => {
        let promise = [];
        projects.map(project => {
            this.getProjectAdditionalPromise(project.$key).then((updateObjAdditional) => {
              console.log(updateObjAdditional);
              let passData = {};
              if(updateObjAdditional !== null){
                passData = {
                  fb_id: project.$key,
                  SiteOps: updateObjAdditional.siteOps ? updateObjAdditional.siteOps.name : "",
                  Designer: updateObjAdditional.designer ? updateObjAdditional.designer.name : "",
                  QS: updateObjAdditional.qs ? updateObjAdditional.qs.name : "",
                  BM: updateObjAdditional.engineer ? updateObjAdditional.engineer.name : "",
                  Team: updateObjAdditional.team ? updateObjAdditional.team : "",
                  Installer: updateObjAdditional.installer ? updateObjAdditional.installer : "",
                  CompletionDate: project.siteCompletion ? project.siteCompletion : "",
                  plannedCompletionDate: project.plannedCompletion ? project.plannedCompletion : "",
                  Gateway: project.gateway ? project.gateway : "",
                  G5Planned: project.gateways && project.gateways.G5Planned ? project.gateways.G5Planned : "",
                  G5Actual: project.gateways && project.gateways.G5Actual ? project.gateways.G5Actual : "",
                  G6Planned: project.gateways && project.gateways.G6Planned ? project.gateways.G6Planned : "",
                  G6Actual: project.gateways && project.gateways.G6Actual ? project.gateways.G6Actual : "",
                  G7aPlanned: project.gateways && project.gateways.G7aPlanned ? project.gateways.G7aPlanned : "",
                  G7aActual: project.gateways && project.gateways.G7aActual ? project.gateways.G7aActual : "",
                  G7bPlanned: project.gateways && project.gateways.G7bPlanned ? project.gateways.G7bPlanned : "",
                  G7bActual: project.gateways && project.gateways.G7bActual ? project.gateways.G7bActual : "",
                  G8Planned: project.gateways && project.gateways.G8Planned ? project.gateways.G8Planned : "",
                  G8Actual: project.gateways && project.gateways.G8Actual ? project.gateways.G8Actual : "",
                  G9Planned: project.gateways && project.gateways.G9Planned ? project.gateways.G9Planned : "",
                  G9Actual: project.gateways && project.gateways.G9Actual ? project.gateways.G9Actual : "",
                  Engineer: updateObjAdditional.engineer2 ? updateObjAdditional.engineer2 : "",
                  SiteStart: project.siteStart ? project.siteStart : "",
                  PlannedStart: project.plannedStart ? project.plannedStart : ""
                };
              }else{
                passData = {
                  fb_id: project.$key,
                  SiteOps: "",
                  Designer: "",
                  QS: "",
                  BM: "",
                  Team: "",
                  Installer: "",
                  CompletionDate: project.siteCompletion ? project.siteCompletion : "",
                  plannedCompletionDate: project.plannedCompletion ? project.plannedCompletion : "",
                  Gateway: project.gateway ? project.gateway : "",
                  G5Planned: project.gateways && project.gateways.G5Planned ? project.gateways.G5Planned : "",
                  G5Actual: project.gateways && project.gateways.G5Actual ? project.gateways.G5Actual : "",
                  G6Planned: project.gateways && project.gateways.G6Planned ? project.gateways.G6Planned : "",
                  G6Actual: project.gateways && project.gateways.G6Actual ? project.gateways.G6Actual : "",
                  G7aPlanned: project.gateways && project.gateways.G7aPlanned ? project.gateways.G7aPlanned : "",
                  G7aActual: project.gateways && project.gateways.G7aActual ? project.gateways.G7aActual : "",
                  G7bPlanned: project.gateways && project.gateways.G7bPlanned ? project.gateways.G7bPlanned : "",
                  G7bActual: project.gateways && project.gateways.G7bActual ? project.gateways.G7bActual : "",
                  G8Planned: project.gateways && project.gateways.G8Planned ? project.gateways.G8Planned : "",
                  G8Actual: project.gateways && project.gateways.G8Actual ? project.gateways.G8Actual : "",
                  G9Planned: project.gateways && project.gateways.G9Planned ? project.gateways.G9Planned : "",
                  G9Actual: project.gateways && project.gateways.G9Actual ? project.gateways.G9Actual : "",
                  Engineer: "",
                  SiteStart: project.siteStart ? project.siteStart : "",
                  PlannedStart: project.plannedStart ? project.plannedStart : ""
                };
              }
              promise.push(this.updateProjectData(passData));
            });
        });
        return Promise.all(promise);
      });
  }

  getproject(key: string): Observable<any> { 
    const path = `${this.basePath}/${key}`;
    this.project = this.db.object(path).valueChanges();
    return this.project;
  }

  getprojectPromise(key: string): Promise<any> { 
    const path = `${this.basePath}/${key}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  getprojectWithId(key: string): Observable<any> { 
    const path = `${this.basePath}/${key}`;
    this.project = this.db.object(path).snapshotChanges().pipe(map(obj => {
      return Object.assign(obj.payload.val(), {$key: obj.key});
    }));
    return this.project;
  }

  getprojectWithIdPromise(key: string): Promise<any> { 
    const path = `${this.basePath}/${key}`;
    return this.db.object(path).snapshotChanges().pipe(map(obj => {
      return Object.assign(obj.payload.val(), {$key: obj.key});
    })).pipe(first()).toPromise();
  }

  getFramingStyles(){
    const path = "config/framingStyles";
    var framingStyles = this.db.object(path).valueChanges();
    return framingStyles;
  }

  getFramingStylesPromise(){
    const path = "config/framingStyles";
    var framingStyles = this.db.object(path).valueChanges().pipe(first()).toPromise();
    return framingStyles;
  }

  getProjectAdditional(key: string): Observable<any> {
    const path = `${this.basePathAdditional}/${key}`;
    this.project = this.db.object(path).valueChanges();
    return this.project;
  }

  getProjectAdditionalPromise(key: string): Promise<any> {
    const path = `${this.basePathAdditional}/${key}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  saveProjectAdditionalRevitUpload(key: string, data: object){
    const path = `${this.basePathAdditional}/${key}`;
    return this.db.object(path).update(data);
  }

  removeRevisions(projectKey){
    return this.db.list('revisions', panelsRef => panelsRef.orderByChild('project').equalTo(projectKey)).snapshotChanges().pipe(first()).toPromise()
      .then((revisions) => this.db.object('revisions').update(
        revisions.reduce((acc, rev) => { acc[rev.key] = null; return acc; }, {})
      ));
  }

  createProject(project) {
    if (!project.timestamps) {
      project.timestamps = {       
        created: this.authService.serverTime(),
        createdBy: this.authService.currentUserId,
        modified: this.authService.serverTime(),
        modifiedBy: this.authService.currentUserId
      };
    }
    project.active = true;

    console.log(JSON.stringify(project));  
    return this.projectsRef.push(project);
  }

  updateprojectMessage(key: string, value: any) {
    console.log('key',key,'value',value);
    this.projectsRef.update(key, {messages: value.messages});
  }

  deleteproject(key: string) {
    this.projectsRef.remove(key);
  }

  rebuildPeople(updateObj, name){
    var newPeople = {
      id: "",
      name: "",
      mobile: "",
      email: ""
    };
    if(updateObj[name] !== undefined && updateObj[name] !== null){
      newPeople.id = updateObj[name].$key || updateObj[name].id;
      newPeople.name = updateObj[name].name;
      if(updateObj[name].phone !== undefined){
        newPeople.mobile = updateObj[name].phone;
      }
      newPeople.email = updateObj[name].email;
    }else{
      newPeople = null;
    }
    return newPeople;
  }

  convertToAdditional(updateObj){
    let newAdditional: ProjectsAdditional = {
      siteOps: this.rebuildPeople(updateObj, "site-ops"),
      designer: this.rebuildPeople(updateObj, "designer"),
      qs: this.rebuildPeople(updateObj, "quantity-surveyor"),
      engineer: this.rebuildPeople(updateObj, "engineer"),
      installer: updateObj.installer || null,
      team: updateObj.team || null,
      engineer2: updateObj.engineer2 || null,
    }
    return newAdditional;
  }

  convertFormToDatabaseStruct(updateObj){
    var newProject = Object.assign({}, updateObj);

    if(newProject.id !== undefined){
      newProject.id = String(newProject.id);
    }

    newProject.deliveryManager = this.rebuildPeople(updateObj, "delivery-manager");
    delete newProject["delivery-manager"];

    if(newProject.notifyUsers === null){
      newProject.notifyUsers = {};
    }else{
      newProject.notifyUsers = this.setNotifyUsers(newProject);
    }

    newProject.estimatedAreas.ExtH = Math.floor((10/ 100) * updateObj.estimatedAreas.Ext);
    
    if(updateObj.siteStart !== undefined){
      newProject.siteStart = this.dateUtilService.toIsoDate(updateObj.siteStart);
    }
    if(updateObj.nextDeliveryDate !== undefined){
      newProject.nextDeliveryDate = this.dateUtilService.toIsoDate(updateObj.nextDeliveryDate);
    }
    
    return newProject;
  }

  setNotifyUsers(newProject){
    let notifyUsers = {};
    newProject.notifyUsers.forEach((selectedUser) => {
      if(notifyUsers[selectedUser] === undefined){
        notifyUsers[selectedUser] = true;
      }
    });
    return notifyUsers;
  }

  

}