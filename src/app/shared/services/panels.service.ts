import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map, first, switchMap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { AreasService } from './areas.service';
import { AuthService } from './auth.service';
import { DateUtilService } from './date-util.service';
import { ProjectsService } from './projects.service';
import { JobsheetDetail } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class PanelsService {


  private basePath = '/panels';
  panelsRef: AngularFireList<any>;
  activePanelsRef: AngularFireList<any>;
  panelRef: AngularFireObject<any>;

  panels: Observable<any>;
  panel: Observable<any>;
  stats: Observable<any>;

  constructor(private db: AngularFireDatabase, private authService: AuthService, private areasService: AreasService, private dateUtilService: DateUtilService, private projectsService: ProjectsService) {
    this.panelsRef = this.db.list(this.basePath);
  }

  purgeDuplicatePanels(){
    return this.getPanelsPromise().then((panels: Array<any>) => {

      let panelsMapped = {};
      let panelsToDelete = [];

      panels.forEach(panel => {
        if(panelsMapped[panel.id] === undefined){
          panelsMapped[panel.id] = [];
        }
        panelsMapped[panel.id].push(panel);
      });    

      Object.keys(panelsMapped).forEach(panelKey => {
        if(panelsMapped[panelKey].length > 1){
          panelsMapped[panelKey].forEach(panel => {
              if(panelsMapped[panelKey][0].$key !== panel.$key){
                panelsToDelete.push((this.panelsRef.remove(panel.$key).then(() => {
                  return this.doAreaUpdates(panel.project);
                })));
              }
          });
        }
      });

      return Promise.all(panelsToDelete);
    });
  }

  getpanelsList() {
    return this.panelsRef.snapshotChanges()
  }

  partitonPanelsByProject(panels) {
    var panelsByProject = {};

    panels.forEach(panel => {
      var project = panel.project;
      if (!panelsByProject[project]) {
        panelsByProject[project] = [];
      }
      panelsByProject[project].push(panel);
    });

    return panelsByProject;
  }

  updateProjectPanels(projectKey, panels) {
    var updateDetail = {
      count: 0
    };

    let project = {};

    return this.projectsService.getprojectWithIdPromise(projectKey)
      .then((pro) => {
        if(project[pro.$key] === undefined){
          project[pro.$key] = pro.id;
        }
        return this.getPanelsForProjecListPromise(projectKey)
      })
      .then(doPanelUpdates.bind(this))
      .then(recordUpdateCount)
      .then(() => {
        return this.doAreaUpdates(projectKey);
      })
      .then(() => {
        return this.doProjectUpdate(projectKey, updateDetail);
      })
      .then(returnDetail);

    function doPanelUpdates(existingPanels) {
      var x = panels.map(upsertPanel.bind(this));
      return Promise.all(x);

      function upsertPanel(panel) {
        var existing;
        var existingKey;
        var doIt = false;
        if(panel.id.split("-")[0] === project[panel.project]){
          doIt = true;
        }
        existingPanels.forEach(panelSnap => {
          var p = panelSnap.payload.val();
          if (p.id === panel.id) {
            existing = p;
            existingKey = panelSnap.key;
          }
        });

        if(doIt){
        
          if (existing) {
            Object.assign(existing, panel);
            this.setModifiedTimestamp(existing);
            return this.updatepanel(existingKey, existing);
          }
          else {
            var newPanel = Object.assign({}, panel);
            this.setModifiedTimestamp(newPanel);
            return this.createpanel(newPanel);
          }
        }

      }
    }

    function recordUpdateCount(panelUpdates) {
      updateDetail.count = panelUpdates.length;

      return Promise.resolve();
    }



    function returnDetail() {
      return Promise.resolve(updateDetail);
    }
  }

  setModifiedTimestamp(item) {
    // Ensure existing data has a created timestamp
    if (!item.timestamps) {
      item.timestamps = {
        created: this.authService.serverTime()
      };
    }

    item.timestamps.modified = this.authService.serverTime();
  }


  doAreaUpdates(projectKey) {
    let areas = [];
    return this.areasService.getAreasForProjectPromise(projectKey)
      .then((areasList) => {
        areas = areasList;
        return this.getPanelsForProjectPromise(projectKey)
      })
      .then((existingPanels) => {
        
        var areaUpdates = [];
        areas.forEach(area => {
          Object.assign(area, calculateActuals(area.$key, existingPanels));
          this.setModifiedTimestamp(area);
          let areaKey = area.$key
          areaUpdates.push(this.areasService.updateArea(area.$key, area));
        });

        return Promise.all(areaUpdates);
      });

    function calculateActuals(areaKey, existingPanels) {
      var actuals = {
        actualPanels: 0,
        actualArea: 0
      };

      existingPanels.forEach(panel => {
        if (panel.area === areaKey) {
          actuals.actualPanels += 1;
          actuals.actualArea += panel.dimensions.area;
        }
      });

      return actuals;
    }
  }

  doProjectUpdate(projectKey, updateDetail) {

    return this.projectsService.getprojectPromise(projectKey)
      .then((project) => {
        updateDetail.projectId = project.id;
        updateDetail.projectName = project.name;

        this.setModifiedTimestamp(project);
        return this.projectsService.updateProject(projectKey, project)
          .then(() => {
            return Promise.resolve(updateDetail);
          });
      });

  }

  updatePanels(panels) {
    var projectUpdates = [];
    let obj = this.partitonPanelsByProject(panels);
    Object.keys(obj).forEach((projectKey) => {
      let panelList = obj[projectKey];
      projectUpdates.push(this.updateProjectPanels(projectKey, panelList));
    });

    return Promise.all(projectUpdates)
      .then(buildSummary);

    function buildSummary(updates) {
      var summary = {
        total: 0,
        details: []
      };
      updates.forEach(value => {
        summary.details.push(value);
        summary.total += value.count;
      });
      summary.details.sort(function (a, b) {
        return a.projectId.localeCompare(b.projectId);
      });

      return Promise.resolve(summary);
    }
  }

  uncompletePanel(panelStats, panel){
    let updates = {};
    if (panel.qa !== undefined && panel.qa["completed"] !== undefined) {
      panelStats.areaCompleted = Math.round((panelStats.areaCompleted - panel.dimensions.area)*10)/10;
      panelStats.completed--;
      updates["panels/" + panel.$key + "/qa"] = null;
      updates["panels/" + panel.$key + "/timestamps/modified"] = this.authService.serverTime();
      updates["areas/" + panel.area + "/completedArea"] = panelStats.areaCompleted;
      updates["areas/" + panel.area + "/completedPanels"] = panelStats.completed;
      updates["areas/" + panel.area + "/timestamps/modified"] = this.authService.serverTime();
    }
    if(Object.keys(updates).length > 0){
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    } 
  }

  addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
  }

  completeSetected(panelStats, selection, dateStart, duration, line){
    let updates = {};
    let area;
    let today = this.dateUtilService.todayAsIso();
    dateStart = this.dateUtilService.toIsoDate(dateStart);
    if(today === dateStart){
      dateStart = new Date().getTime();
    }else{   
      dateStart = new Date(dateStart).getTime();
    }
    let dateComplete = this.addMinutes(new Date(dateStart), duration);
    let dateStartTimestamp = new Date(dateStart).getTime();
    let dateCompleteTimestamp = new Date(dateComplete).getTime();
    selection.selected.forEach(panelStat => {
      let panel = panelStat.panel;
      panelStats.areaCompleted = Math.round((panelStats.areaCompleted + panel.dimensions.area)*10)/10;
      panelStats.completed++;
      area = panel.area;
      updates["panels/" + panel.$key + "/line"] = line;
      updates["panels/" + panel.$key + "/qa/started"] = dateStartTimestamp;
      updates["panels/" + panel.$key + "/qa/completed"] = dateCompleteTimestamp;
      updates["panels/" + panel.$key + "/timestamps/modified"] = this.authService.serverTime();
    });

    if(Object.keys(updates).length > 0){
      updates["areas/" + area + "/completedArea"] = panelStats.areaCompleted;
      updates["areas/" + area + "/completedPanels"] = panelStats.completed;
      updates["areas/" + area + "/timestamps/modified"] = this.authService.serverTime();
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    } 
  }

  deleteSetected(panelStats, selection){
    let updates = {};
    let area;
    selection.selected.forEach(panelStat => {
      let panel = panelStat.panel;
      panelStats.actualArea = Math.round((panelStats.actualArea - panel.dimensions.area)*10)/10;
      panelStats.actualPanels--;
      area = panel.area;
      updates["panels/" + panel.$key] = null;
    });
    if(Object.keys(updates).length > 0){
      updates["areas/" + area + "/actualArea"] = panelStats.actualArea;
      updates["areas/" + area + "/actualPanels"] = panelStats.actualPanels;
      updates["areas/" + area + "/timestamps/modified"] = this.authService.serverTime();
      return this.db.database.ref().update(updates);
    }else{
      return Promise.resolve();
    } 
  }

  getJobsheetsForArea(key: string): Observable<any> {
    const path = "/jobsheetDetails";
    return this.db.list(path, areasRef => areasRef.orderByChild('area').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getProductivity(){
    return this.areasService.getJobsheets().pipe(
      switchMap(jobsheets => {
        let toCombine = [];
        jobsheets.map((jobsheet) => {
          if(jobsheet.panels){
            Object.keys(jobsheet.panels).map((panelKey) => {
              toCombine.push(combineLatest(this.getpanel(panelKey),this.db.object('/additionalData/jobsheets/' + jobsheet.$key).valueChanges()).pipe(map(([panel, additional]) => {
                if(panel){
                  return Object.assign(panel, {
                    $key: panelKey,
                    jobsheetKey: jobsheet.$key,
                    jobsheetRef: jobsheet.projectNumber,
                    additional: additional
                  });
                }else{
                  return {};
                }
              })));
            });
          }
        });
        return combineLatest(toCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(snap => {
          let productivity = {};
          snap.map((panel) => {
            if(productivity[panel.jobsheetKey] === undefined){
              productivity[panel.jobsheetKey] = {
                jobsheetRef: panel.jobsheetRef,
                totalM2: 0,
                framingStyle: panel.additionalInfo && panel.additionalInfo.framingStyle ? panel.additionalInfo.framingStyle : "",
                additional: panel.additional
              };
            }
            productivity[panel.jobsheetKey].totalM2 += panel.dimensions && panel.dimensions.area ? panel.dimensions.area : 0;
          });
          return productivity;
        }));
      })
    );
  }

  getPanelsForArea(key: string): Observable<any> {
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild('area').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getPanelsForAreaPromise(key: string): Promise<any> {
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('area').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getPanelsForProjectPromise(key: string): Promise<any> {
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getPanelsForProjecListPromise(key: string): Promise<any> {
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges().pipe(first()).toPromise();
  }

  getPanelsByIdPromise(key: string): Promise<any> {
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByChild('id').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getPanelsCompleteForRange(start, end){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild("qa/completed").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getlastPanels(){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByKey().limitToLast(6000)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getPanelsPromise(){
    const path = this.basePath;
    return this.db.list(path, areasRef => areasRef.orderByKey().limitToLast(32000)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getPanelsCreatedForRange(start, end){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild("qa/completed").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getPanelsStartedForRange(start, end){
    const path = this.basePath;
    this.panels = this.db.list(path, areasRef => areasRef.orderByChild("qa/started").startAt(start).endAt(end)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.panels;
  }

  getStatsForDay(productionLine: string, date: string) {
    const path = `${'stats'}/${productionLine}/${date}`;
    this.stats = this.db.object(path).snapshotChanges();
    return this.stats;
  }

  getCapacityForDay(productionLine: string, date: string) {
    const path = `${'capacity'}/${productionLine}/${date}`;
    this.stats = this.db.object(path).snapshotChanges();
    return this.stats;
  }

  getProPerformanceData(date: string){
    return combineLatest([
      this.getCapacityForDay("SIP",date),
      this.getCapacityForDay("HSIP",date),
      this.getCapacityForDay("TF",date),
      this.getCapacityForDay("CASS",date),
      this.getStatsForDay("SIP",date),
      this.getStatsForDay("HSIP",date),
      this.getStatsForDay("TF",date),
      this.getStatsForDay("CASS",date)
    ]);
  }

  getpanel(key: string): Observable<any> {
    const path = `${this.basePath}/${key}`;
    this.panel = this.db.object(path).valueChanges();
    return this.panel;
  }

  getpanelPromise(key: string): Promise<any> {
    const path = `${this.basePath}/${key}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  createpanel(panel) {
    console.log("Panel " + panel.id + " created");
    this.panelsRef.push(panel);
  }

  updatepanel(panelKey, panel) {
    if(panel.$key !== undefined){
      delete panel.$key;
    }
    console.log("Panel " + panel.id + " updated");
    this.panelsRef.update(panelKey, panel);
  }

  deletepanel(key: string) {
    this.panelsRef.remove(key);
  }

}