import { Injectable } from '@angular/core';
import { DateUtilService } from './date-util.service';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(private dateUtilService: DateUtilService) { }


  getComponent(components, areaKey){
    var componentType = "";
    components.forEach((component) => {
      Object.keys(component.areas).forEach((key) => {
        if(areaKey === key){
          componentType = component.type;
        }
      });
    });
    return componentType;
  }

  lowestRisk(entryOb) {
    var toFillOb = {};

    Object.keys(entryOb).forEach((productionLine) => {
      let risks = entryOb[productionLine];
      var lowest = Number.POSITIVE_INFINITY;
      var tmp;
      for (var i = risks.length - 1; i >= 0; i--) {
        tmp = risks[i];
        if (tmp < lowest) {
          lowest = tmp;
        }
      }
      if (toFillOb[productionLine] === undefined) {
        toFillOb[productionLine] = lowest;
      }
    });
    return toFillOb;
  }
  
  buildMap(items, property) {
    var map = {};
    items.forEach((item) => {
      map[item.$id] = item[property];
    });
    return map;
  }

  createProjectLabel(project) {
    return project.id + " - " + project.name;
  }

  calculateDeliveryDate(earliestDeliveryDate, area, project) {
    var deliveryDate = area.revisions[project.deliverySchedule.published];
    if (deliveryDate < earliestDeliveryDate) {
      earliestDeliveryDate = deliveryDate;
    }
    return earliestDeliveryDate;
  }

  riskInPast(deliveryDate){
    var today = new Date();
    deliveryDate = new Date(deliveryDate);
    return today < deliveryDate;
  }

  isAtRisk(panelsCount, deliveryDate, riskPeriod) {
    if (panelsCount > 0) {
      return false;
    }
    var today = new Date();
    var startOfRiskPeriod = new Date(this.dateUtilService.subtractWorkingDays(new Date(deliveryDate), riskPeriod));
    return today >= startOfRiskPeriod;
  }

  getOptionalNumber(value) {
    if (value !== undefined) {
      return parseFloat(value);
    }
    return 0;
  }

  calculatePercentage(a, b) {
    if (b === 0) {
      return 0;
    }
    return Math.round((a/b)*100);
  }

  round(value) {
    return Math.round(value * 100) / 100;
  }

  riskDays(area){
    if (!area._productionEndDate || !area._riskDate) {
      return undefined;
    }

    return this.dateUtilService.daysBetweenWeekDaysOnly(area._productionEndDate, area._riskDate);
  }

  getRiskDays(area) {
    let days = this.riskDays(area);
    if (days === undefined) {
      return "!";
    }
    if (days == null) {
      return "";
    }
    return days > 0 ? "+" + days : days.toString();
  }

}
