import { Injectable } from '@angular/core';
import { ForwardPlannerService } from './forward-planner.service';
import { DateUtilService } from './date-util.service';
import Srd  from '../../../assets/srd/srd.json';

@Injectable({
  providedIn: 'root'
})
export class PerformanceService {

  productTypeProductionLine: object;
  productTypes:object;

  constructor(
    private dateUtilService: DateUtilService,
    private forwardPlannerService: ForwardPlannerService,
    private dateUtilsService: DateUtilService
    ) { 
      this.productTypes = Srd.productTypes;
      this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
    }

    //Tested
    getPlannedProductionTotal(areasMapped, buildDate){
      let today = new Date();
      let nextDay = this.dateUtilsService.plusDays(today, 1);
      let totalM2 = {};
      Object.keys(areasMapped).forEach(areaKey =>{
        let area = areasMapped[areaKey];
        if(area._productionLine !== null){
          const productionLineFiltered = area._productionLine.replace(/[0-9]/g, '');
          if(totalM2[productionLineFiltered] === undefined){
            totalM2[productionLineFiltered] = 0;
          }
          if (buildDate < nextDay){
            if (area.additionalData !== undefined && area.additionalData.buildPlannedDays2 !== undefined && area.additionalData.buildPlannedDays2[buildDate] !== undefined) {
              let buildOnDay = area.additionalData.buildPlannedDays2[buildDate];
              totalM2[productionLineFiltered] += this.forwardPlannerService.avgM2(area, buildOnDay);  
            }
          }else{
            if(area.plan !== undefined && area.plan.buildDays !== undefined && area.plan.buildDays[buildDate] !== undefined) {
              let buildOnDay = area.plan.buildDays[buildDate];
              totalM2[productionLineFiltered] += this.forwardPlannerService.avgM2(area, buildOnDay); 
            }
          }
        }
      });
      Object.keys(totalM2).forEach((productionLine)=>{
        totalM2[productionLine] = Math.round(totalM2[productionLine]);
      });
      return totalM2;        
    }

    //Tested
    getGreatestRisksLines(areasMapped){ 
      var allRiskData = {};
      Object.keys(areasMapped).forEach(areaKey =>{
        let area = areasMapped[areaKey];
        if(area._line !== null && area.plan !== undefined){
          const productionLineFiltered = area._line.toString();
          let areaPlan = area.plan;
          if(allRiskData[productionLineFiltered] === undefined){
              allRiskData[productionLineFiltered] = [];
          }
          if (areaPlan != undefined && areaPlan.plannedFinish != undefined && areaPlan.riskDate != undefined) {
            var days = this.dateUtilsService.daysBetweenWeekDaysOnly(areaPlan.plannedFinish, areaPlan.riskDate)
            allRiskData[productionLineFiltered].push(days);  
          }
        } 
      })
      return allRiskData;
    }

    getGreatestRisks(areasMapped){ 
      var allRiskData = {};
      Object.keys(areasMapped).forEach(areaKey =>{
        let area = areasMapped[areaKey];
        if(area._productionLine !== null && area.plan !== undefined){
          const productionLineFiltered = area._productionLine.replace(/[0-9]/g, '');
          let areaPlan = area.plan;
          if(allRiskData[productionLineFiltered] === undefined){
              allRiskData[productionLineFiltered] = [];
          }
          if (areaPlan != undefined && areaPlan.plannedFinish != undefined && areaPlan.riskDate != undefined) {
            var days = this.dateUtilsService.daysBetweenWeekDaysOnly(areaPlan.plannedFinish, areaPlan.riskDate)
            allRiskData[productionLineFiltered].push(days);  
          }
        } 
      })
      return allRiskData;
    }

    calculate(panels, date) {

      date = new Date(date);
      
      var calculatedTotal = this.createDataHolder();

      var start = date.setHours(0, 0, 0, 0);
      var end = date.setHours(23, 59, 59, 999);

      panels.forEach(panel => {
        let line = this.productTypeProductionLine[panel.type];
        if (panel.qa !== undefined) {
          if (panel.qa["completed"] !== undefined) {
            if (panel.qa["completed"] >= start && panel.qa["completed"] <= end) {
              calculatedTotal.area[line] += panel.dimensions.area;
              calculatedTotal.completed[line]++;
            }
          } else if (panel.qa["started"] !== undefined) {
            if (panel.qa["started"] < end) {
              calculatedTotal.started[line]++;
            }
          }
        }
      });
      return calculatedTotal;
    }
    
    createDataHolder(){
      var calculatedTotal = {
        completed : { "SIP" : 0, "HSIP" : 0, "IFAST" : 0, "TF" : 0, "CASS" : 0 },
        started : { "SIP" : 0, "HSIP" : 0, "IFAST" : 0, "TF" : 0, "CASS" : 0 },
        area : { "SIP" : 0, "HSIP" : 0, "IFAST" : 0, "TF" : 0, "CASS" : 0 }
      };
      return calculatedTotal;
    }
    
}
