import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import Srd from '../../../assets/srd/srd.json'
import {map, first} from 'rxjs/operators';
import { DateUtilService } from './date-util.service';
import { ReferenceDataService } from './reference-data.service';
import { ProjectsService } from './projects.service.js';
import { AngularFireDatabase } from '@angular/fire/database';
import { AreasService } from './areas.service.js';
import { Subject } from 'rxjs';
import { ComponentsService } from './components.service.js';
import { SupplyChainService } from '../services/supply-chain.service';
import { AuthService } from './auth.service';
import { Dimensions, Quote, ComponentsHolder, panel } from '../models/models';
import { PanelsService } from './panels.service';
import { PathLocationStrategy } from '@angular/common';
import { AuditsService } from './audits.service';

@Injectable({
  providedIn: 'root'
})
export class LoadPanelsService {

  fileType:string;
  project:any;
  productTypeProductionLine: object;
  productTypeName: object;
  phases: object = Srd.phases;
  floors: object = Srd.floors;
  panelTypes: object = Srd.panelTypes;
  datumTypes: object = Srd.datumTypes;
  productTypes: object = Srd.productTypes;
  components: object = Srd.components;
  componentsMapped: object;
  datumData: Array<any>;
  this:any;
  subject$: Subject<any>;
  quoteKey: string;
  fileName: string;
  dimenstionData: Dimensions;
  PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
  updatePanelsSheet: boolean;
  componentsHolder: Object;
  areasHolder: Object;

  quoteVal = {
    "DAT_Doors":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: false, type: "string" },
      "Phase": { req: false, type: "string" },
      "InstanceID": { req: true, type: "number" },
      "HostID": { req: true, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: true, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: true, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Windows":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: false, type: "string" },
      "Phase": { req: false, type: "string" },
      "InstanceID": { req: true, type: "number" },
      "HostID": { req: true, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: true, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: true, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Wall Materials":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: false, type: "string" },
      "Phase": { req: false, type: "string" },
      "InstanceID": { req: false, type: "number" },
      "HostID": { req: false, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: false, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: false, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Walls":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: false, type: "string" },
      "Phase": { req: false, type: "string" },
      "InstanceID": { req: false, type: "number" },
      "HostID": { req: false, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: false, type: "number" },
      "Length": { req: true, type: "number" },
      "Height": { req: false, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Roof Materials":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: false, type: "string" },
      "Phase": { req: false, type: "string" },
      "InstanceID": { req: false, type: "number" },
      "HostID": { req: true, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: false, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: false, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Roofs":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: true, type: "string" },
      "Phase": { req: true, type: "string" },
      "InstanceID": { req: true, type: "number" },
      "HostID": { req: false, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: false, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: false, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Floor Materials":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: false, type: "string" },
      "Phase": { req: false, type: "string" },
      "InstanceID": { req: false, type: "number" },
      "HostID": { req: true, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: false, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: false, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    },
    "DAT_Floors":{
      "Quote Number": { req: true, type: "string" },
      "Quote Name": { req: true, type: "string" },
      "Level": { req: true, type: "string" },
      "Phase": { req: true, type: "string" },
      "InstanceID": { req: true, type: "number" },
      "HostID": { req: false, type: "number" },
      "Family": { req: true, type: "string" },
      "Type": { req: true, type: "string" },
      "Width": { req: false, type: "number" },
      "Length": { req: false, type: "number" },
      "Height": { req: false, type: "number" },
      "Area (NET)": { req: true, type: "number" }
    }

  }
  

  constructor(private auditsService: AuditsService, private panelsService: PanelsService, private supplyChainService: SupplyChainService, private componentsService: ComponentsService, private areasService : AreasService, private db: AngularFireDatabase, private authService: AuthService, private projectsService: ProjectsService, private dateUtilService: DateUtilService, private referenceDataService: ReferenceDataService) {
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
    this.productTypeName = this.dateUtilService.buildMapReverse(this.productTypes, "name");
    this.componentsMapped = this.mapComponents(this.components);
    this.subject$ = new Subject();
  }


  getFramingStyles() {
    return this.db.object("config/framingStyles").snapshotChanges().pipe(first()).toPromise();
  }

  updateProgress(obj) { 
    this.subject$.next(obj); 
  }
  
  getProgress() {
    return this.subject$.asObservable();
  }

  areaCode(phase, floor, ext, component) {
    return phase + "-" + floor + "-" + ext + "-" + component; 
  }

  addAreas(data, projectKey){

    let areaUpdates = [];

    let division = 0;

    let statusData = {
      loading: true,
      status:"Processing....",
      type: "Delete",
      counter: 0
    }

    this.updateProgress(statusData);
  
    data.result.models.forEach(element => {
      if(element.status === "Update"){
        areaUpdates.push({
          areaKey: element.areaKey,
          estimatedArea: element.estimatedArea,
          estimatedPanels: element.estimatedPanels
        });
      }
    });

    division = 100 / (areaUpdates.length + data.result.models.length);

    if(areaUpdates.length){
      statusData.status = "Found " + areaUpdates.length + " components to update - Updating....";
      let updates = {};
      areaUpdates.forEach((areaUpdate)=>{
        let areaKey = areaUpdate.areaKey;
        delete areaUpdate.areaKey;
        updates[`/areas/${areaKey}/estimatedArea`] = areaUpdate.estimatedArea;
        updates[`/areas/${areaKey}/estimatedPanels`] = areaUpdate.estimatedPanels;
        statusData.counter += Number(division.toFixed(0));
        this.updateProgress(statusData);
      });
      if(Object.keys(areaUpdates).length){
        return this.db.database.ref().update(updates)
        .then(()=>{
          return createAreas(this);
        })
        .then(()=>{
          statusData.status = "Components sucsessfully added.";
          statusData.counter = 100;
          this.updateProgress(statusData);
          return this.projectsService.saveProjectAdditionalRevitUpload(projectKey, {"revitUpload": true});
        })
        .catch((error) => {
          statusData.status = error + " - Please let IT know!";
          this.updateProgress(statusData);
        });
      }else{
        return createAreas(this)
        .then(()=>{
          statusData.status = "Components sucsessfully added.";
          statusData.counter = 100;
          this.updateProgress(statusData);
          return this.projectsService.saveProjectAdditionalRevitUpload(projectKey, {"revitUpload": true});
        })
        .catch((error) => {
          statusData.status = error + " - Please let IT know!";
          this.updateProgress(statusData);
        });
      }
    }else{
      return createAreas(this)
      .then(()=>{
        statusData.status = "Components sucsessfully added.";
        statusData.counter = 100;
        this.updateProgress(statusData);
        return this.projectsService.saveProjectAdditionalRevitUpload(projectKey, {"revitUpload": true});
      })
      .catch((error) => {
        statusData.status = error + " - Please let IT know!";
        this.updateProgress(statusData);
      });
    }

    function createAreas(that){

      statusData.status = "Creating " + data.result.models.length + " components....";
      statusData.type = "create";

      let promises = [];
      data.result.models.forEach(element => {
        if (element.use){
          let component = element.component;
          let deliveryDate = element.deliveryDate;
          let framing = element.framing;
          element.description = element.component;
          element.supplier = "Innovaré";
          element.revitUpload = true;
          delete element.component;
          delete element.productionLine;
          delete element.framing;
          delete element.deliveryDate;
          delete element.status;
          delete element.use;
          delete element.existing;
          delete element.componentId;

          promises.push(that.areasService.createAreaComponentFraming(element, data.project, deliveryDate, framing, component)
          .then(()=>{
            statusData.counter += Number(division.toFixed(0));
            that.updateProgress(statusData);
          }));
        }
      
      })
      return Promise.all(promises);
    
    }
  }

  getAreaId(area, project) {
    return [project.$key, area.phase, area.floor, area.type].join("-");
  }

  mapComponents(components){
    let singleProductTypes = {};
    Object.keys(components).map((componentKey)=>{
      let component = components[componentKey];
      let productTypes = Object.keys(component.productTypes);
      if(componentKey.indexOf("Revit") !== -1){
        let productType = productTypes[0];
        singleProductTypes[productType] = componentKey;
      }
    });
    return singleProductTypes;
  }

  isStatus(element, areasGrouped){

    let hasActual = false;
    let match = false;
    let totalArea = 0;
    let componentArea = {};

    let newAreaCode = this.areaCode(element.phase, element.floor, element.type, element.component);

    for(let i = 0; i < areasGrouped.length; i++){

      let groupedAreas = areasGrouped[i];
      let type = groupedAreas.componentName;
      let typeFiltered = type.replace(/[0-9]/g, '');
      let productType = "";
      let areaKey = "";

      hasActual = false;
      match = false;
      totalArea = 0;

      if(typeFiltered.indexOf("Revit ") !== -1){

        let area = groupedAreas.areas[0];
        totalArea += area.estimatedArea;
        productType = area.type;
        areaKey = area.$key;

        if(area.actualPanels !== undefined){
          hasActual = true;
        }

        let existingAreaCode = this.areaCode(groupedAreas.phase, groupedAreas.floor, productType, typeFiltered);

        if(newAreaCode === existingAreaCode){
          if(componentArea[existingAreaCode] === undefined){
            componentArea[existingAreaCode] = {
              area: 0,
              id: areaKey,
              matches: 0
            };
          }
          componentArea[existingAreaCode].matches++;
          componentArea[existingAreaCode].area += totalArea;
          match = true;
          break;
        }

      }
    }

    if(match){
      if(!hasActual){
        element.status = "Update";
        element.use = false;
        element.existing = componentArea[newAreaCode].area;
        element.areaKey = componentArea[newAreaCode].id;
      }else{
        element.status = "Panels found";
        element.use = false;
        element.existing = componentArea[newAreaCode].area;
      }
    }else{
      element.status = "New";
      element.use = true;
      element.existing = 0;
    }
    
    return element;
      
  }



  prepQuotesData(project, response, areasGrouped){

    let startDate = project.siteStart;

    let sortToAreas = {};

    return this.referenceDataService.getDatumsPromise(project.datumType)
    .then((datumData)=>{

      this.datumData = datumData;

      response.models.forEach(element => {

        element.type = this.productTypeName[element.type];
        element.component = this.componentsMapped[element.type];
        element.project = project.$key;

        if (this.productTypeProductionLine[element.type] !== undefined && this.datumData[0] !== undefined && this.datumData[0].productionLines[this.productTypeProductionLine[element.type]] !== undefined) {
          var panelAvgOverride = this.datumData[0].productionLines[this.productTypeProductionLine[element.type]].panelAvgOverride;
          var panelAvg = this.datumData[0].productionLines[this.productTypeProductionLine[element.type]].panelAvg;
          if (panelAvgOverride > 0) {
            element.estimatedPanels = Math.round(element.estimatedArea / panelAvgOverride) | 0;
          } else {
            element.estimatedPanels = Math.round(element.estimatedArea / panelAvg) | 0;
          }
        }

        if(sortToAreas[element.phase + "-" + element.floor] === undefined){
          sortToAreas[element.phase + "-" + element.floor] = {};
        }
        if(sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]] === undefined){
          sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]] = {
            area: element,
            totalCount: 0,
            totalArea: 0,
            framing: {}
          };
        }
        if(sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] === undefined){
          sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] = 0;
        }
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].totalArea += element.estimatedArea;
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].totalCount += element.estimatedPanels;
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] += element.estimatedArea;
      });
      response.models = [];
      Object.keys(sortToAreas).forEach((key)=>{
        Object.keys(sortToAreas[key]).forEach((key2)=>{
          sortToAreas[key][key2].area.deliveryDate = startDate;
          sortToAreas[key][key2].area.estimatedArea = Number(sortToAreas[key][key2].totalArea.toFixed(1));
          sortToAreas[key][key2].area.estimatedPanels = sortToAreas[key][key2].totalCount;
          Object.keys(sortToAreas[key][key2].area.framing = sortToAreas[key][key2].framing).forEach((frame)=>{
            sortToAreas[key][key2].area.framing[frame] = Number(sortToAreas[key][key2].framing[frame].toFixed(1));
          });
          sortToAreas[key][key2].area.framing = sortToAreas[key][key2].framing;
          sortToAreas[key][key2].area = this.isStatus(sortToAreas[key][key2].area, areasGrouped);
          response.models.push(sortToAreas[key][key2].area);
        });
        startDate = this.dateUtilService.addWorkingDays(startDate, 2);
      });

      for(let i = 0; i < areasGrouped.length; i++){

        let groupedAreas = areasGrouped[i];
        let type = groupedAreas.componentName;
        let typeFiltered = type.replace(/[0-9]/g, '');
        let productType = "";
        let totalArea = 0;

        if(typeFiltered.indexOf("Revit") !== -1){

          groupedAreas.areas.forEach(area => {
            totalArea += area.estimatedArea;
            productType = area.type;
          });

          let existingAreaCode = this.areaCode(groupedAreas.phase, groupedAreas.floor, productType, typeFiltered);
          let count = 0;
          for(let i = 0; i < response.models.length; i++){
            let element = response.models[i];
            let newAreaCode = this.areaCode(element.phase, element.floor, element.type, element.component);
            if(existingAreaCode === newAreaCode){
              count++;
            }
          }

          if(!count){
            let component = {
              phase: groupedAreas.phase,
              floor: groupedAreas.floor,
              type: productType,
              estimatedArea: 0,
              existing: totalArea,
              component: typeFiltered,
              project: project.$key,
              deliveryDate: this.dateUtilService.toIsoDate(groupedAreas.deliveryDate),
              status: "No Change",
              use: false
            }
            response.models.push(component);
          }

        }

      }

      return response;

    });

  }

  loadPanels(file){
    this.fileType = file.type.toString();
    let name = file.name.split(".")[0];
    this.fileName = name;
    return this.readBase64(file)
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbookPanels.bind(this));
  }

  loadAreas(project, file){
    this.project = project;
    this.fileType = file.type.toString();
    return this.referenceDataService.getDatumsPromise(project.datumType)
    .then((datumData)=>{
      this.datumData = datumData;
      return this.readBase64(file);
    })
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbook.bind(this));
  }

  loadQuotes(file, dimenstionData){
    this.dimenstionData = dimenstionData;
    this.fileType = file.type.toString();
    let name = file.name.split(".")[0];
    this.fileName = name;
    return this.readBase64(file)
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbookForRevit.bind(this));
  }

  loadStockLevels(file, dimenstionData){
    this.dimenstionData = dimenstionData;
    this.fileType = file.type.toString();
    let name = file.name.split(".")[0];
    this.fileName = name;
    return this.readBase64(file)
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbookForRevit.bind(this));
  }

  loadStockItem(file){
    this.fileType = file.type.toString();
    let name = file.name.split(".")[0];
    this.fileName = name;
    return this.readBase64(file)
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbookForStockItem.bind(this));
  }

  loadSuppliers(file){
    this.fileType = file.type.toString();
    let name = file.name.split(".")[0];
    this.fileName = name;
    return this.readBase64(file)
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbookForSuppliers.bind(this));
  }

  loadStockDetails(file){
    this.fileType = file.type.toString();
    let name = file.name.split(".")[0];
    this.fileName = name;
    return this.readBase64(file)
    .then(this.parseWorkbook)
    .then(this.loadFromWorkbookForStockDetails.bind(this));
  }

  readBase64(file): Promise<any> {
    let reader  = new FileReader();
    let future = new Promise((resolve, reject) => {
      reader.addEventListener("load", (data) => {
        resolve(decode(reader.result));
      }, false);

      reader.addEventListener("error", (event)=> {
        reject(event);
      }, false);

      if (reader.readAsBinaryString !== undefined) {
        reader.readAsBinaryString(file);
      }
      else {
        reader.readAsArrayBuffer(file);
      }

      function decode(result) {
        if (typeof result === "string") {
          return result;
        }

        let data = "";
        let chunkSize = 10240;
        for (let offset = 0; offset < result.byteLength; offset += chunkSize) {
          data += String.fromCharCode.apply(null, new Uint8Array(result.slice(offset, offset+chunkSize)));
        }
        return data;
      }
    });
    return future;
  }

  parseWorkbook(data) {
    try {
      let workbook;
      workbook = XLSX.read(data, {type: "binary"});
      return Promise.resolve(workbook);
    }
    catch (e) {
      return Promise.reject(e.message);
    }
  }

  loadFromWorkbook(workbook) {

    let framing = {};
    let counter = 0;
    let startDate = this.project.siteStart;
    var noBracketsPattern = /\[.*?\]/g;
    var noneOfThese = /[.#$/,]/g;

    let response:any = {
      models: [],
      errors:[]
    };

    return loadFromWorkbookProcess(response, workbook)
    .then(configFramingStyles.bind(this))
    .then(() => {
      let sortToAreas = {};
      response.models.forEach(element => {
        if (this.productTypeProductionLine[element.type] !== undefined && this.datumData[0] !== undefined && this.datumData[0].productionLines[this.productTypeProductionLine[element.type]] !== undefined) {
          var panelAvgOverride = this.datumData[0].productionLines[this.productTypeProductionLine[element.type]].panelAvgOverride;
          var panelAvg = this.datumData[0].productionLines[this.productTypeProductionLine[element.type]].panelAvg;
          if (panelAvgOverride > 0) {
            element.estimatedPanels = Math.round(element.estimatedArea / panelAvgOverride) | 0;
          } else {
            element.estimatedPanels = Math.round(element.estimatedArea / panelAvg) | 0;
          }
        }
        element.component = this.componentsMapped[element.type];
        element.project = this.project.$key;
        if(sortToAreas[element.phase + "-" + element.floor] === undefined){
          sortToAreas[element.phase + "-" + element.floor] = {};
        }
        if(sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]] === undefined){
          sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]] = {
            area: element,
            totalArea: 0,
            totalCount: 0,
            framing: {}
          };
        }
        if(sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] === undefined){
          sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] = 0;
        }
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].totalArea += element.estimatedArea;
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].totalCount += element.estimatedPanels;
        sortToAreas[element.phase + "-" + element.floor][this.productTypeProductionLine[element.type]].framing[element.framing] += element.estimatedArea;
      });
      response.models = [];
      Object.keys(sortToAreas).forEach((key)=>{
        Object.keys(sortToAreas[key]).forEach((key2)=>{
          sortToAreas[key][key2].area.deliveryDate = startDate;
          sortToAreas[key][key2].area.estimatedArea = Number(sortToAreas[key][key2].totalArea.toFixed(1));
          sortToAreas[key][key2].area.estimatedPanels = sortToAreas[key][key2].totalCount;
          Object.keys(sortToAreas[key][key2].area.framing = sortToAreas[key][key2].framing).forEach((frame)=>{
            sortToAreas[key][key2].area.framing[frame] = Number(sortToAreas[key][key2].framing[frame].toFixed(1));
          });
          sortToAreas[key][key2].area.framing = sortToAreas[key][key2].framing;
          response.models.push(sortToAreas[key][key2].area);
        });
        startDate = this.dateUtilService.addWorkingDays(startDate, 2);
      });
      return response;
    });

    function configFramingStyles() {
 
      let configFramingStyles = {};
      let promises = [];

      Object.keys(framing).forEach((framingStyle)=>{
        let framingStylesData = framing[framingStyle];
        let productionLine = this.productTypeProductionLine[framingStylesData.type];
        promises.push(this.projectsService.getFramingStylesPromise()
          .then((framingStyles) => {
            productionLine = this.productTypeProductionLine[framingStylesData.type];
            if(configFramingStyles[framingStyle] === undefined){
              configFramingStyles[framingStyle] = true;
            }
            if(framingStyles[productionLine] !== undefined && framingStyles[framingStyle] !== undefined ){
              return Promise.resolve();
            }else{
              return this.db.object("config/framingStyles/" + productionLine).update(configFramingStyles);
            }
          }));
      });

      return Promise.all(promises);

    }

    function loadFromWorkbookProcess(response, workbook) {

      let deferredList = [];

      let worksheet = workbook.Sheets["DAT_Wall"];
      let range = XLSX.utils.decode_range(worksheet["!ref"]);
      let columnMap = {};
      let row = -1;

      while (++row <= range.e.r) {
        for (var col = 1; col <= range.e.c; col++) {
          var heading = cellValue(worksheet, col, row);
          if (heading) {
            columnMap[heading] = col;
          }
        }
        break;
      }

      while (++row <= range.e.r) {
        let quote = cellValue(worksheet, 0, row);
        if (quote) {
          let phase = cellValue(worksheet, columnMap["EST_Phase"], row);
          let floor = cellValue(worksheet, columnMap["Base Constraint"], row);
          let type = cellValue(worksheet,columnMap["Function"],row);
          let framing = cellValue(worksheet,columnMap["Type"],row);
          let estimatedArea = cellValue(worksheet,columnMap["Area_Net"],row);

          counter++;
          
          deferredList.push(
            validatePanel(phase, floor, type, framing, estimatedArea, row)
            .then((data) => {
              response.models.push(data);
              return Promise.resolve(data);
            })
            .catch((error) => {
              response.errors.push(error);
              return Promise.resolve(error);
            })
          );
        }
      }

      return Promise.all(deferredList);
      
      function validatePanel(phaseData, floorData, typeData, framingData, estimatedAreaData, row) {

        if(typeData){
          typeData = typeData.substring(0, 3);
        }
        
        const productTypes = Object.keys(Srd.productTypes);
        const phases = Object.keys(Srd.phases);
        const floors = Object.keys(Srd.floors);

        let phase;
        let floor;
        let type;
        let estimatedArea;
        let framingInfo;

        productTypes.forEach(data =>{
          if(typeData === data){
            type = data;
          }
        });

        phases.forEach(data =>{
          if(phaseData === data){
            phase = data;
          }
        });

        floors.forEach(data =>{
          if(floorData === data){
            floor = data;
          }
        });

        if(!isNaN(estimatedAreaData)){
          estimatedArea = estimatedAreaData;
        }

        if(framingData !== "" && !noBracketsPattern.test(framingData) && !noneOfThese.test(framingData)){
          framingInfo = framingData;
        }

        if (phase && floor && type && framingInfo && estimatedArea) {

          let section = {
            phase: "",
            floor: "",
            type: "",
            framing: "",
            estimatedArea: ""
          }

          if(framing[framingData] === undefined){
            counter++;
            framing[framingData] = {
              counter: counter,
              type: type
            }
          }

          section.floor = floor;
          section.phase = phase;
          section.type = type;
          section.framing = framingInfo;
          section.estimatedArea = estimatedArea;

          return Promise.resolve(section);

        }else {

          let errors = {
            row: row + 1,
            phase: {
              message: phase ? phase : "No match", 
              correct: phase ? true : false
            },
            floor: {
              message: floor ? floor : "No match", 
              correct: floor ? true : false
            },
            type: {
              message: type ? type : "No match",
              correct: type ? true : false
            },
            framing: {
              message: framingInfo ? framingInfo : "Contains '','.','#','$','/',','[' or ']'", 
              correct: framingInfo ? true : false
            },
            estimatedArea: {
              message: estimatedArea ? estimatedArea : "Not a number", 
              correct: estimatedArea ? true : false
            }
          }

          return Promise.reject(errors);
        }

      }  


      function cellValue(worksheet, column, row) {
        var cell_address = XLSX.utils.encode_cell({c: column, r: row});
        var cell = worksheet[cell_address];
        return cell ? cell.v : undefined;
      }
    }

  }

  hashCode (str){
    let hash = 0;
    if (str.length == 0) return hash;
    for (let i = 0; i < str.length; i++) {
        let char = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
  }

  fillWallMaterials(instanceGroup, hostGroup, returnData){

    //Doors
    if(returnData.revitData["DAT_Doors"] !== undefined){
      returnData.revitData["DAT_Doors"].forEach(doors => {
        doors["Phase"] = doors["HostID"] && instanceGroup[doors["HostID"]][0]["Phase"] ? instanceGroup[doors["HostID"]][0]["Phase"] : doors["Phase"];
        doors["Level"] = doors["HostID"] && instanceGroup[doors["HostID"]][0]["Level"] ? instanceGroup[doors["HostID"]][0]["Level"] : doors["Level"];
        doors["Area (Gross)"] = doors["Area (NET)"] ? this.dateUtilService.round2(doors["Area (NET)"]) : 0;
      });
    }

    //Windows
    if(returnData.revitData["DAT_Windows"] !== undefined){
      returnData.revitData["DAT_Windows"].forEach(windows => {
        windows["Phase"] = windows["HostID"] && instanceGroup[windows["HostID"]][0]["Phase"] ? instanceGroup[windows["HostID"]][0]["Phase"] : windows["Phase"];
        windows["Level"] = windows["HostID"] && instanceGroup[windows["HostID"]][0]["Level"] ? instanceGroup[windows["HostID"]][0]["Level"] : windows["Level"];
        windows["Area (Gross)"] = windows["Area (NET)"] ? this.dateUtilService.round2(windows["Area (NET)"]) : 0;
      });
    }

    //walls material
    if(returnData.revitData["DAT_Wall Materials"] !== undefined){
      returnData.revitData["DAT_Wall Materials"].forEach(wallMaterials => {

        if(wallMaterials["HostID"] !== undefined && instanceGroup[wallMaterials["HostID"]]){

          let parent = instanceGroup[wallMaterials["HostID"]][0];

          wallMaterials["Phase"] = parent["Phase"];
          wallMaterials["Level"] = parent["Level"];

          wallMaterials["Count"] = Math.round((wallMaterials["Area (NET)"] / parent["Area (NET)"]));
          wallMaterials["Length"] = Math.round((parent["Length"] * wallMaterials["Count"]));
          
        }
      });
    }

    //walls
    if(returnData.revitData["DAT_Walls"] !== undefined){
      returnData.revitData["DAT_Walls"].forEach(walls => {

        let doorAndWindows = 0;

        if(hostGroup[walls["InstanceID"]] !== undefined){
          hostGroup[walls["InstanceID"]].forEach(host => {
            doorAndWindows += host["Area (NET)"];
          });
        }

        walls["Area (Gross)"] = this.dateUtilService.round2(walls["Area (NET)"] + doorAndWindows);
        walls["Height"] = Math.round(((walls["Area (Gross)"] * 1000000) / walls["Length"]));

      });
    }

    if(returnData.revitData["DAT_Wall Materials"] !== undefined){

      //walls material
      returnData.revitData["DAT_Wall Materials"].forEach(wallMaterials => {

        if(wallMaterials["HostID"] !== undefined && instanceGroup[wallMaterials["HostID"]]){

          let parent = instanceGroup[wallMaterials["HostID"]][0];
          wallMaterials["Area (Gross)"] = this.dateUtilService.round2(parent["Area (Gross)"] * wallMaterials["Count"]);
          wallMaterials["Height"] = parent["Height"];

        }

      });

    }

    if(returnData.revitData["DAT_Floors"] !== undefined){

      //Floor
      returnData.revitData["DAT_Floors"].forEach(floors => {

        let doorAndWindows = 0;

        if(hostGroup[floors["InstanceID"]] !== undefined){
          hostGroup[floors["InstanceID"]].forEach(host => {
            doorAndWindows += host["Area (NET)"];
          });
        }

        floors["Area (Gross)"] = (floors["Area (NET)"] + doorAndWindows);

      });

    }

    //Roof
    if(returnData.revitData["DAT_Roofs"] !== undefined){

      returnData.revitData["DAT_Roofs"].forEach(roofs => {

        let doorAndWindows = 0;

        if(hostGroup[roofs["InstanceID"]] !== undefined){
          hostGroup[roofs["InstanceID"]].forEach(host => {
              doorAndWindows += host["Area (NET)"];
          });
        }

        roofs["Area (Gross)"] = this.dateUtilService.round2(roofs["Area (NET)"] + doorAndWindows);

      });

    }

    if(returnData.revitData["DAT_Floor Materials"] !== undefined){

      //floor material
      returnData.revitData["DAT_Floor Materials"].forEach(floorMaterials => {

        if(floorMaterials["HostID"] !== undefined && instanceGroup[floorMaterials["HostID"]]){

          let parent = instanceGroup[floorMaterials["HostID"]][0];

          floorMaterials["Phase"] = parent["Phase"];
          floorMaterials["Level"] = parent["Level"];

          floorMaterials["Count"] = Math.round((floorMaterials["Area (NET)"] / parent["Area (NET)"]));

          floorMaterials["Length"] = parent["Length"];
          floorMaterials["Width"] = Math.round((floorMaterials["Count"] * parent["Width"]));
          floorMaterials["Area (Gross)"] = this.dateUtilService.round2(parent["Area (Gross)"] * floorMaterials["Count"]);
        
        }

      });

    }

    if(returnData.revitData["DAT_Roof Materials"] !== undefined){

      //floor material
      returnData.revitData["DAT_Roof Materials"].forEach(roofMaterials => {

        if(roofMaterials["HostID"] !== undefined && instanceGroup[roofMaterials["HostID"]]){

          let parent = instanceGroup[roofMaterials["HostID"]][0];

          roofMaterials["Phase"] = parent["Phase"];
          roofMaterials["Level"] = parent["Level"];

          roofMaterials["Count"] = Math.round(roofMaterials["Area (NET)"] / parent["Area (NET)"]);

          roofMaterials["Length"] = parent["Length"];
          roofMaterials["Width"] = Math.round((roofMaterials["Count"] * parent["Width"]));
          roofMaterials["Area (Gross)"] = this.dateUtilService.round2(parent["Area (Gross)"] * roofMaterials["Count"]);
        
        } 

      });

    }

    let quoteKey;
    let created = this.authService.serverTime();

    // sort Data to save
    Object.keys(returnData.revitData).forEach(key => {
      returnData.revitData[key].forEach(data => {

        if(returnData.quoteUpdates[data["Quote Number"]] === undefined){
          returnData.quoteUpdates[data["Quote Number"]] = {};

          let quoteData = {
            qid: data["Quote Number"],
            quoteName: data["Quote Name"],
            project: "",
            latest: created,
            timestamps: { 
              created: this.authService.serverTime(),
              createdBy: this.authService.currentUserId,
              modified: this.authService.serverTime(),
              modifiedBy: this.authService.currentUserId
            }
          }

          returnData.quoteUpdates[data["Quote Number"]] = quoteData;
        }

        const quoteDataKey = this.db.database.ref().child('quoteData').push().key;

        if(returnData.quoteDataUpdates[data["Quote Number"]] === undefined){
          returnData.quoteDataUpdates[data["Quote Number"]] = {};
        }

        if(returnData.quoteDataUpdates[data["Quote Number"]]['/quoteData/' + quoteDataKey] === undefined){
          returnData.quoteDataUpdates[data["Quote Number"]]['/quoteData/' + quoteDataKey] = {};    
        }

        let quotesNumbers =  Object.keys(returnData.quoteUpdates)[0];
        if(quotesNumbers === data["Quote Number"]){
          let newData ={
            sheetName: data["Sheet Name"],
            level: data["Level"],
            phase: data["Phase"],
            instanceId: data["InstanceID"] | 0,
            hostId: data["HostID"] | 0,
            family: data["Family"],
            type: data["Type"],
            width: data["Width"] | 0,
            length: data["Length"] | 0,
            height: data["Height"] | 0,
            areaNet: data["Area (NET)"],
            areaGross: data["Area (Gross)"] | 0,
            uniqueId: data["UniqueID"],
            created: created
          };

          returnData.quoteDataUpdates[data["Quote Number"]]['/quoteData/' + quoteDataKey] = newData;
        }
      });
    });

    return returnData;
  }

  saveQuotes(data){
    let quotesNumbers =  Object.keys(data.quoteUpdates)[0];
    return this.projectsService.getQuoteByQuoteIdPromise(quotesNumbers)
      .then((existingQuotes: Quote[]) => {
        if(existingQuotes.length === 1){
          let existingQuote = existingQuotes[0];
          let existingQuoteKey = existingQuote.$key;
          delete existingQuote.$key;
          existingQuote.timestamps.modified = this.authService.serverTime();
          existingQuote.timestamps.modifiedBy = this.authService.currentUserId;
          existingQuote.latest = this.authService.serverTime();
          return this.projectsService.updateQuoteObj(existingQuote, existingQuoteKey)
          .then(() => {
            return this.projectsService.getQuoteWithIdPromise(existingQuoteKey);
          })
          .then((addedQuote: Quote) => {
            Object.keys(data.quoteDataUpdates[quotesNumbers]).forEach(ref => {
              data.quoteDataUpdates[quotesNumbers][ref].quote = existingQuoteKey;
              data.quoteDataUpdates[quotesNumbers][ref].created = addedQuote.latest;
            }, this);
            return this.db.database.ref().update(data.quoteDataUpdates[quotesNumbers]);
          })
          .then(() => {
            return this.auditsService.log("Quote uploaded to " + quotesNumbers,"","000","");
          });
        }else{
          let createdKey;
          return this.projectsService.createQuote(data.quoteUpdates[quotesNumbers])
          .then((created) => {
            createdKey = created.key;
            return this.projectsService.getQuoteWithIdPromise(created.key);
          })
          .then((addedQuote: Quote) => {
            Object.keys(data.quoteDataUpdates[quotesNumbers]).forEach(ref => {
              data.quoteDataUpdates[quotesNumbers][ref].quote = createdKey;
              data.quoteDataUpdates[quotesNumbers][ref].created = addedQuote.latest;
            }, this);
            return this.db.database.ref().update(data.quoteDataUpdates[quotesNumbers]);
          })
          .then(() => {
            return this.auditsService.log("Quote uploaded to " + quotesNumbers,"","000","");
          });
        }
      });
   
  }


  loadFromWorkbookForRevit(workbook) {

    let returnData = {
      quoteDataUpdates: {},
      quoteUpdates: {},
      revitData: {},
      errors: []
    };

    let instanceGroup = {};
    let hostGroup = {};

    let sheetNames = [
      "DAT_Doors",
      "DAT_Windows",
      "DAT_Wall Materials",
      "DAT_Walls",
      "DAT_Roof Materials",
      "DAT_Roofs",
      "DAT_Floor Materials",
      "DAT_Floors"
    ];

    const phases = Object.keys(Srd.phases);
    const floors = Object.keys(Srd.floors);

    sheetNames.forEach(sheetName =>{

      let worksheet = workbook.Sheets[sheetName];

      if(worksheet === undefined){
        returnData.errors.push("Error: Sheet: "+sheetName+" Is does not exist");
      }

      let range = XLSX.utils.decode_range(worksheet["!ref"]);
      let columnMap = {};
      let row = -1;
      
      while (++row <= range.e.r) {
        for (var col = 0; col <= range.e.c; col++) {
          var heading = cellValue(worksheet, col, row);
          if (heading) {
            columnMap[heading] = col;
          }
        }
        break;
      }

      while (++row <= range.e.r) {

        let width;
        let length;
        
        let xData = {
          quoteNumber: cellValue(worksheet, columnMap["Quote Number"], row),
          quoteName: cellValue(worksheet, columnMap["Quote Name"], row),
          level: cellValue(worksheet,columnMap["Level"],row),
          phase: cellValue(worksheet,columnMap["Phase"],row),
          instanceId: cellValue(worksheet,columnMap["InstanceID"],row),
          hostId: cellValue(worksheet,columnMap["HostID"],row),
          family: cellValue(worksheet,columnMap["Family"],row),
          type: cellValue(worksheet,columnMap["Type"],row),
          height: cellValue(worksheet,columnMap["Height"],row),
          areaNet: cellValue(worksheet,columnMap["Area (NET)"],row),
        }

        switch (xData.level) {
          case 0:
            xData.level = "GF";
            break;
          case 1:
            xData.level = "1F";
            break;
          case 2:
            xData.level = "2F";
            break;
          case 3:
            xData.level = "3F";
            break;
        }

        if (xData.level && typeof xData.level === 'string' && xData.level.charAt(0) === "R"){
          xData.level = "RF";
        }
          
        let valCount = 0;
        let dataKeys = Object.keys(xData);
        dataKeys.forEach(field => {
          if(xData[field] === undefined){
            valCount++;
          }
        });

        let id = xData.instanceId === undefined ? xData.hostId : xData.instanceId;
        let uniqueId = this.hashCode(xData.quoteNumber+id+xData.type);

        switch (sheetName) {
          case "DAT_Roofs":
            width = this.dimenstionData.roof.width;
            length = this.dimenstionData.roof.length;
            break;
          case "DAT_Floors":
            width = this.dimenstionData.floor.width;
            length = this.dimenstionData.floor.length;
            break;
          default: 
            width = cellValue(worksheet,columnMap["Width"],row);
            length = cellValue(worksheet,columnMap["Length"],row); 
        }

        let newPhase;
        let newLevel;

        if(xData.type){
          xData.type = xData.type.split('.').join("");
        }

        if(this.fileName){
          this.fileName = this.fileName.split('.').join("");
        }

        phases.forEach(data =>{
          if(xData.phase === data){
            newPhase = data;
          }
        });

        floors.forEach(data =>{
          if(xData.level === data){
            newLevel = data;
          }
        });

        if(newPhase === undefined){
          xData.phase = "unphased";
        }

        if(newLevel === undefined){
          xData.level = "unleveled";
        }

        let data = {
          "Sheet Name": sheetName,
          "Quote Number": this.fileName,
          //"Quote Number": quoteNumber,
          "Quote Name": xData.quoteName,
          "Level": xData.level,
          "Phase": xData.phase,
          "InstanceID": xData.instanceId,
          "HostID": xData.hostId,
          "Family": xData.family,
          "Type": xData.type,
          "Width": width,
          "Length": length | 0,
          "Height": xData.height,
          "Area (NET)": xData.areaNet !== undefined ? this.dateUtilService.round2(xData.areaNet) : 0,
          "Area (Gross)": 0,
          "UniqueID": uniqueId,
          "Count": 0
        };


        //validation
        if(valCount !== dataKeys.length){
          Object.keys(data).forEach(field => {
            if(this.quoteVal[sheetName][field] !== undefined){
              let type = typeof data[field];
              if(type !== "undefined" ){
                if(this.quoteVal[sheetName][field].type !== type){
                  returnData.errors.push("Error: Sheet: "+sheetName+" Field: "+field+" Should be a "+this.quoteVal[sheetName][field].type);
                }
              }else{
                if(this.quoteVal[sheetName][field].req){
                  returnData.errors.push("Error: Sheet: "+sheetName+" Field: "+field+" Is required");
                }
              }
            }
          });
      

          if(returnData.revitData[sheetName] === undefined){
            returnData.revitData[sheetName] = [];
          }

          returnData.revitData[sheetName].push(data);

          //groups
          if(xData.instanceId !== undefined && instanceGroup[xData.instanceId] === undefined){
            instanceGroup[xData.instanceId] = [];
          }

          if(xData.instanceId !== undefined){
            instanceGroup[xData.instanceId].push(data);
          }

          if(sheetName === "DAT_Doors" || sheetName === "DAT_Windows"){

            if(xData.hostId !== undefined && hostGroup[xData.hostId] === undefined){
              hostGroup[xData.hostId] = [];
            }

            if(xData.hostId !== undefined){
              hostGroup[xData.hostId].push(data);
            }

          }

        }
      
      }
      
    });

    returnData = this.fillWallMaterials(instanceGroup, hostGroup, returnData);
    return returnData

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});
      var cell = worksheet[cell_address];
      return cell ? cell.v : undefined;
    }  

  }

  getstockTypePromise(type, key): Promise <any> {
    return this.db.list(type, ref => ref.orderByChild('name').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getstockItemsPromise(key): Promise <any> {
    return this.db.list('stockItem', ref => ref.orderByChild('stockCode').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getSRD(stockItem){

    let deferredList = [];
    
    if(stockItem.stockGroup && stockItem.stockSubCat){
      deferredList.push(this.getstockTypePromise('stockGroup', stockItem.stockGroup));
      deferredList.push(this.getstockTypePromise('stockSubCat', stockItem.stockSubCat));
      deferredList.push(this.getstockItemsPromise(stockItem.stockCode));
      deferredList.push(this.getstockTypePromise('stockType', 'None'));
      return Promise.all(deferredList);
    }else{
      return Promise.reject();
    }
  }

  loadFromWorkbookForStockItem(workbook) {

    let deferredList = [];

    var updatesGroup = {};
    var updatesSubCats = {};
    var updatesItems = {};
    var updatesExistingItems = {};
    
    let response:any = {
      models: [],
      errors:[]
    };

    let worksheet = workbook.Sheets["stock sheet"];
    let range = XLSX.utils.decode_range(worksheet["!ref"]);
    let columnMap = {};
    let row = -1;


    while (++row <= range.e.r) {
      for (var col = 0; col <= range.e.c; col++) {
        var heading = cellValue(worksheet, col, row);
        if (heading) {
          columnMap[heading] = col;
        }
      }
      break;
    }
    
    while (++row <= range.e.r) {
      let stock = cellValue(worksheet, 0, row);
      if (stock) {

        let stockItem = {
          stockGroup: cellValue(worksheet, columnMap["Category Description"], row) !== undefined ? cellValue(worksheet, columnMap["Category Description"], row) : '',
          stockSubCat: cellValue(worksheet, columnMap["Stock Sub Category Description"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Sub Category Description"], row) : '',
          stockType: '',
          stockName: cellValue(worksheet, columnMap["Stock Description"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Description"], row) : '',
          stockCode: cellValue(worksheet, columnMap["Stock Code"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Code"], row) : '',
          width: cellValue(worksheet, columnMap["Stock Width"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Width"], row) : 0,
          coinsName: '',
          coinsCode: '',
          depth: cellValue(worksheet, columnMap["Stock Depth"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Depth"], row) : 0,
          grade: cellValue(worksheet, columnMap["Stock Grade"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Grade"], row) : '',
          length: cellValue(worksheet, columnMap["Stock Length"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Length"], row) : 0,
          treated: cellValue(worksheet, columnMap["Treated"], row) !== undefined ? cellValue(worksheet, columnMap["Treated"], row) : '',
          storageWidth: 0,
          storageDepth: 0,
          storageLength: 0,
          unitOfCount: 0,
          unitOfApplication: 0,
          criticalStockLevel: 0,
          uom: cellValue(worksheet, columnMap["UOM"], row) !== undefined ? cellValue(worksheet, columnMap["UOM"], row) : '',
          status: 'Active',
          lastestAction: 'Create New Stock Item',
          dateTimestamp: this.authService.serverTime()
        };

        if(stockItem.treated){
          stockItem.treated = "true";
        }else{
          stockItem.treated = "false";
        }

        deferredList.push(
          this.getSRD(stockItem)
          .then((data) => {
            return doCheck(this, stockItem, data);
          })
          .catch((error) => {
            response.errors.push(error);
            return Promise.resolve(error);
          })
        );
      }
    }

    return Promise.all(deferredList)
      .then((data) => {
        let updates = {};
        Object.keys(updatesGroup).forEach(element => {
          let groupKey = updatesGroup[element].key;
          delete updatesGroup[element].key;
          updates['/stockGroup/'+ groupKey] = updatesGroup[element];
        });
        Object.keys(updatesSubCats).forEach(element => {
          let subCatKey = updatesSubCats[element].key;
          delete updatesSubCats[element].key;
          updates['/stockSubCat/'+ subCatKey] = updatesSubCats[element];
        });
        Object.keys(updatesItems).forEach(element => {
          let itemKey = this.db.database.ref().child('stockItem').push().key;
          let item = updatesItems[element];
          updates['/stockItem/'+ itemKey] = item;
        });
        Object.keys(updatesExistingItems).forEach(element => {
          let item = updatesExistingItems[element];
          updates['/stockItem/'+ element] = item;
        });
        console.log(JSON.stringify(updates));
        return this.db.database.ref().update(updates)
        .then((data) => {
          response.models.push(data);
          return Promise.resolve(data);
        })
        .catch((error) => {
          response.errors.push(error);
          return Promise.resolve(error);
        })
      });

    function doCheck(that, stockItem, data){

      let stockGroupArr = data[0];
      let stockSubCatArr = data[1];
      let stockItemsArr = data[2];
      let stockTypeArr = data[3];

      if(stockTypeArr.length){
        stockItem.stockType = stockTypeArr[0].$key;      
      }

      if(stockGroupArr.length){
        stockItem.stockGroup = stockGroupArr[0].$key;      
      }else{

        if (updatesGroup[stockItem.stockGroup] === undefined) {
  
          const postData1 = {
            code: stockItem.stockCode.split('-')[0],
            name: stockItem.stockGroup,
            key: that.db.database.ref().child('stockGroup').push().key
          };
    
          updatesGroup[stockItem.stockGroup] = postData1;
          
        }

        stockItem.stockGroup = updatesGroup[stockItem.stockGroup].key;
  
      }
  
      if(stockSubCatArr.length){
        stockItem.stockSubCat = stockSubCatArr[0].$key;      
      }else{

        if (updatesSubCats[stockItem.stockSubCat] === undefined) {
  
          const postData2 = {
            name: stockItem.stockSubCat,
            key: that.db.database.ref().child('stockSubCat').push().key
          };
    
          updatesSubCats[stockItem.stockSubCat] = postData2;

        }

        stockItem.stockSubCat = updatesSubCats[stockItem.stockSubCat].key;
  
      }
      
      if(stockItemsArr.length){
        updatesExistingItems[stockItemsArr[0].$key] = stockItem;      
      }else{
        updatesItems[stockItem.stockCode] = stockItem;
      }
   
    }

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});
      var cell = worksheet[cell_address];
      return cell ? cell.v : undefined;
    }  


  }

  getContactPromise(key): Promise <any> {
    return this.db.list('contacts', ref => ref.orderByChild('emailWork').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getSupplierPromise(key): Promise <any> {
    return this.db.list('suppliers', ref => ref.orderByChild('companyName').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  getSupplierContact(supplier, contact){
    let deferredList = [];
    if(supplier.companyName && contact.emailWork){
      deferredList.push(this.getContactPromise(contact.emailWork));
      deferredList.push(this.getSupplierPromise(supplier.companyName));
      return Promise.all(deferredList);
    }else{ 
      return Promise.reject();
    }
  }

  loadFromWorkbookForSuppliers(workbook) {

    let deferredList = [];

    let updatesContacts = {};
    let contactsHolder = {};
    let updatesSupplier = {};
    let updatesExistingSupplier = {};
    let updatesSupplierHolder = {};
    
    let response:any = {
      models: [],
      errors:[]
    };

    let worksheet = workbook.Sheets["Contacts"];
    let range = XLSX.utils.decode_range(worksheet["!ref"]);
    let columnMap = {};
    let row = -1;

    


    while (++row <= range.e.r) {
      for (var col = 0; col <= range.e.c; col++) {
        var heading = cellValue(worksheet, col, row);
        if (heading) {
          columnMap[heading] = col;
        }
      }
      break;
    } 
    
    while (++row <= range.e.r) {

      let item = cellValue(worksheet, 0, row);
      if (item) {
        let supplier = {
          companyName: cellValue(worksheet, columnMap["Supplier"], row) !== undefined ? cellValue(worksheet, columnMap["Supplier"], row) : '',
          accCurrency: '',
          companyShort: '',
          vat: cellValue(worksheet, columnMap["vat"], row) !== undefined ? cellValue(worksheet, columnMap["vat"], row) : 0,
          payee: '',
          mobile: '',
          address1: cellValue(worksheet, columnMap["Address 1"], row) !== undefined ? cellValue(worksheet, columnMap["Address 1"], row) : '',
          phone: cellValue(worksheet, columnMap["Tel. Number"], row) !== undefined ? cellValue(worksheet, columnMap["Tel. Number"], row) : '',
          address2: cellValue(worksheet, columnMap["Address 2"], row) !== undefined ? cellValue(worksheet, columnMap["Address 2"], row) : '',
          fax: cellValue(worksheet, columnMap["fax"], row) !== undefined ? cellValue(worksheet, columnMap["fax"], row) : '',
          address3: cellValue(worksheet, columnMap["Address 3"], row) !== undefined ? cellValue(worksheet, columnMap["Address 3"], row) : '',
          email: cellValue(worksheet, columnMap["email"], row) !== undefined ? cellValue(worksheet, columnMap["email"], row) : '',
          address4: cellValue(worksheet, columnMap["Address 4"], row) !== undefined ? cellValue(worksheet, columnMap["Address 4"], row) : '',
          postcode: cellValue(worksheet, columnMap["Postcode"], row) !== undefined ? cellValue(worksheet, columnMap["Postcode"], row) : '',
          country: cellValue(worksheet, columnMap["Country"], row) !== undefined ? cellValue(worksheet, columnMap["Country"], row) : '',
          contacts: {},
          mainContact: '',
          status: 'Active',
          lastestAction: 'Create New Stock Item',
          dateTimestamp: this.authService.serverTime()
        };

        let contact = {
          firstName: cellValue(worksheet, columnMap["Contact Name"], row) !== undefined ? cellValue(worksheet, columnMap["Contact Name"], row) : '',
          phone: cellValue(worksheet, columnMap["Work"], row) !== undefined ? cellValue(worksheet, columnMap["Work"], row) : '',
          lastName: cellValue(worksheet, columnMap["Contact Name"], row) !== undefined ? cellValue(worksheet, columnMap["Contact Name"], row) : '',
          mobileWork: cellValue(worksheet, columnMap["Mobile"], row) !== undefined ? cellValue(worksheet, columnMap["Mobile"], row) : '',
          otherName: '',
          mobileOther: '',
          employer: cellValue(worksheet, columnMap["Supplier"], row),
          emailWork: cellValue(worksheet, columnMap["E-Mail"], row) !== undefined ? cellValue(worksheet, columnMap["E-Mail"], row) : '',
          jobTitle: cellValue(worksheet, columnMap["Position"], row) !== undefined ? cellValue(worksheet, columnMap["Position"], row) : '',
          emailOther: '',
          mainContact: false
        }

        deferredList.push(
          this.getSupplierContact(supplier, contact)
          .then((data) => {
            return doCheck(this, supplier, contact, data);
          })
          .catch((error) => {
            response.errors.push(error);
            return Promise.resolve(error);
          })
        );
      }
    }

    return Promise.all(deferredList)
      .then((data) => {
        let updates = {};
        Object.keys(updatesExistingSupplier).forEach(element => {
          let item = updatesExistingSupplier[element];
          updates['/suppliers/'+ element] = item;
        });
        Object.keys(updatesSupplier).forEach(element => {
          let itemKey = this.db.database.ref().child('suppliers').push().key;
          let item = updatesSupplier[element];
          updates['/suppliers/'+ itemKey] = item;
        });
        Object.keys(updatesContacts).forEach(element => {
          let item = updatesContacts[element];
          updates['/contacts/'+ element] = item;
        });
        debugger
        console.log(JSON.stringify(updates));
        return this.db.database.ref().update(updates)
        .then((data) => {
          response.models.push(data);
          return Promise.resolve(data);
        })
        .catch((error) => {
          response.errors.push(error);
          return Promise.resolve(error);
        })
      });

    function doCheck(that, supplier, contact, data){

      let supplierArr = data[1];
      let contactArr = data[0];

      let contactKey = "";

      if(contactArr.length){
        contactKey = contactArr[0].$key;    
      }else{
        contactKey = that.db.database.ref().child('contacts').push().key;
      }

      contact.firstName = contact.firstName.split(' ')[0] || '';
      contact.lastName = contact.lastName.split(' ')[1] || '';

      updatesContacts[contactKey] = contact;

      if(updatesSupplierHolder[supplier.companyName] === undefined){
        contactsHolder = {};
        updatesSupplierHolder[supplier.companyName] = true;
        if(supplierArr.length){
          updatesExistingSupplier[supplierArr[0].$key] = supplier;
          updatesExistingSupplier[supplierArr[0].$key].mainContact = contactKey;  
        }else{
          updatesSupplier[supplier.companyName] = supplier;
          updatesSupplier[supplier.companyName].mainContact = contactKey;
        }
        updatesContacts[contactKey].mainContact = true;
      }

      contactsHolder[contactKey] = true;

      if(supplierArr.length){
        updatesExistingSupplier[supplierArr[0].$key].contacts = contactsHolder; 
      }else{
        updatesSupplier[supplier.companyName].contacts = contactsHolder;
      }



  }

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});
      var cell = worksheet[cell_address];
      return cell ? cell.v : undefined;
    }  


  }


  getStockAndSupplier(supplier1, supplier2, supplier3){
    let deferredList = [];

    deferredList.push(this.getstockItemsPromise(supplier1.stockCode));
    deferredList.push(this.getSupplierPromise(supplier1.supplier));

    deferredList.push(this.getstockItemsPromise(supplier2.stockCode));
    deferredList.push(this.getSupplierPromise(supplier2.supplier));

    deferredList.push(this.getstockItemsPromise(supplier3.stockCode));
    deferredList.push(this.getSupplierPromise(supplier3.supplier));

    return Promise.all(deferredList);
  
  }

  loadFromWorkbookForStockDetails(workbook) {

    let deferredList = [];
    
    let response:any = {
      models: [],
      errors:[]
    };

    let worksheet = workbook.Sheets["Stock List"];
    let range = XLSX.utils.decode_range(worksheet["!ref"]);
    let columnMap = {};
    let row = -1;

    while (++row <= range.e.r) {
      for (var col = 0; col <= range.e.c; col++) {
        var heading = cellValue(worksheet, col, row);
        if (heading) {
          columnMap[heading] = col;
        }
      }
      break;
    } 
    
    while (++row <= range.e.r) {

      let item = cellValue(worksheet, 0, row);
      if (item) {

        let supplier1 = {
          stockCode: cellValue(worksheet, columnMap["Stock Code"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Code"], row) : '',
          type: cellValue(worksheet, columnMap["UoM"], row) !== undefined ? cellValue(worksheet, columnMap["UoM"], row) : '',
          supplier: cellValue(worksheet, columnMap["Supplier1"], row) !== undefined ? cellValue(worksheet, columnMap["Supplier1"], row) : '',
          cost: cellValue(worksheet, columnMap["Price1 (£)"], row) !== undefined ? cellValue(worksheet, columnMap["Price1 (£)"], row) : 0,
          count: cellValue(worksheet, columnMap["Order Qty1"], row) !== undefined ? cellValue(worksheet, columnMap["Order Qty1"], row) : 0,
          delTime: cellValue(worksheet, columnMap["Lead Time"], row) !== undefined ? cellValue(worksheet, columnMap["Lead Time"], row) : 0,
          colTime: cellValue(worksheet, columnMap["Lead Time"], row) !== undefined ? cellValue(worksheet, columnMap["Lead Time"], row) : 0,
          unitCost: 0,
          storageWidth: 0,
          storageDepth: 0,
          storageLength: 0,
          status: 'Active',
          dateTimestamp: this.authService.serverTime()
        }

        let supplier2 = {
          stockCode: cellValue(worksheet, columnMap["Stock Code"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Code"], row) : '',
          type: cellValue(worksheet, columnMap["UoM"], row) !== undefined ? cellValue(worksheet, columnMap["UoM"], row) : '',
          supplier: cellValue(worksheet, columnMap["Supplier2"], row) !== undefined ? cellValue(worksheet, columnMap["Supplier2"], row) : '',
          cost: cellValue(worksheet, columnMap["Price2 (£)"], row) !== undefined ? cellValue(worksheet, columnMap["Price2 (£)"], row) : 0,
          count: cellValue(worksheet, columnMap["Order Qty2"], row) !== undefined ? cellValue(worksheet, columnMap["Order Qty2"], row) : 0,
          delTime: cellValue(worksheet, columnMap["Lead Time"], row) !== undefined ? cellValue(worksheet, columnMap["Lead Time"], row) : 0,
          colTime: cellValue(worksheet, columnMap["Lead Time"], row) !== undefined ? cellValue(worksheet, columnMap["Lead Time"], row) : 0,
          unitCost: 0,
          storageWidth: 0,
          storageDepth: 0,
          storageLength: 0,
          status: 'Active',
          dateTimestamp: this.authService.serverTime()
        }

        let supplier3 = {
          stockCode: cellValue(worksheet, columnMap["Stock Code"], row) !== undefined ? cellValue(worksheet, columnMap["Stock Code"], row) : '',
          type: cellValue(worksheet, columnMap["UoM"], row) !== undefined ? cellValue(worksheet, columnMap["UoM"], row) : '',
          supplier: cellValue(worksheet, columnMap["Supplier3"], row) !== undefined ? cellValue(worksheet, columnMap["Supplier3"], row) : '',
          cost: cellValue(worksheet, columnMap["Price3 (£)"], row) !== undefined ? cellValue(worksheet, columnMap["Price3 (£)"], row) : 0,
          count: cellValue(worksheet, columnMap["Order Qty3"], row) !== undefined ? cellValue(worksheet, columnMap["Order Qty3"], row) : 0,
          delTime: cellValue(worksheet, columnMap["Lead Time"], row) !== undefined ? cellValue(worksheet, columnMap["Lead Time"], row) : 0,
          colTime: cellValue(worksheet, columnMap["Lead Time"], row) !== undefined ? cellValue(worksheet, columnMap["Lead Time"], row) : 0,
          unitCost: 0,
          storageWidth: 0,
          storageDepth: 0,
          storageLength: 0,
          status: 'Active',
          dateTimestamp: this.authService.serverTime()
        }

    
        deferredList.push(
          this.getStockAndSupplier(supplier1, supplier2, supplier3)
          .then((data) => {
            return doCheck(this, supplier1, supplier2, supplier3, data);
          })
          .catch((error) => {
            response.errors.push(error);
            return Promise.resolve(error);
          })
        );
      }
    }

    return Promise.all(deferredList)
        .then((data) => {
          response.models.push(data);
          return Promise.resolve(data);
        })
        .catch((error) => {
          response.errors.push(error);
          return Promise.resolve(error);
        });


    function doCheck(that, supplier1, supplier2, supplier3, data){

      let deferredList = [];

      let stockItemKey1 = "";
      let supplierKey1 = "";

      if(data[0].length && data[1].length){
        stockItemKey1 = data[0][0].$key;
        supplierKey1 = data[1][0].$key;
      }

      let stockItemKey2 = "";
      let supplierKey2 = "";

      if(data[2].length && data[3].length){
        stockItemKey2 = data[2][0].$key;
        supplierKey2 = data[3][0].$key;
      }

      let stockItemKey3 = "";
      let supplierKey3 = "";

      if(data[4].length && data[5].length){
        stockItemKey1 = data[4][0].$key;
        supplierKey1 = data[5][0].$key;
      }

      if(stockItemKey1 !== "" && supplierKey1 !== ""){
        delete supplier1.stockCode;
        delete supplier1.supplier;
        deferredList.push(that.supplyChainService.createSupplierStock(stockItemKey1, supplierKey1, supplier1));
      }
      if(stockItemKey2 !== "" && supplierKey2 !== ""){
        delete supplier2.stockCode;
        delete supplier2.supplier;
        deferredList.push(that.supplyChainService.createSupplierStock(stockItemKey2, supplierKey2, supplier2));
      }
      if(stockItemKey3 !== "" && supplierKey3 !== ""){
        delete supplier3.stockCode;
        delete supplier3.supplier;
        deferredList.push(that.supplyChainService.createSupplierStock(stockItemKey3, supplierKey3, supplier3));
      }

      return Promise.all(deferredList);

    }

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});
      var cell = worksheet[cell_address];
      return cell ? cell.v : undefined;
    }  


  }
  


    loadFromWorkbookPanels(workbook) {
      var response = {
        panels: [],
        errors: [],
        components: {},
        areas: {},
      };

      this.updatePanelsSheet  = false;

      if (this.fileType == "text/csv") {

        var allRows = workbook.split(/\r?\n|\r/);
        
        for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
          var rowCells = allRows[singleRow].split(",");
          if (rowCells[1] === "Job Reference") {
            this.updatePanelsSheet = true;
            break;
          }
        }

      } else {

        Object.keys(workbook.Sheets).forEach(worksheetName => {
          let worksheet = workbook.Sheets[worksheetName];
          var range = XLSX.utils.decode_range(worksheet["!ref"]);
          var row = -1;

          while (++row <= range.e.r) {
            if (cellValue(worksheet, 1, row) === "Job Reference") {
              this.updatePanelsSheet = true;
              break;
            }
          }

        });

      }

      if (!this.updatePanelsSheet) {
        return this.loadFromWorksheetsPanels(response, workbook, this.fileType)
          .then(()  => {
            response.components = this.componentsHolder;
            response.areas = this.areasHolder;
            return response;
          });
      } else {
        return this.loadFromWorksheetsPanelUpdate(response, workbook, this.fileType)
          .then(()  => {
            return response;
          });
      }

      function cellValue(worksheet, column, row) {
        var cell_address = XLSX.utils.encode_cell({c: column, r: row});
    
        var cell = worksheet[cell_address];
    
        return cell ? cell.v : undefined;
      }

    }
  

  loadFromWorksheetsPanels(response, workbook, fileType) {
    var deferredList = [];
    var content = [];

    if (fileType == "text/csv") {

      var row = -1;
      var jobSheet = null;

      var allRows = workbook.split(/\r?\n|\r/);

      for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(",");
        if (rowCells[0] === "Jobsheet No.") {
          jobSheet = rowCells[1];
          break;
        }
      }

      for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(",");
        if (rowCells[0] != "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];
            if (rowCells[0] === "Panel Ref") {
              row = singleRow + 1;
              content.push(value);
            }
          }
        }
      }

      for (var singleRow = row; singleRow < allRows.length; singleRow++) {
        
        var rowCells = allRows[singleRow].split(",");
        if (rowCells[0] != "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];
            if (value) {
              switch (content[rowCell]) {
              case "Panel Ref":
                var id = value;
                break;
              case "Type":
                var type = value;
                break;
              case "Length":
                var length = value;
                break;
              case "Height":
                var height = value;
                break;
              case "Depth":
                var width = value;
                break;
              case "Area":
                var area = value;
                break;
              case "Weight":
                var weight = value;
                break;
              case "Framing Style":
                var framingStyle = value;
                break;
              case "Stud Size":
                var studSize = value;
                break;
              case "Sheathing":
                var sheathing = value;
                break;
              case "Components":
                var components = value;
                break;
              case "Nailing":
                var nailing = value;
                break;
              case "Spandrel":
                var spandrel = value;
                break;
              case "Doors":
                var doors = value;
                break;
              case "Windows":
                var windows = value;
                break;
              case "Pockets":
                var pockets = value;
                break;
              case "Qty":
                var qty = value;
                break;
              }
            }
          }
          if (id) {
            deferredList.push(
              this.validatePanel(id, type, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty)
              .then(function (panel) {
                response.panels.push(panel);
              })
              .catch(function (error) {
                response.errors.push(error);
              })
              );
          }
        }
      }


    } else {

      Object.keys(workbook.Sheets).forEach(worksheetName => {
        let worksheet = workbook.Sheets[worksheetName];
        var range = XLSX.utils.decode_range(worksheet["!ref"]);

        var columnMap = {};
        var row = -1;
        var jobSheet = null;

        while (++row <= range.e.r) {
          if (cellValue(worksheet, 0, row) === "Job Reference") {
            jobSheet = cellValue(worksheet, 1, row);
            break;
          }
        }
        
        row = -1;

        while (++row <= range.e.r) {
          if (cellValue(worksheet, 0, row) === "Panel Ref") {
            for (var col = 1; col <= range.e.c; col++) {
              var heading = cellValue(worksheet, col, row);
              if (heading) {
                columnMap[heading] = col;
              }
            }
            break;
          }
        }

        while (++row <= range.e.r) {
          var id = cellValue(worksheet, 0, row);
          if (id) {
            var type = cellValue(worksheet, columnMap["Type"], row);
            var length = cellValue(worksheet, columnMap["Length"], row);
            var height = cellValue(worksheet, columnMap["Height"], row);
            // INN-88 rationalised panel terms. Depth became width,
            // but the imported spreadsheet has a Depth column...
            var width = cellValue(worksheet, columnMap["Depth"], row);
            var area = cellValue(worksheet, columnMap["Area"], row);
            var weight = cellValue(worksheet, columnMap["Weight"], row);
            
            var framingStyle = cellValue(worksheet, columnMap["Framing Style"], row);
            var studSize = cellValue(worksheet, columnMap["Stud Size"], row);
            var sheathing = cellValue(worksheet, columnMap["Sheathing"], row);
            var components = cellValue(worksheet, columnMap["Components"], row);
            var nailing = cellValue(worksheet, columnMap["Nailing"], row);
            var spandrel = cellValue(worksheet, columnMap["Spandrel"], row);
            var doors = cellValue(worksheet, columnMap["Doors"], row);
            var windows = cellValue(worksheet, columnMap["Windows"], row);
            var pockets = cellValue(worksheet, columnMap["Pockets"], row);
            var qty = cellValue(worksheet, columnMap["Qty"], row);

            deferredList.push(
              this.validatePanel(id, type, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty)
              .then(function (panel) {
                response.panels.push(panel);
              })
              .catch(function (error) {
                response.errors.push(error);
              })
              );
          }
        }

      });

    }

    return Promise.all(deferredList);

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});
  
      var cell = worksheet[cell_address];
  
      return cell ? cell.v : undefined;
    }
  }
  
  loadFromWorksheetsPanelUpdate(response, workbook, fileType) {
    var deferredList = [];

    if (fileType == "text/csv") {

      var allRows = workbook.split(/\r?\n|\r/);
      var content = [];
      var row = -1;
      
      for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(",");
        if (rowCells[0] != "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];
            if (rowCells[0] === "Panel Ref") {
              row = singleRow + 1;
              content.push(value);
            }
          }
        }
      }
      
      for (var singleRow = row; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(",");
        if (rowCells[0] !== "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];
            if (value) {
              switch (content[rowCell]) {
              case "Panel Ref":
                var id = value;
                break;
              case "Jobsheet No.":
                var jobSheet = value;
                break;
              }
            }
          }
          if (id) {
            var jobSheetParts = jobSheet.split("-");
            var jobSheetString = jobSheetParts[5];
            var jobSheetNumber = parseInt(jobSheetString);
            deferredList.push(
              this.panelExists(id, jobSheetNumber)
              .then(this.getProductTypeName)
              .then(function (panel) {
                return this.validatePanel(panel.id, panel.productType, panel.length, panel.height, panel.width, panel.area, panel.weight, panel.jobSheetNumber, panel.framingStyle, panel.studSize, panel.sheathing, panel.components, panel.nailing, panel.spandrel, panel.doors, panel.windows, panel.pockets, panel.qty);
              })
              .then(function (panel) {
                response.panels.push(panel);
              })
              .catch(function (error) {
                response.errors.push(error);
              })
              );
          }
        }
      }
      

    } else {

      Object.keys(workbook.Sheets).forEach(worksheetName => {
        let worksheet = workbook.Sheets[worksheetName];
        var range = XLSX.utils.decode_range(worksheet["!ref"]);

        var columnMap = {};

        var row = -1;

        while (++row <= range.e.r) {
          if (cellValue(worksheet, 0, row) === "Panel Ref") {
            for (var col = 1; col <= range.e.c; col++) {
              var heading = cellValue(worksheet, col, row);
              if (heading) {
                columnMap[heading] = col;
              }
            }
            break;
          }
        }

        while (++row <= range.e.r) {
          var id = cellValue(worksheet, 0, row);
          if (id) {
            var jobSheet = cellValue(worksheet, columnMap["Jobsheet No."], row);
            var jobSheetParts = jobSheet.split("-");
            var jobSheetString = jobSheetParts[5];
            var jobSheetNumber = parseInt(jobSheetString);
            deferredList.push(
              this.panelExists(id, jobSheetNumber)
              .then(this.getProductTypeName)
              .then(function (panel) {
                return this.validatePanel(panel.id, panel.productType, panel.length, panel.height, panel.width, panel.area, panel.weight, panel.jobSheetNumber, panel.framingStyle, panel.studSize, panel.sheathing, panel.components, panel.nailing, panel.spandrel, panel.doors, panel.windows, panel.pockets, panel.qty);
              })
              .then(function (panel) {
                response.panels.push(panel);
              })
              .catch(function (error) {
                response.errors.push(error);
              })
              );
          }
        }

      });

    }

    return Promise.all(deferredList);

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});
  
      var cell = worksheet[cell_address];
  
      return cell ? cell.v : undefined;
    }
  }
  
  panelExists(id, jobSheetNumber) {

    var panel = {
      id: id,
      productType: null,
      length: null,
      height: null,
      width: null,
      area: null,
      weight: null,
      framingStyle: null,
      studSize: null,
      sheathing: null,
      components: null,
      nailing: null,
      spandrel: null,
      doors: null,
      windows: null,
      pockets: null,
      qty: null,
      dimensions: null,
      project: null,
      jobSheetNumber: null,
    };


    return this.panelsService.getPanelsByIdPromise(panel.id)
      .then(function (matchPanel) {
        if (matchPanel.length > 0) {
          panel.productType = matchPanel[0].type;
          panel.length = matchPanel[0].dimensions.length;
          panel.height = matchPanel[0].dimensions.height;
          panel.width = matchPanel[0].dimensions.width;
          panel.area = matchPanel[0].dimensions.area;
          panel.weight = matchPanel[0].dimensions.weight;
          panel.framingStyle = matchPanel[0].additionalInfo.framingStyle;
          panel.studSize = matchPanel[0].additionalInfo.studSize;
          panel.sheathing = matchPanel[0].additionalInfo.sheathing;
          panel.components = matchPanel[0].additionalInfo.components;
          panel.nailing = matchPanel[0].additionalInfo.nailing;
          panel.spandrel = matchPanel[0].additionalInfo.spandrel;
          panel.doors = matchPanel[0].additionalInfo.doors;
          panel.windows = matchPanel[0].additionalInfo.windows;
          panel.pockets = matchPanel[0].additionalInfo.pockets;
          panel.qty = matchPanel[0].additionalInfo.qty;
          panel.jobSheetNumber = jobSheetNumber;
          return Promise.resolve(panel);
        } else {
          return Promise.reject("Unknown panel '" + panel.id + "'");
        }
      })
      .catch(function (error) {
        return Promise.reject(id + " " + error);
      });

  }

  getProductTypeName(panel) {

    var found;
    Object.keys(this.productTypes).forEach((objectName)=>{
      let object = this.productTypes[objectName];
      if (panel.productType === object.$id) {
        found = object;
      }
    });
    if (found) {
      panel.productType = found.name;
      return Promise.resolve(panel);
    } else {
      return Promise.reject("Unknown type '" + panel.type + "'");
    }

  }

  
  validatePanel(id, productType, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty) {
    
    // Temporary values during processing
    var projectId;
    var phase;
    var floor;
    var panelType;
    var panel = {
      id: id,
      area: null,
      dimensions: null,
      type: null,
      project: null,
      additionalInfo: null
    };
    var areaByRef = {};
    var areas = [];

    return validatePanelReference(this)
      .then(resolveProject.bind(this))
      .then(validatePhase.bind(this))
      .then(validateFloor.bind(this))
      .then(validatePanelType.bind(this))
      .then(validateProductType.bind(this))
      .then(resolveAreas.bind(this))
      .then(resolveComponent.bind(this))
      .then(resolveDimensions)
      .then(resolveAdditionalInfo)
      .then(function () {
        return Promise.resolve(panel);
      })
      .catch(function (error) {
        return Promise.reject(id + " " + error);
      });

    function validatePanelReference(that) {
      var idParts = panel.id.split("-");
      if (idParts) {
        projectId = idParts[0];
        phase = idParts[2];
        floor = idParts[3];
        panelType = idParts[4];
        
        return Promise.resolve();
      }
      else {
        return Promise.reject("Invalid panel reference");
      }
    }

    function resolveDimensions() {
      var dimensions = {
        length: parseInt(length) || null,
        height: parseInt(height) || null,
        width: parseInt(width) || null,
        area: parseFloat(area) || null,
        weight: parseInt(weight) || null
      };
      if (!dimensions.length || !dimensions.height || !dimensions.width || !dimensions.area || !dimensions.weight) {
        return Promise.reject("Missing dimension information");
      }
      else {
        panel.dimensions = dimensions;
        return Promise.resolve();
      }
    }
    
    function resolveAdditionalInfo() {
      var additionalInfo = {
        framingStyle: String(framingStyle) || null,
        studSize: String(studSize) || null,
        sheathing: String(sheathing) || null,
        components: parseInt(components) || null,
        nailing: String(nailing) || null,
        spandrel: String(spandrel) || null,
        doors: parseInt(doors) || null,
        windows: parseInt(windows) || null,
        pockets: parseInt(pockets) || null,
        qty: parseInt(qty) || null
      };
      if (additionalInfo.framingStyle === "undefined") {
        return Promise.reject("Missing additional information");
      }
      else {
        panel.additionalInfo = additionalInfo;
        return Promise.resolve();
      }
    }

    function resolveProject() {

      return this.projectsService.getProjectsListPromise()
        .then((projects) => {
          var found;
          projects.forEach(project => {
            if (projectId === project.id) {
              found = project;
            }
          });
          if (found) {
            panel.project = found.$key;
            return Promise.resolve();
          }
          else {
            return Promise.reject("Unknown project '" + projectId + "'");
          }
        });
    }

    function validatePhase() {
      return validateReferenceDataKey(this.phases, phase, "phase");
    }

    function validateFloor() {
      return validateReferenceDataKey(this.floors, floor, "floor");
    }

    function validatePanelType() {
      return validateReferenceDataKey(this.panelTypes, panelType, "panel type");
    }

    function validateReferenceDataKey(referenceData, key, description) {
      let found = 0;
      Object.keys(referenceData).forEach((keyNumber)=>{
        if(keyNumber === key){
          found = 1;
        }
      });

      if (found) {
        Promise.resolve();
      }
      else {
        Promise.reject("Unknown " + description + " '" + key + "'");
      }
    }

    function validateProductType() {

      var found;
      if(this.productTypeProductionLine === undefined){
        this.productTypeProductionLine = this.buildMap(this.productTypes, "productionLine");
      }
      Object.keys(this.productTypes).forEach((objectKey)=>{
        let value = this.productTypes[objectKey].name;
        if(productType === value){
          found = objectKey;
        }
      });
      if (found) {
        panel.type = this.dateUtilService.uploaderLogicHSIP(found, panelType, height, length);

        Promise.resolve();
      }
      else {
        Promise.reject("Unknown type '" + productType + "'");
      }


    }
    
    
    function resolveAreas() {

      return this.areasService.getAreasForProjectPromise(panel.project)
        .then((projectAreas) => {
          this.areasHolder = {};
          projectAreas.forEach(object => {
            if(areaByRef[object.$key] === undefined){
              areaByRef[object.$key] = object;
            }
            if(this.areasHolder[object.$key] === undefined){
              this.areasHolder[object.$key] = object;
            }
            if (phase === object.phase
              && floor === object.floor
              && panel.type === object.type) {
              areas.push(object.$key);
            }
          });
          if (areas.length) {
            if (areas.length === 1) {
              panel.area = areas[0];
            }
            return Promise.resolve();
          }
          else {
            return Promise.reject("Unknown area '" + phase + "-" + floor + "-" + panel.type + "'");
          }
        });
    }
    
    function resolveComponent() {
      var found = 0;
      if (!this.updatePanelsSheet) {
        return this.componentsService.getcomponentsforprojectPromise(panel.project)
          .then((projectComponents) => {
            this.componentsHolder = {};
            projectComponents.forEach(object => {
  
              Object.keys(object.areas).forEach((areaKey) => {
                found++;
                if (areas.length > 1) {
                  for (var i = 0; i < areas.length; i++) {
                    var area = areas[i];
                    if (area === areaKey) {
                      var areaObj = areaByRef[areaKey];
                      var nameHolder = projectId + "-" + areaObj.phase + "-" + areaObj.floor + "-" + areaObj.type;
                      if (this.componentsHolder[nameHolder] === undefined) {
                        this.componentsHolder[nameHolder] = {
                          "selected": false,
                          "type": {}
                        };
                      }
                      if (this.componentsHolder[nameHolder]["type"][object.type] === undefined) {
                        this.componentsHolder[nameHolder]["type"][object.type] = {};
                      }
                      if (this.componentsHolder[nameHolder]["type"][object.type][areaKey] === undefined) {
                        this.componentsHolder[nameHolder]["type"][object.type][areaKey] = {
                          areaObj: {}
                        };
                      }
                      this.componentsHolder[nameHolder]["type"][object.type][areaKey].areaObj = areaObj;
                    }
                  }
                }
              });
            });
            areas = [];
            return Promise.resolve();
          });
      } else {
        return Promise.resolve();
      }
    }
    
  }
  
  buildMap(items, property) {
    var map = {};
    Object.keys(items).forEach(itemKey => {
      let item = items[itemKey];
      map[itemKey] = item[property];
    });
    return map;
  }

  incrimentDesignCount(result){
      
    var panels = result;
    var date = this.dateUtilService.todayAsIso();
    var promises = [];

    panels.forEach(panel => {
      promises.push(this.incriment(panel,date));
    });
    return Promise.all(promises).then((data) => {
      return data;
    });
  }

  loadApiPanels(workbook) {

    var response = {

      panels: [],

      errors: [],

      components: {},

      areas: {},

    };

    return this.loadFromApi(response, workbook)

      .then(()  => {

        response.components = this.componentsHolder;

        response.areas = this.areasHolder;

        return response;

      });

  }

  loadFromApi(response, apiData) {

    var deferredList = [];

      apiData.forEach(panel => {

        var id = panel["Panel_ref"];

        if (id) {

          var type = panel["Type"];

          var length = panel["Length"];

          var height = panel["Height"];

          var width = panel["Depth"];

          var area = panel["Area"];

          var weight = panel["Weight"];

          

          var framingStyle = panel["Panel_Framing_Style"];

          var studSize = panel["Studsize"];

          var sheathing = panel["Sheathing"];

          var components = panel["Components"];

          var nailing = panel["Nailing"];

          var spandrel = panel["Spandrel"];

          var doors = panel["Doors"];

          var windows = panel["Windows"];

          var pockets = panel["Pockets"];

          var qty = panel["Qty"];

          var jobSheet = "0";

          deferredList.push(

            this.validatePanel(id, type, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty)

            .then(function (panel) {

              response.panels.push(panel);

            })

            .catch(function (error) {

              response.errors.push(error);

            })

            );

        }

    

      });

    return Promise.all(deferredList);

  }
   
  incriment(panel, date) {

      var productionLines = this.buildMap(this.productTypes, "productionLine");
      var countRef = this.db.database.ref().child("stats").child(productionLines[panel.type]).child(date).child("designedCount");
      var m2Ref = this.db.database.ref().child("stats").child(productionLines[panel.type]).child(date).child("designedM2");

      var m2 = Math.round(panel.dimensions.area * 10) / 10;
      return countRef.transaction((current) => {
        return (current || 0) + 1;
      }).then(() => {
        return m2Ref.transaction(function (current) {
          return (current || 0) + m2;
        });
      });
    
  }

  averageMeterSquareOfDatum() {
      
    var projectRefsOfDatums = {};

    return this.projectsService.getProjectsListPromise()
      .then(projectRefsOfDatumTypes.bind(this))
      .then(panelsForProjects.bind(this))
      .then(panelsAverage.bind(this))
      .then(function () {
        return Promise.resolve(projectRefsOfDatums);
      });


    function projectRefsOfDatumTypes(projects) {

      var productionLines = this.buildMap(this.productTypes, "productionLine");

      Object.keys(this.datumTypes).forEach(datumKey => { 
        let datum = this.datumTypes[datumKey];
        var datumHolder = projectRefsOfDatums[datumKey];
        if (datumHolder === undefined) {  
          datumHolder = projectRefsOfDatums[datumKey] = {
            projectRefs: [],
            name:"",
            productionLines:{}
          };
        }
        
        Object.keys(productionLines).forEach(productionLine => {
          let productionLineKey = productionLines[productionLine];
          if(datumHolder.productionLines[productionLineKey] === undefined && productionLineKey !== undefined) {
            datumHolder.productionLines[productionLineKey] = {
              panelCount: 0,
              panelAreaTotal: 0,
              panelAvg: 0,
            };
          }
        });
        
        projects.forEach(project => {
          var projectDatumTypes = project.datumType;
          if (projectDatumTypes && projectDatumTypes === datumKey) {
            datumHolder.projectRefs.push(project.$key);
          }
        });
      });
    }
    
    function panelsForProjects() {
      var promises = [];
      var productTypeProductionLine = this.buildMap(this.productTypes, "productionLine");
      Object.keys(projectRefsOfDatums).forEach((datumKey) => {
        let datum = projectRefsOfDatums[datumKey];
        var datumProjectRefs = datum.projectRefs;
        var datumHolder = projectRefsOfDatums[datumKey];
        if (datumProjectRefs.length > 0) {
          datumProjectRefs.forEach(projectRef => {
            promises.push(
              this.panelsService.getPanelsForProjectPromise(projectRef)
              .then((panels) => {
                panels.forEach(panel => {
                  var type = panel.type;
                  var panelType = null;
                  //Identify If ExtH
                  var idParts = panel.id.split("-");
                  if (idParts) {
                    panelType = idParts[4];
                  }
                  type =  this.dateUtilService.logicHSIP(type, panelType, panel.dimensions.height, panel.dimensions.length)
                  var productionLineKey = productTypeProductionLine[type];
                  if (productionLineKey !== undefined) {
                    var productionLine = datumHolder.productionLines[productionLineKey];
                    productionLine.panelCount++;
                    productionLine.panelAreaTotal += panel.dimensions.area;
                  }
                });
              })
              );
          });
        }
      });
      return Promise.all(promises);
    }
    
    function panelsAverage() {
      var promises = [];
      Object.keys(projectRefsOfDatums).forEach((datumKey) => {
        let datum = projectRefsOfDatums[datumKey];
        var datumProjectRefs = datum.projectRefs;
        delete datum.projectRefs;
        datum.name = datumKey;
        if (datumProjectRefs.length > 0) {
          var productionLines = this.buildMap(this.productTypes, "productionLine");
          Object.keys(productionLines).forEach(productionLine => {  
            let productionLineKey = productionLines[productionLine];
            var datumForProductionLines = datum.productionLines[productionLineKey];
            if (productionLineKey !== undefined && datumForProductionLines.panelCount > 0) {
              datumForProductionLines.panelAreaTotal = Math.round(datumForProductionLines.panelAreaTotal);
              datumForProductionLines.panelAvg = Math.round(datumForProductionLines.panelAreaTotal / datumForProductionLines.panelCount);
            }
          });
          promises.push(
            this.updateDatum(datumKey, datum)
            );
        }
      });
      return Promise.all(promises);
      
    }

  }

  updateDatum(datumName, datumData) {
      
   return this.damumByName(datumName)
      .then(updateDatum.bind(this));

    function updateDatum(existingDatums) {
      var existing;
      existingDatums.forEach(d => {
        if (d.name === datumData.name) {
          existing = d;
        }
      });

      if (existing) {
        Object.assign(existing, datumData);
        return this.updateTheDatum(existing.$key, existing);
      } else {
        var newDatumData = Object.assign({}, datumData);
        return this.createDatum(newDatumData);
      }
    }
  }

    
  getDatums() {
    const path = "/datums";
    return this.db.list(path).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }
  
  damumByName(datumName) {
    const path = "/datums";
    return this.db.list(path, areasRef => areasRef.orderByChild('name').equalTo(datumName)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    })).pipe(first()).toPromise();
  }

  updateTheDatum(quoteKey, obj){
    if(obj.$key !== undefined){
      delete obj.$key;
    }
    const path = "/datums";
    return this.db.list(path).update(quoteKey, obj);
  }

  createDatum(obj){
    const path = "/datums";
    return this.db.list(path).push(obj);
  }
  
}
  


