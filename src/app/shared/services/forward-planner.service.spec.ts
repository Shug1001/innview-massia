import { TestBed, getTestBed, inject } from "@angular/core/testing";
import { AngularFireDatabaseModule, AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { ForwardPlannerService } from './forward-planner.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of, Observable } from "rxjs";
import { panel, area,Project } from "../models/models";
import { MockFirebase } from "firebase-mock";
import { ProjectsService } from "./projects.service";
import { AreasService } from "./areas.service";

describe('ForwardPlannerService', () => {
  let angularFireDatabaseMock: any;
  let fbListMock: any;
  let fbObjectMock: any;
  let data:any;
  let avgData:any;
  let projectsServiceMock: any;
  let areasServiceMock: any;
  let httpMock: HttpTestingController;
  let service: ForwardPlannerService;
  let firebaseRoot;
  let projectsRef;
  let areasRef;
  let helper: Helper;
  
  beforeEach(() => {
    helper = new Helper();

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");

    helper.addQuerySupport(projectsRef);
    helper.addQuerySupport(areasRef);

    //mocking for this service
    angularFireDatabaseMock = jasmine.createSpyObj('AngularFireDatabase', ['list', 'object']);

    fbListMock = jasmine.createSpyObj('list', ['snapshotChanges', 'valueChanges']);
    fbObjectMock = jasmine.createSpyObj('object', ['snapshotChanges', 'valueChanges']);

    angularFireDatabaseMock.list.and.returnValue(fbListMock);
    angularFireDatabaseMock.object.and.returnValue(fbObjectMock);

    fbListMock.snapshotChanges.and.returnValue(of([]));
    fbObjectMock.snapshotChanges.and.returnValue(of([]));

    //mocking for dependancy services
    projectsServiceMock = jasmine.createSpyObj('projectsServiceMock', ['getActiveProjectsList']);
    projectsServiceMock.getActiveProjectsList.and.callFake(function(){
      let ref = projectsRef.orderByChild("active").equalTo(true);
      let data = ref.getData();
      return of(helper.getMappedValue(data));
    });

    areasServiceMock = jasmine.createSpyObj('areasServiceMock', ['getAreasForProject']);
    areasServiceMock.getAreasForProject.and.callFake(function(projectKey){
      let ref = areasRef.orderByChild("project").equalTo(projectKey);
      let data = ref.getData();
      return of(helper.getSnapShotChanges(data, false));
    });

    TestBed.configureTestingModule({
      imports: [
        AngularFireDatabaseModule,
        HttpClientTestingModule
      ],
      providers: [
        {provide: AngularFireDatabase, useValue: angularFireDatabaseMock},
        {provide: ProjectsService, useValue: projectsServiceMock},
        {provide: AreasService, useValue: areasServiceMock}
      ]
    });
    httpMock = getTestBed().get(HttpTestingController);
    service = TestBed.get(ForwardPlannerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getActiveAreas', () => {

    beforeEach(() => {
      projectsRef.set(
        {
          "PROJ1": {
            "id": "001",
            "chainOfCustody":"PEFC",
            "siteAccess":"Rigid",
            "deliverySchedule": {
              "published": "PROJ1REV1",
              "revisions": {
                "PROJ1REV0": true,
                "PROJ1REV1": true,
                "PROJ1REV2":true
              },
              "unpublished": "PROJ1REV2"
            },
            "datumType": "Education Student Accommodation",
            "active": true
          },
          "PROJ2": {
            "id": "002",
            "chainOfCustody":"PEFC",
            "siteAccess":"Rigid",
            "deliverySchedule": {
              "published": "PROJ2REV1",
              "revisions": {
                "PROJ2REV0": true,
                "PROJ2REV1": true,
                "PROJ2REV2":true
              },
              "unpublished": "PROJ2REV2"
            },
            "datumType": "Education Student Accommodation",
            "active": false
          }
        }
      );
      areasRef.set({
        "AREA1": {
          "floor": "GF",
          "phase": "P1",
          "type": "Ext",
          "project": "PROJ1",
          "estimatedArea": 50,
          "estimatedPanels": 20,
          "actualArea": 60,
          "actualPanels": 15,
          "completedArea": 95,
          "completedPanels": 14,
          "supplier": "Innovaré",
          "revisions": {
            "PROJ1REV0": "2018-11-20",
            "PROJ1REV1": "2018-11-20",
            "PROJ1REV2": "2018-11-21"
          },
          "timestamps": {
            "created": new Date("2018-11-20").getTime(),
            "modified": new Date("2018-11-20").getTime()
          }
        },
        "AREA2": {
          "floor": "GF",
          "phase": "P2",
          "type": "Ext",
          "project": "PROJ1",
          "estimatedArea": 50,
          "estimatedPanels": 20,
          "actualArea": 60,
          "actualPanels": 15,
          "completedArea": 95,
          "completedPanels": 14,
          "supplier": "Innovaré",
          "revisions": {
            "PROJ1REV0": "2018-11-20",
            "PROJ1REV1": "2018-11-20",
            "PROJ1REV2": "2018-11-21"
          },
          "timestamps": {
            "created": new Date("2018-11-20").getTime(),
            "modified": new Date("2018-11-20").getTime()
          }
        }
      });
    });

    it('should call getActiveAreas return all capacities with correct properties', () => {    
        service.getActiveAreas().subscribe( areas => {
          expect(areas[0]).toEqual({
            actualArea: 60,
            actualPanels: 15,
            completedArea: 95,
            completedPanels: 14,
            estimatedArea: 50,
            estimatedPanels: 20,
            floor: 'GF',
            phase: 'P1',
            project: 'PROJ1',
            revisions: { PROJ1REV0: '2018-11-20', PROJ1REV1: '2018-11-20', PROJ1REV2: '2018-11-21' },
            supplier: 'Innovaré',
            timestamps: { created: 1542672000000, modified: 1542672000000 },
            type: 'Ext',
            $key: "AREA1",
            _productionLine: 'SIP',
            _line: 1,
            _projectDetails: { id: '001', active: true, chainOfCustody: 'PEFC', datumType: 'Education Student Accommodation', deliverySchedule: { published: 'PROJ1REV1', revisions: { PROJ1REV0: true, PROJ1REV1: true, PROJ1REV2: true }, unpublished: 'PROJ1REV2' }, siteAccess: 'Rigid', $key: 'PROJ1' },
            _stats: { type: 'actual', count: 15, display: 15, percent: 93, oneTrue: 'actual', completed: 14 },
            _deliveryDate: '2018-11-20',
            _hasUnpublished: true,
            _hasUnpublishedOnly: false,
            _id: '001-P1-GF-Ext',
            _projId: "001",
            _dispatchDate: '2018-11-19',
            _sortKey: 0,
            _riskDate: '2018-11-16',
            _designHandoverDate: '2018-10-09',
            _unpublishedDeliveryDate: '2018-11-21',
            _unpublishedDispatchDate: '2018-11-20',
            _unpublishedRiskDate: '2018-11-19',
            _unpublishedDesignHandoverDate: '2018-10-10',
          });
          expect(areas[1]).toEqual({
            "floor": "GF",
            "phase": "P2",
            "type": "Ext",
            "project": "PROJ1",
            "estimatedArea": 50,
            "estimatedPanels": 20,
            "actualArea": 60,
            "actualPanels": 15,
            "completedArea": 95,
            "completedPanels": 14,
            "supplier": "Innovaré",
            "revisions": { "PROJ1REV0": "2018-11-20", "PROJ1REV1": "2018-11-20", "PROJ1REV2": "2018-11-21" },
            "timestamps": { "created": new Date("2018-11-20").getTime(), "modified": new Date("2018-11-20").getTime() },
            $key: "AREA2",
            _productionLine: 'SIP',
            _line: 1,
            _projectDetails: { id: '001', active: true, chainOfCustody: 'PEFC', datumType: 'Education Student Accommodation', deliverySchedule: { published: 'PROJ1REV1', revisions: { PROJ1REV0: true, PROJ1REV1: true, PROJ1REV2: true }, unpublished: 'PROJ1REV2' }, siteAccess: 'Rigid', $key: 'PROJ1' },
            _stats: { type: 'actual', count: 15, display: 15, percent: 93, oneTrue: 'actual', completed: 14 },
            _deliveryDate: '2018-11-20',
            _hasUnpublished: true,
            _hasUnpublishedOnly: false,
            _id: '001-P2-GF-Ext',
            _projId: "001",
            _dispatchDate: '2018-11-19',
            _sortKey: 1,
            _riskDate: '2018-11-16',
            _designHandoverDate: '2018-10-09',
            _unpublishedDeliveryDate: '2018-11-21',
            _unpublishedDispatchDate: '2018-11-20',
            _unpublishedRiskDate: '2018-11-19',
            _unpublishedDesignHandoverDate: '2018-10-10',
          });
        })
      });
  });

  describe('getCapacitiesCalls', () => {
    beforeEach(() => {
      service.getCapacitiesFromToday();
    });

    it('should call list 1 time on AngularFirebase service', () => {
      expect(angularFireDatabaseMock.list).toHaveBeenCalledTimes(3);
    });

    it('should call snapshotChanges 1 time on AngularFirebase service', () => {
      expect(fbListMock.snapshotChanges).toHaveBeenCalledTimes(1);
    });

    it('should call getCapacities return all capacities with correct properties', () => {

        var capacities = {
          "SIP" : {
            "2017-08-07" : 1,
            "2017-08-08" : 0
          },
          "SIP2" : {
            "2018-03-05" : 0,
            "2018-03-07" : 20
          },
          "HSIP" : {
            "2018-03-05" : 0,
            "2018-03-07" : 20
          },
          "TF" : {
            "2018-03-05" : 0,
            "2018-03-07" : 20
          },
          "TF2" : {
            "2018-03-05" : 0,
            "2018-03-07" : 20
          },
          "CASS" : {
            "2018-03-05" : 0,
            "2018-03-07" : 20
          }
        }
        fbListMock.snapshotChanges.and.returnValue(helper.getSnapShotChanges(capacities, true));
        service.getCapacitiesFromToday().subscribe( capacities => {
          expect(capacities[0].payload.val()).toEqual({"2017-08-07": 1, "2017-08-08": 0});
        })
    });
  });

  describe('should create Date Updates', () =>{
    beforeEach(() => {
      var panel1:panel = {
        "area": "AREA1",
        "id": "001-DA-P1-GF-I-001",
        "project": "PROJ1",
        "type": "Ext",
        "dimensions": {
          "area": 0.5,
          "width": 1062,
          "height": 430,
          "length": 6100,
          "weight": 19
        },
        "timestamps": {
          "created": new Date("2016-04-20T12:13:00").getTime(),
          "modified": new Date("2016-04-20T12:13:00").getTime()
        }
      };

      data = service.dateUpdate('2018-09-12',panel1);
    })
    it("should set panel to date", () =>{
      expect(data).toEqual({
        '2018-09-12':{
          "area": "AREA1",
          "id": "001-DA-P1-GF-I-001",
          "project": "PROJ1",
          "type": "Ext",
          "dimensions": {
            "area": 0.5,
            "width": 1062,
            "height": 430,
            "length": 6100,
            "weight": 19
          },
          "timestamps": {
            "created": new Date("2016-04-20T12:13:00").getTime(),
            "modified": new Date("2016-04-20T12:13:00").getTime()
          }
        }
      })
    })

  })

  describe("should get average metre squared", () =>{
   
    beforeEach(() => {
      
      var area1: area = {
        "$id": "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 60,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        }
      }
      avgData = service.avgM2(area1,5);
    });
    it("should calculate correct avg m2", () =>{
      expect(avgData).toEqual(20);
    });
  });
  describe("should get panels count", () =>{
   
    beforeEach(() => {
      
      var area1: area = {
        "$id": "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 60,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        }
      }
      avgData = service.getPanelsCount(area1);
    });
    it("should calculate correct avg m2", () =>{
      expect(avgData).toEqual({"type":"actual","count":15,"display":15,"percent":93,"oneTrue":"actual","completed":14});
    });
  });
  describe("should build plan areas", () =>{

    beforeEach(() =>{

      var area1: area = {
        "$id": "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 60,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        }
      }
      var project1:Project = {
        $key: "fdfs544554gfdfgdg",
        "specialRequirements": "none",
        "name": "Project1",
        "id": "001",  
        "active":true,
        "chainOfCustody":"PEFC",
        "client":"Bob",
        "siteAccess":"Rigid",
        "postCode": "CV21 3HB",
        "nextDeliveryDate": "2017-05-05",
        "siteStart": "2017-05-05",
        "includeSaturday": true,
        "siteAddress": "27 Charlotte Street",
        "deliveryManager": {
          "email" : "nicholas.clarke@innovaresystems.co.uk",
          "id" : "c926461e-d30f-411c-ac43-c2d639709335",
          "mobile" : "07736597280",
          "name" : "Nicholas Clarke"
        },
        "estimatedAreas": {
          "Ext": 500,
          "ExtF": 500,
          "ExtH": 500,
          "Floor": 500,
          "Int": 500,
          "Roof": 500
        },
        "gateway": "G6",
        "weekDuration": 7,
        "notifyUsers": "James",
        "deliverySchedule": {
          "published": "PROJ1REV5",
          "revisions": {
            "PROJ1REV0": true,
            "PROJ1REV1": true,
            "PROJ1REV2": true,
            "PROJ1REV3": true,
            "PROJ1REV4": true,
            "PROJ1REV5": true
          },
          "unpublished": "PROJ1REV6"
        },
        "datumType": "Education Student Accommodation"
      }  



      data = service.getAreaId(area1,project1);
    });
    it("should call get area by id",() =>{
      expect(data).toEqual("001-P1-GF-Ext");
    });
  })
  describe("should Area Filter",() =>{
    beforeEach(() =>{
      var area1: area = {
        "$id": "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 60,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        }
      }
      data = service.extendAreaFilter(area1);
    })
    it("should extend ",() =>{
      expect(data).toEqual({
          "$id": "AREA1",
          "floor": "GF",
          "phase": "P1",
          "type": "Ext",
          "project": "PROJ1",
          "estimatedArea": 50,
          "estimatedPanels": 20,
          "actualArea": 60,
          "actualPanels": 15,
          "completedArea": 95,
          "completedPanels": 14,
          "supplier": "Innovaré",
          "revisions": {
            "PROJ1REV0": "2018-11-20",
            "PROJ1REV1": "2018-11-20"
          },
          "timestamps": {
            "created": new Date("2018-11-20").getTime(),
            "modified": new Date("2018-11-20").getTime()
          }
        
      })
    })
  })

  describe("should extend Area",() =>{
    beforeEach(() =>{

      var area1: area = {
        "$id": "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 60,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20",
          "PROJ1REV2": "2018-11-21"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        }
      }
      var area2: area = {
        "$id": "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ2",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 60,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ2REV0": "2018-11-20",
          "PROJ2REV1": "2018-11-20",
          "PROJ2REV2": "2018-11-21",
          "PROJ2REV3": "2018-11-25"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        }
      }
      var project1:Project = {
        $key: "fdfs544554gfdfgdg",
        "specialRequirements": "none",
        "name": "Project1",
        "id": "001",  
        "active":true,
        "chainOfCustody":"PEFC",
        "client":"Bob",
        "siteAccess":"Rigid",
        "postCode": "CV21 3HB",
        "nextDeliveryDate": "2017-05-05",
        "siteStart": "2017-05-05",
        "includeSaturday": true,
        "siteAddress": "27 Charlotte Street",
        "deliveryManager": {
          "email" : "nicholas.clarke@innovaresystems.co.uk",
          "id" : "c926461e-d30f-411c-ac43-c2d639709335",
          "mobile" : "07736597280",
          "name" : "Nicholas Clarke"
        },
        "estimatedAreas": {
          "Ext": 500,
          "ExtF": 500,
          "ExtH": 500,
          "Floor": 500,
          "Int": 500,
          "Roof": 500
        },
        "gateway": "G6",
        "weekDuration": 7,
        "notifyUsers": "James",
        "deliverySchedule": {
          "published": "PROJ1REV2",
          "revisions": {
            "PROJ1REV0": true,
            "PROJ1REV1": true,
            "PROJ1REV2": true,
            "PROJ1REV3": true,
            "PROJ1REV4": true,
            "PROJ1REV5": true,
          },
          "unpublished": "PROJ1REV5"
        },
        "datumType": "Education Student Accommodation"
      }        
      var project2:Project = {
        $key: "fdfs544554gfdfgdg",
        "specialRequirements": "none",
        "name": "Project2",
        "id": "002",  
        "active":true,
        "chainOfCustody":"PEFC",
        "client":"Bob",
        "siteAccess":"Rigid",
        "postCode": "CV21 3HB",
        "nextDeliveryDate": "2017-05-05",
        "siteStart": "2017-05-05",
        "includeSaturday": true,
        "siteAddress": "27 Charlotte Street",
        "deliveryManager": {
          "email" : "nicholas.clarke@innovaresystems.co.uk",
          "id" : "c926461e-d30f-411c-ac43-c2d639709335",
          "mobile" : "07736597280",
          "name" : "Nicholas Clarke"
        },
        "estimatedAreas": {
          "Ext": 500,
          "ExtF": 500,
          "ExtH": 500,
          "Floor": 500,
          "Int": 500,
          "Roof": 500
        },
        "gateway": "G6",
        "weekDuration": 7,
        "notifyUsers": "James",
        "deliverySchedule": {
          "published": "PROJ2REV2",
          "revisions": {
            "PROJ2REV0": true,
            "PROJ2REV1": true,
            "PROJ2REV2": true,
            "PROJ2REV3": true,
          },
          "unpublished": "PROJ2REV3"
        },
        "datumType": "Education Student Accommodation"
      }  

      var areaKey = "AREA1";
      avgData = service.extendArea(project2,area2,areaKey);
      data = service.extendArea(project1,area1,areaKey);
    })
    it("extends area with unpublished data",() =>{
      expect(data).toEqual(
        {
          "$id": 'AREA1',
          "floor": 'GF',
          "phase": 'P1',
          "type": 'Ext',
          "project": 'PROJ1',
          "estimatedArea": 50,
          "estimatedPanels": 20,
          "actualArea": 60,
          "actualPanels": 15,
          "completedArea": 95,
          "completedPanels": 14,
          "supplier": 'Innovaré',
          "revisions": { 
            "PROJ1REV0": "2018-11-20",
            "PROJ1REV1": "2018-11-20",
            "PROJ1REV2": "2018-11-21"
          },
          "timestamps": {
             "created": 1542672000000,
             "modified": 1542672000000 
          },
          "$key": 'AREA1',
          "_productionLine": 'SIP',
          "_line": 1,
          "_projectDetails": {
            $key: "fdfs544554gfdfgdg",
            "specialRequirements": "none",
            "name": "Project1",
            "id": "001",  
            "active":true,
            "chainOfCustody":"PEFC",
            "client":"Bob",
            "siteAccess":"Rigid",
            "postCode": "CV21 3HB",
            "nextDeliveryDate": "2017-05-05",
            "siteStart": "2017-05-05",
            "includeSaturday": true,
            "siteAddress": "27 Charlotte Street",
            "deliveryManager": {
              "email" : "nicholas.clarke@innovaresystems.co.uk",
              "id" : "c926461e-d30f-411c-ac43-c2d639709335",
              "mobile" : "07736597280",
              "name" : "Nicholas Clarke"
            },
            "estimatedAreas": {
              "Ext": 500,
              "ExtF":500,
              "ExtH": 500,
              "Floor": 500,
              "Int": 500,
              "Roof": 500
            },
            "gateway": "G6",
            "weekDuration": 7,
            "notifyUsers": "James",
            "deliverySchedule": {
              "published": "PROJ1REV2",
              "revisions": {
                "PROJ1REV0": true,
                "PROJ1REV1": true,
                "PROJ1REV2": true,
                "PROJ1REV3": true,
                "PROJ1REV4": true,
                "PROJ1REV5": true
              },
              "unpublished": "PROJ1REV5"
            },
            "datumType": "Education Student Accommodation"
          },
          "_stats": { 
            "type": 'actual',
            "count": 15,
            "display": 15,
            "percent": 93,
            "oneTrue": 'actual',
            "completed": 14 
          },
          "_deliveryDate": '2018-11-21',
          "_hasUnpublished": undefined,
          "_hasUnpublishedOnly": false,
          "_id": '001-P1-GF-Ext',
          "_projId": '001',
          "_dispatchDate": '2018-11-20',
          "_sortKey": 0,
          "_riskDate": '2018-11-19',
          "_designHandoverDate": '2018-10-10',
          "_unpublishedDeliveryDate": '',
          "_unpublishedDispatchDate": '',
          "_unpublishedRiskDate": '',
          "_unpublishedDesignHandoverDate": ''
           
        }
      )
    })
    it("extends area without unpublished data",() =>{
      expect(avgData).toEqual(
        {
          "$id": 'AREA1',
          "floor": 'GF',
          "phase": 'P1',
          "type": 'Ext',
          "project": 'PROJ2',
          "estimatedArea": 50,
          "estimatedPanels": 20,
          "actualArea": 60,
          "actualPanels": 15,
          "completedArea": 95,
          "completedPanels": 14,
          "supplier": 'Innovaré',
          "revisions": { 
            "PROJ2REV0": "2018-11-20",
            "PROJ2REV1": "2018-11-20",
            "PROJ2REV2": "2018-11-21",
            "PROJ2REV3": "2018-11-25",
          },
          "timestamps": {
             "created": 1542672000000,
             "modified": 1542672000000 
          },
          "$key": 'AREA1',
          "_productionLine": 'SIP',
          "_line": 1,
          "_projectDetails": {
            $key: "fdfs544554gfdfgdg",
            "specialRequirements": "none",
            "name": "Project2",
            "id": "002",  
            "active":true,
            "chainOfCustody":"PEFC",
            "client":"Bob",
            "siteAccess":"Rigid",
            "postCode": "CV21 3HB",
            "nextDeliveryDate": "2017-05-05",
            "siteStart": "2017-05-05",
            "includeSaturday": true,
            "siteAddress": "27 Charlotte Street",
            "deliveryManager": {
              "email" : "nicholas.clarke@innovaresystems.co.uk",
              "id" : "c926461e-d30f-411c-ac43-c2d639709335",
              "mobile" : "07736597280",
              "name" : "Nicholas Clarke"
            },
            "estimatedAreas": {
              "Ext": 500,
              "ExtF": 500,
              "ExtH": 500,
              "Floor": 500,
              "Int": 500,
              "Roof": 500
            },
            "gateway": "G6",
            "weekDuration": 7,
            "notifyUsers": "James",
            "deliverySchedule": {
              "published": "PROJ2REV2",
              "revisions": {
                "PROJ2REV0": true,
                "PROJ2REV1": true,
                "PROJ2REV2": true,
                "PROJ2REV3": true,
              },
              "unpublished": "PROJ2REV3"
            },
            "datumType": "Education Student Accommodation"
          },
          "_stats": { 
            "type": 'actual',
            "count": 15,
            "display": 15,
            "percent": 93,
            "oneTrue": 'actual',
            "completed": 14 
          },
          "_deliveryDate": '2018-11-21',
          "_hasUnpublished": true,
          "_hasUnpublishedOnly": false,
          "_id": '002-P1-GF-Ext',
          "_projId": '002',
          "_dispatchDate": '2018-11-20',
          "_sortKey": 0,
          "_riskDate": '2018-11-19',
          "_designHandoverDate": '2018-10-10',
          "_unpublishedDeliveryDate": '2018-11-25',
          "_unpublishedDispatchDate": '2018-11-23',
          "_unpublishedRiskDate": '2018-11-22',
          "_unpublishedDesignHandoverDate": '2018-10-15'
           
        }
      )
    })
  })

});

class Helper {
  actions: any[] = [];
  actions2: any[] = [];
  actions3: any[] = [];
  getSnapShotChanges(data: object, asObserv: boolean){
      const dataKeys = Object.keys(data);
      for(let i = 0; i < dataKeys.length; i++){
          this.actions.push({
            payload: {
              val:  function() { return data[dataKeys[i]] },
              key: dataKeys[i]
            },
            prevKey: null,
            type: "value"
          });
      }
      if(asObserv){
        return of(this.actions);
      }else{
        return this.actions;
      }
  };
  getValueChanges(data: object): Observable<any[]>{
    const dataKeys = Object.keys(data);
    for(let i = 0; i < dataKeys.length; i++){
        this.actions2.push(data[dataKeys[i]]);
    }
    return of(this.actions2);
  };
  getMappedValue(data: Array<any>){
    const dataKeys = Object.keys(data);
    for(let i = 0; i < dataKeys.length; i++){
      data[dataKeys[i]].$key = dataKeys[i];
      this.actions3.push(data[dataKeys[i]]);
    }
    return this.actions3;
  };
  addQuerySupport(firebaseRef) {
    var childKey = ".";
  
    firebaseRef.orderByChild = jasmine.createSpy("orderByChild").and.callFake(mockOrderByChild);
  
    function mockOrderByChild(key) {
      childKey = key;
  
      var queryResults = firebaseRef.root().child(firebaseRef.key + "-orderByChild(" + childKey + ")");
      queryResults.autoFlush(true);
  
      var data = firebaseRef.getData();
      var keys = firebaseRef.getKeys();
  
      keys.sort(function (a, b) {
        return compare(data[a][childKey], data[b][childKey]);
      });
  
      // Use priority to get required order
      keys.forEach(function (key, index) {
        queryResults.child(key).setWithPriority(data[key], index);
      });
  
      queryResults.on("child_added", resync);
      queryResults.on("child_changed", resync);
  
      // Brute force to reflect changes in query results to original
      function resync() {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          firebaseRef.child(key).set(item);
        });
      }
  
      queryResults.equalTo = jasmine.createSpy("equalTo").and.callFake(mockEqualTo);
      queryResults.startAt = jasmine.createSpy("startAt").and.callFake(mockStartAt);
      queryResults.endAt = jasmine.createSpy("endAt").and.callFake(mockEndAt);
  
      return queryResults;
  
      function mockEqualTo(value) {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          if (item[childKey] !== value) {
            queryResults._removeChild(key);
          }
        });
  
        return queryResults;
      }
      
      function mockStartAt(value) {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          if (item[childKey] < value) {
            queryResults._removeChild(key);
          }
        });
  
        return queryResults;
      }
      
      function mockEndAt(value) {
        let data = queryResults.getData();
        let dataKeys = Object.keys(data);
        dataKeys.forEach(function ( key ) {
          let item = data[key];
          if (item[childKey] > value) {
            queryResults._removeChild(key);
          }
        });
  
        return queryResults;
      }
    }

    function compare(a, b) {
      if (typeof a === "number") {
        return a - b;
      }
      else if (typeof a === "string") {
        return a.localeCompare(b);
      }
      else {
        return 0;
      }
    }

  };
}