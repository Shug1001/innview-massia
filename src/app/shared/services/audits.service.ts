import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '.';
import { ReferenceDataService } from './reference-data.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuditsService {

  constructor(private authService: AuthService, private db: AngularFireDatabase, private referenceDataService: ReferenceDataService) { }


  log(log, link, projectId, productionLine){

    let logEntry = {
      log: log,
      timestamp: this.authService.serverTime(),
      link: link,
      projectId: projectId,
      productionLine: productionLine,
      userName: ""
    }

    return this.referenceDataService.getUserPromise(this.authService.currentUserId)
    .then((user)=>{
      logEntry.userName = user.name;
      return this.db.list("/audits").push(logEntry);
    })
    
  }

  getAudits(){
    return this.db.list("/audits").snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }
}
