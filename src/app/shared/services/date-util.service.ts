import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateUtilService {

  constructor() { }

  isWeekDay(value) {
    var day = this.asDate(value).getDay();
    return day >= 1 && day <= 5;
  }

  dayOfWeek(value) {
    var date = this.asDate(value);
    return (date.getDay() + 6) % 7;
  }

  todayAsIso() {
    return this.toIsoDate(new Date());
  }

  plusDays(value, days) {
    var date = new Date(value);
    date.setDate(date.getDate() + days);
    return this.toIsoDate(date);
  }
  
  minusDays(value, days) {
    var date = new Date(value);
    date.setDate(date.getDate() - days);
    return this.toIsoDate(date);
  }

  getInitials(name){
    var parts = name.split(' ')
    var initials = ''
    for (var i = 0; i < parts.length; i++) {
      if (parts[i].length > 0 && parts[i] !== '') {
        initials += parts[i][0]
      }
    }
    return initials
  }

  dateMonth(value) {
    var date = this.asDate(value);
    return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2);
  }

  weekStart(value) {
    if (value !== undefined) {
      var date = new Date(value);
    } else {
      var date = new Date();
    }
    date.setDate(date.getDate() - this.dayOfWeek(date));

    return this.toIsoDate(date);
  }

  isValidDate(dateObject) {
    var date = new Date(dateObject);
    return !isNaN(date.valueOf());
  }

  subtractWorkingDays(startDate, days) {
    var date = this.asDate(startDate);

    if (!this.isValidDate(date)) {
      console.error('Can not subtract working days from invalid date');
      return;
    }

    while (days > 0) {
      date.setDate(date.getDate()-1);
      if (this.isWeekDay(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  subtractWorkingDaysAndSat(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()-1);
      if (!this.isSun(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  asDate(value) {
    return value === undefined ? value : new Date(value);
  }

  isDateFormat(value) {
    return value instanceof Date && !isNaN(+value);
  }

  mapper(buildObj){
    let objHolder = {};
    buildObj.map((snap) => {
      objHolder[snap.key] = snap.payload.val();
    });
    return objHolder;
  }
  areaMapper(buildObj){
    let objHolder = {};
    buildObj.map((snap) => {
      objHolder[snap.$key] = snap;
    });
    return objHolder;
  }
  
  validDate(date) {
    date = this.asDate(date);
    var isDate = false;
    if (moment.isDate(date)) {
      isDate = true;
      if (isNaN(date.getTime())) {  // d.valueOf() could also work
        isDate = false;
      } else {
        isDate = true;
      }
    } else {
      isDate = false;
    }
    return isDate;
  }

  toPlannerDate(value){
    let MonthArr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var date = this.asDate(value);
    return ("0" + date.getDate()).slice(-2) + "-" + MonthArr[date.getMonth()];
  }

  toUserDate(value) {
    var date = this.asDate(value);
    return ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
  }

  toUserSlachDate(value) {
    var date = this.asDate(value);
    return ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth()+1)).slice(-2) + "/" + date.getFullYear();
  }

  toIsoDate(value) {
    var date = this.asDate(value);
    return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
  }

  toTime(value) {
    var date = this.asDate(value);
    return date.getHours()+":" + date.getMinutes();
  }

  daysBetween(from, to) {
    var toDate = this.asDate(to);
    var fromDate = this.asDate(from);
    var millis = toDate.getTime() - fromDate.getTime();
    return Math.round(millis / 1000 / 24 / 60 / 60);
  }

  addWorkingDays(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()+1);
      if (this.isWeekDay(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  addWorkingDaysAndSat(startDate, days) {
    var date = this.asDate(startDate);

    while (days > 0) {
      date.setDate(date.getDate()+1);
      if (!this.isSun(date)) {
        days--;
      }
    }
    return this.toIsoDate(date);
  }

  daysBetweenWeekDaysOnly(from, to) {
    var toDate = this.asDate(to);
    var fromDate = this.asDate(from);
    var minusWeekends = 0;
    if (fromDate < toDate){
      while(fromDate < toDate){
        if (this.isWeekDay(fromDate)){
          minusWeekends++;
        }
        fromDate = this.plusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    } else if (fromDate > toDate){
      while(fromDate > toDate){
        if (this.isWeekDay(fromDate)){
          minusWeekends--;
        }
        fromDate = this.minusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    }
    return minusWeekends;
  }

  daysBetweenWeekDaysAndSat(from, to) {
    var toDate = this.asDate(to);
    var fromDate = this.asDate(from);
    var minusSun = 0;
    if (fromDate < toDate){
      while(fromDate < toDate){
        if (!this.isSun(fromDate)){
          minusSun++;
        }
        fromDate = this.plusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    } else if (fromDate > toDate){
      while(fromDate > toDate){
        if (!this.isSun(fromDate)){
          minusSun--;
        }
        fromDate = this.minusDays(fromDate, 1);
        fromDate = this.asDate(fromDate);
      }
    }
    return minusSun;
  }

  isSun(value) {
    var day = this.asDate(value).getDay();
    return day === 0;
  }

  buildMapKey(items, property) {
    const map = {};
    Object.keys(items).forEach(function (itemkey) {
      let item = items[itemkey];
      map[item.$key] = item[property];
    });
    return map;
  }

  buildMap(items, property) {
    const map = {};
    Object.keys(items).forEach(function (itemkey) {
      let item = items[itemkey];
      map[itemkey] = item[property];
    });
    return map;
  }

  round2(data){
    return Math.round(data * 100) / 100;
  }

  round1(data){
    return Math.round(data * 10) / 10;
  }

  isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  }

  buildMapReverse(items, property) {
    const map = {};
    Object.keys(items).forEach(function (itemkey) {
      let item = items[itemkey];
      map[item[property]] = itemkey;
    });
    return map;
  }

  hasNumber(myString) {
    return /\d/.test(myString);
  }

  logicHSIP(foundProductType, panelType, height, length) {

    var isHSIP = "H";
    var isIFAST = "F";

    if (foundProductType === "Ext" && foundProductType !== "ExtH") {

      if (panelType !== undefined && height !== undefined && length !== undefined) {

        if (panelType === "H" || (height <= 600 && length <= 2650) || (length <= 600 && height <= 2650)) {
          foundProductType = foundProductType + isHSIP;
        }else if(panelType === "F") {
          foundProductType = foundProductType + isIFAST;
        }

      }

    }
    return foundProductType;
  }

  uploaderLogicHSIP(foundProductType, panelType, height, length) {

    var isHSIP = "H";
    var isIFAST = "F";
    var isBelly = "B";
    var isParapet = "P";

    if (foundProductType === "Ext" && foundProductType !== "ExtH" && foundProductType !== "ExtF" && foundProductType !== "ExtB" && foundProductType !== "ExtP") {

      if (panelType !== undefined) {

        if (panelType === "H") {
          foundProductType = foundProductType + isHSIP;
        }else if(panelType === "F") {
          foundProductType = foundProductType + isIFAST;
        }else if(panelType === "B") {
          foundProductType = foundProductType + isBelly;
        }else if(panelType === "P") {
          foundProductType = foundProductType + isParapet;
        }

      }

    }
    return foundProductType;
  }

  fieldSorter(fields) {
    return function (a, b) {
        return fields
            .map(function (o) {
                var dir = 1;
                if (o[0] === '-') {
                   dir = -1;
                   o=o.substring(1);
                }
                if (a[o] > b[o]) return dir;
                if (a[o] < b[o]) return -(dir);
                return 0;
            })
            .reduce(function firstNonZeroValue (p,n) {
                return p ? p : n;
            }, 0);
    };
}

}
