import { Injectable } from '@angular/core';
import { AreasService } from './areas.service';
import { ProjectsService } from './projects.service';
import { AreaExt } from '../models/models';
import { DateUtilService } from './date-util.service';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, combineLatest, Subject } from 'rxjs';
import { map, first } from 'rxjs/operators';
import { ReferenceDataService } from './reference-data.service';
import { AuditsService } from './audits.service';

@Injectable({
  providedIn: 'root'
})
export class RevisionsService {

  private changeRouteSource = new Subject<string>();
  changeRoute$ = this.changeRouteSource.asObservable();

  private basePath = '/revisions';
  revisions: Observable<any>;

  constructor(
    private db: AngularFireDatabase,
    private authService: AuthService,
    private dateUtilService: DateUtilService,
    private areasService: AreasService,
    private projectsService: ProjectsService,
    private referenceDataService: ReferenceDataService,
    private auditsService: AuditsService) { }

  // Service message commands
  changeRoute(route: string) {
    this.changeRouteSource.next(route);
  }

  getProjectFramingAdditional(id) {
    return combineLatest([
      this.projectsService.getprojectWithId(id),
      this.projectsService.getProjectAdditional(id),
      this.projectsService.getFramingStyles()
    ]);
  }

  getProjectRevisions(id){
    return combineLatest([
      this.projectsService.getprojectWithId(id),
      this.getRevisionsForProject(id),
      this.projectsService.getProjectAdditional(id)
    ]);
  }

  getRevisionsForProject(key: string): Observable<any> {
    const path = this.basePath;
    this.revisions = this.db.list(path, areasRef => areasRef.orderByChild('project').equalTo(key)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
    return this.revisions;
  }

  createRevisionForProject(projectKey) {
    return this.referenceDataService.getUserPromise(this.authService.authState.uid)
      .then((user) => {
        return this.createRevision(projectKey, user)
      })
      .then((revision) => {
        return {
          project: projectKey,
          revision: revision.key
        };
      });
  }

  createRevision(projectKey, user) {
    const revision = {
      deliveryManager: {},
      createdDate: this.dateUtilService.toIsoDate(new Date()),
      project: projectKey
    };

    if (this.authService.emailLogin !== undefined && user !== undefined){
      revision.deliveryManager = user.name;
    }

    return this.db.list('revisions').push(revision);
  }

  setUnpublishedRevisionOnProject(keys) {
    const deliveryScheduleUpdate = {
      deliverySchedule: {},
      nextDeliveryDate: null
    };
    deliveryScheduleUpdate.deliverySchedule['unpublished'] = keys.revision;
    return this.projectsService.updateProject(keys.project, deliveryScheduleUpdate);
  }

  setUnpublishedRevisionOnProjectAreas(keys) {

    return this.areasService.getAreasForProjectPromise(keys.project)
    .then((areas) => {
      let updates = {}
      areas.map((area) => {
        updates['/areas/' + area.$key + '/revisions/' + keys.revision] = this.dateUtilService.toIsoDate(new Date());
      });
      return this.db.database.ref().update(updates);
    })
    .then(() => {
      const deliveryScheduleUpdate = {
        deliverySchedule: {},
        nextDeliveryDate: null
      };
      deliveryScheduleUpdate.deliverySchedule['unpublished'] = keys.revision;
      return this.projectsService.updateProject(keys.project, deliveryScheduleUpdate);
    });
  }

  bulkMoveAreas(project, includeSaturday, diffDays, from){

    return this.areasService.getAreasForProjectPromise(project.$key)
      .then((areasHolder: AreaExt[]) => {
        let plusDate;
        const today = this.dateUtilService.todayAsIso();
        const areas = areasHolder;
        from = this.dateUtilService.toIsoDate(from);
        const areasToUpdate = {};
        areas.map((area) => {
          if (diffDays > 0) {
            if (includeSaturday) {
              plusDate = this.dateUtilService.addWorkingDaysAndSat(area.revisions[project.deliverySchedule.unpublished], diffDays);
            } else {
              plusDate = this.dateUtilService.addWorkingDays(area.revisions[project.deliverySchedule.unpublished], diffDays);
            }
          } else {
            const diffDaysReverse = Math.abs(diffDays);
            if (includeSaturday) {
              // tslint:disable-next-line: max-line-length
              plusDate = this.dateUtilService.subtractWorkingDaysAndSat(area.revisions[project.deliverySchedule.unpublished], diffDaysReverse);
            } else {
              plusDate = this.dateUtilService.subtractWorkingDays(area.revisions[project.deliverySchedule.unpublished], diffDaysReverse);
            }
          }
          const beforeChange = area.revisions[project.deliverySchedule.unpublished];

          if (beforeChange >= from) {
            areasToUpdate['/areas/' + area.$key + '/revisions/' + project.deliverySchedule.unpublished] = plusDate;
          }

        });
        return this.db.database.ref().update(areasToUpdate);
      });
  }

  bulkMoveAreasPhases(project, includeSaturday, diffDays, from, selected){

    return this.areasService.getAreasForProjectPromise(project.$key)
      .then((areasHolder: AreaExt[]) => {
        let plusDate;
        const today = this.dateUtilService.todayAsIso();
        const areas = areasHolder;
        from = this.dateUtilService.toIsoDate(from);
        const areasToUpdate = {};
        areas.map((area) => {
          if (diffDays > 0) {
            if (includeSaturday) {
              plusDate = this.dateUtilService.addWorkingDaysAndSat(area.revisions[project.deliverySchedule.unpublished], diffDays);
            } else {
              plusDate = this.dateUtilService.addWorkingDays(area.revisions[project.deliverySchedule.unpublished], diffDays);
            }
          } else {
            const diffDaysReverse = Math.abs(diffDays);
            if (includeSaturday) {
              // tslint:disable-next-line: max-line-length
              plusDate = this.dateUtilService.subtractWorkingDaysAndSat(area.revisions[project.deliverySchedule.unpublished], diffDaysReverse);
            } else {
              plusDate = this.dateUtilService.subtractWorkingDays(area.revisions[project.deliverySchedule.unpublished], diffDaysReverse);
            }
          }
          const beforeChange = area.revisions[project.deliverySchedule.unpublished];

          if (beforeChange >= from && selected.includes(area.phase)) {
            areasToUpdate['/areas/' + area.$key + '/revisions/' + project.deliverySchedule.unpublished] = plusDate;
          }

        });
        return this.db.database.ref().update(areasToUpdate);
      });
  }

  publishDeliverySchedule(key, project) {

    return this.areasService.getAreasForProjectPromise(project.$key)
    .then((areasHolder: AreaExt[]) => {

      const areas = areasHolder;

      project.deliverySchedule['published'] = project.deliverySchedule['unpublished'];
      if (project.deliverySchedule.revisions === undefined) {
        project.deliverySchedule.revisions = {};
      }
      project.deliverySchedule['revisions'][project.deliverySchedule['unpublished']] = true;
      if (project.deliverySchedule === undefined) {
        project.deliverySchedule = {};
      }
      project.deliverySchedule['unpublished'] = key;

      const revisionRef = project.deliverySchedule['published'];
      let earliestDeliveryDate = '9999-99-99';

      areas.map((area) => {
        const revisions = Object.keys(area.revisions);
        const lastRevisionDate = area.revisions[revisions[revisions.length - 1]];
        area.revisions[key] =  lastRevisionDate;
        if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < earliestDeliveryDate) {
          earliestDeliveryDate = area.revisions[revisionRef];
        }
      });
      if (project.siteStart === undefined) {
        project.siteStart = earliestDeliveryDate;
      }
      if (project.siteStart > this.dateUtilService.todayAsIso()) {
        let earliestDeliveryDate = '9999-99-99';

        areas.forEach((area) => {
          if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < earliestDeliveryDate) {
            earliestDeliveryDate = area.revisions[revisionRef];
          }
        });
        if (earliestDeliveryDate !== '9999-99-99') {
          project.siteStart = earliestDeliveryDate;
        }
      } else {
        let AfterToday = '9999-99-99';
        const todaysDate = this.dateUtilService.todayAsIso();
        areas.forEach((area) => {
          if (area.revisions[revisionRef] !== undefined
             && area.revisions[revisionRef] < AfterToday
              && area.revisions[revisionRef] > todaysDate) {
            AfterToday = area.revisions[revisionRef];
          }
        });
        if (AfterToday !== '9999-99-99') {
          project.nextDeliveryDate = AfterToday;
        }
      }
      return this.projectsService.updateProject(project.$key, project)
      .then(() => {
        return this.areasService.updateAreas(areas);
      });

    });
  }

  publishDeliverySchedule2(project) {

    let revision;

    return this.referenceDataService.getUserPromise(this.authService.authState.uid)
    .then((user) => {
      revision = {
        deliveryManager: {},
        createdDate: this.dateUtilService.toIsoDate(new Date()),
        project: project.$key
      };
  
      if (this.authService.emailLogin !== undefined && user !== undefined){
        revision.deliveryManager = user.name;
      }
      return this.areasService.getAreasForProjectPromise(project.$key);
    })
    .then((areasHolder: AreaExt[]) => {

      const areas = areasHolder;

      let updates = {};

      var key = this.db.database.ref().child('revisions').push().key;


      project.deliverySchedule['published'] = project.deliverySchedule['unpublished'];
      if (project.deliverySchedule.revisions === undefined) {
        project.deliverySchedule.revisions = {};
      }
      project.deliverySchedule['revisions'][project.deliverySchedule['unpublished']] = true;
      if (project.deliverySchedule === undefined) {
        project.deliverySchedule = {};
      }
      project.deliverySchedule['unpublished'] = key;

      const revisionRef = project.deliverySchedule['published'];
      let earliestDeliveryDate = '9999-99-99';

      areas.map((area) => {
        const revisions = Object.keys(area.revisions);
        const lastRevisionDate = area.revisions[revisions[revisions.length - 1]];
        area.revisions[key] =  lastRevisionDate;
        if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < earliestDeliveryDate) {
          earliestDeliveryDate = area.revisions[revisionRef];
        }
      });
      if (project.siteStart === undefined) {
        project.siteStart = earliestDeliveryDate;
      }
      if (project.siteStart > this.dateUtilService.todayAsIso()) {
        let earliestDeliveryDate = '9999-99-99';

        areas.forEach((area) => {
          if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < earliestDeliveryDate) {
            earliestDeliveryDate = area.revisions[revisionRef];
          }
        });
        if (earliestDeliveryDate !== '9999-99-99') {
          project.siteStart = earliestDeliveryDate;
        }
      } else {
        let AfterToday = '9999-99-99';
        const todaysDate = this.dateUtilService.todayAsIso();
        areas.forEach((area) => {
          if (area.revisions[revisionRef] !== undefined
             && area.revisions[revisionRef] < AfterToday
              && area.revisions[revisionRef] > todaysDate) {
            AfterToday = area.revisions[revisionRef];
          }
        });
        if (AfterToday !== '9999-99-99') {
          project.nextDeliveryDate = AfterToday;
        }
      }
      
      updates['/revisions/' + key] = revision;
      let projectKey = project.$key;
        if(project.$key !== undefined){
          delete project.$key;
        }
      updates['/projects/' + projectKey] = project;
      areas.forEach((area) => {
        let areaKey = area.$key;
        if(area.$key !== undefined){
          delete area.$key;
        }
        if (!area.timestamps) {
          area.timestamps = {
            created: this.authService.serverTime()
          };
        }
        updates['/areas/' + areaKey + '/revisions/'] = area.revisions;
      });
      

      console.log(JSON.stringify(updates));
      return this.db.database.ref().update(updates);
    });
  }

  promoteUnpublishedRevision(project) {
    return this.publishDeliverySchedule2(project) 
      .then(()=>{
        let log = "Published Revision";
        return this.auditsService.log(log, "", project.id, "");
      })
      .then(function () {
        return Promise.resolve(project);
      });
  }

  removeRevisions(projectKey){
    return this.projectsService.removeRevisions(projectKey)
    .then(() => {
      const projectHolder = {
        key: projectKey
      };
      return this.createRevisionForProject(projectHolder.key);
    })
    .then((revision) => {
     return this.setUnpublishedRevisionOnProject(revision);
    });
  }

  removeRevisionsAllAreas(projectKey){
   return this.projectsService.removeRevisions(projectKey)
    .then(() => {
      const projectHolder = {
        key: projectKey
      };
      return this.createRevisionForProject(projectHolder.key);
    })
    .then((revision) => {
     return this.setUnpublishedRevisionOnProjectAreas(revision);
    });
  }

  deleteAreaProcess(area){
    return this.db.list('panels', panelsRef => panelsRef.orderByChild('area')
    .equalTo(area.$key))
    .snapshotChanges()
    .pipe(first())
    .toPromise()
    .then((panels) => this.db.object('panels').update(
      panels.reduce((acc, op) => { acc[op.key] = null; return acc; }, {})
    ))
    .then(() => {
      return this.areasService.deletearea(area.$key);
    })
    .then(() => {
      return this.areasService.getAreasForProjectPromise(area.project);
    }).then((areas) => {
      let isRevit = false;
      areas.forEach((area) => {
        if(area.revitUpload !== undefined && area.revitUpload){
          isRevit = true;
        }
      });
      if (areas.length === 0) {
        return this.removeRevisions(area.project).then(() => {
          return this.projectsService.saveProjectAdditionalRevitUpload(area.project,{"revitUpload": false});
        });
      } else {
        if(isRevit){
          return this.projectsService.saveProjectAdditionalRevitUpload(area.project,{"revitUpload": true});
        }else{
          return this.projectsService.saveProjectAdditionalRevitUpload(area.project,{"revitUpload": false});
        }
      }
    });
  }
}
