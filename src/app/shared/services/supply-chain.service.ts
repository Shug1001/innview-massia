import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { map, first, switchMap, } from 'rxjs/operators';
import { Subject, of, combineLatest, Observable } from 'rxjs';
import { StockItem } from '../../shared/models/models';

@Injectable({
  providedIn: 'root'
})
export class SupplyChainService {

  private whichRouteSource = new Subject<string>();

  whichRouteSource$ = this.whichRouteSource.asObservable();

  constructor(private db: AngularFireDatabase) { }

  setRoute(route: string) {
    this.whichRouteSource.next(route);
  }

  createSupplier(contacts, supplier){

    const updates = {};
    const contactKeys = {};

    contacts.forEach((contact) => {
      const newPostKey = this.db.database.ref().child('posts').push().key;
      if (contactKeys[newPostKey] === undefined) {
        contactKeys[newPostKey] = true;
      }
      if (contact.mainContact) {
        supplier.mainContact = newPostKey;
      }
      updates['/contacts/' + newPostKey] = contact;
    });

    supplier.contacts = contactKeys;
    const supplierPostKey = this.db.database.ref().child('posts').push().key;
    updates['/suppliers/' + supplierPostKey] = supplier;
    return this.db.database.ref().update(updates);
  }

  updateSupplier(existingContacts, contacts, supplierKey, supplier) {

    const updates = {};
    let contactKeys = {};

    if (Object.keys(contacts).length > 0) {
      contactKeys = supplier.contacts;
    }

    Object.keys(existingContacts).forEach(contactKey => {
      let selectedItem: any;
      selectedItem = contacts.filter(item => item.$key === contactKey)[0];
      if (selectedItem === undefined) {
        delete contactKeys[contactKey];
      }
    });

    contacts.forEach((contact) => {
      if (contact.$key !== undefined) {
        const contactKey = contact.$key;
        delete contact.$key;
        if (contact.mainContact) {
          supplier.mainContact = contactKey;
        }
        updates['/contacts/' + contactKey] = contact;
      } else {
        const newPostKey = this.db.database.ref().child('posts').push().key;
        if (contactKeys[newPostKey] === undefined) {
          contactKeys[newPostKey] = true;
        }
        if (contact.mainContact) {
          supplier.mainContact = newPostKey;
        }
        updates['/contacts/' + newPostKey] = contact;
      }
    });

    supplier.contacts = contactKeys;
    updates['/suppliers/' + supplierKey] = supplier;
    return this.db.database.ref().update(updates);
  }

  createStockTypes(type: string, data: object) {
    const path = `/${type}`;
    return this.db.list(path).push(data);
  }

  updateStockTypes(type: string, key: string, data: object) {
    const path = `/${type}`;
    return this.db.list(path).update(key, data);
  }

  deleteStockTypes(key: string, stockItems: Array<any>) {
    const updates = {};
    if (stockItems.length === 1) {
      updates['/stockItem'] = {};
    } else {
      updates['/stockItem/' + key] = {};
    }
    this.db.database.ref().update(updates);
  }

  getStockItems() {
    return this.db.list('/stockItem').snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getSuppliersList() {
    return this.db.list('/suppliers').snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getSuppliersByDate() {
    return this.db.list('/suppliers', ref => ref.orderByChild('dateTimestamp').limitToLast(3)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getStockItemsByDate() {
    return this.db.list('/stockItem', ref => ref.orderByChild('dateTimestamp').limitToLast(3)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getContacts() {
    return this.db.list('/contacts').snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }

  getSuppliersWithContacts() {
    return combineLatest([this.getSuppliersList(), this.getContacts()]);
  }

  getItemsWithType() {
    return combineLatest([this.getStockTypes('stockItem'), this.getStockTypes('stockGroup'), this.getStockTypes('stockSubCat')]);
  }

  getStock(variant, id) {
    const path = `${variant}/${id[variant]}`;
    return this.db.object(path).valueChanges().pipe(first()).toPromise();
  }

  getStockSRD(table: string, key: string): Observable<any> {
    const path = `${table}/${key}`;
    return this.db.object(path).snapshotChanges().pipe(map(obj => {
      if (obj.key) {
        return Object.assign(obj.payload.val(), {$key: obj.key});
      }
    }));
  }

  getStockItem(key: string): Observable<any> {
    const path = `${'/stockItem'}/${key}`;
    let stockItemSnap: any;
    return this.db.object(path).snapshotChanges().pipe(
      switchMap(obj => {
        stockItemSnap = obj;
        if (stockItemSnap.key === null) {
          return of(null);
        }
        const stockItem: any = obj.payload.val();
        return combineLatest([
          this.getStockSRD('stockGroup', stockItem.stockGroup),
          this.getStockSRD('stockSubCat', stockItem.stockSubCat),
          this.getStockSRD('stockType', stockItem.stockType)
        ]);
      }),
      map(obj => {
        if (stockItemSnap.key === null) {
          return of([]);
        }
        const stockItem: any = stockItemSnap.payload.val();
        return Object.assign(stockItem, {
          $key: stockItemSnap.key,
          stockGroupObj: obj[0],
          stockSubCatObj: obj[1],
          stockTypeObj: obj[2]
        });
    }));
  }

  createSupplierStock(stockItemKey, supplierKey, data) {

    const updates = {};

    const newstockDetailsKey = this.db.database.ref().child('stockDetails').push().key;

    updates['/stockDetails/' + newstockDetailsKey] = data;
    console.log(JSON.stringify(updates));
    updates['/supplies/stockItems/' + stockItemKey + '/' + newstockDetailsKey] = supplierKey;
    updates['/supplies/suppliers/' + supplierKey + '/' + newstockDetailsKey] = stockItemKey;

    return this.db.database.ref().update(updates);

  }

  deleteSupplierStock(stockItemKey, supplierKey, stockDetailsKey){
    const updates = {};
    updates['/stockDetails/' + stockDetailsKey] = {};
    updates['/supplies/stockItems/' + stockItemKey + '/' + stockDetailsKey] = {};
    updates['/supplies/suppliers/' + supplierKey + '/' + stockDetailsKey] = {};
    return this.db.database.ref().update(updates);
  }

  deleteStockItem(stockItemKey){
    const path = `${'/supplies/stockItems'}/${stockItemKey}`;
    return this.db.object(path).snapshotChanges().pipe(first()).toPromise().then((obj) => {
      if (obj.key === null) {
        return of(null);
      }
      const updates = {};
      updates['/stockItem/' + stockItemKey] = {};
      const suppliers: any = obj.payload.val();
      Object.keys(suppliers).forEach((stockDetailsKey) => {
        const supplierKey = suppliers[stockDetailsKey];
        updates['/stockDetails/' + stockDetailsKey] = {};
        updates['/supplies/stockItems/' + stockItemKey] = {};
        updates['/supplies/suppliers/' + supplierKey + '/' + stockDetailsKey] = {};
      });
      return this.db.database.ref().update(updates);
    });
  }

  deleteSupplier(supplierKey){
    const path = `${'/supplies/suppliers'}/${supplierKey}`;
    return this.db.object(path).snapshotChanges().pipe(first()).toPromise().then((obj) => {
      if (obj.key === null) {
        return of(null);
      }
      const updates = {};
      updates['/suppliers/' + supplierKey] = {};
      const stockItems: any = obj.payload.val();
      Object.keys(stockItems).forEach((stockDetailsKey) => {
        const stockItemKey = stockItems[stockDetailsKey];
        updates['/stockDetails/' + stockDetailsKey] = {};
        updates['/supplies/stockItems/' + stockItemKey + '/' + stockDetailsKey] = {};
        updates['/supplies/suppliers/' + supplierKey] = {};
      });
      return this.db.database.ref().update(updates);
    });
  }

  updateSupplierStock(key, data) {
    const updates = {};
    updates['/stockDetails/' + key] = data;
    return this.db.database.ref().update(updates);
  }

  getContact(key: string): Observable<any> {
    const path = `${'/contacts'}/${key}`;
    return this.db.object(path).snapshotChanges().pipe(map(obj => {
      if (obj.key) {
        return Object.assign(obj.payload.val(), {$key: obj.key});
      }
    }));
  }

  getSupplier(key: string): Observable<any> {
    const path = `${'/suppliers'}/${key}`;
    let supplierSnap: any;
    return this.db.object(path).snapshotChanges().pipe(
      switchMap(obj => {
        supplierSnap = obj;
        if (obj.key === null) {
          return of([]);
        }
        const arrToCombine = [];
        const supplier: any = obj.payload.val();
        Object.keys(supplier.contacts).map(contactKey => {
          arrToCombine.push(this.getContact(contactKey));
        });
        return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], []));
      }),
      map(obj => {
        const supplier: any = supplierSnap.payload.val();
        if (supplierSnap.key) {
          return Object.assign(supplier, {
            $key: supplierSnap.key,
            contactsArr: obj
          });
        }
    }));
  }

  getSuppliesSuppliers(stockItemKey: string) {
    const path = `${'/supplies/stockItems'}/${stockItemKey}`;
    return this.db.object(path).snapshotChanges().pipe(switchMap(obj => {
      if (obj.key === null) {
        return of([]);
      }
      const arrToCombine = [];
      const suppliers: any = obj.payload.val();
      Object.keys(suppliers).map(stockDetailsKey => {
        arrToCombine.push(this.getStockDetailsByStockItem(stockDetailsKey, suppliers[stockDetailsKey]));
      });
      return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(arr => {
        let sortKey = 1;
        arr.forEach(element => {
          element.no = sortKey;
          sortKey++;
        });
        return arr;
      }));
    }));
  }

  getSuppliesStockItems(supplierKey: string) {
    const path = `${'/supplies/suppliers'}/${supplierKey}`;
    return this.db.object(path).snapshotChanges().pipe(switchMap(obj => {
      if (obj.key === null) {
        return of([]);
      }
      const arrToCombine = [];
      const stockitems: any = obj.payload.val();
      Object.keys(stockitems).map(stockDetailsKey => {
        arrToCombine.push(this.getStockDetailsBySupplier(stockDetailsKey, stockitems[stockDetailsKey]));
      });
      return combineLatest(arrToCombine, (...arrays) => arrays.reduce((acc, array) => [...acc, ...array], [])).pipe(map(arr => {
        let sortKey = 1;
        arr.forEach(element => {
          element.no = sortKey;
          sortKey++;
        });
        return arr;
      }));
    }));
  }

  getStockDetailsByStockItem(key: string, supplierKey: string): Observable<any> {
    const path = `${'/stockDetails'}/${key}`;
    let stockDetailsSnap: any = {};
    return this.db.object(path).snapshotChanges().pipe(
      switchMap(obj => {
        stockDetailsSnap = obj;
        return this.getSupplier(supplierKey);
      }),
      map(obj => {
        const stockDetails = stockDetailsSnap.payload.val();
        return Object.assign(stockDetails, {
          $key: stockDetailsSnap.key,
          companyName: obj.companyName,
          supplierKey: obj.$key
        });
      }));
  }

  getStockDetailsBySupplier(key: string, stockItemKey: string): Observable<any> {
    const path = `${'/stockDetails'}/${key}`;
    let stockDetailsSnap: any = {};
    return this.db.object(path).snapshotChanges().pipe(
      switchMap(obj => {
        stockDetailsSnap = obj;
        return this.getStockItem(stockItemKey);
      }),
      map(obj => {
        const stockDetails = stockDetailsSnap.payload.val();
        return Object.assign(stockDetails, {
          $key: stockDetailsSnap.key,
          stockName: obj.stockName,
          stockItemKey: obj.$key
        });
      }));
  }

  getStockTypes(type) {
    const path = `/${type}`;
    return this.db.list(path).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), {$key: snap.key}));
    }));
  }
}
