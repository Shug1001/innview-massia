import { Pipe, PipeTransform } from '@angular/core';
import { DateUtilService } from '../services/date-util.service';

@Pipe({
  name: 'unpublishedRevision'
})
export class unpublishedRevisionFormatPipe implements PipeTransform {

  constructor(private dateUtilsService: DateUtilService) {}

  transform(value: any, args?: any): any {
    if(this.dateUtilsService.isDateFormat(value) && this.dateUtilsService.toUserSlachDate(value) !== "30/12/2100"){
      return this.dateUtilsService.toUserSlachDate(value);
    }else{
      return "Unpublished";
    }
  }

}
