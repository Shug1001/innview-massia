import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortQuotes'
})
export class SortQuotesPipe implements PipeTransform {

  transform(quotes: any, args?: any): any {
    quotes.sort(function (quote1, quote2) {

      let floorChange = {};
	    floorChange["GF"] = 0;
	    floorChange["1F"] = 1;
	    floorChange["2F"] = 2;
	    floorChange["3F"] = 3;
	    floorChange["4F"] = 4;
	    floorChange["5F"] = 5;
	    floorChange["6F"] = 6;
	    floorChange["7F"] = 7;
	    floorChange["8F"] = 8;
      floorChange["9F"] = 9;
      floorChange["RF"] = 10;
      floorChange["unleveled"] = 11;

      if (quote1.type > quote2.type) return 1;
      if (quote1.type < quote2.type) return -1;
    
      if (floorChange[quote1.level] > floorChange[quote2.level] ) return 1;
      if (floorChange[quote1.level] < floorChange[quote2.level] ) return -1;
    
    });
    
    return quotes;
  }

}
