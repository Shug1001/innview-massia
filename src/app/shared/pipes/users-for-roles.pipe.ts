import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usersForRoles'
})
export class UsersForRolesPipe implements PipeTransform {

  transform(users: Array<any>, role: string): any {
    if(users.length){
      return users.filter(user => user.roles[role]);
    }else{
      return null;
    }
  }

}
