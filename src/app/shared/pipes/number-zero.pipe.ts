import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberZero'
})
export class NumberZeroPipe implements PipeTransform {

  transform(value: number, type: string, weekend?: boolean): any {
    if(value !== null && type !== null && value !== undefined && type !== undefined){
      let m2: string = value.toString();
      if (m2 !== "0" && type === "m2"){
        m2 = value.toFixed(1);
        
      }
      if(weekend && m2 === "0" && type === "m2"){
        m2 = null;
      }
      return m2;
    }
  }

}
