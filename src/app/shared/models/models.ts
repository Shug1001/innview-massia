export interface AreaPlan {
    save: boolean;
    areaKey: string;
    startDate: string;
    userChange: boolean;
  }
  
  export interface Blotter {
    planStart: string;
    planDays: number;
    viewStart: string;
    current: number;
    productionLines: object;
    areas: Array<any>
  }
  
  export interface Plan {
    viewStart: string;
    current: number;
    capacity: object;
    dailyM2Totals: object;
    lineM2Totals: object;
    areas: Array<any>;
  }

  export interface ExtendedArea {
    $key: string;
    _productionLine: string;
    _line: number;
    _projectDetails: object;
    _sortKey: number;
    _stats: object;
    _deliveryDate: string;
    _hasUnpublished: boolean;
    _hasUnpublishedOnly: boolean;
    _id: string;
    _projId:string;
    _dispatchDate: string;
    _riskDate:string;
    _designHandoverDate:string;
    _unpublishedDeliveryDate:string;
    _unpublishedDispatchDate:string;
    _unpublishedRiskDate:string;
    _unpublishedDesignHandoverDate:string;
  }
  
  export interface ProductionPlan {
    buildDays : object;
    plannedFinish : string;
    plannedStart : string;
    project : string;
    riskDate : string;
    timestamps : innviewTimestamps;
  }
  
  export interface innviewTimestamps {
    created: object;
    modified: object;
  }

  export interface panel {
    area: string;
    id: string;
    project: string;
    type: string;
    dimensions: dimensions;
    timestamps: timestamps;
  }
  export interface dimensions {
    area: number;
    width: number;
    height: number;
    length: number;
    weight: number;
  }
  export interface timestampsObj {
    created:object;
    modified:object;
    createdBy:string;
    modifiedBy:string;
  }
  export interface timestamps {
    created:number;
    modified:number;
  }

  export interface area {
      $id:string;
      floor: string;
      phase: string;
      type: string;
      project:string;
      estimatedArea: number;
      estimatedPanels: number;
      actualArea: number;
      actualPanels: number;
      completedArea: number;
      completedPanels: number;
      supplier: string;
      revisions: revisions;
      timestamps:timestamps;
  }

  export interface deliverySchedule{
    published: string;
    revisions: revisions;
    unpublished: string;
  }

  export interface SrdComponent {
    name: string;
    productTypes: Set<string>;
    seq: number;
  }

  export interface Components {
    project: string;
    areas: Set<string>;
    type: string;
    comments: string;
  }

  export interface Location {
    lat: number;
    lng: number;
  }

  export interface StockType {
    name: string;
    code: string;
  }

  export interface ComponentsHolder {
    project: Project;
    componentWeek: string;
    componentName: string;
    componentId: string;
    comments: string;
    phase: string;
    floor: string;
    supplier: string;
    offload: string;
    areas: Array<any>;
    areasDeliveryDate: Array<any>;
    areasLatestRevision: Array<any>;
    areasDelivered: Array<any>;
    delivered: boolean;
    areasActive: Array<any>;
    active: boolean;
    same: boolean;
    isSame: Array<any>;
    deliveryDate: Date;
    latestRevision: Date;
  }
  
  export interface Project {
    $key: string;
    id: string;
    name:string;
    client: string;
    siteAccess: string;
    datumType: string;
    chainOfCustody: string;
    specialRequirements: string;
    siteAddress: string;
    postCode: string;
    deliverySchedule: {
      published: string;
      revisions: Object;
      unpublished: string;
    };
    gateways: gateways;
    gatewayDates: object;
    siteCompletion: string;
    steel: number;
    gifa: number;
    siteStart: string;
    plannedCompletion: string;
    plannedStart: string;
    nextDeliveryDate: string;
    includeSaturday: boolean;
    deliveryManager: ContactDetails;
    estimatedAreas: EstimatedAreas;
    notifyUsers: any;
    gateway: string;
    weekDuration: number;
    active: boolean;
  }
  export interface gateways {
    'G5Planned': string,
    'G5Actual': string,
    'G6Planned': string,
    'G6Actual': string,
    'G7aPlanned': string,
    'G7aActual': string,
    'G7bPlanned': string,
    'G7bActual': string,
    'G8Planned': string,
    'G8Actual': string,
    'G9Planned': string,
    'G9Actual': string
  }

  export interface Quote {
    $key: string;
    qid: string;
    latest: object;
    quoteName:string;
    project: string;
    timestamps:timestampsObj;
  }

  export interface EstimatedAreas {
    Ext: number;
    ExtH: number;
    ExtF: number;
    Floor: number;
    Int: number;
    Roof: number;
  }

  export interface ContactDetails {
    id: string;
    name: string;
    mobile: string;
    email: string;
}
  
  export interface DeliverySchedule {
    published: String;
    revisions: Object;
    unpublished: String;
  }
  
  export interface AreasGrouped {
    percentage: number;
    groups: Array<any>;
  }

  export interface AreaExt {
    $key: string
    id: string;
    _stats: _stats;
    _productionLine: string;
    _line: string;
    line: number;
    active: boolean;
    comments: string;
    description: string;
    floor: string;
    offloadMethod: string;
    phase: string;
    project: string;
    revisions: object;
    supplier: string;
    timestamps: object;
    type: string;
  }

  export interface area {
    $id:string;
    floor: string;
    phase: string;
    type: string;
    project:string;
    estimatedArea: number;
    estimatedPanels: number;
    actualArea: number;
    actualPanels: number;
    completedArea: number;
    completedPanels: number;
    supplier: string;
    revisions: revisions;
    timestamps:timestamps;
}
  
  export interface _stats {
    type: string;
    count: number;
    display: string;
    percent: number;
    oneTrue: string;
    completed: number;
  }
  
  export interface AreasMap {
    [key: string]: AreaExt;
  }

  export interface Users {
    "email": string;
    "name": string;
    "phone": string;
    "roles" : {
      "delivery-manager" : boolean,
      "designer" : boolean,
      "operations" : boolean,
      "performance" : boolean,
      "production" : boolean
    };
  }

  export interface ProjectsAdditional {
    "siteOps": ContactDetails,
    "designer": ContactDetails,
    "qs": ContactDetails,
    "engineer": ContactDetails,
    "team": ContactDetails,
    "installer": ContactDetails,
    "engineer2": ContactDetails
  }

  export interface revisions {
    
  }

  export interface designIssued {
    "type": object,
    "totalCompleteCount": number,
    "totalDesignedCount": number,
    "totalCompleteM2": number,
    "totalDesignedM2": number,
    "totalDesignIssuedCount": number,
    "totalDesignIssuedM2": number
  }

  export interface AreasAdditional {
    bookings: object;
    matcheck: AdditionalDataTypes;
    checkedDFM: AdditionalDataTypes;
    handoverConfirmed: AdditionalDataTypes;
    issued: AdditionalDataTypes;
    collection: AdditionalDataTypes;
    panelsUploaded: AdditionalDataTypes;
    buildPlannedDays: object;
    buildPlannedDays2: object;
  }
  
  export interface AdditionalDataTypes {
    timeDate: Number;
    status: Boolean;
    user: String;
  }

  export interface MaterialRequest {
    $key: string;
    email: string;
    person: string;
    projectId: string;
    projectName: string;
    materialRequired: string;
    dateRequired: string;
    mainContractor: string;
    siteContact: string;
    siteContactNo: number;
    reason: string;
    qsName: string;
    qsAuth: boolean;
    action: boolean;
    deliveryDate: string;
    note: string;
    timestamps:timestamps;
  }

  export interface Reason {
    name: string;
    abbreviation: string;
  }

  export interface ProjectStats {
    stats: object;
    project: object;
    projectId: string;
  }

  export interface AreaStats {
    id: string;
    areaRef: string;
    name: string;
    revision: number;
    areas: area[];
    areasExport: any[];
    exportArea: any[];
  }

  export interface PanelStats {
    stats: PanelStat[],
    actualPanels: number,
    actualArea: number,
    started: number,
    completed: number,
    percentage: number
  }

  export interface ProjectStat {
    projectId: string;
    project: Project;
    label: string;
    delivery: string;
    revision: number;
    estimated: object;
    actual: object;
    completed: object;
    risk: object;
    allProjectRisks : [];
  }

  export interface PanelStat {
    position: number;
    id: string;
    uploaded: string;
    updated: string;
    status: string;
    complete: boolean;
    panel: panel;
  }

  export interface dimensionType {
    "width": number;
    "length": number;
  }

  export interface Dimensions {
    "floor": dimensionType;
    "roof": dimensionType;
  }

  export interface QuoteData {
    quote: string,
    group: string,
    hostId: number,
    family: string,
    type: string,
    width: number,
    height: number,
    area: number,
    created: number
  }


  export interface DesignIssued {
    totalCompleteCount: number;
    totalDesignedCount: number;
    totalCompleteM2: number;
    totalDesignedM2: number;
  };

  export interface Response {
    errors: Array<any>,
    models: Array<any>
  }

  export interface CreatedComponents {
    areasGrouped: object, 
    project: object,
    result: Response
  }

  export interface StockTypes {
    name: string;
    code: string;
  }

  export interface StockItem {
    stockGroup: string;
    stockSubCat: string;
    stockType: string;
    stockName: string;
    stockCode: string;
    width: number;
    coinsName: string;
    coinsCode: string;
    depth: number;
    grade: string;
    length: number;
    treated: string;
    storageWidth: number;
    storageDepth: number;
    storageLength: number;
    unitOfCount: number;
    unitOfApplication: number;
    criticalStockLevel: number;
    status: string;
    lastestAction: string;
    dateTimestamp: number;
  }

  export interface Contact {
    firstName: string;
    phone: string;
    lastName: string;
    mobileWork: string;
    otherName: string;
    mobileOther: string;
    employer: string;
    emailWork: string;
    jobTitle: string;
    emailOther: string;
    mainContact: boolean;
  }

  export interface Supplier {
    companyName: string;
    accCurrency: string;
    companyShort: string;
    vat: number;
    payee: string;
    mobile: string;
    address1: string;
    phone: string;
    address2: string;
    fax: string;
    address3: string;
    email: string;
    address4: string;
    postcode: string;
    country: string;
    status: string;
    contacts: object;
    mainContact: string;
    lastestAction: string;
    dateTimestamp: number;
  }

  export interface SupplierStock {
    no: number;
    stockName: string;
    companyName: string;
    type: string;
    count: number;
    cost: number;
    unitCost: number;
    delTime: number;
    colTime: number;
    storageWidth: number;
    storageDepth: number;
    storageLength: number;
    status: string;
    dateTimestamp: number;
  }

  export interface DisplayTile{
    unique: number;
    unitCostTotal: number;
    colTimeTotal: number;
    delTimeTotal: number;
    avgUnitCost: number;
    maxUnitCost: number;
    minUnitCost: number;
    avgDelTime: number;
    maxDelTime: number;
    minDelTime: number;
    avgColTime: number;
    maxColTime: number;
    minColTime: number;
    unitCostArr: Array<any>;
    colTimeArr: Array<any>;
    delTimeArr: Array<any>;
  }

  export interface JobsheetDetail {
    $key: string,
    projectNumber: string,
    panels: object,
    area: string,
    createdBy: string,
    done1: object,
    done2: object,
    done3: object,
    done4: object,
    done5: object,
    done6: object,
    done7: object,
    done8: object,
  }

  export interface ChartData {
    weekStartReturn: string,
    chartColours: object,
    daysOfWeeks: object,
    chartData: object,
    weeks: object,
    areasByDesignHandoverDate: object
  }
