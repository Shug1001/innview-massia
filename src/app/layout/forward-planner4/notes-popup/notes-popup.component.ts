import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ComponentsService } from 'src/app/shared/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-notes-popup',
  templateUrl: './notes-popup.component.html',
  styleUrls: ['./notes-popup.component.scss']
})
export class NotesPopupComponent implements OnInit{

  @Input() area: any;
  _subscription:Subscription;
  additionalDataAddon: string;
  mappAddons: object;

  constructor(
    public componentsService: ComponentsService,
    public dialog: MatDialog) {}

  ngOnInit(){
    this.mappAddons = {
      'Windows': 'W',
      'Cladding': 'C',
      'Windows & Cladding': 'W/C'
    };
    this.additionalDataAddon = '';
    if(this.area.type === "ExtF"){
      this._subscription = this.componentsService.getComponentAdditionalByArea(this.area).subscribe((snap) => {
        this.additionalDataAddon = snap[0] !== null && snap[0].addons !== undefined ? snap[0].addons : '';
      });
    }
  }

  ngOnDestroy(){
    if(this._subscription !== undefined){
      this._subscription.unsubscribe();
    }
  }
}
