import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { ForwardPlanner2Component } from './forward-planner4.component';
import {ForwardPlannerService} from '../../shared/services';
import {Observable, of} from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import {Location, CommonModule} from '@angular/common';
import {DOMHelper} from '../../../testing/dom-helper';
import {Router} from '@angular/router';
import { MatGridListModule, MatDialogModule, MatCardModule, MatBadgeModule, MatProgressBarModule, MatListModule, MatCheckboxModule, MatButtonModule, MatIconModule, MatInputModule, MatButtonToggleModule, MatNativeDateModule, MatDatepickerModule } from "@angular/material";
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { CapacityInputComponent } from './capacity-input/capacity-input.component';
import { HandoverConfirmedComponent } from './handover-confirmed/handover-confirmed.component';
import { PanelTooltipComponent } from './panel-tooltip/panel-tooltip.component';
import { DialogComponent } from './panel-tooltip/dialog/dialog.component';
import { StartDatePickerComponent } from './start-date-picker/start-date-picker.component';

describe('ForwardPlanner2Component', () => {
  let component: ForwardPlanner2Component;
  let fixture: ComponentFixture<ForwardPlanner2Component>;
  let dh: DOMHelper<ForwardPlanner2Component>;
  let forwardPlannerServiceMock: any;
  beforeEach(async(() => {
    forwardPlannerServiceMock = jasmine.createSpyObj("ForwardPlannerService", ["getCapacities","getActiveAreasAndCapacity"]);
    TestBed.configureTestingModule({
      declarations: [
        ForwardPlanner2Component,CapacityInputComponent, HandoverConfirmedComponent, PanelTooltipComponent, DialogComponent, StartDatePickerComponent
      ],
      imports: [
        CommonModule,
        PipesModule,
        RouterTestingModule,
        MatGridListModule,
        MatDialogModule,
        MatCardModule,
        MatBadgeModule,
        MatProgressBarModule,
        MatListModule,   
        MatCheckboxModule,
        MatButtonModule,
        MatIconModule,
        FormsModule,
        MatInputModule,
        MatButtonToggleModule,
        MatNativeDateModule, 
        MatDatepickerModule,
        AngularFireFunctionsModule,
      ],
      providers: [
        {provide: ForwardPlannerService, useValue: forwardPlannerServiceMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForwardPlanner2Component);
    component = fixture.componentInstance;
    dh = new DOMHelper(fixture);
  });

  xdescribe('Simple HTML', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });
    // Simple HTML
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    xit('should contain an h2 tag', () => {
      expect(dh.singleText('h2')).toBe('List all Products');
    });

    it('Should minimum be one button on the page', () => {
      component.getForwardPlan("2018-12-12");
      expect(dh.count('mat-grid-list')).toBe(1);
    });

    xit('Should be a + button first on the page', () => {
      expect(dh.singleText('button')).toBe('+');
    });
  });

  // xdescribe('List Products', () => {
  //   let helper: Helper;
  //   beforeEach(() => {
  //     helper = new Helper();
  //     fixture.detectChanges();
  //   });
  //   it('Should show One Unordered List Item', () => {
  //     expect(dh.count('ul')).toBe(1);
  //   });

  //   it('Should show no list item when no products are available', () => {
  //     expect(dh.count('li')).toBe(0);
  //   });

  //   it('Should show one list item when I have one product', () => {
  //     component.products = helper.getProducts(1);
  //     fixture.detectChanges();
  //     expect(dh.count('li')).toBe(1);
  //   });

  //   it('Should show 100 list item when I have 100 products', () => {
  //     component.products = helper.getProducts(100);
  //     fixture.detectChanges();
  //     expect(dh.count('li')).toBe(100);
  //   });

  //   it('Should show 100 Delete buttons, 1 pr. item', () => {
  //     component.products = helper.getProducts(100);
  //     fixture.detectChanges();
  //     expect(dh.countText('button', 'Delete')).toBe(100);
  //   });

  //   it('Should show 1 span pr product', () => {
  //     component.products = helper.getProducts(1);
  //     fixture.detectChanges();
  //     expect(dh.count('span')).toBe(1);
  //   });

  //   it('Should show 1 product name and id in span', () => {
  //     component.products = helper.getProducts(1);
  //     fixture.detectChanges();
  //     expect(dh.singleText('span'))
  //       .toBe(helper.products[0].name + ' -- ' + helper.products[0].id);
  //   });

  //   it('Should show 5 spans, 1 pr. product', () => {
  //     component.products = helper.getProducts(5);
  //     fixture.detectChanges();
  //     expect(dh.count('span')).toBe(5);
  //   });

  //   it('Should show 5 product names and ids in spans', () => {
  //     component.products = helper.getProducts(5);
  //     fixture.detectChanges();
  //     for (let i = 0; i < 5; i++) {
  //       const product = helper.products[i];
  //       expect(dh.countText('span', product.name + ' -- ' + product.id))
  //         .toBe(1);
  //     }
  //   });

  //   it('Should not show img tag without a url on the Product', () => {
  //     component.products = helper.getProducts(1);
  //     fixture.detectChanges();
  //     expect(dh.count('img'))
  //       .toBe(0);
  //   });

  //   it('Should show img tag with a url on the Product', () => {
  //     component.products = helper.getProducts(1);
  //     helper.products[0].url = 'http://a-url';
  //     fixture.detectChanges();
  //     expect(dh.count('img'))
  //       .toBe(1);

  //   });
  // });

  xdescribe('Navigation', () => {
    let location: Location;
    let router: Router;
    beforeEach(() => {
      location = TestBed.get(Location);
      router = TestBed.get(Router);
      fixture.detectChanges();
    });
    // Navigation
    it('Should navigate to / before + button click',
      () => {
        // find DebugElements with an attached RouterLinkStubDirective
        expect(location.path()).toBe('');
      }
    );

    it('Should navigate to /add on + button click',
      () => {
        spyOn(router, 'navigateByUrl');
        dh.clickButton('+');
        expect(router.navigateByUrl).
        toHaveBeenCalledWith(router.createUrlTree(['/add']),
          { skipLocationChange: false, replaceUrl: false });
      }
    );
  });

});

// class Helper {
//   products: Product[] = [];
//   getProducts(amount: number): Observable<Product[]> {
//     for (let i = 0; i < amount; i++) {
//       this.products.push(
//         {id: 'abc' + i, name: 'item' + i, pictureId: 'abc' + i}
//       );
//     }
//     return of(this.products);
//   }
// }


