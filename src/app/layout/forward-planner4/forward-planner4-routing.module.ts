import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForwardPlanner4Component } from './forward-planner4.component';

const routes: Routes = [
    {
        path: '',
        component: ForwardPlanner4Component
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ForwardPlanner4RoutingModule {}
