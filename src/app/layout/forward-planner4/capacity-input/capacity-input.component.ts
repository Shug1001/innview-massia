import { Component, Input, EventEmitter, Output} from '@angular/core';
import { ForwardPlannerService } from 'src/app/shared/services';
import { AuditsService } from 'src/app/shared/services/audits.service';
import { Plan } from '../../../shared/models/models';

@Component({
  selector: 'app-capacity-input',
  templateUrl: './capacity-input.component.html',
  styleUrls: ['./capacity-input.component.scss']
})
export class CapacityInputComponent {

  @Input()
    capacity: number;

  @Input()
    plan: Plan;

  @Input()
    productionLine: String;

  @Input()
    date: String;

  @Input()
    type: String;

  @Output() saveState = new EventEmitter<boolean>();

  constructor(private forwardPlannerService: ForwardPlannerService, private auditsService: AuditsService) { }

  update() {
    this.saveState.emit(true);
    return this.forwardPlannerService.setCapacity(this.plan, this.productionLine, this.date, this.capacity)
    .then(()=>{
      let log = "Capacity updated to " + this.capacity;
      return this.auditsService.log(log, "", null, this.productionLine);
    })
  }

}
