import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityInputComponent } from './capacity-input.component';

xdescribe('CapacityInputComponent', () => {
  let component: CapacityInputComponent;
  let fixture: ComponentFixture<CapacityInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
