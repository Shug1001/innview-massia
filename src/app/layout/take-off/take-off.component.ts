import  {Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectsService } from '../../shared/services';
import { DateUtilService } from '../../shared/services/date-util.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-take-off',
  templateUrl: './take-off.component.html',
  styleUrls: ['./take-off.component.scss']
})
export class TakeOffComponent implements OnInit {

  _subscription: Subscription;
  quotes: {};
  quoteHolder: {};
  loaded: boolean;
  subTypeObj: {};

  constructor(private route: ActivatedRoute, private router: Router, private projectsService: ProjectsService, private dateUtilsService: DateUtilService) {
    this.loaded = false;
    this.subTypeObj= {
      "DAT_Walls": function(type) {
        let subType = "No Type";
        if (type.indexOf("iSIP") !== -1 || type.indexOf("iFAST") !== -1) {
          subType = "External Walls";
        } else if (type.indexOf("TF") !== -1) {
          subType = "Internal Walls";
        }
        return subType;
      },
      "DAT_Floors": function(type) {
        let subType = "No Type";
        if (type.indexOf("TC") !== -1 || 
        type.indexOf("FS") !== -1 || 
        type.indexOf("Posi") !== -1|| 
        type.indexOf("245D") !== -1 || 
        type.indexOf("300D") !== -1 || 
        type.indexOf("350D") !== -1 || 
        type.indexOf("400D") !== -1 || 
        type.indexOf("450D") !== -1) {
          subType = "Timber Floors";
        } else if (type.indexOf("Concrete")  !== -1) {
          subType = "Concrete Floors";
        }
        return subType;
      },
      "DAT_Roofs": function(type) {
        let subType = "No Type";
        if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || 
        type.indexOf("Posi") !== -1 || 
        type.indexOf("Cassette") !== -1 || 
        type.indexOf("245D") !== -1 || 
        type.indexOf("300D") !== -1 || 
        type.indexOf("350D") !== -1 || 
        type.indexOf("400D") !== -1 || 
        type.indexOf("450D") !== -1) {
          subType = "Timber Cassette Roofs";
        } else if (type.indexOf("Cassette") === -1 ||
        type.indexOf("89") !== -1 || 
        type.indexOf("140") !== -1 || 
        type.indexOf("184") !== -1 || 
        type.indexOf("235") !== -1) {
          subType = "Timber Frame Roofs";
        }
        return subType;
      },
      "DAT_Windows": function(type) {
        let subType = "No Type";
        return subType;
      },
      "DAT_Doors": function(type) {
        let subType = "No Type";
        return subType;
      },
    };
   }

  ngOnInit() {
    const id = this.route.parent.parent.snapshot.paramMap.get('id');

    this._subscription = this.projectsService.getQuote(id).subscribe((quotes) => {
      this.quotes = quotes;
      let filteredQuotes = {};
      quotes.forEach(quote => {
        if(this.subTypeObj[quote.sheetName] !== undefined){
          quote.subType = this.subTypeObj[quote.sheetName](quote.type);
          if(filteredQuotes[quote.sheetName] === undefined){
            filteredQuotes[quote.sheetName] = {};
          }
          if(filteredQuotes[quote.sheetName][quote.subType] === undefined){
            filteredQuotes[quote.sheetName][quote.subType] = {};
          }
          if(filteredQuotes[quote.sheetName][quote.subType][quote.type] === undefined){
            filteredQuotes[quote.sheetName][quote.subType][quote.type] = {};
          }         
          if(filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level] === undefined){
            filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level] = {
              length:0,
              lengthAvg:[],
              width:[],
              height:[],
              areaNet: 0,
              gross:0,
              count:0
            }
          }
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].count++;
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].length += quote.length;
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].lengthAvg.push(quote.length);
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].height.push(quote.height);
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].width.push(quote.width);
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].areaNet += quote.areaNet;
          filteredQuotes[quote.sheetName][quote.subType][quote.type][quote.level].gross += quote.areaGross;
        }
      });
      this.quoteHolder = {};
      for (let sheetName in filteredQuotes) {
        for (let subType in filteredQuotes[sheetName]) {
          for (let type in filteredQuotes[sheetName][subType]) {
            for (let level in filteredQuotes[sheetName][subType][type]) {  
              let newView = {
                level: level,
                type: type,
                count: filteredQuotes[sheetName][subType][type][level].count,
                length: filteredQuotes[sheetName][subType][type][level].length === 0 ? filteredQuotes[sheetName][subType][type][level].length : this.dateUtilsService.round1(filteredQuotes[sheetName][subType][type][level].length / 1000),
                lengthAvg: filteredQuotes[sheetName][subType][type][level].lengthAvg === 0 ? filteredQuotes[sheetName][subType][type][level].lengthAvg : this.dateUtilsService.round1(this.getAvg(filteredQuotes[sheetName][subType][type][level].lengthAvg )/ 1000),
                avgWidth: filteredQuotes[sheetName][subType][type][level].width === 0 ? filteredQuotes[sheetName][subType][type][level].width : this.dateUtilsService.round1(this.getAvg(filteredQuotes[sheetName][subType][type][level].width )/ 1000),
                avgHeight: filteredQuotes[sheetName][subType][type][level].height === 0 ? filteredQuotes[sheetName][subType][type][level].height : this.dateUtilsService.round1(this.getAvg(filteredQuotes[sheetName][subType][type][level].height )/ 1000),
                areaNet: this.dateUtilsService.round2(filteredQuotes[sheetName][subType][type][level].areaNet),
                gross: this.dateUtilsService.round2(filteredQuotes[sheetName][subType][type][level].gross)
              };
              if(this.quoteHolder[sheetName] === undefined){
                this.quoteHolder[sheetName] = {};
              }
              if(this.quoteHolder[sheetName][subType] === undefined){
                this.quoteHolder[sheetName][subType] = [];
              }
              this.quoteHolder[sheetName][subType].push(newView);
            }
          }
        }
      }
      this.subTypeObj
      this.loaded = true;
    });
  }

  getAvg(data) {
    const total = data.reduce((acc, c) => acc + c, 0);
    return total / data.length;
  }

  getObjectKeys(data){
    return Object.keys(data);
  }
}