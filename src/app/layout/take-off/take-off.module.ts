import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatTabsModule } from '@angular/material';
import { TakeOffRoutingModule } from './take-off-routing.module';
import { TakeOffComponent } from './take-off.component';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

@NgModule({
  declarations: [TakeOffComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatTabsModule,
    TakeOffRoutingModule,
    PipesModule
  ]
})
export class TakeOffModule { }
