import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TakeOffComponent } from './take-off.component';

const routes: Routes = [{
  path: '',
  component: TakeOffComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TakeOffRoutingModule { }
