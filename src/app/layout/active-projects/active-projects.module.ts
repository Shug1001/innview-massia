import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActiveProjectsRoutingModule } from './active-projects-routing.module';
import { ActiveProjectsComponent } from './active-projects.component';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSliderModule, MatSlideToggleModule, MatSortModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { ProjectLineModule } from 'src/app/shared/modules/project-line/project-line.module';

@NgModule({
  declarations: [ActiveProjectsComponent],
  imports: [
    CommonModule,
    ActiveProjectsRoutingModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatToolbarModule,
    MatSlideToggleModule,
    ProjectLineModule
  ]
})
export class ActiveProjectsModule { }
