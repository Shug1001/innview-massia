import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProjectsService } from 'src/app/shared/services';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { Project } from '../../shared/models/models'
@Component({
  selector: 'app-active-projects',
  templateUrl: './active-projects.component.html',
  styleUrls: ['./active-projects.component.scss']
})
export class ActiveProjectsComponent implements OnInit {

  _subscription: Subscription;
  projects: Array<any>;
  displayedColumns = ['id', 'name', 'setLine', 'active'];
  dataSource: MatTableDataSource<Project>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private projectsService: ProjectsService) { }

  ngOnInit() {
    this._subscription = this.projectsService.getProjectsList().subscribe((projects) => {
      this.projects = projects;
      this.dataSource = new MatTableDataSource(this.projects);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onToggle(project, event){
    let update = {
      active: event.checked
    }
    return this,this.projectsService.updateProject(project.$key, update);
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }
}
