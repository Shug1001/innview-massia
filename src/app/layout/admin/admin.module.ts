import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AdminRoutingModule } from './admin-routing.module';
import { StockTypesModule } from 'src/app/shared/modules/stock-types/stock-types.module';
import { TextDialogModule } from 'src/app/shared/modules/text-dialog/text-dialog.module';
import { AdminComponent } from './admin.component';
import { MatCardModule } from '@angular/material';

@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    MatCardModule,
    AdminRoutingModule,
    StockTypesModule,
    TextDialogModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class AdminModule { }
