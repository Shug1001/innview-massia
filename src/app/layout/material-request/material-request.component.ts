import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Subscription } from 'rxjs';
import { ProjectsService } from 'src/app/shared/services';
import { MaterialRequest } from 'src/app/shared/models/models';
import { DateUtilService } from 'src/app/shared/services/date-util.service';

@Component({
  selector: 'app-material-request',
  templateUrl: './material-request.component.html',
  styleUrls: ['./material-request.component.scss']
})
export class MaterialRequestComponent implements OnInit {

  dataSource: MatTableDataSource<MaterialRequest>;
  loaded: boolean;
  materialrequests: Array<any>;
  _subscription: Subscription;

  displayedColumns: string[] = [
    'action', 
    'qsAuth', 
    'update', 
    'materialNumber',
    'timestamp', 
    'person', 
    'projectId', 
    'projectName',
    'materialRequired',
    'dateRequired',
    'reason',
    'mainContractor',
    'siteContact',
    'note',
    'deliveryDate'
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private projectsService: ProjectsService, private dateUtilsService: DateUtilService) {
    this.loaded = false;
  }

  ngOnInit() {
    this._subscription = this.projectsService.getMaterialRequests().subscribe(materialrequests => {
      this.materialrequests = materialrequests;
      this.dataSource = new MatTableDataSource(this.materialrequests);
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'timestamp': return item.timestamps.created;
          default: return item[property];
        }
      };
      this.dataSource.paginator = this.paginator;
      this.loaded = true;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}