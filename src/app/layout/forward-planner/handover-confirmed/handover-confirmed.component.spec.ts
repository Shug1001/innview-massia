import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandoverConfirmedComponent } from './handover-confirmed.component';

xdescribe('HandoverConfirmedComponent', () => {
  let component: HandoverConfirmedComponent;
  let fixture: ComponentFixture<HandoverConfirmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandoverConfirmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandoverConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
