import { Component, Input } from '@angular/core';
import { PanelsService } from 'src/app/shared/services/panels.service';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../panel-tooltip/dialog/dialog.component';
import { Subscription } from 'rxjs';
import { AreaExt } from 'src/app/shared/models/models';

@Component({
  selector: 'app-panel-tooltip',
  templateUrl: './panel-tooltip.component.html',
  styleUrls: ['./panel-tooltip.component.scss']
})
export class PanelTooltipComponent {

  @Input() area: AreaExt;

  totalM2: number;
  toDisplay: string;
  _subscription: Subscription;

  constructor(private panelsService: PanelsService, public dialog: MatDialog) { 
    this.totalM2 = 0;
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogComponent, {
      width: '800px',
      data: {area: this.area, loading: true}
    });

    this._subscription = this.panelsService.getPanelsForArea(this.area.$key).subscribe((panels)=>{

      const panDataArr = [];

      panels.forEach((panel)=>{

        let panData = {
          id:"",
          framingStyle:"",
          m2:"",
          completed: false
        };

        if(panel.id !== undefined){
          panData.id = panel.id;
        }
        if(panel.additionalInfo !== undefined && panel.additionalInfo.framingStyle !== undefined && panel.additionalInfo.framingStyle !== "undefined"){
          panData.framingStyle = panel.additionalInfo.framingStyle;
        }
        if(panel.additionalInfo !== undefined && panel.additionalInfo.sheathing !== undefined && panel.additionalInfo.sheathing !== "undefined"){
          panData.framingStyle = panData.framingStyle + " - " + panel.additionalInfo.sheathing;
        }
        if(panel.dimensions !== undefined && panel.dimensions.area !== undefined){
          panData.m2 = panel.dimensions.area;
        }
        if(panel.qa !== undefined && panel.qa.completed){
          panData.completed = true;
        }
        panDataArr.push(panData);

      });

      dialogRef.componentInstance.data = {area: this.area, panels: panDataArr, loading: false};

    });

    dialogRef.afterClosed().subscribe(result => {
      this._subscription.unsubscribe();
    });

  }
}
