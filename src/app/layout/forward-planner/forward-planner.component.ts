import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy, Inject } from '@angular/core';
import { ForwardPlannerService} from '../../shared/services';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatCalendar, MatDateFormats, MatSnackBar } from '@angular/material';
import { DateUtilService } from '../../shared/services/date-util.service';
import Srd  from '../../../assets/srd/srd.json';
import { Subscription, Subject, Observable } from 'rxjs';
import { Blotter, Plan } from 'src/app/shared/models/models';
import { CustomDateAdapter } from '../../shared/helpers/custom-adapter-date';
import { takeUntil } from 'rxjs/operators';
import { AngularFireFunctions } from '@angular/fire/functions';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-forward-planner',
  templateUrl: './forward-planner.component.html',
  styleUrls: ['./forward-planner.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }, 
    {provide: CustomDateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ForwardPlannerComponent implements OnInit {
  
  minDate = new Date();
  areasMap: object;
  newAreas: Array<any>;
  productionLines: object;
  defaultLineCapacity: object;
  lineCapacities: object;
  productTypes: object;
  productTypeProductionLine: object;
  blotter: Blotter;
  plan: Plan;
  areasLen: number;
  sortKey: number;
  columnsToDisplay: Array<any>;
  dayHeadings: Array<any>;
  days: Array<any>;
  productionLineFilter: object;
  toggleCount: object;
  toggleType: object;
  m2Type: string;
  areas: Array<any>;
  viewStart: string;
  skipWeeks: number;
  areasSelection: object;
  fieldFilters: Array<any>;
  loaded: boolean;
  _subscription: Subscription;
  panelsCompleteToday: Array<any>;
  plannerSort: Array<any>;
  capacities: Array<any>;
  areaPlans: Array<any>;
  mappedPanelsComplete: object;
  areaPlansMapped: object;
  save: boolean;
  startDateCounter: number;
  isStartDateChanged: boolean;
  state: boolean;
  todaysDate:Subscription;
  data$: Observable<any>;
  exportAreas: Array<any>;
  dateHeader = resetCustomHeader;
 
  @Input('capacity') capacity: string;

  constructor(
    private dateUtilService: DateUtilService,
    private customDateAdapter:CustomDateAdapter,
    private fns: AngularFireFunctions,
    private _snackBar: MatSnackBar,
    private forwardPlannerService: ForwardPlannerService) {

    this.areasMap = [];
    this.newAreas = [];
    this.state = false;

    this.m2Type = "count";

    this.loaded = false;
    this.save = true;
    this.isStartDateChanged = false;

    this.productionLineFilter = { SIP: true, SIP2: true, HSIP: true, IFAST: true, IFAST2: true, TF: true, TF2: true, CASS: true };
    this.toggleType = { actual: true, design: true, est: true };
    this.toggleCount = { action: true };

    this.dayHeadings = this.getDayHeadings();

    this.lineCapacities = {};
    this.areasSelection ={};

    this.areasLen = 0;
    this.sortKey = 0;

    this.days = [];

    this.productionLines = Srd.productionSubLines;
    this.productTypes = Srd.productTypes;
    this.defaultLineCapacity = this.dateUtilService.buildMap(this.productionLines, "defaultCapacity");
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");

    this.todaysDate = this.forwardPlannerService.getDate().subscribe(date =>{
      this.plan.viewStart = this.dateUtilService.weekStart(date);
      return this.getForwardPlan(this.plan.viewStart);
    });


  }
  open(picker){
    picker.open();
  }  
  changeState(){
    this.state = !this.state;
  }
  saveState(save: boolean){
    this.save = save;
  }

  startChange(save: boolean){
    this.save = save;
  }

  addEvent(type: string, event: MatDatepickerInputEvent<any>) {
    this.save = true;
    const date = this.dateUtilService.toIsoDate(event.value._d);
    this.getForwardPlan(date);
  }

  getAreaId(area, project) {
    return [project.id, area.phase, area.floor, area.type].join("-");
  }

  ngOnInit() {
    var today = new Date(); 
    this.getForwardPlan(today);
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
    this.todaysDate.unsubscribe();
  }
 
  getForwardPlan(startDate) {

    this.loaded = false;

    this.days = [];

    var today = new Date();
    var start = new Date(startDate || today);
    if (start < today) {
      start = today;
    }

    // Plan displays from Monday of selected start date
    this.viewStart = this.dateUtilService.weekStart(start);
    this.skipWeeks = Math.ceil(this.dateUtilService.daysBetween(today, this.viewStart) / 7);

    this.plan = {
      viewStart: this.viewStart,
      current: this.skipWeeks == 0 ? this.dateUtilService.dayOfWeek(today) : -1,
      capacity: {},
      dailyM2Totals: {},
      lineM2Totals: {},
      areas: []
    };

    if(this._subscription){
      this._subscription.unsubscribe();
    }

    this._subscription = this.forwardPlannerService.getActiveAreasAndCapacity(start.setHours(0,0,0,0), start.setHours(23,59,59,999)).subscribe(([areas, capacities, plannerSort, panels, startDateCounter]) => {

      this._snackBar.open('Update in progress!', 'Hide', {
        duration: 1500,
        panelClass: ['innviewSnack']
      });

      this.panelsCompleteToday = panels;
      this.plannerSort = plannerSort;
      this.capacities = capacities;
      this.startDateCounter = startDateCounter[0];
      this.areaPlans = startDateCounter[1];
      this.areas = areas;

      this.mapFieldSort(this.plannerSort);
      this.lineCapacities = this.dateUtilService.mapper(this.capacities);
      this.areaPlansMapped = this.dateUtilService.mapper(this.areaPlans);
      this.mapPanelsCompleteToday(this.panelsCompleteToday);

      this.initiatePlan();
      this.buildExportData();
      this.loaded = true;
      
    });

  }

  initiatePlan(){

    this.blotter = this.createBlotter(this.plan, this.skipWeeks, 3);
    this.plan.capacity = this.getCapacityFromBlotter(this.skipWeeks);

    this.areas.sort((areaA, areaB) => {
        var a = this.areasSelection[areaA.$key] ? areaA._unpublishedDeliveryDate : areaA._deliveryDate;
        var b = this.areasSelection[areaB.$key] ? areaB._unpublishedDeliveryDate : areaB._deliveryDate;
        var r = a.localeCompare(b);
        if (r === 0) {
          r = (areaA._sortKey !== undefined && areaA._sortKey !== undefined) ? areaA._sortKey - areaB._sortKey : 0;
        }
        return r;
      }
    );

    this.areas.map((area) => {
      if (this.areasSelection[area.$key] === undefined){
        this.areasSelection[area.$key] = false;
      }
      this.calculateAreaPlan(area, this.areasSelection[area.$key]);
    });

    let blotterAreas = this.getAreasFromBlotter(this.skipWeeks);
    var calculateM2 = this.calculateM2( blotterAreas );
    this.plan.lineM2Totals = calculateM2.line;
    this.plan.dailyM2Totals = calculateM2.total;
    blotterAreas.sort(this.dateUtilService.fieldSorter(this.fieldFilters));
    this.plan.areas = blotterAreas;

    if(this.save){
      this.forwardPlannerService.buildAreasPlan(this.plan);
      // const callable = this.fns.httpsCallable('updateProductionPlan');
      // this.data$ = callable({ plan: this.plan });
    }

    this.save = false;

    this.days = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

  }

  buildExportData(){

    let exportAreas = [];

    Object.keys(this.plan.areas).forEach((area) => {
      let count = this.plan.areas[area]._stats.count - this.plan.areas[area]._stats.completed;
      let m2: any = this.forwardPlannerService.existingM2(this.plan.areas[area]);
      let exportArea = {
        "Area" : this.plan.areas[area]._id,
        "Panel Qty": count,
        "Panel M2 (Existing)": m2,
        "Start Date": this.dateUtilService.toUserDate(this.plan.areas[area]._productionStartDate),
        "End Date": this.plan.areas[area]._productionEndDate === null ? "" : this.dateUtilService.toUserDate(this.plan.areas[area]._productionEndDate),
        "Delivery Date": this.dateUtilService.toUserDate(this.plan.areas[area]._deliveryDate)
      }
      exportAreas.push(exportArea);
    });

    this.exportAreas = exportAreas;
  }

  calculateAreaPlan(area, useUnpublishedDate){

    delete area._productionStartDate;
    let productionLine = area._productionLine;
    let lineBlotter = this.blotter.productionLines[productionLine];
    if (!lineBlotter) {
      // Just drop area
      return false;
    }

    if (lineBlotter.nextStart < 0) {
      // No available capacity
      return false;
    }

    let productionPlan = [];

    for (let i = 0; i < this.blotter.planDays; i++) {
      if(productionPlan[i] === undefined){
        productionPlan[i] = {
          count: null,
          m2: null
        };
      }
    }

    let designHandoverDate = useUnpublishedDate ? area._unpublishedDesignHandoverDate : area._designHandoverDate;

    let buildDay = lineBlotter.nextStart;
    let buildDate = this.dateUtilService.plusDays(this.blotter.planStart, buildDay);  
    area._userChange = false;
        
    //if there is a change of plannedStart by user
    if (this.areaPlansMapped[area.$key] !== undefined && this.areaPlansMapped[area.$key].userChange) {
      var difference = this.dateUtilService.daysBetween(this.blotter.planStart, this.areaPlansMapped[area.$key].plannedStart);
      if (difference > -1){
        buildDay = difference;
        buildDate = this.areaPlansMapped[area.$key].plannedStart;
        area._userChange = true;
      }
    }
    
    if (!area._userChange){
      // Skip forward to design handover date
      while (buildDate < designHandoverDate) {
        buildDate = this.dateUtilService.plusDays(this.blotter.planStart, ++buildDay);
      }
    }

    if (buildDay > this.blotter.planDays) {
      return false;
    }

    area._productionPlan = productionPlan;

    if (area._hasUnpublishedOnly && !useUnpublishedDate) {
      // Don't consume capacity, but potentially include in area list
      return buildDate >= this.blotter.viewStart;
    }

    if(productionPlan[buildDay] === undefined){
      productionPlan[buildDay] = {
        count: null,
        m2: null
      };
    }

    let panels = (area.actualPanels || area.numberOfPanels || area.estimatedPanels || 0) - (area.completedPanels || 0);
    if (panels == 0) {
      productionPlan[buildDay].count = 0;
      productionPlan[buildDay].m2 = 0;
      area._productionStartDate = buildDate;
      area._productionEndDate = null;
    }
    while (panels > 0) {
      // Extend planning capacity
      if (lineBlotter.available.length <= buildDay) {
        let capacity = this.getCapacity(productionLine, buildDate);
        lineBlotter.available[buildDay] = capacity;
      }

      let available = lineBlotter.available[buildDay];
      if (available) {
        if (!area._productionStartDate) {
          area._productionStartDate = buildDate;
        }
        let buildCount = Math.min(panels, available);

        panels -= buildCount;
        available -= buildCount;

        lineBlotter.available[buildDay] = available;
          if (buildDay < productionPlan.length) {

            let m2 = this.forwardPlannerService.avgM2(area, buildCount);

            productionPlan[buildDay].m2 = m2;
            productionPlan[buildDay].count = buildCount;
  
          }

        if (panels == 0) {
          area._productionEndDate = buildDate;
        }
      }

      if (!available) {
        while (buildDay < lineBlotter.available.length && !lineBlotter.available[buildDay]) {
          buildDay++;
        }
        buildDate = this.dateUtilService.plusDays(this.blotter.planStart, buildDay);
      }
    }

    lineBlotter.nextStart = buildDay < this.blotter.planDays ? buildDay : -1;

    let toDo = false;

    if (area._productionEndDate) {
      toDo = area._productionEndDate >= this.blotter.viewStart;
    }
    else {
      toDo = area._productionStartDate >= this.blotter.viewStart;
    }
    if (area._deliveryDate !== undefined) {
      if (toDo) {
        //if exists in array overright if not add
        const index = this.blotter.areas.indexOf(area);
        if (index === -1) {
          this.sortKey++;
          this.blotter.areas.push(area);
        } else {
          this.blotter.areas[index] = area;
        }
      }
    }
  }

  createBlotter(plan, skipWeeks, planWeeks){
    var today = new Date();
    var planStart = this.dateUtilService.weekStart(today);
    var planDays = 7 * (skipWeeks + planWeeks);

    this.blotter = {
      planStart: planStart,
      planDays: planDays,
      viewStart: plan.viewStart,
      current: plan.current,
      productionLines: {},
      areas: []
    };

    var nextStart = this.dateUtilService.dayOfWeek(today);
    Object.keys(this.defaultLineCapacity).forEach((productionLine) => {
      var lineBlotter = {
        capacity: [],
        m2:[],
        available: [],
        nextStart: nextStart
      };

      for (var i = 0; i < planDays; i++) {
        var buildDate = this.dateUtilService.plusDays(planStart, i);
        var capacity = this.getCapacity(productionLine, buildDate);
        let available = this.getAvailable(productionLine, capacity, buildDate, today);
        
        lineBlotter.m2.push(0);
        lineBlotter.capacity.push(capacity);
        lineBlotter.available.push(available);
      }

      this.blotter.productionLines[productionLine] = lineBlotter;
    });

    return this.blotter;
  }

  getDate(area){
    let date = area.additionalData !== undefined && area.additionalData.DFMDate !== undefined ? area.additionalData.DFMDate : area._designHandoverDate;
    return this.dateUtilService.toPlannerDate(date);
  }


  getDateNormal(area){
    return area.additionalData !== undefined && area.additionalData.DFMDate !== undefined ? area.additionalData.DFMDate : area._designHandoverDate;
  }

  getBackground(area){
    let dt = this.getDateNormal(area);
    let dfm = new Date(dt);
    let productionStartDate = new Date(area._productionStartDate);
    let dec = dfm >= productionStartDate ? true : false;
    return dec;
  }

  areasMapper(buildObj){
    let objHolder = {};
    buildObj.map((snap) => {
      objHolder[snap.$key] = snap;
    });
    return objHolder;
  }

  mapFieldSort(plannerSort){
    this.fieldFilters = [];
    const returnPlannerSort = plannerSort.payload.val();
    if(returnPlannerSort !== null){
      Object.keys(returnPlannerSort).map((dataType) => {
        let order = returnPlannerSort[dataType];
        this.fieldFilters[order] = dataType;
      });
    }
  }

  mapPanelsCompleteToday(panels){
    this.mappedPanelsComplete = {};
    panels.map((panel) => {
      let productionLine = this.productTypeProductionLine[panel.type] || null;
      if(this.mappedPanelsComplete[productionLine] === undefined){
        this.mappedPanelsComplete[productionLine] = 0;
      }
      this.mappedPanelsComplete[productionLine]++;
    });
  }

  trackByFn(index) {
    return index; // or item.id
  }

  calculateM2( areas ) {

    var calculatedM2 = {
      "line": {},
      "total": {}
    };

    areas.map((area) => {
      area._productionPlan.map((data, buildDays) => {
        if(this.plan.capacity["m2"][area._productionLine] === undefined){
          this.plan.capacity["m2"][area._productionLine] = {};
        }

        if(this.plan.capacity["m2"][area._productionLine][buildDays] === undefined){
          this.plan.capacity["m2"][area._productionLine][buildDays] = 0;
        }

        if(this.plan.capacity["total"][buildDays] === undefined){
          this.plan.capacity["total"][buildDays] = 0;
        }

        this.plan.capacity["m2"][area._productionLine][buildDays] += data.m2;
        this.plan.capacity["total"][buildDays] += data.m2;
      });
    });

    return calculatedM2;
  }

  isMeterSquaredChecked(action){
    this.toggleCount = action;
    let type = "count";
    if(!this.toggleCount){
      type = "m2";
    }
    this.m2Type = type;
  }

  getAreasFromBlotter(skipWeeks) {
    Object.keys(this.blotter.areas).forEach((areaKey) => {
      let area = this.blotter.areas[areaKey];
      area._productionPlan.splice(0, 7 * skipWeeks);
    });

    return this.blotter.areas;
  }

  getCapacityFromBlotter(skipWeeks){
    var capacity = {
      count: {},
      m2: {},
      total: {}
    };
    Object.keys(this.blotter.productionLines).forEach(productionLine => {
      let lineBlotter = this.blotter.productionLines[productionLine];
      lineBlotter.capacity.splice(0, 7 * skipWeeks);
      capacity["count"][productionLine] = lineBlotter.capacity;
      capacity["m2"][productionLine] = lineBlotter.m2;
    });
    return capacity;
  }

  getCapacity(productionLine, date) {

    if(this.lineCapacities[productionLine] === undefined){
      return null;
    }
    
    var capacity = this.lineCapacities[productionLine][date];
    if (capacity !== undefined) {
      return capacity;
    }

    if (this.dateUtilService.isWeekDay(date)) {
      return this.defaultLineCapacity[productionLine];
    }
    else {
      return null;
    }
  }

  getAvailable(productionLine, capacity, buildDate, today) {
      
    var available;
    var sum = 0;
    var panelsCompleteOnDay;
      
    panelsCompleteOnDay  = this.mappedPanelsComplete[productionLine];
    
    if(panelsCompleteOnDay === undefined || isNaN(panelsCompleteOnDay)){
      panelsCompleteOnDay = 0;
    }

    if (panelsCompleteOnDay > capacity) {
      sum = capacity;
    } else {
      sum = panelsCompleteOnDay;
    }

    if (this.dateUtilService.isWeekDay(today) && buildDate === this.dateUtilService.toIsoDate(today)) {
      available = capacity - sum;
    } else {
      available = capacity;
    }

    return available;
  }

  updateSort(field){
    //this.save = true;
    var arr = this.fieldFilters;
    var exists = arr.indexOf(field);
    if (exists > -1){
      arr.splice(exists, 1);
    }else{
      arr.push(field);
    }

    this.fieldFilters = arr;
    var newData = {};
    arr.map((filt, key) => {
      if (newData[filt] === undefined){
        newData[filt] = key;
      }
    });
    if(Object.keys(newData).length > 0){
      this.forwardPlannerService.setPlannerSort(newData);
    }else{
      this.forwardPlannerService.removePlannerSort();
    }
    
  }

  getDayHeadings() {
    var week = ["M", "T", "W", "T", "F", "S", "S" ];
    return week.concat(week, week);
  }

  weekStart(n) {
    return this.dateUtilService.plusDays(this.plan.viewStart, 7 * n);
  }

  weekFinish(n) {
    return this.dateUtilService.plusDays(this.plan.viewStart, 7 * n + 6);
  }

  useUnpublishedOnly(area) {
    return area._hasUnpublishedOnly && this.useUnpublished(area);
  }

  useUnpublished(area) {
    return area._hasUnpublished && this.areasSelection[area.$key];
  }

  atRisk(area) {
    var riskDays = this.riskDays(area);
    return riskDays === undefined || riskDays < 0;
  }

  riskDays(area){
    if (this.useUnpublishedOnly(area)) {
      return null;
    }

    if (!area._productionEndDate || !area._riskDate) {
      return undefined;
    }

    let riskDate = this.useUnpublished(area) ? area._unpublishedRiskDate : area._riskDate;
    return this.dateUtilService.daysBetweenWeekDaysOnly(area._productionEndDate, riskDate);
  }

  getRiskDays(area) {
    let days = this.riskDays(area);
    if (days === undefined) {
      return "!";
    }
    if (days == null) {
      return "";
    }
    return days > 0 ? "+" + days : days.toString();
  }

  areaUserChange(area){
    let userChange = false;
    if(this.areaPlansMapped[area.$key] !== undefined && this.areaPlansMapped[area.$key].userChange !== undefined){
      userChange = this.areaPlansMapped[area.$key].userChange;
    }
    return userChange;
  }
 
}



@Component({
  selector: 'date-header',
  styles: [`
  .example-header {
    display: flex;
    align-items: center;
    padding: 0.5em;
  }

  .example-header-label {
    flex: 1;
    height: 1em;
    font-weight: 500;
    text-align: center;
  }

  .example-double-arrow .mat-icon {
    margin: -22%;
  }
`],
  template: `
    <div>
    <div style="text-align:center;">
      <button mat-icon-button  (click)="setToday()">Reset View</button>
    </div>
    <div style="text-align:center;">
      <button mat-icon-button class="example-double-arrow" (click)="previousClicked('year')">
      <mat-icon>keyboard_arrow_left</mat-icon>
      <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <button mat-icon-button (click)="previousClicked('month')">
        <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <span class="example-header-label">{{periodLabel}}</span>
      <button mat-icon-button (click)="nextClicked('month')">
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
      <button mat-icon-button class="example-double-arrow" (click)="nextClicked('year')">
        <mat-icon>keyboard_arrow_right</mat-icon>
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
    </div>
  </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class resetCustomHeader<D> implements OnDestroy {
  private _destroyed = new Subject<void>();

  constructor(private forwardPlannerService:ForwardPlannerService,private _calendar: MatCalendar<any>,private customDateAdapter:CustomDateAdapter,@Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats, cdr: ChangeDetectorRef) {
    _calendar.stateChanges
      .pipe(takeUntil(this._destroyed))
      .subscribe(() => cdr.markForCheck());
  }
  get periodLabel() {
    return this.customDateAdapter
      .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }
  setToday(){
    let today = new Date();
    this.forwardPlannerService.setDate(today);
  }
  previousClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month' ? this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, -1) : this.customDateAdapter.addCalendarYears(this._calendar.activeDate, -1);
  }
  
  nextClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month' ?this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, 1) : this.customDateAdapter.addCalendarYears(this._calendar.activeDate, 1);
  }  
  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }
}