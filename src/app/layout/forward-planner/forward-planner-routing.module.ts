import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForwardPlannerComponent } from './forward-planner.component';

const routes: Routes = [
    {
        path: '',
        component: ForwardPlannerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ForwardPlannerRoutingModule {}
