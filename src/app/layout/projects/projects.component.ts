import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { ProjectsService } from '../../shared/services';
import { PanelsService } from '../../shared/services/panels.service'
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-projects',
    templateUrl: './projects.component.html',
    styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
    displayedColumns = ['id', 'name', 'siteStart', 'isRevit', 'createdOn', 'createdBy', 'modifiedOn', 'modifiedBy'];
    dataSource: MatTableDataSource<ProjectData>;
    _subscription: Subscription;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    projects: Array<any>;

    loaded: boolean;

    unfilteredData : Array<any>;
    sortedData:any;

    slides: object;

    constructor(private projectsService: ProjectsService, private panelsService: PanelsService) {
        this.projects = []; 
        this.loaded = false;
        this.slides = [
            {"image": "assets/images/carousel/001.jpg"}, 
            {"image": "assets/images/carousel/002.jpg"}, 
            {"image": "assets/images/carousel/003.jpg"}, 
            {"image": "assets/images/carousel/004.jpg"}, 
            {"image": "assets/images/carousel/005.jpg"}
        ];   
    }

    ngOnInit() {
        this.dataSource = new MatTableDataSource(this.projects);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this._subscription = this.projectsService.getActiveProjectsList().subscribe(projects => {
            const projectsHolder = [];   
            projects.forEach(project => {
                let projectHolder = {'$key': "",'id': "",'name': "", 'siteStart': "", 'createdOn': "", 'createdBy': "", 'modifiedOn': "",'modifiedBy': ""};
                projectHolder.$key = project.$key;
                projectHolder.id = project.id;
                projectHolder.name = project.name; 
                projectHolder.siteStart = project.siteStart;
                projectHolder.createdOn = project.timestamps.created;
                projectHolder.createdBy = project.timestamps.createdBy;
                if(project.timestamps !== undefined && project.timestamps.modified !== undefined ){
                    projectHolder.modifiedOn = project.timestamps.modified; 
                }
                if(project.timestamps !== undefined && project.timestamps.modifiedBy !== undefined ){
                    projectHolder.modifiedBy = project.timestamps.modifiedBy;
                }
                projectsHolder.push(projectHolder);
            });
            this.loaded = true;
            this.projects = projectsHolder;
            this.dataSource = new MatTableDataSource(this.projects);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.unfilteredData = Object.assign({}, this.dataSource.data);
        })
    } 

    deleteDuplicatePanels(){
        return this.panelsService.purgeDuplicatePanels();
    }

    ngOnDestroy(){
        this._subscription.unsubscribe();
    }

    sortData(sort: Sort) {
        const data = this.dataSource["data"];
        if (!sort.active || sort.direction === '') {
            sort.direction = "asc";
        }
        if(sort.direction === "asc"){
            this.dataSource.data = data.sort((a, b) => {
                switch (sort.active) {
                    case 'id': return a["id"].localeCompare(b["id"])
                    case 'name': return a["name"].localeCompare(b["name"])
                    case 'siteStart': return a["siteStart"].localeCompare(b["siteStart"])
                    default: return 0;
                }
            });            
        }
        if(sort.direction === "desc"){
            this.dataSource.data = data.sort((b, a) => {
                switch (sort.active) {
                    case 'id': return a["id"].localeCompare(b["id"])
                    case 'name': return a["name"].localeCompare(b["name"])
                    case 'siteStart': return a["siteStart"].localeCompare(b["siteStart"])
                    default: return 0;
                }
            });            
        }       
    }    

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    
}

export interface ProjectData {
    '$key': string;
    'id': string;
    'name': string;
    'siteStart': string;
    'createdOn': string;
    'createdBy': string;
    'modifiedOn': string;
    'modifiedBy': string;
}