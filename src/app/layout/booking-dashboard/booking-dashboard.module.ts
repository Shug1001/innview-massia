import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BookingDashboardRoutingModule } from './booking-dashboard-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule, MatToolbarModule, MatTooltipModule } from '@angular/material';
import { StackedGroupedChartModule } from 'src/app/shared/modules/stacked-grouped-chart/stacked-grouped-chart.module';
import { DesignEnteredModule } from 'src/app/shared/modules/design-entered/design-entered.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { AdditionalDataModule } from 'src/app/shared/modules/additional-data/additional-data.module';
import { DatePickerModule } from 'src/app/shared/modules/date-picker/date-picker.module';
import { DfmBufferModule } from 'src/app/shared/modules/dfm-buffer/dfm-buffer.module';
import { DashAreaSearchPopupComponent } from './dash-area-search-popup/dash-area-search-popup.component';
import { BookingDashboardComponent } from './booking-dashboard.component';
import { AddBookingModule } from 'src/app/shared/modules/add-booking/add-booking.module';
import { ChangeDateTimeModule } from 'src/app/shared/modules/change-date-time/change-date-time.module';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';

@NgModule({
  declarations: [BookingDashboardComponent, DashAreaSearchPopupComponent],
  entryComponents: [DashAreaSearchPopupComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatSortModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatGridListModule,
    MatInputModule,
    DfmBufferModule,
    MatToolbarModule,
    MatTooltipModule,
    DatePickerModule,
    ChangeDateTimeModule,
    AdditionalDataModule,
    PipesModule,
    DesignEnteredModule,
    AddBookingModule,
    DataExportModule,
    StackedGroupedChartModule,
    BookingDashboardRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class BookingDashboardModule { }
