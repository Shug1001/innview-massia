import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashAreaSearchPopupComponent } from './dash-area-search-popup.component';

describe('DashAreaSearchPopupComponent', () => {
  let component: DashAreaSearchPopupComponent;
  let fixture: ComponentFixture<DashAreaSearchPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashAreaSearchPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashAreaSearchPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
