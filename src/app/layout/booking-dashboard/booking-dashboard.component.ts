import { Component, OnInit } from '@angular/core';
import { Subscription, combineLatest } from 'rxjs';
import { DashboardsService } from 'src/app/shared/services/dashboards.service';
import { ForwardPlannerService } from 'src/app/shared/services/forward-planner.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material';
import { DashAreaSearchPopupComponent } from './dash-area-search-popup/dash-area-search-popup.component';
import { ActivatedRoute } from '@angular/router';
import { ReferenceDataService } from 'src/app/shared/services/reference-data.service';
import { AuthService, ProjectsService } from 'src/app/shared/services';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-booking-dashboard',
  templateUrl: './booking-dashboard.component.html',
  styleUrls: ['./booking-dashboard.component.scss']
})
export class BookingDashboardComponent implements OnInit {

  selectDate: string;
  _subscription: Subscription;
  _subscription2: Subscription;
  chartData: object;
  loaded: boolean;
  chartColors: object;
  buildAreas: Array<any>;
  bookings: object;
  true = true;
  false = false;
  noInit: boolean;
  exportAreas: Array<any>;
  projectList: object;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private projectsService: ProjectsService, private forwardPlannerService: ForwardPlannerService, private dashboardsService: DashboardsService, private dateUtilsService: DateUtilService, private dateUtilService: DateUtilService, private referenceDataService: ReferenceDataService, private authService: AuthService) {
    this.loaded = false;
    this.selectDate = this.dateUtilsService.weekStart(new Date());
    this.chartColors = {
      booked: "#0000FF",
      dispatches: "#FF0000"
    };
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DashAreaSearchPopupComponent, {
      width: '350px',
      data: {areas: this.buildAreas}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.selectDate = result;
      this.getData();
    });
  }

  calculatePercentage(a, b) {
    if (b === 0) {
      return 0;
    }
    return Math.round((a/b)*100);
  }

  round1(val){
    return this.dateUtilsService.round1(val);
  }

  objKeys(obj){
    return Object.keys(obj);
  }

  panelStat(area){
    let note = "";
    note += "Estimates: " + area.estimatedPanels || 0 + " \n ";
    note += "Design: " + area.numberOfPanels || 0 + " \n ";
    return note;
  }

  m2Stat(area){
    let note = "";
    note += "Estimates: " + area.estimatedArea || 0 + " \n ";
    note += "Design: " + area.areaOfPanels || 0 + " \n ";
    return note;
  }

  areasWithActuals(chartData, day){  
    let count = 0;
    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined && chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day] !== undefined){
      let areasArr  = chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day];
      if (areasArr !== undefined){
        areasArr.forEach((area) => {
          area.panStatus = false;
          if(area.actualPanels !== undefined && area.actualPanels > 0){
            area.panStatus = true;
            count++;
          }
        });
      }
    }
    return count;
  }

  isDrawingChecked(booking){
    let returnVal = true;
    let finder = [];
    if(booking.areas){
      for (let i = 0; i < Object.keys(booking.areas).length; i++) {
        finder.push(false);
      } 
      let j = 0;
      Object.keys(booking.areas).forEach((areaKey) => {
        this.buildAreas.forEach((area) => {
          if(areaKey === area.$key){
            if(area.additionalData && area.additionalData.drawings && area.additionalData.drawings.status){
              finder[j] = true;
            }
          }
        });
        j++;
      });

      finder.forEach((val) => {
        if(!val){
          returnVal = false;
        }
      });
    }else{
      returnVal = false;
    }

    return returnVal;
  }

  getAreas(chartData, day){
    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined && chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day] !== undefined){
      return chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day].length;
    }else{
      return 0;
    }
  }

  getRiskDays(plan){
    if (plan && plan.plannedFinish && plan.riskDate) {
      return this.dateUtilService.daysBetweenWeekDaysOnly(plan.plannedFinish, plan.riskDate);
    } else {
      return 0;
    }
  }

  dateChange(dt){
    this.selectDate = this.dateUtilsService.weekStart(new Date(dt));
    this.getData();
  }

  buildExportData(chartData){

    let exportAreas = [];

    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined){

      Object.keys(chartData.areasByDesignHandoverDate[chartData.weekStartReturn]).forEach((weekDay) => {

        chartData.areasByDesignHandoverDate[chartData.weekStartReturn][weekDay].forEach(booking => {

          let exportArea = {
            "Project Number": this.projectList[booking.project].id,
            "Project Name": this.projectList[booking.project].name,
            "Postcode": this.projectList[booking.project].postCode,
            "Vehicle Type": booking.vehicleType,
            "Load Description": booking.loadDesc,
            "Haullier": booking.haulierType,
            "Collection Date": booking.bookedTime === undefined ? "" : this.dateUtilService.toUserDate(booking.bookedTime),
            "Loading Time": booking.bookedTimeText,
            "Delivery Date": booking.deliveryTime === undefined ? "" : this.dateUtilService.toUserDate(booking.deliveryTime),
            "Delivery Time": booking.deliveryTimeText,
            "PO Number": booking.poNumber,
            "Delivery No.": booking.delNumber,
            "m2": booking.m2
          }
          exportAreas.push(exportArea);
        });

      });

    }

    this.exportAreas = exportAreas;
  }

  getData(){
    this.loaded = false;
    const daysOfWeeks = this.dashboardsService.getWeekDaysBooking(this.selectDate);
    this.chartData = this.dashboardsService.getWeeksBooking(this.selectDate, daysOfWeeks, this.buildAreas, 6, 21, this.bookings);
    this.buildExportData(this.chartData);
    console.log(this.chartData);
    this.loaded = true;
  }

  ngOnInit() {
    this.noInit = false;
    this.selectDate = this.route.snapshot.paramMap.get('id');
    this._subscription = combineLatest([this.projectsService.getProjectsList(), this.forwardPlannerService.getPlansAndAddNoFilter(), this.dashboardsService.getAllBookings()]).subscribe(([projects, buildAreas, bookings]) => {
      this.projectList = {};
      projects.forEach(element => {
        if (this.projectList[element.$key] === undefined){
          this.projectList[element.$key] = element;
        }
      });
      this.bookings = bookings;
      this.buildAreas = buildAreas;
      this.getData();
      this.noInit = true;
    });
  }

  changeAdditional(evt, data, type, bookingRef){
    data = !data;
    let updateObj = {
      "status" : data,
      "timeDate" : this.authService.serverTime(),
      "user" : null
    };
    return this.referenceDataService.getUserPromise(this.authService.authState.uid)
    .then((user) => {
      updateObj.user = this.dateUtilService.getInitials(user.name);
      return this.dashboardsService.updateBookingCheck(bookingRef, type, updateObj);
    });  
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }



}
