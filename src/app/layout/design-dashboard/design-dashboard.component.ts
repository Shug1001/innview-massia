import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DashboardsService } from 'src/app/shared/services/dashboards.service';
import { ForwardPlannerService } from 'src/app/shared/services/forward-planner.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material';
import { DashAreaSearchPopupComponent } from './dash-area-search-popup/dash-area-search-popup.component';
import { ActivatedRoute } from '@angular/router';
import { AreasService } from 'src/app/shared/services';
import { ChartData } from 'src/app/shared/models/models';

@Component({
  selector: 'app-design-dashboard',
  templateUrl: './design-dashboard.component.html',
  styleUrls: ['./design-dashboard.component.scss']
})
export class DesignDashboardComponent implements OnInit {

  selectDate: string;
  _subscription: Subscription;
  chartData: ChartData;
  loaded: boolean;
  chartColors: object;
  buildAreas: object;
  true = true;
  false = false;
  exportsDash: Array<any>;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private areasService: AreasService, private forwardPlannerService: ForwardPlannerService, private dashboardsService: DashboardsService, private dateUtilsService: DateUtilService, private dateUtilService: DateUtilService) { 
    this.loaded = false;
    this.selectDate = this.dateUtilsService.weekStart(new Date());
    this.chartColors = {
      actual: "#f70d1a",
      designed: "#76fa4e",
      predicted: "#131af8"
    };
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DashAreaSearchPopupComponent, {
      width: '350px',
      data: {areas: this.buildAreas}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.selectDate = result;
      this.getData();
    });
  }

  areasWithActuals(chartData, day){  
    let count = 0;
    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined && chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day] !== undefined){
      let areasArr  = chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day];
      if (areasArr !== undefined){
        areasArr.forEach((area) => {
          area.panStatus = false;
          if(area.actualPanels !== undefined && area.actualPanels > 0){
            area.panStatus = true;
            count++;
          }
        });
      }
    }
    return count;
  }

  getAreas(chartData, day){
    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined && chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day] !== undefined){
      return chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day].length;
    }else{
      return 0;
    }
  }

  getRiskDays(plan){
    if (plan && plan.plannedFinish && plan.riskDate) {
      return this.dateUtilService.daysBetweenWeekDaysOnly(plan.plannedFinish, plan.riskDate);
    } else {
      return 0;
    }
  }

  dateChange(dt){
    this.selectDate = this.dateUtilsService.weekStart(new Date(dt));
    this.getData();
  }

  getData(){
    let exportsDash = [];
    const daysOfWeeks = this.dashboardsService.getWeekDays(this.selectDate);
    const weeks = this.dashboardsService.getWeeks(this.selectDate, this.buildAreas, 6, 21); 
    this.chartData = this.dashboardsService.areasForWeek(this.selectDate, weeks, daysOfWeeks, this.buildAreas);
  
      Object.keys(this.chartData.areasByDesignHandoverDate[this.chartData.weekStartReturn]).forEach(day => {
        Object.keys(this.chartData.areasByDesignHandoverDate[this.chartData.weekStartReturn][day]).forEach(number => {
        let dayData = this.chartData.areasByDesignHandoverDate[this.chartData.weekStartReturn][day][number];
        let exportArea = {
          "Day" : day,
          "Area": dayData._id,
          "Number_ofPanels": dayData.numberOfPanels,
          "Area of Panels": dayData.areaOfPanels,
          "Risk": this.getRiskDays(dayData.plan),
          "Start Date": dayData.plan && dayData.plan.plannedStart ? dayData.plan.plannedStart : "No Plan",
          "DFM Date": dayData.additionalData && dayData.additionalData.DFMDate ? dayData.additionalData.DFMDate : dayData._designHandoverDate,
          "DFM Checked": dayData.additionalData && dayData.additionalData.checkedDFM ? dayData.additionalData.checkedDFM.status : "",
          "DFM Checked User": dayData.additionalData && dayData.additionalData.checkedDFM ? dayData.additionalData.checkedDFM.user : "",
          "HandoverConfirmed": dayData.additionalData && dayData.additionalData.handoverConfirmed ? dayData.additionalData.handoverConfirmed.status : "",
          "HandoverConfirmed User": dayData.additionalData && dayData.additionalData.handoverConfirmed ? dayData.additionalData.handoverConfirmed.user : "",
          "Area Uploaded": dayData.actualPanels ? "True" : "False",
          "Drawing": dayData.additionalData && dayData.additionalData.drawings ? dayData.additionalData.drawings.status : "",
          "Drawing User": dayData.additionalData && dayData.additionalData.drawings ? dayData.additionalData.drawings.user : ""
        }
        exportsDash.push(exportArea);
      });
    
  
    });

    this.exportsDash = exportsDash;
    console.log(this.chartData);
  }

  ngOnInit() {
    this.selectDate = this.route.snapshot.paramMap.get('id');
    this._subscription = this.forwardPlannerService.getPlansAndAddNoFilterAll().subscribe((buildAreas) => {
      this.buildAreas = buildAreas;
      this.getData();
      this.loaded = true;
    });
  }

  dateChange2(areaRef, date){
    return this.areasService.updateProjectAdditionalSingular(areaRef, "DFMDate", date);
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }



}
