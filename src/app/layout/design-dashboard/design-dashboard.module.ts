import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignDashboardRoutingModule } from './design-dashboard-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule, MatSortModule, MatTableModule, MatPaginatorModule, MatCardModule, MatInputModule, MatFormFieldModule, MatIconModule, MatGridListModule, MatCheckboxModule, MatDatepickerModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { DesignDashboardComponent } from './design-dashboard.component';
import { DfmBufferModule } from 'src/app/shared/modules/dfm-buffer/dfm-buffer.module';
import { AdditionalDataModule } from 'src/app/shared/modules/additional-data/additional-data.module';
import { StackedGroupedChartModule } from 'src/app/shared/modules/stacked-grouped-chart/stacked-grouped-chart.module';
import { DesignEnteredModule } from 'src/app/shared/modules/design-entered/design-entered.module';
import { DatePickerModule } from 'src/app/shared/modules/date-picker/date-picker.module';
import { DashAreaSearchPopupComponent } from './dash-area-search-popup/dash-area-search-popup.component';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';

@NgModule({
  declarations: [DesignDashboardComponent, DashAreaSearchPopupComponent],
  entryComponents: [DashAreaSearchPopupComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatSortModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatGridListModule,
    MatInputModule,
    DfmBufferModule,
    DataExportModule,
    DatePickerModule,
    AdditionalDataModule,
    PipesModule,
    MatDatepickerModule,
    DesignEnteredModule,
    StackedGroupedChartModule,
    DesignDashboardRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class DesignDashboardModule { }
