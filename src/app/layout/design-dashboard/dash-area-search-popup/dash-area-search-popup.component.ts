import { Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dash-area-search-popup',
  templateUrl: './dash-area-search-popup.component.html',
  styleUrls: ['./dash-area-search-popup.component.scss']
})
export class DashAreaSearchPopupComponent implements OnInit {

  displayedColumns: string[] = ['id', 'find'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialogRef: MatDialogRef<DashAreaSearchPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.data.areas);
      }
    
      ngOnInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    
      applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
      
      onNoClick(): void {
        this.dialogRef.close();
      }
    }


