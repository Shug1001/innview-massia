import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectStatusRoutingModule } from './project-status-routing.module';
import { ProjectStatusComponent } from './project-status.component';
import { MatToolbarModule, MatSortModule, MatTableModule, MatFormFieldModule, MatPaginatorModule, MatIconModule, MatInputModule, MatGridListModule, MatSnackBarModule, MatTabsModule, MatButtonModule, MatAutocompleteModule } from '@angular/material';
import { UserFromRefModule } from 'src/app/shared/modules/user-from-ref/user-from-ref.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { ProjectStatusDetailsModule } from 'src/app/shared/modules/project-status-details/project-status-details.module';
import { AreaStatusDetailsModule } from 'src/app/shared/modules/area-status-details/area-status-details.module';
import { PanelStatusDetailsModule } from 'src/app/shared/modules/panel-status-details/panel-status-details.module';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';
import { DownloadDialogComponent } from './download-dialog/download-dialog.component';
import { UpdateAzureComponent } from './update-azure/update-azure.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProjectStatusComponent, DownloadDialogComponent, UpdateAzureComponent],
  entryComponents: [DownloadDialogComponent, UpdateAzureComponent],
  imports: [
    CommonModule,
    ProjectStatusRoutingModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
    MatGridListModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    UserFromRefModule,
    PipesModule,
    DataExportModule,
    ProjectStatusDetailsModule,
    AreaStatusDetailsModule,
    PanelStatusDetailsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class ProjectStatusModule { }
