import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { ProjectsService, AreasService, ForwardPlannerService, ComponentsService } from 'src/app/shared/services';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { FormControl } from '@angular/forms';
import { ProjectStat } from 'src/app/shared/models/models';
import { StatusService } from 'src/app/shared/services/status.service';
import { DownloadDialogComponent } from './download-dialog/download-dialog.component';
import { UpdateAzureComponent } from './update-azure/update-azure.component';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-project-status',
  templateUrl: './project-status.component.html',
  styleUrls: ['./project-status.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectStatusComponent implements OnInit {

  _subscription: Subscription;
  _subscription2: Subscription;
  projects: Array<any>;
  projectsStatus: ProjectStat[];
  loaded: boolean;
  loaded2: boolean;
  unfilteredData : Array<any>;
  sortedData:any;
  projectStatus: object;
  loadedCount: number;
  selected = new FormControl(0);
  selectedProject: object;
  selectedArea: object;
  filterValue: string;
  dataSource: MatTableDataSource<ProjectStat>;
  exportAreas: Array<any>;
  status: string;
  statusLoading: boolean;
  proIds: Array<any>;
  myControl = new FormControl('');
  filteredOptions: Observable<string[]>;
  projectsHolder: object;

  constructor(private _snackBar: MatSnackBar, 
    private plannerService: ForwardPlannerService, 
    private dateUtilService: DateUtilService,
    private componentsService: ComponentsService,
    public dialog: MatDialog,
    private statusService: StatusService,
    private projectsService: ProjectsService) {
      this.projects = [];
      this.projectsStatus = [];
      this.projectsHolder = {};
      this.projectStatus = {};
      this.loadedCount = 0;
      this.filterValue = "";
      this.dataSource = new MatTableDataSource<ProjectStat>([]);
      this.loaded = false;
      this.loaded2 = false;
  }

  ngOnInit() {
    this.exportAreas = [];
    this.status = "";
    this.proIds = [];
    this._subscription = this.projectsService.getActiveProjectsList().subscribe(projects => {
      this.sortBySiteStart(projects);
      this.projects = projects;
      this.proIds = [];
      this.projectStatus = {};
      projects.forEach((pro) => {
        this.proIds.push(pro.id);
        if(this.projectsHolder[pro.id]===undefined){
          this.projectsHolder[pro.id] = pro;
        }
      });
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value || '')),
      );
      this.loaded = true;
    });

  }

  onSelectionChange(evt){
    this.loaded2 = false;
    let project = this.projectsHolder[evt.option.value];
    this._subscription2 = this.plannerService.getProjectStats(project).subscribe((projectStats)=>{
      
      const index = this.projectsStatus.findIndex((e) => e.projectId === projectStats.projectId);
      if (index === -1) {
        this.projectsStatus.push(projectStats);
      } else {
        this.projectsStatus[index] = projectStats;
      }
      this.dataSource = new MatTableDataSource<ProjectStat>(this.projectsStatus);
      this.loaded2 = true;
      
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.proIds.filter(option => option.toLowerCase().includes(filterValue));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DownloadDialogComponent, {
      width: '250px'
    });

  }

  openDialog2(): void {
    const dialogRef = this.dialog.open(UpdateAzureComponent, {
      width: '250px'
    });

  }

  projectTabSelected(){
    if(this.projects.length && this.loadedCount < this.projects.length){
      this.plannerService.setProjectStats(this.projects[this.loadedCount]);
    }
  }

  getArea(stats){
    this._snackBar.dismiss();
    this.selectedArea = null;
    this.selectedProject = stats
    this.selected.setValue(1);
  }

  selectPanel(areaStatus){
    this.selectedArea = areaStatus;
    this.selected.setValue(2);
  }

  sortByDeliveryDate(items) {
    items.sort(function (a, b) {
      if (a.delivery < b.delivery) {
        return 1;
      } else if (a.delivery > b.delivery) {
        return -1;
      } else {
        return 0;
      }
    });
  }

  sortBySiteStart(items) {
    items.sort(function (a, b) {
      if (a.siteStart < b.siteStart) {
        return -1;
      } else if (a.siteStart > b.siteStart) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  ngOnDestroy(){
    if(this._subscription){
      this._subscription.unsubscribe();
    }
    if(this._subscription2){
      this._subscription2.unsubscribe();
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.data = this.projectsStatus;
    filterValue = filterValue.trim().toLowerCase(); // Remove whitespace
    this.filterValue = filterValue;
    this.dataSource.filter = filterValue;
  }

}
