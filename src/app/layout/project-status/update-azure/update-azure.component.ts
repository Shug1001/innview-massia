import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { combineLatest, Subscription } from 'rxjs';
import { ComponentsService, ForwardPlannerService, ProjectsService } from 'src/app/shared/services';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { StatusService } from 'src/app/shared/services/status.service';

@Component({
  selector: 'app-update-azure',
  templateUrl: './update-azure.component.html',
  styleUrls: ['./update-azure.component.scss']
})
export class UpdateAzureComponent implements OnInit {


  status: string;
  exportAreas: Array<any>;
  _subscription: Subscription;

  constructor(
    private projectsService: ProjectsService,
    private plannerService: ForwardPlannerService,
    private componentsService: ComponentsService,
    private statusService: StatusService,
    private dateUtilService: DateUtilService,
    private http: HttpClient,
    public dialogRef: MatDialogRef<UpdateAzureComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  projectPlanLoad(val){
    console.log(JSON.stringify(val));
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectPlanLoad',val).toPromise();
  }

  ngOnInit() {
    let promises = [];
    this.status = "loading...";
    this._subscription = combineLatest(this.plannerService.getPlansAndAddNoFilterAll(), this.componentsService.getcomponentsList()).subscribe(([status, components]) => {
      console.log(status);
      let exportAreas = [];
      
      this.status = "updating...";

      status.forEach((stat) => {

        let exportArea = {
          "AreaKey": "",
          "Project_ID": "",
          "Project_Name": "",
          "Area": "",
          "Component": "",
          "Delivery_Date": "",
          "Design_Handover": "",
          "Planned_Start": "",
          "Planned_Finish": "",
          "Handover_Conf": "",
          "Risk": 0,
          "Estimated_Count": "",
          "Estimated_M2": "",
          "Actual_Count": "",
          "Actual_M2": "",
          "Completed_Count": "",
          "Completed_M2": "",
          "Supplier": ""
        }

        exportArea.AreaKey = stat.$key;
        exportArea.Project_ID = stat._projectDetails.id;
        exportArea.Project_Name = stat._projectDetails.name;
        exportArea.Area = stat.phase + "-" + stat.floor + "-" + stat.type;;
        exportArea.Component = this.statusService.getComponent(components, stat.$key);
        exportArea.Delivery_Date = stat._deliveryDate ? stat._deliveryDate : stat._unpublishedDeliveryDate;
        exportArea.Design_Handover = stat._designHandoverDate;
        exportArea.Planned_Start = stat.plan && stat.plan.plannedStart ? stat.plan.plannedStart : "";
        exportArea.Planned_Finish = stat.plan && stat.plan.plannedFinish ? stat.plan.plannedFinish : "";
        exportArea.Handover_Conf = stat.additionalData && stat.additionalData.handoverConfirmed ? this.dateUtilService.toIsoDate(stat.additionalData.handoverConfirmed.timeDate) : "";
        exportArea.Risk = Number(this.dateUtilService.daysBetweenWeekDaysOnly(stat.plan && stat.plan.plannedFinish ? stat.plan.plannedFinish : "", stat.plan && stat.plan.riskDate ? stat.plan.riskDate : ""));
        exportArea.Estimated_Count = stat.estimatedPanels ? String(stat.estimatedPanels): "0";
        exportArea.Estimated_M2 = stat.estimatedArea ? String(stat.estimatedArea): "0";
        exportArea.Actual_Count = stat.actualPanels ? String(stat.actualPanels) : "0";
        exportArea.Actual_M2 = stat.actualArea ? String(stat.actualArea) : "0";
        exportArea.Completed_Count = stat.completedPanels ? String(stat.completedPanels) : "0";
        exportArea.Completed_M2 = stat.completedArea ? String(stat.completedArea) : "0";
        exportArea.Supplier = stat.supplier;

        promises.push(this.projectsService.updateProjectPlanLoad(exportArea));
      });

      return Promise.all(promises)
      .then(()=>{
        this.status = "complete";
      });

    });

    
  }
}
