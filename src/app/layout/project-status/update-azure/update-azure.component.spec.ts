import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAzureComponent } from './update-azure.component';

describe('UpdateAzureComponent', () => {
  let component: UpdateAzureComponent;
  let fixture: ComponentFixture<UpdateAzureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAzureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAzureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
