import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { combineLatest, Subscription } from 'rxjs';
import { ComponentsService, ForwardPlannerService } from 'src/app/shared/services';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { StatusService } from 'src/app/shared/services/status.service';

@Component({
  selector: 'app-download-dialog',
  templateUrl: './download-dialog.component.html',
  styleUrls: ['./download-dialog.component.scss']
})
export class DownloadDialogComponent implements OnInit {

  status: string;
  exportAreas: Array<any>;
  _subscription: Subscription;

  constructor(
    private plannerService: ForwardPlannerService,
    private componentsService: ComponentsService,
    private statusService: StatusService,
    private dateUtilService: DateUtilService,
    private http: HttpClient,
    public dialogRef: MatDialogRef<DownloadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  projectPlanLoad(val){
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectPlanLoad',val).toPromise();
  }

  ngOnInit() {
    let promises = [];
    this.status = "loading...";
    this._subscription = combineLatest(this.plannerService.getPlansAndAddNoFilterAll(), this.componentsService.getcomponentsList()).subscribe(([status, components]) => {
      console.log(status);
      let exportAreas = [];
      

      status.forEach((stat) => {

        let exportArea = {
          "Project_ID": "",
          "Project_Name": "",
          "Area": "",
          "Area_Key": "",
          "Component": "",
          "Delivery_Date": "",
          "Design_Handover": "",
          "Start_Date": "",
          "Finish_Date": "",
          "Handover_Conf": "",
          "Risk": "",
          "Estimated_Count": "",
          "Estimated_M2": "",
          "Actual_Count": "",
          "Actual_M2": "",
          "Completed_Count": "",
          "Completed_M2": "",
          "Supplier": ""
        }

        exportArea.Project_ID = stat._projectDetails.id;
        exportArea.Project_Name = stat._projectDetails.name;
        exportArea.Area = stat._id;
        exportArea.Area_Key = stat.$key;
        exportArea.Component = this.statusService.getComponent(components, stat.$key);
        exportArea.Delivery_Date = stat._deliveryDate ? stat._deliveryDate : stat._unpublishedDeliveryDate;
        exportArea.Design_Handover = stat._designHandoverDate
        exportArea.Start_Date = stat.plan && stat.plan.plannedStart ? stat.plan.plannedStart : "";
        exportArea.Finish_Date = stat.plan && stat.plan.plannedFinish ? stat.plan.plannedFinish : "";
        exportArea.Handover_Conf = stat.additionalData && stat.additionalData.handoverConfirmed ? this.dateUtilService.toIsoDate(stat.additionalData.handoverConfirmed.timeDate) : "";
        exportArea.Risk = String(this.dateUtilService.daysBetweenWeekDaysOnly(stat.plan && stat.plan.plannedFinish ? stat.plan.plannedFinish : "", stat.plan && stat.plan.riskDate ? stat.plan.riskDate : ""));
        exportArea.Estimated_Count = stat.estimatedPanels ? String(stat.estimatedPanels): "0";
        exportArea.Estimated_M2 = stat.estimatedArea ? String(stat.estimatedArea): "0";
        exportArea.Actual_Count = stat.actualPanels ? String(stat.actualPanels) : "0";
        exportArea.Actual_M2 = stat.actualArea ? String(stat.actualArea) : "0";
        exportArea.Completed_Count = stat.completedPanels ? String(stat.completedPanels) : "0";
        exportArea.Completed_M2 = stat.completedArea ? String(stat.completedArea) : "0";
        exportArea.Supplier = stat.supplier;

        exportAreas.push(exportArea);

      });

      this.exportAreas = exportAreas;
      this.status = "ready";

    });

  }

}
