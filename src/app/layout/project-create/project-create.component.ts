import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ProjectsService, AreasService } from 'src/app/shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import { Router } from '@angular/router';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { HttpClient } from '@angular/common/http';
import { DateUtilService } from 'src/app/shared/services/date-util.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ProjectCreateComponent implements OnInit {

  minDate = new Date();
  projectControl: FormGroup;
  additionalControl: FormGroup;
  siteStarted: boolean;
  siteStartDateIsValid: boolean;
  nextDeliveryDateAfterToday: boolean;
  userKeys: Array<any>;
  submitted: boolean;
  gatewayDate: string;

  constructor(private http: HttpClient, public dialog: MatDialog,  private dateUtilService: DateUtilService, private router: Router, private revisionsService: RevisionsService, private fb: FormBuilder, private projectsService: ProjectsService, private areasService: AreasService) { 
    this.userKeys = [];
    this.submitted = false;
    this.nextDeliveryDateAfterToday = false;
    this.siteStarted = false;
    this.gatewayDate = this.dateUtilService.toIsoDate(new Date());
  }

  ngOnInit(){
    this.setUpForm();
  }

  toHumanDate(date){
    return this.dateUtilService.toUserSlachDate(date);
  }

  isLoginToken(): boolean {
    const field = this.projectControl.get('id');
    return field.hasError('idAvailable');
  }

  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 49 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }

  setUpForm(){

    this.projectControl = this.fb.group({
      'name': new FormControl(
        // initial value
        null,
        // sync built-in validators
        Validators.compose(
          [ Validators.required ],
        ),
        // custom async validator
        this.areasService.projNameValidator()), 
      'id': new FormControl(
        // initial value
        null,
        // sync built-in validators
        Validators.compose(
          [ Validators.required, Validators.pattern('\\d\\d\\d$') ],
        ),
        // custom async validator
        this.areasService.projIdValidator()), 
      'client': new FormControl(null, [Validators.required]),
      'datumType': new FormControl(null, [Validators.required]),
      'chainOfCustody': new FormControl(),
      'siteAccess': new FormControl(),
      'siteAddress': new FormControl(),
      'postCode': new FormControl(),
      'weekDuration': new FormControl(),
      'siteStart': new FormControl(null, [Validators.required]),
      'siteCompletion': new FormControl(null),
      'plannedStart': new FormControl(null),
      'plannedCompletion': new FormControl(null),
      'includeSaturday': new FormControl(false),
      'specialRequirements': new FormControl(),
      'estimatedAreas': this.fb.group({
        'Int': new FormControl(null, [Validators.required]),
        'Ext': new FormControl(null, [Validators.required]),
        'Floor': new FormControl(null, [Validators.required]),
        'Roof': new FormControl(null, [Validators.required]),
        'ExtF': new FormControl(null, [Validators.required])
      }),
      'gateways': this.fb.group({
        'G5Planned': new FormControl(null),
        'G5Actual': new FormControl(null),
        'G6Planned': new FormControl(null),
        'G6Actual': new FormControl(null),
        'G7aPlanned': new FormControl(null),
        'G7aActual': new FormControl(null),
        'G7bPlanned': new FormControl(null),
        'G7bActual': new FormControl(null),
        'G8Planned': new FormControl(null),
        'G8Actual': new FormControl(null),
        'G9Planned': new FormControl(null),
        'G9Actual': new FormControl(null)
      }),
      'gateway': new FormControl(),
      'gifa': new FormControl(),
      'steel': new FormControl(),
      'delivery-manager': new FormControl(),
      'notifyUsers': new FormControl()
    });

    this.additionalControl = this.fb.group({
      'site-ops': new FormControl(),
      'designer': new FormControl(),
      "quantity-surveyor":new FormControl(), 
      "engineer": new FormControl(),
      "engineer2": new FormControl(),
      "team": new FormControl(),  
      "installer": new FormControl(), 
    });

  }

  projectData(val){
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectData',val).toPromise();
  }

  onSubmit(): void{
    this.submitted = true;
    let updateObj = this.projectControl.value;
    updateObj = this.projectsService.convertFormToDatabaseStruct(updateObj);


    updateObj.siteCompletion = updateObj.siteCompletion !== null ? this.dateUtilService.toIsoDate(updateObj.siteCompletion) : null;
    updateObj.plannedCompletion = updateObj.plannedCompletion !== null ? this.dateUtilService.toIsoDate(updateObj.plannedCompletion) : null;
    updateObj.plannedStart = updateObj.plannedStart !== null ? this.dateUtilService.toIsoDate(updateObj.plannedStart) : null;

    if(updateObj.gateways !== null){
      if(updateObj.gateways.G5Planned !== null){
        updateObj.gateways.G5Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G5Planned);
      }
      if(updateObj.gateways.G5Actual !== null){
        updateObj.gateways.G5Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G5Actual);
      }
      if(updateObj.gateways.G6Planned !== null){
        updateObj.gateways.G6Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G6Planned);
      }
      if(updateObj.gateways.G6Actual !== null){
        updateObj.gateways.G6Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G6Actual);
      }
      if(updateObj.gateways.G7aPlanned !== null){
        updateObj.gateways.G7aPlanned = this.dateUtilService.toIsoDate(updateObj.gateways.G7aPlanned);
      }
      if(updateObj.gateways.G7aActual !== null){
        updateObj.gateways.G7aActual = this.dateUtilService.toIsoDate(updateObj.gateways.G7aActual);
      }
      if(updateObj.gateways.G7bPlanned !== null){
        updateObj.gateways.G7bPlanned = this.dateUtilService.toIsoDate(updateObj.gateways.G7bPlanned);
      }
      if(updateObj.gateways.G7bActual !== null){
        updateObj.gateways.G7bActual = this.dateUtilService.toIsoDate(updateObj.gateways.G7bActual);
      }
      if(updateObj.gateways.G8Planned !== null){
        updateObj.gateways.G8Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G8Planned);
      }
      if(updateObj.gateways.G8Actual !== null){
        updateObj.gateways.G8Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G8Actual);
      }
      if(updateObj.gateways.G9Planned !== null){
        updateObj.gateways.G9Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G9Planned);
      }
      if(updateObj.gateways.G9Actual !== null){
        updateObj.gateways.G9Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G9Actual);
      }
    }

    let updateObjAdditional = this.additionalControl.value;
    updateObjAdditional = this.projectsService.convertToAdditional(updateObjAdditional);

    let progKey = "";
    this.projectsService.createProject(updateObj)
      .then((createdProject)=>{
        progKey = createdProject.key;
        return this.projectsService.updateProjectAdditional(progKey, updateObjAdditional);
      })
      .then(()=>{
        return this.revisionsService.createRevisionForProject(progKey);
      })
      .then((revision)=>{
        return this.revisionsService.setUnpublishedRevisionOnProject(revision);
      })
      .then(()=>{
        this.router.navigate(['/project/' + progKey]);
      });
  }
  
}
