import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            },
            {
                path: 'components',
                loadChildren: './material-components/material-components.module#MaterialComponentsModule'
            },
            {
                path: 'forms',
                loadChildren: './forms/forms.module#FormsModule'
            },
            {
                path: 'grid',
                loadChildren: './grid/grid.module#GridModule'
            },
            {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            },
            {
                path: 'blank-page',
                loadChildren: './blank-page/blank-page.module#BlankPageModule'
            },
            {
                path: 'design-issued',
                loadChildren: './design-issued/design-issued.module#DesignIssuedModule'
            },
            {
                path: 'performance',
                loadChildren: './performance/performance.module#PerformanceModule'
            },
            {
                path: 'performance2',
                loadChildren: './performance2/performance.module#PerformanceModule'
            },
            {
                path: 'forward-planner2',
                loadChildren: './forward-planner2/forward-planner2.module#ForwardPlanner2Module'
            },
            {
                path: 'forward-planner3',
                loadChildren: './forward-planner3/forward-planner3.module#ForwardPlanner3Module'
            },
            {
                path: 'forward-planner4',
                loadChildren: './forward-planner4/forward-planner4.module#ForwardPlanner4Module'
            },
            {
                path: 'projects',
                loadChildren: './projects/projects.module#ProjectsModule'
            },
            {
                path: 'quotes',
                loadChildren: './quotes/quotes.module#QuotesModule'
            },
            {
                path: 'quote/:id',
                loadChildren: './quote/quote.module#QuoteModule'
            },
            {
                path: 'project/:id',
                loadChildren: './project/project.module#ProjectModule'
            },
            {
                path: 'project-create',
                loadChildren: './project-create/project-create.module#ProjectCreateModule'
            },
            {
                path: 'forecast',
                loadChildren: './forecast/forecast.module#ForecastModule'
            },
            {
                path: 'material-request',
                loadChildren: './material-request/material-request.module#MaterialRequestModule'
            },
            {
                path: 'material-request-create',
                loadChildren: './material-request-create/material-request-create.module#MaterialRequestCreateModule'
            },
            {
                path: 'material-request-details/:id',
                loadChildren: './material-request-details/material-request-details.module#MaterialRequestDetailsModule'
            },
            {
                path: 'project-status',
                loadChildren: './project-status/project-status.module#ProjectStatusModule'
            },
            {
                path: 'design-dashboard/:id',
                loadChildren: './design-dashboard/design-dashboard.module#DesignDashboardModule'
            },
            {
                path: 'logistics-dashboard/:id',
                loadChildren: './logistics-dashboard/logistics-dashboard.module#LogisticsDashboardModule'
            },
            {
                path: 'booking-dashboard/:id',
                loadChildren: './booking-dashboard/booking-dashboard.module#BookingDashboardModule'
            },
            {
                path: 'admin',
                loadChildren: './admin/admin.module#AdminModule'
            },
            {
                path: 'active-projects',
                loadChildren: './active-projects/active-projects.module#ActiveProjectsModule'
            },
            {
                path: 'export',
                loadChildren: './export/export.module#ExportModule'
            },
            {
                path: 'supply-chain',
                loadChildren: './supply-chain/supply-chain.module#SupplyChainModule'
            },
            {
                path: 'load-panels',
                loadChildren: './load-panels/load-panels.module#LoadPanelsModule'
            },
            {
                path: 'datums',
                loadChildren: './datums/datums.module#DatumsModule'
            },
            {
                path: 'audits',
                loadChildren: './audits/audits.module#AuditsModule'
            },
            {
                path: 'dev-progress',
                loadChildren: './dev-progress/dev-progress.module#DevProgressModule'
            },
            {
                path: 'stacking',
                loadChildren: './stacking/stacking.module#StackingModule'
            },
            {
                path: 'jobsheets',
                loadChildren: './jobsheets/jobsheets.module#JobsheetsModule'
            },
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
