import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { ProjectsService } from '../../shared/services';
import { Subscription, combineLatest } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DimensionComponent } from './dimension/dimension.component';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss']
})
export class QuotesComponent implements OnInit {

  displayedColumns = ['qid', 'project', 'quoteName', 'isActive', 'delete'];
  dataSource: MatTableDataSource<QuotesData>;
  _subscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  quotes: Array<any>;
  projects: Array<any>;
  quotesList: Array<any>;
  activeProjects = {};

  loaded: boolean;
  result: {};

  unfilteredData : Array<any>;
  sortedData:any;

  constructor(private projectsService: ProjectsService, public dialog: MatDialog) {
      this.quotes = [];
      this.quotesList = [];
      this.loaded = false;
  }

  ngOnInit() {
      this.dataSource = new MatTableDataSource(this.quotes);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this._subscription = combineLatest(this.projectsService.getQuotesList(), this.projectsService.getProjectsList()).subscribe(([quotes, projects]) => {
          this.projects = projects;
          this.quotesList = quotes;
          const quotesHolder = [];   
          this.projects.forEach(project => {
            if(this.activeProjects[project.$key] === undefined){
              this.activeProjects[project.$key] = project.active;
            }
          });
          quotes.forEach(quote => {
            let projectHolder = {'$key': "",'qid': "", 'project': "", 'quoteName': "", 'latest':""};
            projectHolder.$key = quote.$key;
            projectHolder.qid = quote.qid;
            projectHolder.project = quote.project;
            projectHolder.quoteName = quote.quoteName;
            projectHolder.latest = quote.latest;  
            quotesHolder.push(projectHolder);
          });
          this.loaded = true;
          this.quotes = quotesHolder;

          this.quotes.sort((quoteA, quoteB) => {
            var o1 = quoteA.qid.toLowerCase();
            var o2 = quoteB.qid.toLowerCase();
            
            var p1 = quoteA.quoteName.toLowerCase();
            var p2 = quoteB.quoteName.toLowerCase();

            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
          });

          this.quotes.reverse();

          this.dataSource = new MatTableDataSource(this.quotes);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.unfilteredData = Object.assign({}, this.dataSource.data);
      })
  }

  projectActive(projectKey){
    return this.projects.find(element => element.$key === projectKey);
  }

  deleteQuotesInactive(quoteKey){
    return this.projectsService.deleteQuotesInactive(quoteKey);
  }

  dimensionsPopup(){
    const dialogRef = this.dialog.open(DimensionComponent, {
        data: {
            quotes: this.quotesList
        }
    });
    dialogRef.componentInstance.data = {quotes: []};
  }

  returnQuote(title){
    return title.split("-")[0];
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  sortData(sort: Sort) {
      const data = this.dataSource["data"];
      if (!sort.active || sort.direction === '') {
          sort.direction = "asc";
      }
      if(sort.direction === "asc"){
          this.dataSource.data = data.sort((a, b) => {
              switch (sort.active) {
                  case 'qid': return a["qid"].localeCompare(b["qid"])
                  case 'quoteName': return a["quoteName"].localeCompare(b["quoteName"])
                  default: return 0;
              }
          });            
      }
      if(sort.direction === "desc"){
          this.dataSource.data = data.sort((b, a) => {
              switch (sort.active) {
                  case 'qid': return a["qid"].localeCompare(b["qid"])
                  case 'quoteName': return a["quoteName"].localeCompare(b["quoteName"])
                  default: return 0;
              }
          });            
      }       
  }    

  applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
      if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
      }
  }
}

export interface QuotesData {
  '$key': string;
  'qid': string;
  'latest': number;
  'quoteName': string;
  'createdOn': string;
  'createdBy': string;
  'modifiedOn': string;
  'modifiedBy': string;
}
