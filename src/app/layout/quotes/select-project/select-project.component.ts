import { Component, OnInit, Input } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Project, Quote } from 'src/app/shared/models/models';
import { ProjectsService } from '../../../shared/services/';

@Component({
  selector: 'app-select-project',
  templateUrl: './select-project.component.html',
  styleUrls: ['./select-project.component.scss']
})
export class SelectProjectComponent implements OnInit {

  @Input() projects: Project[];
  @Input() quote: Quote;
  @Input() quotes: Quote[];
  myControl = new FormControl();
  options: string[];
  projectsById = {};
  projectsByKey = {};
  filteredOptions: Observable<string[]>;

  constructor(private projectsService: ProjectsService) {}

  ngOnInit() {
    let notToPush = [];
    this.options = [];
    this.options.push("No Match");
    this.quotes.forEach(quote => {
      if(quote.project !== ""){
        notToPush.push(quote.project);
      }
    });
    this.projects.forEach(project => {
      if(this.projectsById[project.id] === undefined){
        this.projectsById[project.id] = project;
      }
      if(this.projectsByKey[project.$key] === undefined){
        this.projectsByKey[project.$key] = project.id;
      }

      if(this.quote.project !== "" && this.quote.project !== undefined){
        this.myControl.setValue(this.projectsByKey[this.quote.project]);
      }
      if(notToPush.indexOf(project.$key) === -1){
        this.options.push(project.id);
      }     
    });

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  setProject(id){
    let project = id === "No Match" ? "" :  this.projectsById[id].$key;
    return this.projectsService.updateQuoteProject(this.quote.$key, project);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}