import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatSortModule, MatToolbarModule, MatIconModule, MatDialogModule, MatButtonModule, MatAutocompleteModule } from '@angular/material';
import { MatFormFieldModule, MatPaginatorModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { QuotesComponent } from './quotes.component';
import { FileUploaderModule } from 'src/app/shared/modules/file-uploader/file-uploader.module';
import { QuotesRoutingModule } from './quotes-routing.module';
import { UserFromRefModule } from '../../shared/modules/user-from-ref/user-from-ref.module';
import { DimensionComponent } from './dimension/dimension.component';
import { SelectProjectComponent } from './select-project/select-project.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [QuotesComponent, DimensionComponent, SelectProjectComponent],
  imports: [
    CommonModule,
    QuotesRoutingModule,
    MatToolbarModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatAutocompleteModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploaderModule,
    UserFromRefModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  entryComponents: [DimensionComponent]
})
export class QuotesModule { }
