import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectScopeComponent } from './project-scope.component';

const routes: Routes = [
  {
      path: '',
      component: ProjectScopeComponent

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectScopeRoutingModule { }
