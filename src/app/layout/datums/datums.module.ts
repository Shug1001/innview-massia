import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatumsRoutingModule } from './datums-routing.module';
import { DatumsComponent } from './datums.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';

@NgModule({
  declarations: [DatumsComponent],
  imports: [
    CommonModule,
    DatumsRoutingModule,
    FormsModule,
    MatInputModule
  ]
})
export class DatumsModule { }
