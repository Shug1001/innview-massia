import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatumsComponent } from './datums.component';

describe('DatumsComponent', () => {
  let component: DatumsComponent;
  let fixture: ComponentFixture<DatumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
