import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadPanelsService } from 'src/app/shared/services/load-panels.service';

@Component({
  selector: 'app-datums',
  templateUrl: './datums.component.html',
  styleUrls: ['./datums.component.scss']
})
export class DatumsComponent implements OnInit {

  datums: any;
  loaded: boolean;
  _subscription: Subscription;

  constructor(private loadPanelsService: LoadPanelsService) { }

  ngOnInit() {
    this.loaded = false;
    this._subscription = this.loadPanelsService.getDatums().subscribe(datums => {
      this.datums = datums;
      this.loaded = true;
    });
  }

  updateOverride(datumName, datumData){
    this.loadPanelsService.updateDatum(datumName, datumData);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }


}
