import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatumsComponent } from './datums.component';

const routes: Routes = [    {
  path: '',
  component: DatumsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatumsRoutingModule { }
