import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialRequestCreateComponent } from './material-request-create.component';

xdescribe('MaterialRequestCreateComponent', () => {
  let component: MaterialRequestCreateComponent;
  let fixture: ComponentFixture<MaterialRequestCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialRequestCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialRequestCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
