import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialRequestCreateRoutingModule } from './material-request-create-routing.module';
import { MaterialRequestCreateComponent } from './material-request-create.component';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule, MatToolbarModule, MatAutocompleteModule, MatSelectModule, MatChipsModule, MatIconModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  declarations: [MaterialRequestCreateComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatCheckboxModule,
    MatSelectModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    MatAutocompleteModule,
    TextFieldModule,
    MaterialRequestCreateRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [MaterialRequestCreateComponent]
})
export class MaterialRequestCreateModule { }
