import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Project, Reason } from '../../shared/models/models';
import { ProjectsService, AuthService, AreasService } from '../../shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MatAutocompleteSelectedEvent, MAT_DATE_FORMATS, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Subscription, combineLatest, Observable } from 'rxjs';
import { ReferenceDataService } from '../../shared/services/reference-data.service';
import { startWith, map } from 'rxjs/operators';
import { MaterialRequestService } from '../../shared/services/material-request.service';
import { Router } from '@angular/router';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { SupplyChainService } from 'src/app/shared/services/supply-chain.service';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-material-request-create',
  templateUrl: './material-request-create.component.html',
  styleUrls: ['./material-request-create.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class MaterialRequestCreateComponent implements OnInit {

  twoDays: string;
  minDate = new Date();
  materialControl: FormGroup;
  projects: Project[];
  _subscription: Subscription;
  loaded: boolean;
  stockItems: Array<any>;
  materialRequests: Array<any>;
  submitted: boolean;
  amount: number;
  uom: string;
  stockItemName: string;
  stockItemKey: string;
  currentUserEmail: string;
  currentUserName: string;
  projectIds: Array<any>;
  filteredOptions: Observable<string[]>;
  filteredOptions2: Observable<string[]>;
  reasons: Reason[] = [
    {name: "Missing from Delivery", abbreviation: "MD"},
    {name: "Ancillaries Request", abbreviation: "AR"},
    {name: "Variation Order", abbreviation: "VO"},
    {name: "Site Request (Additional to budget)", abbreviation: "SR"},
  ];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredStockItems: Observable<string[]>;
  stockItemsArr = [];
  stockItemsKeyArr = [];
  projectsHolder = {};
  areasHolder = {};
  areaIds = [];
  setupCount: number;
  _subscription2: Subscription;


  @ViewChild('stockItemInput') stockItemInput: ElementRef<HTMLInputElement>;
  @ViewChild('autoStockName') matAutocomplete: MatAutocomplete;

  constructor(private areasService: AreasService, private supplyChainService: SupplyChainService, private dateUtilService: DateUtilService, private router: Router, private referenceDataService: ReferenceDataService, private fb: FormBuilder, public dialog: MatDialog, private projectsService: ProjectsService, private authService: AuthService, private materialRequestService: MaterialRequestService) { 
    this.twoDays = this.dateUtilService.addWorkingDays(this.minDate, 2);
    this.minDate = new Date(this.twoDays);
    this.loaded = false;
    this.submitted =false;
    this.setupCount = 0;
  }

  ngOnInit(){
    this._subscription = combineLatest(this.projectsService.getMaterialRequests(), this.referenceDataService.getUser(this.authService.currentUserId), this.projectsService.getProjectsList(), this.supplyChainService.getStockItems()).subscribe(([materialRequests, user, projects, stockItems]) => {
      this.projectIds = [];
      this.areaIds = [];
      this.setupCount++;
      this.materialRequests = materialRequests;
      projects.forEach((project)=>{
        this.projectIds.push(project.id);
        if(this.projectsHolder[project.id] === undefined){
          this.projectsHolder[project.id] = project;
        }
      });

      this.amount = 0;
      this.stockItems = stockItems;
      this.projects = projects;
      this.currentUserName = user.name;
      this.currentUserEmail = user.email;
      
      if(this.setupCount <= 1){
        this.setUpForm();
      } 

      this.filteredOptions = this.materialControl.get('projectId').valueChanges.pipe(
        startWith(''),
        map((value) => {
          if(value === ""){
            this.materialControl.get('projectName').setValue(null);
          }
          const filteredArray = this._filter(value);
          if(filteredArray.length === 1){
            this.projects.forEach((project)=>{
              if(project.id === filteredArray[0]){
                this.materialControl.get('projectName').setValue(project.name);
              }
            });
          }else{
            this.materialControl.get('projectName').setValue(null);
          }
          return filteredArray;
        })
      );

      this.filteredStockItems = this.materialControl.get('stockNames').valueChanges.pipe(
        startWith(null),
        map((stockItem: string | null) => {
          return stockItem ? this._filter2(stockItem) : this.stockItems.slice();
        }));


      this.materialControl.get('amount').valueChanges.pipe(
        startWith(null),
        map((amount: number | null) => {
          this.amount = amount;
        }));

      this.loaded = true;
    });
  }

  add(event: MatAutocompleteSelectedEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      this.materialControl.get('stockNames').setValue(null);
    }else{
      this.uom = event.option.value.uom;
      this.stockItemKey = event.option.value.$key;
      this.stockItemName = event.option.viewValue;
      this.materialControl.get('stockNames').setValue(event.option.viewValue);
    }
  }

  remove(fruit: string): void {
    const index = this.stockItemsArr.indexOf(fruit);

    if (index >= 0) {
      this.stockItemsArr.splice(index, 1);
      this.stockItemsKeyArr.splice(index, 1);
    }
  }

  projectSelected(evt){
    let project = this.projectsHolder[evt.option.viewValue];
    this._subscription2 = this.areasService.getAreasForProjectProcessed(project.$key).subscribe((areaSnaps) => {
      areaSnaps.forEach((area)=>{
        this.areaIds.push(area.phase+"-"+area.floor+"-"+area.type);
        if(this.areasHolder[area.phase+"-"+area.floor+"-"+area.type] === undefined){
          this.areasHolder[area.phase+"-"+area.floor+"-"+area.type] = area;
        }
      });
      this.filteredOptions2 = this.materialControl.get('areaId').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter3(value))
      );
    });
  }

  selected(): void {
    let found = 0;
    if(this.stockItemName && this.stockItemKey){
      this.stockItems.forEach(element => {
        if(element.stockName === this.stockItemName){
          found = 1;
        }
      });
      if(found && this.amount !== undefined && this.uom !== undefined){
        let newData = {};
        newData[this.stockItemKey] = this.amount + " " + this.uom;
        this.stockItemsArr.push({
          stockItem: this.stockItemName,
          amount: this.amount,
          uom: this.uom
        });
        this.stockItemsKeyArr.push(newData);
        this.uom = "";
      }
    }
    this.materialControl.get('stockNames').setValue(null);
  }

  private _filter2(value: any) {

    return this.stockItems.filter((stockItem) => {
      return stockItem.stockName.toLowerCase().indexOf(value) === 0
    });

  }

  private _filter3(value: any) {

    return this.areaIds.filter((areaId) => {
      return areaId.toLowerCase().indexOf(value) === 0
    });

  }

  setUpForm(){
    this.materialControl = this.fb.group({
      email: new FormControl({ value: this.currentUserEmail, disabled: true }, [Validators.required]),
      person: new FormControl({ value: this.currentUserName, disabled: true }, [Validators.required]),
      projectId: new FormControl(null, [Validators.required]),
      areaId: new FormControl(null, [Validators.required]),
      projectName: new FormControl({ value: null, disabled: true }, [Validators.required]),
      reason: new FormControl(null, [Validators.required]),
      materialNumber: new FormControl(this.materialRequests.length),
      stockNames: new FormControl(null),
      amount: new FormControl(null),
      dateRequired: new FormControl(null, [Validators.required]),
      materialRequired: new FormControl(null, [Validators.required]),
      mainContractor: new FormControl(null),
      siteContact: new FormControl(null),
      siteContactNo: new FormControl(null)
    });
  }

  private _filter(value: string): string[] {
    return this.projectIds.filter(option => option.indexOf(value) === 0);
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
    if(this._subscription2){
      this._subscription2.unsubscribe();
    }
  }

  back(){
    this.router.navigate(['/material-request']);
  }

  onSubmit(): void{
    if(this.materialControl.valid && this.materialControl.dirty){
      this.submitted = true;
      let updateObj = this.materialControl.getRawValue();
      let update = {}
      this.stockItemsKeyArr.forEach(element => {
        update[Object.keys(element)[0]] = element[Object.keys(element)[0]];
      });
      updateObj.areaId = this.areasHolder[updateObj.areaId].$key;
      updateObj.stockItems = update;
      updateObj.dateRequired = this.dateUtilService.toIsoDate(updateObj.dateRequired._d);
      this.materialRequestService.createMaterialRequest(updateObj)
      .then(()=>{
        this.router.navigate(['/material-request']);
      });
    }
  }
}