import { Component, OnInit, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import { Subscription } from 'rxjs';
import { SupplyChainService } from '../../../shared/services/supply-chain.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Supplier } from '../../../shared/models/models';
import { DateUtilService } from '../../../shared/services/date-util.service';
import { LoadPanelsService, } from '../../../shared/services/load-panels.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { TextDialogPopupComponent } from '../text-dialog-popup/text-dialog-popup.component';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;
  suppliers: Array<any>;
  contacts: Array<any>;
  _subscription: Subscription;
  loaded: boolean;
  text: string;
  show: boolean;
  displayedColumns: string[] = ['no', 'companyName', 'mainContactHolder', 'dtf', 'status', 'delete'];
  dataSource: MatTableDataSource<Supplier>;
  dialogRef: MatDialogRef<TextDialogPopupComponent>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, private dateUtilService: DateUtilService, private router: Router, private supplyChainService: SupplyChainService, private loadPanelsService: LoadPanelsService) { }

  ngOnInit() {
    this.whichRoute = 'suppliers';
    this.setRoute(this.whichRoute);
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });
    this._subscription = this.supplyChainService.getSuppliersWithContacts().subscribe(([suppliers, contacts]) => {
      this.contacts = contacts;
      this.suppliers = suppliers;
      let count = 1;
      this.suppliers.forEach((supplier) => {
        let mainContactObj: any;
        mainContactObj = contacts.filter(item => item.$key === supplier.mainContact)[0];
        supplier.mainContactHolder = mainContactObj.firstName + ' ' + mainContactObj.lastName;
        supplier.no = count;
        supplier.dtf = this.dateUtilService.toUserSlachDate(supplier.dateTimestamp);
        count++;
      });
      this.dataSource = new MatTableDataSource(this.suppliers);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loaded = true;
    });
  }
  
  uploadFile(data){
    this.text = "uploading data...";
    this.show = true;
    this.openDialog();
    return this.loadPanelsService.loadStockItem(data)
    .then((data)=>{
      this.text = "upload complete";
      this.show = false;
      this.dialogRef.componentInstance.data = {
        text: this.text,
        show: this.show
      };
    })
    .catch((error)=>{
      this.text = error;
    })
  }

  openDialog(): void {
    this.dialogRef = this.dialog.open(TextDialogPopupComponent, {
      width: '500px',
      data: {
        text: this.text,
        show: this.show
      }
    });
  }

  setNav() {
    this.setRoute('create-supplier');
    this.router.navigate(['supply-chain/suppliers/create-supplier']);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._subscription.unsubscribe();
  }

}