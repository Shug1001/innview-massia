import { Component, OnInit, ViewChild } from '@angular/core';
import { SupplyChainService } from '../../../../shared/services/supply-chain.service';
import { Subscription, combineLatest } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DateUtilService } from '../../../../shared/services/date-util.service';
import { MatDialog } from '@angular/material';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SupplierStock, Supplier, DisplayTile } from '../../../../shared/models/models';
import { DisplayDetailsComponent } from '../../stock/stock-details/display-details/display-details.component';

@Component({
  selector: 'app-supplier-details',
  templateUrl: './supplier-details.component.html',
  styleUrls: ['./supplier-details.component.scss']
})
export class SupplierDetailsComponent implements OnInit {

  displayGrid: DisplayTile;

  whichRoute: string;
  _subscription: Subscription;
  _routeSubscribe: Subscription;
  supplier: Supplier;
  supplierExportArr: Array<any>;
  loaded: boolean;
  supplierStock: SupplierStock[];
  displayedColumns: string[] = ['no', 'stockName', 'type', 'count', 'cost', 'unitCost', 'delTime', 'colTime', 'status', 'details'];
  dataSource: MatTableDataSource<SupplierStock>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private supplyChainService: SupplyChainService,
    private route: ActivatedRoute,
    private router: Router,
    private dateUtilService: DateUtilService,
    public dialog: MatDialog) {
      this.loaded = false;
   }

   ngOnInit() {

    this.whichRoute = 'supplier-details';
    this.setRoute(this.whichRoute);

    const id = this.route.snapshot.paramMap.get('id');

    this._subscription = combineLatest([
      this.supplyChainService.getSupplier(id),
      this.supplyChainService.getSuppliesStockItems(id)
    ]).subscribe(returnData => {
      this.supplier = returnData[0];
      this.supplierStock = returnData[1];
      this.calculateTiles(this.supplierStock);
      this.createExport();
      this.dataSource = new MatTableDataSource(this.supplierStock);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loaded = true;
    });

    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });

  }

  createExport() {

    this.supplierExportArr = [];
 
    this.supplierStock.forEach((stockDetails) => {
      const supplierExport = {
        'No.': stockDetails.no,
        'Stock Item': stockDetails.stockName,
        'Unit of Purchase': stockDetails.count,
        'Cost': '£ ' + stockDetails.cost,
        'Cost per Unit': '£ ' + stockDetails.unitCost,
        'Delivery Lead Time': stockDetails.delTime + ' days',
        'Collection Lead Time': stockDetails.colTime + ' days',
        'Status': stockDetails.status,
        'Date': this.dateUtilService.toUserSlachDate(stockDetails.dateTimestamp)
      };
      this.supplierExportArr.push(supplierExport);
    });
  }

  calculateTiles(supplierStock) {

    const supplierStockArr = [];

    this.setUpDisplayTile();

    if (supplierStock.length) {

      supplierStock.forEach(element => {
        if (supplierStockArr.indexOf(element.stockName)) {
          supplierStockArr.push(element.stockName);
        }
        this.displayGrid.unitCostTotal += element.unitCost;
        this.displayGrid.colTimeTotal += element.colTime;
        this.displayGrid.delTimeTotal += element.delTime;
        this.displayGrid.unitCostArr.push(element.unitCost);
        this.displayGrid.colTimeArr.push(element.colTime);
        this.displayGrid.delTimeArr.push(element.delTime);
      });

      this.displayGrid.unitCostArr.sort();
      this.displayGrid.unique = supplierStockArr.length;
      this.displayGrid.avgUnitCost = Math.round(this.displayGrid.unitCostTotal / this.displayGrid.unitCostArr.length * 10) / 10;
      this.displayGrid.minUnitCost = this.displayGrid.unitCostArr[0];
      this.displayGrid.maxUnitCost = this.displayGrid.unitCostArr[this.displayGrid.unitCostArr.length - 1];

      this.displayGrid.delTimeArr.sort();
      this.displayGrid.avgDelTime = Math.round(this.displayGrid.delTimeTotal / this.displayGrid.delTimeArr.length * 10) / 10;
      this.displayGrid.minDelTime = this.displayGrid.delTimeArr[0];
      this.displayGrid.maxDelTime = this.displayGrid.delTimeArr[this.displayGrid.delTimeArr.length - 1];

      this.displayGrid.colTimeArr.sort();
      this.displayGrid.avgColTime = Math.round(this.displayGrid.colTimeTotal / this.displayGrid.colTimeArr.length * 10) / 10;
      this.displayGrid.minColTime = this.displayGrid.colTimeArr[0];
      this.displayGrid.maxColTime = this.displayGrid.colTimeArr[this.displayGrid.colTimeArr.length - 1];

    }

  }

  setUpDisplayTile() {
    this.displayGrid = {
      unique: 0,
      unitCostTotal: 0,
      colTimeTotal: 0,
      delTimeTotal: 0,
      avgUnitCost: 0,
      maxUnitCost: 0,
      minUnitCost: 0,
      avgDelTime: 0,
      maxDelTime: 0,
      minDelTime: 0,
      avgColTime: 0,
      maxColTime: 0,
      minColTime: 0,
      unitCostArr: [],
      colTimeArr: [],
      delTimeArr: []
    };
  }

  displayDetails(stockDetails): void {

    const dialogRef = this.dialog.open(DisplayDetailsComponent, {
      width: '770px',
      data: {stockDetails: stockDetails}
    });

    dialogRef.componentInstance.data = {stockDetails: stockDetails};

    dialogRef.afterClosed().subscribe(result => {

    });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  setNav(path, route) {
    this.setRoute(route);
    this.router.navigate([path]);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._subscription.unsubscribe();
  }


}
