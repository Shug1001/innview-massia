import { Component, OnInit } from '@angular/core';
import { SupplyChainService } from '../../../../../shared/services/supply-chain.service';
import { Subscription } from 'rxjs';
import { Contact, Supplier } from '../../../../../shared/models/models';
import { AuthService } from '../../../../../shared/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.scss']
})
export class EditSupplierComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;
  _subscription: Subscription;
  contact: Contact;
  contactArr: Array<any>;
  existingContactArr: Array<any>;
  supplier: Supplier;
  saved: boolean;
  id: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private supplyChainService: SupplyChainService) { }

  ngOnInit() {

    this.id = this.route.snapshot.parent.paramMap.get('id');

    this.contactArr = [];
    this.existingContactArr = [];

    this.contact = {
      firstName: '',
      phone: '',
      lastName: '',
      mobileWork: '',
      otherName: '',
      mobileOther: '',
      employer: '',
      emailWork: '',
      jobTitle: '',
      emailOther: '',
      mainContact: true
    };

    this.supplier = {
      companyName: '',
      accCurrency: '',
      companyShort: '',
      vat: 0,
      payee: '',
      mobile: '',
      address1: '',
      phone: '',
      address2: '',
      fax: '',
      address3: '',
      email: '',
      address4: '',
      postcode: '',
      contacts: {},
      mainContact: '',
      country: '',
      status: 'Active',
      lastestAction: 'Edit Supplier',
      dateTimestamp: 0
    };

    this.saved = false;
    this.whichRoute = 'edit-supplier';
    this._subscription = this.supplyChainService.getSupplier(this.id).subscribe(supplier => {

      this.contactArr = supplier.contactsArr;
      this.existingContactArr = supplier.contacts;

      this.supplier.companyName = supplier.companyName;
      this.supplier.accCurrency = supplier.accCurrency;
      this.supplier.companyShort = supplier.companyShort;
      this.supplier.vat = supplier.vat;
      this.supplier.payee = supplier.payee;
      this.supplier.mobile = supplier.mobile;
      this.supplier.address1 = supplier.address1;
      this.supplier.phone = supplier.phone;
      this.supplier.address2 = supplier.address2;
      this.supplier.fax = supplier.fax;
      this.supplier.address3 = supplier.address3;
      this.supplier.email = supplier.email;
      this.supplier.address4 = supplier.address4;
      this.supplier.postcode = supplier.postcode;
      this.supplier.country = supplier.country;
      this.supplier.contacts = supplier.contacts;
      this.supplier.mainContact = supplier.mainContact;

    });
    this.setRoute(this.whichRoute);
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      this.whichRoute = route;
    });
  }

  changeMainContact(con) {
    this.contactArr.forEach((contact) => {
      if(contact !== con) {
        contact.mainContact = false;
      }
    });
  }

  add() {
    this.contactArr.forEach((contact) => {
      contact.mainContact = false;
    });
    this.contactArr.push(this.contact);
    this.contact = {
      firstName: '',
      phone: '',
      lastName: '',
      mobileWork: '',
      otherName: '',
      mobileOther: '',
      employer: '',
      emailWork: '',
      jobTitle: '',
      emailOther: '',
      mainContact: true
    };
  }

  remove(keyString) {
    const key = Number(keyString);
    this.contactArr.splice(key, 1);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  save(contacts, supplier) {
    if(contacts.length > 0) {
      supplier.dateTimestamp = this.authService.serverTime();
      return this.supplyChainService.updateSupplier(this.existingContactArr, contacts, this.id, supplier)
        .then(() => {
          this.saved = true;
        });
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  done() {
    this.setRoute('suppliers');
    this.router.navigate(['supply-chain/suppliers']);
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._subscription.unsubscribe();
  }

  reset(stepper) {
    this.contact = {
      firstName: '',
      phone: '',
      lastName: '',
      mobileWork: '',
      otherName: '',
      mobileOther: '',
      employer: '',
      emailWork: '',
      jobTitle: '',
      emailOther: '',
      mainContact: false
    };

    this.supplier = {
      companyName: '',
      accCurrency: '',
      companyShort: '',
      vat: 0,
      payee: '',
      mobile: '',
      address1: '',
      phone: '',
      address2: '',
      fax: '',
      address3: '',
      email: '',
      address4: '',
      postcode: '',
      contacts: {},
      mainContact: '',
      country: '',
      status: 'Active',
      lastestAction: 'Edit Supplier',
      dateTimestamp: 0
    };
    stepper.reset();
  }


}
