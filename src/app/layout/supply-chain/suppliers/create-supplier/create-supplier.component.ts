import { Component, OnInit } from '@angular/core';
import { SupplyChainService } from '../../../../shared/services/supply-chain.service';
import { Subscription } from 'rxjs';
import { Contact, Supplier } from '../../../../shared/models/models';
import { AuthService } from '../../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-supplier',
  templateUrl: './create-supplier.component.html',
  styleUrls: ['./create-supplier.component.scss']
})
export class CreateSupplierComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;
  contact: Contact;
  contactArr: Array<any>;
  supplier: Supplier;
  saved: boolean;

  constructor(private router: Router, private authService: AuthService, private supplyChainService: SupplyChainService) { }

  ngOnInit() {

    this.contactArr = [];

    this.contact = {
      firstName: '',
      phone: '',
      lastName: '',
      mobileWork: '',
      otherName: '',
      mobileOther: '',
      employer: '',
      emailWork: '',
      jobTitle: '',
      emailOther: '',
      mainContact: true
    };

    this.supplier = {
      companyName: '',
      accCurrency: '',
      companyShort: '',
      vat: 0,
      payee: '',
      mobile: '',
      address1: '',
      phone: '',
      address2: '',
      fax: '',
      address3: '',
      email: '',
      address4: '',
      postcode: '',
      contacts: {},
      mainContact: '',
      country: '',
      status: 'Active',
      lastestAction: 'Create New Supplier',
      dateTimestamp: 0
    };

    this.saved = false;
    this.whichRoute = 'create-supplier';
    this.setRoute(this.whichRoute);
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      this.whichRoute = route;
    });
  }

  changeMainContact(con) {
    this.contactArr.forEach((contact) => {
      if(contact !== con) {
        contact.mainContact = false;
      }
    });
  }

  add() {
    this.contactArr.push(this.contact);
    this.contact = {
      firstName: '',
      phone: '',
      lastName: '',
      mobileWork: '',
      otherName: '',
      mobileOther: '',
      employer: '',
      emailWork: '',
      jobTitle: '',
      emailOther: '',
      mainContact: false
    };
  }

  remove(keyString) {
    const key = Number(keyString);
    this.contactArr.splice(key, 1);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  save(contacts, supplier) {
    if(contacts.length > 0) {
      supplier.dateTimestamp = this.authService.serverTime();
      return this.supplyChainService.createSupplier(contacts, supplier)
        .then(() => {
          this.saved = true;
        });
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  done(){
    this.setRoute('suppliers');
    this.router.navigate(['supply-chain/suppliers']);
  }

  reset(stepper) {
    this.contact = {
      firstName: '',
      phone: '',
      lastName: '',
      mobileWork: '',
      otherName: '',
      mobileOther: '',
      employer: '',
      emailWork: '',
      jobTitle: '',
      emailOther: '',
      mainContact: false
    };

    this.supplier = {
      companyName: '',
      accCurrency: '',
      companyShort: '',
      vat: 0,
      payee: '',
      mobile: '',
      address1: '',
      phone: '',
      address2: '',
      fax: '',
      address3: '',
      email: '',
      address4: '',
      postcode: '',
      contacts: {},
      mainContact: '',
      country: '',
      status: 'Active',
      lastestAction: 'Create New Supplier',
      dateTimestamp: 0
    };
    stepper.reset();
  }


}
