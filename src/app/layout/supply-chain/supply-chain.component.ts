import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { SupplyChainService } from '../../shared/services/supply-chain.service';

@Component({
  selector: 'app-supply-chain',
  templateUrl: './supply-chain.component.html',
  styleUrls: ['./supply-chain.component.scss']
})
export class SupplyChainComponent implements OnInit {

  whichRoute: string;
  hideDash: boolean;
  _routeSubscribe: Subscription;
  _routeSubscribe2: Subscription;

  constructor(private router: Router, private supplyChainService: SupplyChainService) {}

  ngOnInit() {
    this.whichRoute = 'supply-chain';
    this.setRoute(this.whichRoute);
    this.hideDash = false;
    this._routeSubscribe2 = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });
    this._routeSubscribe = this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd && e.url === '/supply-chain') {
        this.whichRoute = 'supply-chain';
      }
    });
  }

  setRoute(route) {
    route = route.replace('/:id', '');
    this.supplyChainService.setRoute(route);
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._routeSubscribe2.unsubscribe();
  }

}
