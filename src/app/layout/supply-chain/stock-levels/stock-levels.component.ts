import { Component, OnInit, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import { Subscription } from 'rxjs';
import { SupplyChainService } from '../../../shared/services/supply-chain.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { StockItem } from '../../../shared/models/models';
import { DateUtilService } from '../../../shared/services/date-util.service';

@Component({
  selector: 'app-stock-levels',
  templateUrl: './stock-levels.component.html',
  styleUrls: ['./stock-levels.component.scss']
})
export class StockLevelsComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;

  constructor(private dateUtilService: DateUtilService, private router: Router, private supplyChainService: SupplyChainService) { }

  ngOnInit() {
    this.whichRoute = 'stock';
    this.setRoute(this.whichRoute);
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });
  }

  uploadFile(data){
    console.log(data);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }


  onDestroy() {
    this._routeSubscribe.unsubscribe();
  }
}
