import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DataExportModule } from '../../shared/modules/data-export/data-export.module';

import {
  MatCardModule,
  MatIconModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatStepperModule,
  MatSelectModule,
  MatButtonModule,
  MatSortModule,
  MatExpansionModule,
  MatSlideToggleModule,
  MatTableModule,
  MatPaginatorModule,
  MatGridListModule,
  MatDialogModule,
  MatCheckboxModule,
  MatListModule,
  MatProgressBarModule,
} from '@angular/material';

import { SupplyChainRoutingModule } from './supply-chain-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'angular-crumbs';
import { SupplyChainComponent } from './supply-chain.component';
import { StockComponent } from './stock/stock.component';
import { CreateStockComponent } from './stock/create-stock/create-stock.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { CreateSupplierComponent } from './suppliers/create-supplier/create-supplier.component';
import { StockDetailsComponent } from './stock/stock-details/stock-details.component';
import { SupplierDetailsComponent } from './suppliers/supplier-details/supplier-details.component';

import { CreateSupplierStockComponent } from './stock/stock-details/create-supplier-stock/create-supplier-stock.component';
import { EditSupplierStockComponent } from './stock/stock-details/edit-supplier-stock/edit-supplier-stock.component';

import { EditStockComponent } from './stock/stock-details/edit-stock/edit-stock.component';
import { EditSupplierComponent } from './suppliers/supplier-details/edit-supplier/edit-supplier.component';
import { SupplyChainPreviewModule } from '../../shared/modules/supply-chain-preview/supply-chain-preview.module';
import { DeleteSupplyChainModule } from '../../shared/modules/delete-supply-chain/delete-supply-chain.module';
import { DisplayDetailsComponent } from './stock/stock-details/display-details/display-details.component';
import { StockLevelsComponent } from './stock-levels/stock-levels.component';

import { FileUploaderModule } from 'src/app/shared/modules/file-uploader/file-uploader.module';

import { TextDialogPopupComponent } from './text-dialog-popup/text-dialog-popup.component';

@NgModule({
  declarations: [
    SupplyChainComponent,
    CreateStockComponent,
    StockComponent,
    SuppliersComponent,
    CreateSupplierComponent,
    StockDetailsComponent,
    SupplierDetailsComponent,
    CreateSupplierStockComponent,
    EditSupplierStockComponent,
    EditStockComponent,
    EditSupplierComponent,
    DisplayDetailsComponent,
    StockLevelsComponent,
    TextDialogPopupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    BreadcrumbModule,
    MatIconModule,
    MatInputModule,
    MatExpansionModule,
    MatStepperModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatGridListModule,
    MatProgressBarModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    DataExportModule,
    FileUploaderModule,
    SupplyChainPreviewModule,
    SupplyChainRoutingModule,
    DeleteSupplyChainModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  entryComponents: [
    CreateSupplierStockComponent,
    EditSupplierStockComponent,
    DisplayDetailsComponent,
    TextDialogPopupComponent
  ]
})
export class SupplyChainModule { }
