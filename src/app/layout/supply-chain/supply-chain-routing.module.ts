import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SupplyChainComponent } from './supply-chain.component';

import { StockComponent } from './stock/stock.component';
import { CreateStockComponent } from './stock/create-stock/create-stock.component';

import { SuppliersComponent } from './suppliers/suppliers.component';
import { CreateSupplierComponent } from './suppliers/create-supplier/create-supplier.component';

import { SupplierDetailsComponent } from './suppliers/supplier-details/supplier-details.component';
import { StockDetailsComponent } from './stock/stock-details/stock-details.component';

import { EditStockComponent } from './stock/stock-details/edit-stock/edit-stock.component';
import { EditSupplierComponent } from './suppliers/supplier-details/edit-supplier/edit-supplier.component';

import { StockLevelsComponent } from './stock-levels/stock-levels.component';

const routes: Routes = [
  {
    path: '',
    component: SupplyChainComponent,
    children: [
      {
        path: '',
        pathMatch: 'full'
      },
      {
        path: 'stock',
        data: { breadcrumb: 'Stock'},
        component: StockComponent,
        children: [
          {
            path: 'create-stock',
            data: { breadcrumb: 'Create Stock'},
            component: CreateStockComponent
          },
          {
            path: 'stock-details/:id',
            data: { breadcrumb: 'Stock Details'},
            component: StockDetailsComponent,
            children: [
              {
                path: 'edit-stock',
                data: { breadcrumb: 'Edit Stock'},
                component: EditStockComponent
              }
            ]
          }
        ]
      },
      {
        path: 'suppliers',
        data: { breadcrumb: 'Suppliers'},
        component: SuppliersComponent,
        children: [
          {
            path: 'create-supplier',
            data: { breadcrumb: 'Create Supplier'},
            component: CreateSupplierComponent
          },
          {
            path: 'supplier-details/:id',
            data: { breadcrumb: 'Supplier-details'},
            component: SupplierDetailsComponent,
            children: [
              {
                path: 'edit-supplier',
                data: { breadcrumb: 'Edit Supplier'},
                component: EditSupplierComponent
              }
            ]
          }
        ]
      },
      {
        path: 'stockLevels',
        data: { breadcrumb: 'StockLevels'},
        component: StockLevelsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplyChainRoutingModule { }