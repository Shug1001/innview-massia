import { Component, OnInit, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import { Subscription } from 'rxjs';
import { SupplyChainService, } from '../../../shared/services/supply-chain.service';
import { LoadPanelsService, } from '../../../shared/services/load-panels.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { StockItem } from '../../../shared/models/models';
import { DateUtilService } from '../../../shared/services/date-util.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { TextDialogPopupComponent } from '../text-dialog-popup/text-dialog-popup.component';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;
  stockItems: Array<any>;
  _subscription: Subscription;
  loaded: boolean;
  text: string;
  show: boolean;
  displayedColumns: string[] = ['no', 'stockName', 'groupName', 'subCatName', 'dtf', 'status', 'delete'];
  dataSource: MatTableDataSource<StockItem>;
  dialogRef: MatDialogRef<TextDialogPopupComponent>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, private dateUtilService: DateUtilService, private router: Router, private supplyChainService: SupplyChainService, private loadPanelsService: LoadPanelsService) { }

  ngOnInit() {

    this.whichRoute = 'stock';
    this.setRoute(this.whichRoute);
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });
    this._subscription = this.supplyChainService.getItemsWithType().subscribe(([stockItems, groupArr, subCatArr]) => {
        this.stockItems = stockItems;
        let count = 1;
        this.stockItems.forEach((stockItem) => {
          let group: any;
          let subCat: any;
          group = groupArr.filter(item => item.$key === stockItem.stockGroup)[0];
          subCat = subCatArr.filter(item => item.$key === stockItem.stockSubCat)[0];
          stockItem.no = count;
          stockItem.groupName = group.code + ' - ' + group.name;
          stockItem.subCatName = subCat.name;
          stockItem.dtf = this.dateUtilService.toUserSlachDate(stockItem.dateTimestamp);
          count++;
        });
        this.dataSource = new MatTableDataSource(this.stockItems);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loaded = true;
      });
  }

  uploadFile(data){
    this.text = "uploading data...";
    this.show = true;
    this.openDialog();
    return this.loadPanelsService.loadStockItem(data)
    .then((data)=>{
      this.text = "upload complete";
      this.show = false;
      this.dialogRef.componentInstance.data = {
        text: this.text,
        show: this.show
      };
    })
    .catch((error)=>{
      this.text = error;
    })
  }

  openDialog(): void {
    this.dialogRef = this.dialog.open(TextDialogPopupComponent, {
      width: '500px',
      data: {
        text: this.text,
        show: this.show
      }
    });
  }

  setNav() {
    this.setRoute('create-stock');
    this.router.navigate(['supply-chain/stock/create-stock']);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._subscription.unsubscribe();
  }

}
