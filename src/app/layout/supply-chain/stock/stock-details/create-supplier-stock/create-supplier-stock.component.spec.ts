import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSupplierStockComponent } from './create-supplier-stock.component';

describe('CreateSupplierStockComponent', () => {
  let component: CreateSupplierStockComponent;
  let fixture: ComponentFixture<CreateSupplierStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSupplierStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSupplierStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
