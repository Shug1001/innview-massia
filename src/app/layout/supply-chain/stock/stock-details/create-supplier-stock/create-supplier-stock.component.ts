import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SupplyChainService } from '../../../../../shared/services/supply-chain.service';
import { AuthService } from '../../../../../shared/services/auth.service';
import { Subscription, Observable } from 'rxjs';
import { SupplierStock } from '../../../../../shared/models/models';

@Component({
  selector: 'app-create-supplier-stock',
  templateUrl: './create-supplier-stock.component.html',
  styleUrls: ['./create-supplier-stock.component.scss']
})
export class CreateSupplierStockComponent implements OnInit {

  supplerStock: SupplierStock;
  suppliersArr: Observable<{ $key: string; }[]>;
  supplierKey: string;
  _suubscription: Subscription;
  statusOptions: Array<any>;
  unitOfPurchaseOptions: Array<any>;
  countDisable: boolean;
  unitOfPurchaseSelected: boolean;

  constructor(
    public dialogRef: MatDialogRef<CreateSupplierStockComponent>,
    public supplyChainService: SupplyChainService,
    public authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {

    this.supplierKey = '';
    this.statusOptions = ['Active', 'Inactive'];
    this.unitOfPurchaseOptions = ['BX', 'EA', 'LT', 'KG', 'RL', 'SH'];
    this.countDisable = false;
    this.unitOfPurchaseSelected = false;

    this.supplerStock = {
        no: 0,
        stockName: '',
        companyName: '',
        type: '',
        count: 0,
        cost: 0,
        unitCost: 0,
        delTime: 0,
        colTime: 0,
        storageWidth: 0,
        storageDepth: 0,
        storageLength: 0,
        status: 'Active',
        dateTimestamp: 0
      };

      this.suppliersArr = this.supplyChainService.getSuppliersList();
  }

  onSupplierStockOptionsSelected(value) {
    if (value === 'EA') {
      this.supplerStock.storageWidth = this.data.stockItem.storageWidth;
      this.supplerStock.storageDepth = this.data.stockItem.storageDepth;
      this.supplerStock.storageLength = this.data.stockItem.storageLength;
      this.supplerStock.count = 1;
      this.countDisable = true;
    } else {
      this.countDisable = false;
    }
    this.unitOfPurchaseSelected = true;
  }

  onSuppliersOptionsSelected(value: any, typeArr: Array<any>) {
    let selectedItem: any;
    selectedItem = typeArr.filter(item => item.companyName === value)[0];
    this.supplierKey = selectedItem.$key;
  }

  onDestroy(){
    if  (this._suubscription) {
      this._suubscription.unsubscribe();
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  save(supplerStock) {
    supplerStock.unitCost = Math.round(supplerStock.cost / supplerStock.count * 10) / 10;
    delete supplerStock.no;
    delete supplerStock.stockName;
    delete supplerStock.companyName;
    supplerStock.dateTimestamp = this.authService.serverTime();
    return this.supplyChainService.createSupplierStock(this.data.stockItem.$key, this.supplierKey, supplerStock)
      .then((no) => {
        this.dialogRef.close();
      });
  }

}
