import { Component, OnInit } from '@angular/core';
import { SupplyChainService } from '../../../../../shared/services/supply-chain.service';
import { Subscription, Observable } from 'rxjs';
import { StockItem } from '../../../../../shared/models/models';
import { AuthService } from '../../../../../shared/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-stock',
  templateUrl: './edit-stock.component.html',
  styleUrls: ['./edit-stock.component.scss']
})
export class EditStockComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;
  _subscription: Subscription;
  stockItem: StockItem;

  stockGroup: object;
  stockSubCat: object;
  stockType: object;

  id: string;

  saved: boolean;

  stockGroupArr: Observable<{ $key: string; }[]>;
  stockSubCatArr: Observable<{ $key: string; }[]>;
  stockTypeArr: Observable<{ $key: string; }[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private supplyChainService: SupplyChainService) { }

  ngOnInit() {

    this.id = this.route.snapshot.parent.paramMap.get('id');

    this.stockItem = {
      stockGroup: '',
      stockSubCat: '',
      stockType: '',
      stockCode: '',
      stockName: '',
      width: 0,
      coinsName: '',
      coinsCode: '',
      depth: 0,
      grade: '',
      length: 0,
      treated: '',
      storageWidth: 0,
      storageDepth: 0,
      storageLength: 0,
      unitOfCount: 0,
      unitOfApplication: 0,
      criticalStockLevel: 0,
      status: 'Active',
      lastestAction: 'Edit Stock Item',
      dateTimestamp: 0
    };

    this.stockGroup = {};
    this.stockSubCat = {};
    this.stockType = {};

    this.saved = false;
    this.whichRoute = 'edit-stock';
    this.setRoute(this.whichRoute);
    this.stockGroupArr = this.supplyChainService.getStockTypes('stockGroup');
    this.stockSubCatArr = this.supplyChainService.getStockTypes('stockSubCat');
    this.stockTypeArr = this.supplyChainService.getStockTypes('stockType');
    this._subscription = this.supplyChainService.getStockItem(this.id).subscribe(stockItem => {

      this.stockGroup = stockItem.stockGroupObj;
      this.stockSubCat = stockItem.stockSubCatObj;
      this.stockType = stockItem.stockTypeObj;

      this.stockItem.stockGroup = stockItem.stockGroupObj.$key;
      this.stockItem.stockSubCat = stockItem.stockSubCatObj.$key;
      this.stockItem.stockType = stockItem.stockTypeObj.$key;
      this.stockItem.stockCode = stockItem.stockCode;
      this.stockItem.stockName = stockItem.stockName;
      this.stockItem.width = stockItem.width;
      this.stockItem.coinsName = stockItem.coinsName;
      this.stockItem.coinsCode = stockItem.coinsCode;
      this.stockItem.depth = stockItem.depth;
      this.stockItem.grade = stockItem.grade;
      this.stockItem.length = stockItem.length;
      this.stockItem.treated = stockItem.treated;
      this.stockItem.storageWidth = stockItem.storageWidth;
      this.stockItem.storageDepth = stockItem.storageDepth;
      this.stockItem.storageLength = stockItem.storageLength;
      this.stockItem.unitOfCount = stockItem.unitOfCount;
      this.stockItem.unitOfApplication = stockItem.unitOfApplication;
      this.stockItem.criticalStockLevel = stockItem.criticalStockLevel;

    });
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });
  }

  onGroupOptionsSelected(value: any, groupArr: Array<any>) {
    let selectedItem: any;
    selectedItem = groupArr.filter(item => item.name === value)[0];
    this.stockGroup = selectedItem;
    this.stockItem.stockGroup = selectedItem.$key;
  }

  onSubCatOptionsSelected(value: any, subCatArr: Array<any>) {
    let selectedItem: any;
    selectedItem = subCatArr.filter(item => item.name === value)[0];
    this.stockSubCat = selectedItem;
    this.stockItem.stockSubCat = selectedItem.$key;
  }

  onTypeOptionsSelected(value: any, typeArr: Array<any>) {
    let selectedItem: any;
    selectedItem = typeArr.filter(item => item.name === value)[0];
    this.stockType = selectedItem;
    this.stockItem.stockType = selectedItem.$key;
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  isFull(item) {
    let save = false;
    if (item.stockType !== '' && item.stockGroup !== '' && item.stockSubCat !== '') {
      save = true;
    }
    return save;
  }

  save(item) {
    const all = this.isFull(item);
    if (all) {
      delete item.new;
      item.dateTimestamp = this.authService.serverTime();
      console.log(JSON.stringify(item));
      return this.supplyChainService.updateStockTypes('stockItem', this.id, item)
        .then(() => {
          this.saved = true;
        });
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  done(){
    this.setRoute('stock');
    this.router.navigate(['supply-chain/stock']);
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._subscription.unsubscribe();
  }

  reset(stepper) {
    this.stockItem = {
      stockGroup: '',
      stockSubCat: '',
      stockType: '',
      stockCode: '',
      stockName: '',
      width: 0,
      coinsName: '',
      coinsCode: '',
      depth: 0,
      grade: '',
      length: 0,
      treated: '',
      storageWidth: 0,
      storageDepth: 0,
      storageLength: 0,
      unitOfCount: 0,
      unitOfApplication: 0,
      criticalStockLevel: 0,
      status: 'Active',
      lastestAction: 'Edit Stock Item',
      dateTimestamp: 0
    };
    this.stockGroup = {};
    this.stockSubCat = {};
    this.stockType = {};
    stepper.reset();
  }


}
