import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SupplyChainService } from '../../../../../shared/services/supply-chain.service';
import { AuthService } from '../../../../../shared/services/auth.service';
import { Subscription, Observable } from 'rxjs';
import { SupplierStock } from '../../../../../shared/models/models';

@Component({
  selector: 'app-edit-supplier-stock',
  templateUrl: './edit-supplier-stock.component.html',
  styleUrls: ['./edit-supplier-stock.component.scss']
})
export class EditSupplierStockComponent implements OnInit {

  supplerStock: SupplierStock;
  suppliersArr: Observable<{ $key: string; }[]>;
  stockDetailsKey: string;
  stockItemKey: string;
  supplierKey: string;
  _suubscription: Subscription;
  statusOptions: Array<any>;
  unitOfPurchaseOptions: Array<any>;
  countDisable: boolean;

  constructor(
    public dialogRef: MatDialogRef<EditSupplierStockComponent>,
    public supplyChainService: SupplyChainService,
    public authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {

    this.stockDetailsKey = this.data.stockDetails.$key;
    this.stockItemKey = this.data.stockItem.$key;
    this.supplierKey = this.data.stockDetails.supplierKey;

    this.statusOptions = ['Active', 'Inactive'];
    this.unitOfPurchaseOptions = ['Pack/Box', 'Each', 'Litres', 'kg'];
    this.countDisable = false;

    this.supplerStock = {
      no: this.data.stockDetails.no,
      stockName: this.data.stockDetails.stockName,
      companyName: this.data.stockDetails.companyName,
      type: this.data.stockDetails.type,
      count: this.data.stockDetails.count,
      cost: this.data.stockDetails.cost,
      unitCost: this.data.stockDetails.unitCost,
      delTime: this.data.stockDetails.delTime,
      colTime: this.data.stockDetails.colTime,
      storageWidth: this.data.stockDetails.storageWidth || 0,
      storageDepth: this.data.stockDetails.storageDepth || 0,
      storageLength: this.data.stockDetails.storageLength || 0,
      status: 'Active',
      dateTimestamp: 0
    };

    if (this.supplerStock.count === 1) {
      this.countDisable = true;
    }

    this.suppliersArr = this.supplyChainService.getSuppliersList();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  delete(supplerStock) {
    return this.supplyChainService.deleteSupplierStock(this.stockItemKey, this.supplierKey, this.stockDetailsKey)
      .then((no) => {
        this.dialogRef.close();
      });
  }

  onSupplierStockOptionsSelected(value) {
    if (value === 'Each') {
      this.supplerStock.count = 1;
      this.countDisable = true;
      this.supplerStock.storageWidth = this.data.stockItem.storageWidth;
      this.supplerStock.storageDepth = this.data.stockItem.storageDepth;
      this.supplerStock.storageLength = this.data.stockItem.storageLength;
    } else {
      this.countDisable = false;
    }
  }

  save(supplerStock) {
    supplerStock.unitCost = Math.round(supplerStock.cost / supplerStock.count * 10) / 10;
    delete supplerStock.no;
    delete supplerStock.stockName;
    delete supplerStock.companyName;
    supplerStock.dateTimestamp = this.authService.serverTime();
    return this.supplyChainService.updateSupplierStock(this.stockDetailsKey, supplerStock)
      .then((no) => {
        this.dialogRef.close();
      });
  }

}

