import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSupplierStockComponent } from './edit-supplier-stock.component';

describe('EditSupplierStockComponent', () => {
  let component: EditSupplierStockComponent;
  let fixture: ComponentFixture<EditSupplierStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSupplierStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSupplierStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
