import { Component, OnInit, ViewChild } from '@angular/core';
import { SupplyChainService } from '../../../../shared/services/supply-chain.service';
import { Subscription, combineLatest } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DateUtilService } from '../../../../shared/services/date-util.service';
import { CreateSupplierStockComponent } from './create-supplier-stock/create-supplier-stock.component';
import { EditSupplierStockComponent } from './edit-supplier-stock/edit-supplier-stock.component';
import { DisplayDetailsComponent } from './display-details/display-details.component';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { StockItem, SupplierStock, DisplayTile } from '../../../../shared/models/models';
import { LoadPanelsService, } from '../../../../shared/services/load-panels.service';
import { TextDialogPopupComponent } from '../../text-dialog-popup/text-dialog-popup.component';

@Component({
  selector: 'app-stock-details',
  templateUrl: './stock-details.component.html',
  styleUrls: ['./stock-details.component.scss']
})
export class StockDetailsComponent implements OnInit {

  displayGrid: DisplayTile;

  whichRoute: string;
  _subscription: Subscription;
  _routeSubscribe: Subscription;
  stockItem: StockItem;
  loaded: boolean;
  text: string;
  show: boolean;
  stockItemExportArr: Array<any>;
  supplierStock: SupplierStock[];
  displayedColumns: string[] = [
    'no',
    'companyName',
    'type',
    'count',
    'cost',
    'unitCost',
    'delTime',
    'colTime',
    'status',
    'edit',
    'details'
  ];
  dataSource: MatTableDataSource<SupplierStock>;
  dialogRef: MatDialogRef<TextDialogPopupComponent>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private supplyChainService: SupplyChainService,
    private route: ActivatedRoute,
    private router: Router,
    private loadPanelsService: LoadPanelsService,
    private dateUtilService: DateUtilService,
    public dialog: MatDialog) {
      this.loaded = false;
   }

   ngOnInit() {

    this.whichRoute = 'stock-details';
    this.setRoute(this.whichRoute);

    const id = this.route.snapshot.paramMap.get('id');

    this._subscription = combineLatest([
      this.supplyChainService.getStockItem(id),
      this.supplyChainService.getSuppliesSuppliers(id)
    ]).subscribe(returnData => {
      this.stockItem = returnData[0];
      this.supplierStock = returnData[1];
      this.calculateTiles(this.supplierStock);
      this.createExport();
      this.dataSource = new MatTableDataSource(this.supplierStock);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loaded = true;
    });

    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });

  }

  uploadFile(data){
    this.text = "uploading data...";
    this.show = true;
    this.openDialog();
    return this.loadPanelsService.loadStockItem(data)
    .then((data)=>{
      this.text = "upload complete";
      this.show = false;
      this.dialogRef.componentInstance.data = {
        text: this.text,
        show: this.show
      };
    })
    .catch((error)=>{
      this.text = error;
    })
  }

  openDialog(): void {
    this.dialogRef = this.dialog.open(TextDialogPopupComponent, {
      width: '500px',
      data: {
        text: this.text,
        show: this.show
      }
    });
  }

  createSupplierStock(): void {

    const dialogRef = this.dialog.open(CreateSupplierStockComponent, {
      width: '770px',
      data: {stockItem: this.stockItem}
    });

    dialogRef.componentInstance.data = {stockItem: this.stockItem};

    dialogRef.afterClosed().subscribe(result => {

    });

  }

  displayDetails(stockDetails): void {

    const dialogRef = this.dialog.open(DisplayDetailsComponent, {
      width: '770px',
      data: {stockDetails: stockDetails}
    });

    dialogRef.componentInstance.data = {stockDetails: stockDetails};

    dialogRef.afterClosed().subscribe(result => {

    });

  }

  calculateTiles(supplierStock) {

    const supplierStockArr = [];

    this.setUpDisplayTile();

    if (supplierStock.length) {

      supplierStock.forEach(element => {
        if (supplierStockArr.indexOf(element.companyName)) {
          supplierStockArr.push(element.companyName);
        }
        this.displayGrid.unitCostTotal += element.unitCost;
        this.displayGrid.colTimeTotal += element.colTime;
        this.displayGrid.delTimeTotal += element.delTime;
        this.displayGrid.unitCostArr.push(element.unitCost);
        this.displayGrid.colTimeArr.push(element.colTime);
        this.displayGrid.delTimeArr.push(element.delTime);
      });

      this.displayGrid.unitCostArr.sort();
      this.displayGrid.unique = supplierStockArr.length;
      this.displayGrid.avgUnitCost = Math.round(this.displayGrid.unitCostTotal / this.displayGrid.unitCostArr.length * 10) / 10;
      this.displayGrid.minUnitCost = this.displayGrid.unitCostArr[0];
      this.displayGrid.maxUnitCost = this.displayGrid.unitCostArr[this.displayGrid.unitCostArr.length - 1];

      this.displayGrid.delTimeArr.sort();
      this.displayGrid.avgDelTime = Math.round(this.displayGrid.delTimeTotal / this.displayGrid.delTimeArr.length * 10) / 10;
      this.displayGrid.minDelTime = this.displayGrid.delTimeArr[0];
      this.displayGrid.maxDelTime = this.displayGrid.delTimeArr[this.displayGrid.delTimeArr.length - 1];

      this.displayGrid.colTimeArr.sort();
      this.displayGrid.avgColTime = Math.round(this.displayGrid.colTimeTotal / this.displayGrid.colTimeArr.length * 10) / 10;
      this.displayGrid.minColTime = this.displayGrid.colTimeArr[0];
      this.displayGrid.maxColTime = this.displayGrid.colTimeArr[this.displayGrid.colTimeArr.length - 1];

    }

  }

  setUpDisplayTile() {
    this.displayGrid = {
      unique: 0,
      unitCostTotal: 0,
      colTimeTotal: 0,
      delTimeTotal: 0,
      avgUnitCost: 0,
      maxUnitCost: 0,
      minUnitCost: 0,
      avgDelTime: 0,
      maxDelTime: 0,
      minDelTime: 0,
      avgColTime: 0,
      maxColTime: 0,
      minColTime: 0,
      unitCostArr: [],
      colTimeArr: [],
      delTimeArr: []
    };
  }

  editSupplierStock(stockDetails): void {

    const dialogRef = this.dialog.open(EditSupplierStockComponent, {
      width: '770px',
      data: {
        stockItem: this.stockItem,
        stockDetails: stockDetails
      }
    });

    dialogRef.componentInstance.data = {
      stockItem: this.stockItem,
      stockDetails: stockDetails
    };

    dialogRef.afterClosed().subscribe(result => {

    });

  }


  createExport() {

    this.stockItemExportArr = [];
 
    this.supplierStock.forEach((stockDetails) => {
      const supplierExport = {
        'No.': stockDetails.no,
        'Supplier': stockDetails.companyName,
        'Unit of Purchase': stockDetails.count,
        'Cost': '£ ' + stockDetails.cost,
        'Cost per Unit': '£ ' + stockDetails.unitCost,
        'Delivery Lead Time': stockDetails.delTime + ' days',
        'Collection Lead Time': stockDetails.colTime + ' days',
        'Status': stockDetails.status,
        'Date': this.dateUtilService.toUserSlachDate(stockDetails.dateTimestamp)
      };
      this.stockItemExportArr.push(supplierExport);
    });
  }

  editStockItem(){

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  setNav(path, route) {
    this.setRoute(route);
    this.router.navigate([path]);
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  onDestroy() {
    this._routeSubscribe.unsubscribe();
    this._subscription.unsubscribe();
  }


}
