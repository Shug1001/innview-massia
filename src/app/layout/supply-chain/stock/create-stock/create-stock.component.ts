import { Component, OnInit } from '@angular/core';
import { SupplyChainService } from '../../../../shared/services/supply-chain.service';
import { Subscription, Observable } from 'rxjs';
import { StockItem } from '../../../../shared/models/models';
import { AuthService } from '../../../../shared/services/auth.service';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-stock',
  templateUrl: './create-stock.component.html',
  styleUrls: ['./create-stock.component.scss']
})
export class CreateStockComponent implements OnInit {

  whichRoute: string;
  _routeSubscribe: Subscription;
  stockItem: StockItem;

  stockGroup: object;
  stockSubCat: object;
  stockType: object;

  formArray: Array<any>;

  saved: boolean;

  stockGroupArr: Observable<{ $key: string; }[]>;
  stockSubCatArr: Observable<{ $key: string; }[]>;
  stockTypeArr: Observable<{ $key: string; }[]>;

  stockItemFormGroup1: FormGroup;
  stockItemFormGroup2: FormGroup;
  stockItemFormGroup3: FormGroup;
  stockItemFormGroup4: FormGroup;

  isEditable: boolean;

  constructor(private router: Router, private authService: AuthService, private supplyChainService: SupplyChainService) { }

  ngOnInit() {
    this.stockItem = {
      stockGroup: '',
      stockSubCat: '',
      stockType: '',
      stockName: '',
      stockCode: '',
      width: 0,
      coinsName: '',
      coinsCode: '',
      depth: 0,
      grade: '',
      length: 0,
      treated: '',
      storageWidth: 0,
      storageDepth: 0,
      storageLength: 0,
      unitOfCount: 0,
      unitOfApplication: 0,
      criticalStockLevel: 0,
      status: 'Active',
      lastestAction: 'Create New Stock Item',
      dateTimestamp: 0
    };

    this.stockGroup = {};
    this.stockSubCat = {};
    this.stockType = {};

    this.saved = false;
    this.whichRoute = 'create-stock';
    this.setRoute(this.whichRoute);
    this.stockGroupArr = this.supplyChainService.getStockTypes('stockGroup');
    this.stockSubCatArr = this.supplyChainService.getStockTypes('stockSubCat');
    this.stockTypeArr = this.supplyChainService.getStockTypes('stockType');
    this._routeSubscribe = this.supplyChainService.whichRouteSource$.subscribe(route => {
      console.log(route);
      this.whichRoute = route;
    });
  }

  onGroupOptionsSelected(value: any, groupArr: Array<any>) {
    let selectedItem: any;
    selectedItem = groupArr.filter(item => item.name === value)[0];
    this.stockGroup = selectedItem;
    this.stockItem.stockGroup = selectedItem.$key;
  }

  onSubCatOptionsSelected(value: any, subCatArr: Array<any>) {
    let selectedItem: any;
    selectedItem = subCatArr.filter(item => item.name === value)[0];
    this.stockSubCat = selectedItem;
    this.stockItem.stockSubCat = selectedItem.$key;
  }

  onTypeOptionsSelected(value: any, typeArr: Array<any>) {
    let selectedItem: any;
    selectedItem = typeArr.filter(item => item.name === value)[0];
    this.stockType = selectedItem;
    this.stockItem.stockType = selectedItem.$key;
  }

  setRoute(route) {
    this.supplyChainService.setRoute(route);
  }

  isFull(item) {
    let save = false;
    if (item.stockType !== '' && item.stockGroup !== '' && item.stockSubCat !== '') {
      save = true;
    }
    return save;
  }

  save(item) {
    const all = this.isFull(item);
    if (all) {
      delete item.new;
      item.dateTimestamp = this.authService.serverTime();
      console.log(JSON.stringify(item));
      return this.supplyChainService.createStockTypes('stockItem', item)
        .then(() => {
          this.saved = true;
        });
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  done(){
    this.setRoute('stock');
    this.router.navigate(['supply-chain/stock']);
  }

  reset(stepper) {
    this.stockItemFormGroup1.reset();
    this.stockItemFormGroup2.reset();
    this.stockItemFormGroup3.reset();
    this.stockItemFormGroup4.reset();
    this.stockItem = {
      stockGroup: '',
      stockSubCat: '',
      stockType: '',
      stockName: '',
      stockCode: '',
      width: 0,
      coinsName: '',
      coinsCode: '',
      depth: 0,
      grade: '',
      length: 0,
      treated: '',
      storageWidth: 0,
      storageDepth: 0,
      storageLength: 0,
      unitOfCount: 0,
      unitOfApplication: 0,
      criticalStockLevel: 0,
      status: 'Active',
      lastestAction: 'Create New Stock Item',
      dateTimestamp: 0
    };
    this.stockGroup = {};
    this.stockSubCat = {};
    this.stockType = {};
    stepper.reset();
  }


}
