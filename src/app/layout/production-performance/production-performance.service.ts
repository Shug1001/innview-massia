import { Injectable } from '@angular/core';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { AreasService, ProjectsService, ForwardPlannerService } from 'src/app/shared/services';
import { PanelsService } from 'src/app/shared/services/panels.service';
import { Subscription, combineLatest } from 'rxjs';
import Srd  from '../../../assets/srd/srd.json';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ProductionPerformanceService {



  totalM2:number;
  plan: object;

  productionLines: object;
  defaultCapacities: object;
  today: string;

  constructor(private db: AngularFireDatabase,private forwardPlannerService: ForwardPlannerService,private panelsService: PanelsService,private projectsService: ProjectsService,private areasService:AreasService, private dateUtilsService: DateUtilService) { 
    this.productionLines = Srd.productionLines;
    this.today = this.dateUtilsService.todayAsIso();
    this.defaultCapacities = this.dateUtilsService.buildMap(this.productionLines, "defaultCapacity");
  }

//Tested
  getProductionPerformanceData(){
    return combineLatest([this.forwardPlannerService.getCapacities(), this.forwardPlannerService.getStats()])
  }
  currentDateChange(date: string){
    this.today = date;
  }
  dayTargetColorIndicator(date,value, entry1, entry2, entry3){
      var colours = [];
      colours.push({"red-indicator": true},{"amber-indicator": true},{"green-indicator": true},{"white-indicator": true});
      
      var now = new Date(date);
      var endTime = new Date();
      endTime.setHours(15,0,0,0);
      var three = new Date(endTime);
      
      if(now > three && value != undefined){
        if (value >= entry3) {
          return "green-indicator";   
        } else if (value >= entry2) {
          return "amber-indicator";
        } else if (value <= entry1) {
          return "red-indicator";
        }
      }else{
        return "white-indicator";
      }   
  }
  colorIndicator(value,byNow){
      var colours = [];
      colours.push({"red-indicator": true},{"amber-indicator": true},{"green-indicator": true},{"purple-indicator": true},{"white-indicator": true});
      var now = new Date();
      var endTime = new Date();
      endTime.setHours(15,0,0,0);
      var three = new Date(three);       
    
      if (value > 0 && byNow != undefined) {
        var percent = (value * 100) / parseInt(byNow);
        if (percent > 1 && percent < 90) {
          return "red-indicator";
        } else if (percent >= 90 && percent < 100) {
          return "amber-indicator";
        } else if (percent >= 100 && percent < 105) {
          return "green-indicator";
        } else if (percent >= 105) {
          return "purple-indicator";
        } 
      }
  }
  //Tested
  target(capacityMapped,date,productionLine){
    if(capacityMapped[productionLine] !== undefined && capacityMapped[productionLine][date] !== undefined){
      var data = capacityMapped[productionLine][date]
      return data;      
    }else{
        return this.defaultCapacities[productionLine];
    }
  }
  //Tested
  getPanelQty(data,date,productionLine,type){
    if(data[productionLine] && data[productionLine][date] && data[productionLine][date][type]){
      var data = data[productionLine][date][type]
      if(data !== undefined){
          return parseInt(data);
      }else{
          return 0;
      }
    }else{
      return 0;
    }
  }
  //Tested
  dayTarget(stats,capacities,date,productionLine){
    var target = this.target(capacities,date,productionLine);
    var completedCount = this.getPanelQty(stats,date,productionLine,"completedCount");
    return completedCount - target;
  }
  //Tested
  getInProgressCount(stats,productionLine){
      var stats = stats[productionLine];
      var prodLineCompletedCount = 0;
      var prodLineStartedCount = 0;
      Object.keys(stats).forEach(value => {
        if(stats[value].completedCount !== undefined && stats[value].startedCount !== undefined){
          prodLineCompletedCount += stats[value].completedCount;
          prodLineStartedCount += stats[value].startedCount;
        }
      })
      return prodLineStartedCount - prodLineCompletedCount;
  }
  //Tested
  getGreatestRisks(plansMapped,areasMapped){ 
      var allRiskData = {};
      Object.keys(plansMapped).forEach(plan =>{
        if(areasMapped[plan] != undefined){
          var area = areasMapped[plan];
          if(area._productionLine !== null){
            const productionLineFiltered = area._productionLine.replace(/[0-9]/g, '');
            var areaPlan = plansMapped[plan];
            if(allRiskData[productionLineFiltered] === undefined){
                allRiskData[productionLineFiltered] = [];
            }
            if (areaPlan != undefined && areaPlan.plannedFinish != undefined && areaPlan.riskDate != undefined) {
              var days = this.dateUtilsService.daysBetweenWeekDaysOnly(areaPlan.plannedFinish, areaPlan.riskDate)
              allRiskData[productionLineFiltered].push(days);  
            }
          }
        }
      })
      return allRiskData;
  }
  //Tested
  getLowestRisk(riskData){
      var currentLowest = 0;
      riskData.forEach(risk =>{
          if(risk <= currentLowest){
              currentLowest = risk;
          }
      })
      return currentLowest;
  }
  //Tested
  getAreasForProductionToday(areas,plans,today){
    var totalM2 = 0;
    var plannedAreas = [];
    Object.keys(plans).forEach(planKey =>{
      var plannedDate = plans[planKey].plannedStart;
      if(plannedDate != undefined && plannedDate === today){
          plannedAreas.push(planKey)
      }
    })
    plannedAreas.forEach(planAreaKey =>{
      var area = areas[planAreaKey];
      if(area != undefined && area.actualArea != undefined){
        totalM2 += area.actualArea 
      }
      else if(area != undefined && area.estimatedArea != undefined){
          totalM2 += area.estimatedArea ;
      }
    })
    return Math.round(totalM2);
  }
  //Tested
  getPlannedProductionTotal(areas,plans, additionalData, buildDate){
    let today = new Date();
    let nextDay = this.dateUtilsService.plusDays(today, 1);
    let totalM2 = {};
    Object.keys(areas).forEach(areaKey =>{
      const area = areas[areaKey];
      if(area._productionLine !== null){
        const productionLineFiltered = area._productionLine.replace(/[0-9]/g, '');
        if(totalM2[productionLineFiltered] === undefined){
          totalM2[productionLineFiltered] = 0;
        }
        if (buildDate < nextDay){
          if (additionalData[area.$key] !== undefined && additionalData[area.$key].buildPlannedDays2 !== undefined && additionalData[area.$key].buildPlannedDays2[buildDate] !== undefined) {
            let buildOnDay = additionalData[area.$key].buildPlannedDays2[buildDate];
            totalM2[productionLineFiltered] += this.forwardPlannerService.avgM2(area, buildOnDay);;     
          }
        }else{
          if(plans[area.$key] !== undefined && plans[area.$key].buildDays !== undefined && plans[area.$key].buildDays[buildDate] !== undefined) {
            let buildOnDay = plans[area.$key].buildDays[buildDate];
            totalM2[productionLineFiltered] += this.forwardPlannerService.avgM2(area, buildOnDay);;   
          }
        }
      }
    });
    Object.keys(totalM2).forEach((productionLine)=>{
      totalM2[productionLine] = Math.round(totalM2[productionLine]);
    });
    return totalM2;        
  }

  getCapacityTotal(areas,plans){
      return this.getAreasForProductionToday(areas,plans,this.today);

  }

}


