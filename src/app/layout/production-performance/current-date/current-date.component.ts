import { EventEmitter, Component, Output, Input } from '@angular/core';
import { DateUtilService } from '../../../shared/services/date-util.service';
import { MatDatepickerInputEvent,MatDatepickerModule, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MMM',
  },
  display: {
    dateInput: 'DD-MMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-current-date',
  templateUrl: './current-date.component.html',
  styleUrls: ['./current-date.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})

export class CurrentDateComponent {

  minDate = new Date();
  @Input() todayDate: string;
  @Output() currentDateChange = new EventEmitter<string>();
  
  constructor(private dateUtilService: DateUtilService) {}

  addEvent(type: string, event: MatDatepickerInputEvent<any>) {
    const date = this.dateUtilService.toIsoDate(event.value);
    this.currentDateChange.emit(date);
  }

}
