import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionPerformanceComponent } from './production-performance.component';

xdescribe('ProductionPerformanceComponent', () => {
    let component: ProductionPerformanceComponent;
    let fixture: ComponentFixture<ProductionPerformanceComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ProductionPerformanceComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductionPerformanceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
