import { Component, OnInit, EventEmitter } from '@angular/core';
import { PanelsService } from 'src/app/shared/services/panels.service';
import { Subscription } from 'rxjs';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { ForwardPlannerService, AreasService } from 'src/app/shared/services';
import Srd  from '../../../assets/srd/srd.json';
import { ProjectsService } from '../../shared/services/projects.service'
import { switchMap, map } from 'rxjs/operators';
import { MatDatepickerInputEvent, MAT_DATE_LOCALE, DateAdapter } from '@angular/material';
import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';
import { ProductionPerformanceService } from './production-performance.service';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date.js';
@Component({
    selector: 'app-production-performance',
    templateUrl: './production-performance.component.html',
    styleUrls: ['./production-performance.component.scss'],
    animations: [
        // animation triggers go here
        trigger('flipState', [
            state('active', style({
              transform: 'rotateY(179.5deg)'
            })),
            state('inactive', style({
              transform: 'rotateY(179.5deg)'
            })),
            transition('active => inactive', animate('5000ms ease-out')),
            transition('inactive => active', animate('5000ms ease-in'))
          ]) 
      ]
})

export class ProductionPerformanceComponent implements OnInit {
    _subscription: Subscription;
    today: string;
    totalM2:number;
    _riskSubscription: Subscription;
    capacityMapped: object
    statsMapped: object;
    plansMapped: object;
    areasMapped: object;
    additionalAreaDataMapped: object;
    productionLines: object;
    defaultCapacities: object;
    textSize:object
    plannedProductionTotal: object;
    loaded: boolean;

    constructor( private productionPerformanceService : ProductionPerformanceService, private forwardPlannerService: ForwardPlannerService, private dateUtilsService: DateUtilService) {
        this.loaded = false;
    }
    flip = "active";
    ngOnInit() {
        this.textSize = {};
        this.productionLines = Srd.productionLines;
        this.today = this.dateUtilsService.todayAsIso();

        this._subscription = this.forwardPlannerService.getProductionPerformanceData(this.today).subscribe(([capacities, stats, additionalDataArea, plans, areas])=>{
            this.capacityMapped = this.dateUtilsService.mapper(capacities);
            this.statsMapped = stats;
            this.plansMapped = this.dateUtilsService.mapper(plans);
            this.additionalAreaDataMapped = this.dateUtilsService.mapper(additionalDataArea);
            this.areasMapped = this.dateUtilsService.areaMapper(areas);
            this.defaultCapacities = this.dateUtilsService.buildMap(this.productionLines, "defaultCapacity");
            this.loaded = true;
        });
    }
    toggleFlip() {
        this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
    }  
    currentDateChange(date: string){
        this.today = date;
        this.totalM2 = this.productionPerformanceService.getAreasForProductionToday(this.areasMapped,this.plansMapped,this.today);
        var productionLines = ["SIP","CASS", "IFAST", "TF","HSIP"]
        productionLines.forEach(productionLine => {
            this.getPanelQty(productionLine, date, "completedM2");
            this.getCapacityTotalForProduction(productionLine);
        });
       
        return this.productionPerformanceService.currentDateChange(date);
    }
    dayTargetColorIndicator(value, entry1, entry2, entry3){
        return this.productionPerformanceService.dayTargetColorIndicator(this.today,value, entry1, entry2, entry3); 
    }
    colorIndicator(value,byNow){
        if(value != "~"){
            return this.productionPerformanceService.colorIndicator(value,byNow); 
        }
    }
    
    target(productionLine, date){
        return this.productionPerformanceService.target(this.capacityMapped,date,productionLine); 
    }
    targetTotals(date){
        var total = 0;
        Object.keys(this.productionLines).forEach(productionLine =>{
            total += this.productionPerformanceService.target(this.capacityMapped,date,productionLine);            
        })
        return total;
        
    }
    getPanelQty(productionLine, date, type){
        if(this.statsMapped[productionLine] != undefined && this.statsMapped[productionLine][date] != undefined && this.statsMapped[productionLine][date][type] != undefined ){
            return this.statsMapped[productionLine][date][type];
        }else{
            return 0;
        }
    }
    getTextSize(info){
        if(info < 0){
            return "twoDigit" 
        }
        else if(info < 1000 ){
            return "threeDigit" 
        }
        else if(info >= 1000 && info < 10000){
            return "fourDigit" 
        }
    }
    getAllPanelsCompleted(date,type){
        var total = 0;
        Object.keys(this.productionLines).forEach(productionLine =>{
            if(this.statsMapped[productionLine] != undefined && this.statsMapped[productionLine][date] != undefined && this.statsMapped[productionLine][date][type] != undefined ){
                var data = this.statsMapped;
                total += this.productionPerformanceService.getPanelQty(data,date,productionLine,type); 
            }
        });
        return total;
    }
    dayTarget(productionLine, date){
        return this.productionPerformanceService.dayTarget(this.statsMapped,this.capacityMapped,date,productionLine); 
    }
    getTotalDayTarget(date){
        var total = 0;
        Object.keys(this.productionLines).forEach(productionLine => {
            var data = this.capacityMapped;
            total += this.productionPerformanceService.dayTarget(this.statsMapped,data,date,productionLine);
        })
        return total;
    }
    getInProgressCount(productionLine){
        if(this.statsMapped != undefined){
            var stats = this.statsMapped
            return this.productionPerformanceService.getInProgressCount(stats,productionLine); 
        }
    }
    getAllInProgress(){
        var total = 0;
        Object.keys(this.productionLines).forEach(productionLine =>{
            var stats = this.statsMapped;
            total += this.productionPerformanceService.getInProgressCount(stats,productionLine);             
        })
        return total;
    }
    getGreatestRisks(productionLine){ 
        var allRiskData = this.productionPerformanceService.getGreatestRisks(this.plansMapped,this.areasMapped); 
        return this.getLowestRisk(allRiskData[productionLine]);
    }
    getLowestRisk(riskData){
        var currentLowest = 0;
        riskData.forEach(risk =>{
            if(risk <= currentLowest){
                currentLowest = risk;
            }
        })
        return currentLowest;
    }
    getAreasForProductionToday(areas,plans){
        return this.productionPerformanceService.getAreasForProductionToday(areas,plans,this.today); 
    }

    getCapacityTotal(){
        var plannedAreas =[];
        Object.keys(this.plansMapped).forEach(planKey =>{
            var plannedDate = this.plansMapped[planKey].plannedStart;
            if(plannedDate != undefined && plannedDate === this.today){
                plannedAreas.push(planKey)
            }
        })
        return this.productionPerformanceService.getAreasForProductionToday(this.areasMapped,plannedAreas,this.today);
    }

    getCapacityTotalForProduction(productionLine){
        const plannedProduction = this.productionPerformanceService.getPlannedProductionTotal(this.areasMapped, this.plansMapped, this.additionalAreaDataMapped, this.today);
        return plannedProduction[productionLine];
    }

    ngOnDestroy(){
        this._subscription.unsubscribe();
      }
}