import { Component, OnInit } from '@angular/core';
import { DateUtilService } from 'src/app/shared/services/date-util.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public showMenu: string;
    constructor(private dateUtilsService: DateUtilService) {}
    todayDate: string;

    ngOnInit() {
        this.todayDate = this.dateUtilsService.todayAsIso();
        this.showMenu = '';
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
}
