import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { JobsheetsRoutingModule } from './jobsheets-routing.module';
import { JobsheetsComponent } from './jobsheets.component';
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatFormFieldModule, MatIconModule, MatInputModule, MatToolbarModule } from '@angular/material';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [JobsheetsComponent],
  imports: [
    CommonModule,
    JobsheetsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    DataExportModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class JobsheetsModule { }
