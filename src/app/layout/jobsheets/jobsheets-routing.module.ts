import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsheetsComponent } from './jobsheets.component'; 

const routes: Routes = [
  {
    path: '',
    component: JobsheetsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsheetsRoutingModule { }
