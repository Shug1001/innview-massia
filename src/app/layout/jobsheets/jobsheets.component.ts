import { Component, OnInit } from '@angular/core';
import { AreasService, ProjectsService } from 'src/app/shared/services';
import {FormControl} from '@angular/forms'; 
import {combineLatest, Observable, Subscription} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
import { PanelsService } from 'src/app/shared/services/panels.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import Srd  from '../../../assets/srd/srd.json';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-jobsheets',
  templateUrl: './jobsheets.component.html',
  styleUrls: ['./jobsheets.component.scss']
})
export class JobsheetsComponent implements OnInit {

  myControl = new FormControl();
  jobsheets: Array<any>;
  projects: Array<any>;
  jobsheetIds: Array<any>;
  filteredOptions: Observable<string[]>;
  filteredOptions2: Observable<string[]>;
  filteredOptions3: Observable<string[]>;
  panels: Array<any>;
  productTypeProductionLine: object;
  productTypes: object;
  additional: object;
  packDifference: number;
  panelDifference: number;
  showTable: boolean;
  jobsheet: string;
  exportAreas: Array<any>;
  _subscription: Subscription;
  loaded: boolean;
  projectIds: Array<any>;
  projectMapped: object;
  _subscription2: Subscription;
  project: Array<any>;
  areas: Array<any>;
  areasIds: Array<any>;
  areasMapped: object;
  myControl2 = new FormControl();
  area: Array<any>;
  jobSheetsMapped: object;
  _subscription3: Subscription;
  myControl3 = new FormControl();
  
  


  constructor(private db: AngularFireDatabase, private areasService: AreasService, private projectsService: ProjectsService, private dateUtilService: DateUtilService, private areaService: AreasService, private panelsService: PanelsService) { }

  ngOnInit() {

    this.showTable = false;
    this.loaded = false;
    this.productTypes = Srd.productTypes;
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");

    this._subscription = combineLatest(this.projectsService.getActiveProjectsList(), this.panelsService.getProductivity()).subscribe(([projects, productivity]) => {
      this.projects = projects;

      this.sortExport(productivity);

      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );

      this.projectIds = [];
      this.projectMapped = {};

      projects.forEach((project)=>{
        this.projectIds.push(project.id);
        if(this.projectMapped[project.id] === undefined){
          this.projectMapped[project.id] = project;
        }
      });

      this.loaded = true;

    });
  }

  onSelectionProjectChange(event){
    console.log('onProjectChange', this.projectMapped[event.option.value].$key);
    this._subscription2 = this.areasService.getAreasForProject(this.projectMapped[event.option.value].$key).subscribe(areas=>{
      this.project = this.projectMapped[event.option.value];
      this.areas = areas;
      this.areasIds = [];
      this.areasMapped = {};

      areas.forEach((areaSnap)=>{
        let area = areaSnap.payload.val();
        let areaId = area.phase + "-" + area.floor + "-" + area.type;
        this.areasIds.push(areaId);
        if(this.areasMapped[areaId] === undefined){
          this.areasMapped[areaId] = areaSnap.key;
        }
      });

      this.areasIds.sort();

      this.filteredOptions2 = this.myControl2.valueChanges.pipe(
        startWith(''),
        map(value => this._filter2(value))
      );
    });

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.projectIds.filter(projectIds => projectIds.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filter2(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.areasIds.filter(areaId => areaId.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filter3(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.jobsheetIds.filter(jobsheetId => jobsheetId.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionAreaChange(event){
    console.log('onAreaChange', this.areasMapped[event.option.value]);
    this._subscription3 = this.panelsService.getJobsheetsForArea(this.areasMapped[event.option.value]).subscribe((jobsheets) => {
      this.area = this.areasMapped[event.option.value];
      this.jobsheets = jobsheets;
      this.jobsheetIds = [];
      this.jobSheetsMapped = {};

      this.jobsheets.forEach((jobsheet)=>{
        this.jobsheetIds.push(jobsheet.projectNumber);
        if(this.jobSheetsMapped[jobsheet.projectNumber] === undefined){
          this.jobSheetsMapped[jobsheet.projectNumber] = jobsheet;
        }
      });

      this.filteredOptions3 = this.myControl3.valueChanges.pipe(
        startWith(''),
        map(value => this._filter3(value))
      );

    });
  }

  sortExport(productivityList){

    let exportAreas = [];

    Object.keys(productivityList).forEach((areaKey) => {

      let productivity = productivityList[areaKey];

      let key1 = productivity.additional ? Object.keys(productivity.additional)[0] : "";
      let key2 = productivity.additional ? Object.keys(productivity.additional)[1] : "";
      let key3 = productivity.additional ? Object.keys(productivity.additional)[2] : "";
      let key4 = productivity.additional ? Object.keys(productivity.additional)[3] : "";
      let key5 = productivity.additional ? Object.keys(productivity.additional)[4] : "";
      let key6 = productivity.additional ? Object.keys(productivity.additional)[5] : "";
      let key7 = productivity.additional ? Object.keys(productivity.additional)[6] : "";
      let key8 = productivity.additional ? Object.keys(productivity.additional)[7] : "";

      let exportArea = {
        "Jobsheet Ref": productivity.jobsheetRef,
        "Framing Style": productivity.framingStyle,
        "Total M2": productivity.totalM2,
        "Type": key1,
        "Operatives": productivity.additional && productivity.additional[key1] ? productivity.additional[key1].operatives : "",
        "Duration": productivity.additional && productivity.additional[key1] ? productivity.additional[key1].duration : "",
        "Type2": key2,
        "Operatives1": productivity.additional && productivity.additional[key2] ? productivity.additional[key2].operatives : "",
        "Duration2": productivity.additional && productivity.additional[key2] ? productivity.additional[key2].duration : "",
        "Type3": key3,
        "Operatives3": productivity.additional && productivity.additional[key3] ? productivity.additional[key3].operatives : "",
        "Duration3": productivity.additional && productivity.additional[key3] ? productivity.additional[key3].duration : "",
        "Type4": key4,
        "Operatives4": productivity.additional && productivity.additional[key4] ? productivity.additional[key4].operatives : "",
        "Duration4": productivity.additional && productivity.additional[key4] ? productivity.additional[key4].duration : "",
        "Type5": key5,
        "Operatives5": productivity.additional && productivity.additional[key5] ? productivity.additional[key5].operatives : "",
        "Duration5": productivity.additional && productivity.additional[key5] ? productivity.additional[key5].duration : "",
        "Type6": key6,
        "Operatives6": productivity.additional && productivity.additional[key6] ? productivity.additional[key6].operatives : "",
        "Duration6": productivity.additional && productivity.additional[key6] ? productivity.additional[key6].duration : "",
        "Type7": key7,
        "Operatives7": productivity.additional && productivity.additional[key7] ? productivity.additional[key7].operatives : "",
        "Duration7": productivity.additional && productivity.additional[key7] ? productivity.additional[key7].duration : "",
        "Type8": key8,
        "Operatives8": productivity.additional && productivity.additional[key8] ? productivity.additional[key8].operatives : "",
        "Duration8": productivity.additional && productivity.additional[key8] ? productivity.additional[key8].duration : "",
      }
      exportAreas.push(exportArea);

    });

    this.exportAreas = exportAreas;
  }

  preKitUpdate(key, type, data){
    let updates = {};
    updates['/additionalData/jobsheets/' + key + '/' + type ] = data;
    console.log(updates);
    return this.db.database.ref().update(updates);
  }

  onSelectionJobsheetChange(event){
    console.log('onSelectionJobsheetChange', this.jobSheetsMapped[event.option.value]);
    
    let jobsheet = this.jobSheetsMapped[event.option.value];
    this.jobsheet = jobsheet;

    var getPanels = []
    

    Object.keys(jobsheet.panels).forEach(panelKey => {
      getPanels.push(this.panelsService.getpanelPromise(panelKey));
    });

    this.panels = [];
    this.showTable = false;

    return Promise.all(getPanels)
      .then((panels) =>{
       this.panels = panels;
       let startedArr = [];
       let finishArr = [];
       let perPanelArr = [];
       this.panels.forEach(panel => {
         let started = panel.qa && panel.qa.started ? panel.qa.started : 0;
         let completed = panel.qa && panel.qa.completed ? panel.qa.completed : 0;
        startedArr.push(started);
        finishArr.push(completed);
        let panelDuration = completed - started;
        perPanelArr.push(panelDuration);
       });
       let packStart = Math.min(...startedArr);
       let packFinish = Math.max(...finishArr);
       this.packDifference =  packFinish - packStart;
       this.panelDifference = perPanelArr.reduce((a, b) => a + b, 0);
       return this.areaService.getAdditionalJobsheetTypePromise(jobsheet.$key);
      }).then((additional) =>{
        

        if(additional.key !== null){
          let additionalHolder = additional.payload.val();

          this.additional = {
            SIP:  {
              operatives: additionalHolder.SIP && additionalHolder.SIP.operatives ? additionalHolder.SIP.operatives : 0,
              duration: additionalHolder.SIP && additionalHolder.SIP.duration ? additionalHolder.SIP.duration : 0
            },
            HSIP:  {
              operatives: additionalHolder.HSIP && additionalHolder.HSIP.operatives ? additionalHolder.HSIP.operatives : 0,
              duration: additionalHolder.HSIP && additionalHolder.HSIP.duration ? additionalHolder.HSIP.duration : 0
            },
            IFAST:  {
              operatives: additionalHolder.IFAST && additionalHolder.IFAST.operatives ? additionalHolder.IFAST.operatives : 0,
              duration: additionalHolder.IFAST && additionalHolder.IFAST.duration ? additionalHolder.IFAST.duration : 0
            },
            TF:  {
              operatives: additionalHolder.TF && additionalHolder.TF.operatives ? additionalHolder.TF.operatives : 0,
              duration: additionalHolder.TF && additionalHolder.TF.duration ? additionalHolder.TF.duration : 0
            },
            CASS:  {
              operatives: additionalHolder.CASS && additionalHolder.CASS.operatives ? additionalHolder.CASS.operatives : 0,
              duration: additionalHolder.CASS && additionalHolder.CASS.duration ? additionalHolder.CASS.duration : 0
            },
            Hundegger:  {
              operatives: additionalHolder.Hundegger && additionalHolder.Hundegger.operatives ? additionalHolder.Hundegger.operatives : 0,
              duration: additionalHolder.Hundegger && additionalHolder.Hundegger.duration ? additionalHolder.Hundegger.duration : 0
            },
            HM:  {
              operatives: additionalHolder.HM && additionalHolder.HM.operatives ? additionalHolder.HM.operatives : 0,
              duration: additionalHolder.HM && additionalHolder.HM.duration ? additionalHolder.HM.duration : 0
            },
            BeamSaw:  {
              operatives: additionalHolder.BeamSaw && additionalHolder.BeamSaw.operatives ? additionalHolder.BeamSaw.operatives : 0,
              duration: additionalHolder.BeamSaw && additionalHolder.BeamSaw.duration ? additionalHolder.BeamSaw.duration : 0
            },
            WallSaw:  {
              operatives: additionalHolder.WallSaw && additionalHolder.WallSaw.operatives ? additionalHolder.WallSaw.operatives : 0,
              duration: additionalHolder.WallSaw && additionalHolder.WallSaw.duration ? additionalHolder.WallSaw.duration : 0
            },
            ESP:  {
              operatives: additionalHolder.ESP && additionalHolder.ESP.operatives ? additionalHolder.ESP.operatives : 0,
              duration: additionalHolder.ESP && additionalHolder.ESP.duration ? additionalHolder.ESP.duration : 0
            },
            Logistics:  {
              operatives: additionalHolder.Logistics && additionalHolder.Logistics.operatives ? additionalHolder.Logistics.operatives : 0,
              duration: additionalHolder.Logistics && additionalHolder.Logistics.duration ? additionalHolder.Logistics.duration : 0
            },
            ProcessLead:  {
              operatives: additionalHolder.Logistics && additionalHolder.Logistics.operatives ? additionalHolder.Logistics.operatives : 0,
              duration: additionalHolder.Logistics && additionalHolder.Logistics.duration ? additionalHolder.Logistics.duration : 0
            }
          };

        }else{
          this.additional = {
            SIP:  {
              operatives: 0,
              duration: 0
            },
            HSIP:  {
              operatives: 0,
              duration: 0
            },
            IFAST:  {
              operatives: 0,
              duration: 0
            },
            TF:  {
              operatives: 0,
              duration: 0
            },
            CASS:  {
              operatives: 0,
              duration: 0
            },
            Hundegger:  {
              operatives: 0,
              duration: 0
            },
            HM:  {
              operatives: 0,
              duration: 0
            },
            BeamSaw:  {
              operatives: 0,
              duration: 0
            },
            WallSaw:  {
              operatives: 0,
              duration: 0
            },
            ESP:  {
              operatives: 0,
              duration: 0
            },
            Logistics:  {
              operatives: 0,
              duration: 0
            },
            ProcessLead:  {
              operatives: 0,
              duration: 0
            }
          };
        }

        this.showTable = true;
        
      });

      
  }

  calculateTiming(type, operatives, duration){
    if(type === "SIP" || type === "HSIP" || type === "IFAST" || type === "TF" || type === "CASS"){
      let totalTimestamp = this.panelDifference * operatives;
      var daysDifference = Math.floor(totalTimestamp/1000/60/60/24);
      totalTimestamp -= daysDifference*1000*60*60*24
  
      var hoursDifference = Math.floor(totalTimestamp/1000/60/60);
      totalTimestamp -= hoursDifference*1000*60*60
  
      var minutesDifference = Math.floor(totalTimestamp/1000/60);
      totalTimestamp -= minutesDifference*1000*60
  
      var secondsDifference = Math.floor(totalTimestamp/1000);
      return daysDifference + ' days ' + hoursDifference + ' hours ' + minutesDifference + ' minutes ' + secondsDifference + ' seconds ';
    }else{
      let totalTimestamp = (duration * 60000) * operatives;
      var daysDifference = Math.floor(totalTimestamp/1000/60/60/24);
      totalTimestamp -= daysDifference*1000*60*60*24
  
      var hoursDifference = Math.floor(totalTimestamp/1000/60/60);
      totalTimestamp -= hoursDifference*1000*60*60
  
      var minutesDifference = Math.floor(totalTimestamp/1000/60);
      totalTimestamp -= minutesDifference*1000*60
  
      var secondsDifference = Math.floor(totalTimestamp/1000);
      return daysDifference + ' days ' + hoursDifference + ' hours ' + minutesDifference + ' minutes ' + secondsDifference + ' seconds ';
    }
  }


  ngOnDestroy(){
    this._subscription.unsubscribe();
  }


}
