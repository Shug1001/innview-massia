import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialRequestDetailsComponent } from './material-request-details.component';

xdescribe('MaterialRequestDetailsComponent', () => {
  let component: MaterialRequestDetailsComponent;
  let fixture: ComponentFixture<MaterialRequestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialRequestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
