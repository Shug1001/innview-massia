import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Reason, MaterialRequest } from '../../shared/models/models';
import { ProjectsService, AuthService } from '../../shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MatAutocompleteSelectedEvent, MAT_DATE_FORMATS, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { Subscription, combineLatest, Observable } from 'rxjs'
import { MaterialRequestService } from 'src/app/shared/services/material-request.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { StockItem } from '../../shared/models/models';
import { startWith, map } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { SupplyChainService } from 'src/app/shared/services/supply-chain.service';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-material-request-details',
  templateUrl: './material-request-details.component.html',
  styleUrls: ['./material-request-details.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class MaterialRequestDetailsComponent implements OnInit {

  minDate = new Date();
  materialControl: FormGroup;
  _subscription: Subscription;
  loaded: boolean;
  submitted: boolean;
  projectIds: Array<any>;
  stockItems: Array<any>;
  id: string;
  uom: string;
  stockItemName: string;
  stockItemKey: string;
  amount: number;
  materialRequest: MaterialRequest;
  reasons: Reason[] = [
    {name: "Missing from Delivery", abbreviation: "MD"},
    {name: "Ancillaries Request", abbreviation: "AR"},
    {name: "Variation Order", abbreviation: "VO"},
    {name: "Site Request (Additional to budget)", abbreviation: "SR"},
  ];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredStockItems: Observable<string[]>;
  stockItemsArr = [];
  stockItemsKeyArr = [];

  @ViewChild('stockItemInput') stockItemInput: ElementRef<HTMLInputElement>;
  @ViewChild('autoStockName') matAutocomplete: MatAutocomplete;

  constructor(private supplyChainService: SupplyChainService, private router: Router, private dateUtilService: DateUtilService, private route: ActivatedRoute, private materialRequestService: MaterialRequestService, private fb: FormBuilder, public dialog: MatDialog, private projectsService: ProjectsService, private authService: AuthService) { 
    this.loaded = false;
    this.submitted = false;
  }

  ngOnInit(){
    this.amount = 0;
    this.id = this.route.snapshot.paramMap.get('id');
    this._subscription = combineLatest(this.materialRequestService.getMaterialRequest(this.id), this.supplyChainService.getStockItems()).subscribe(([materialRequest, stockItems]) => {
      this.materialRequest = materialRequest;
      this.loaded = true;
      this.setUpForm();

      this.stockItems = stockItems;
      if(materialRequest.stockItems !== undefined){
        Object.keys(materialRequest.stockItems).forEach(stockItemKey => { 
          for (let i = 0; i < stockItems.length; i++){
            let stockItem: any = stockItems[i];
            if(stockItemKey === stockItem.$key) {
              let newData = {};
              newData[stockItem.$key] = materialRequest.stockItems[stockItemKey];
              this.stockItemsArr.push(
                { 
                  stockItem: stockItem.stockName,
                  amount: materialRequest.stockItems[stockItemKey].split(" ")[0],
                  uom: materialRequest.stockItems[stockItemKey].split(" ")[1]
                });
              this.stockItemsKeyArr.push(newData);
            }
          }
        });
      }

      this.filteredStockItems = this.materialControl.get('stockNames').valueChanges.pipe(
        startWith(null),
        map((stockItem: string | null) => {
          return stockItem ? this._filter2(stockItem) : this.stockItems.slice();
        }));
    });
  }

  back(){
    this.router.navigate(['/material-request']);
  }

  add(event: MatAutocompleteSelectedEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      this.materialControl.get('stockNames').setValue(null);
    }else{
      this.uom = event.option.value.uom;
      this.stockItemKey = event.option.value.$key;
      this.stockItemName = event.option.viewValue;
      this.materialControl.get('stockNames').setValue(event.option.viewValue);
    }
  }

  remove(fruit: string): void {
    const index = this.stockItemsArr.indexOf(fruit);

    if (index >= 0) {
      this.stockItemsArr.splice(index, 1);
      this.stockItemsKeyArr.splice(index, 1);
    }
  }

  selected(): void {
    let found = 0;
    if(this.stockItemName && this.stockItemKey){
      this.stockItems.forEach(element => {
        if(element.stockName === this.stockItemName){
          found = 1;
        }
      });
      if(found && this.amount !== undefined && this.uom !== undefined){
        let newData = {};
        newData[this.stockItemKey] = this.amount + " " + this.uom;
        this.stockItemsArr.push({
          stockItem: this.stockItemName,
          amount: this.amount,
          uom: this.uom
        });
        this.stockItemsKeyArr.push(newData);
        this.uom = "";
      }
    }
    this.materialControl.get('stockNames').setValue(null);
  }

  private _filter2(value: any) {

    return this.stockItems.filter((stockItem) => {
      return stockItem.stockName.toLowerCase().indexOf(value) === 0
    });

  }

  setUpForm(){
    this.materialControl = this.fb.group({
      email: new FormControl({ value: this.materialRequest.email || null, disabled: true }, [Validators.required]),
      person: new FormControl({ value: this.materialRequest.person || null, disabled: true }, [Validators.required]),
      projectId: new FormControl({ value: this.materialRequest.projectId || null, disabled: true }, [Validators.required]),
      projectName: new FormControl({ value: this.materialRequest.projectName || null, disabled: true }, [Validators.required]),
      reason: new FormControl({ value: this.materialRequest.reason || null, disabled: true }, [Validators.required]),
      dateRequired: new FormControl({ value: this.materialRequest.dateRequired || null, disabled: true }, [Validators.required]),
      stockNames: new FormControl({ value: null, disabled: this.materialRequest.action }),
      materialRequired: new FormControl({ value: this.materialRequest.materialRequired || null, disabled: this.materialRequest.action }, [Validators.required]),
      mainContractor: new FormControl({ value: this.materialRequest.mainContractor || null, disabled: this.materialRequest.action }),
      siteContact: new FormControl({ value: this.materialRequest.siteContact || null, disabled: this.materialRequest.action }),
      siteContactNo: new FormControl({ value: this.materialRequest.siteContactNo || null, disabled: this.materialRequest.action }),
      qsName: new FormControl({ value: this.materialRequest.qsName || null, disabled: this.materialRequest.action }),
      qsAuth: new FormControl({ value: this.materialRequest.qsAuth || null, disabled: this.materialRequest.action }),
      action: new FormControl({ value: this.materialRequest.action || null, disabled: this.materialRequest.action }),
      deliveryDate: new FormControl({ value: this.materialRequest.deliveryDate || null, disabled: this.materialRequest.action }),
      note: new FormControl({ value: this.materialRequest.note || null, disabled: this.materialRequest.action })
    });
  }

  onSubmit(): void{
    this.submitted = true;
    let updateObj = this.materialControl.getRawValue();
    updateObj = Object.assign(this.materialRequest, updateObj);
    let update = {}
    this.stockItemsKeyArr.forEach(element => {
      update[Object.keys(element)[0]] = element[Object.keys(element)[0]];
    });
    updateObj.stockItems = update;
    updateObj.dateRequired = moment.isMoment(updateObj.dateRequired) ? this.dateUtilService.toIsoDate(updateObj.dateRequired._d) : updateObj.dateRequired;
    updateObj.deliveryDate = moment.isMoment(updateObj.deliveryDate) ? this.dateUtilService.toIsoDate(updateObj.deliveryDate._d) : updateObj.deliveryDate;
    this.materialRequestService.updateMaterialRequest(this.id, updateObj)
    .then(()=>{
      this.router.navigate(['/material-request']);
    });
  }
}