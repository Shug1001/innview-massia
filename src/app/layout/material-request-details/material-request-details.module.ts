import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialRequestDetailsRoutingModule } from './material-request-details-routing.module';
import { MaterialRequestDetailsComponent } from './material-request-details.component';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule, MatToolbarModule, MatAutocompleteModule, MatSelectModule, MatListModule, MatChipsModule, MatIconModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextFieldModule } from '@angular/cdk/text-field';

@NgModule({
  declarations: [MaterialRequestDetailsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatNativeDateModule, 
    MatDatepickerModule,
    MatAutocompleteModule,
    MatListModule,
    TextFieldModule,
    MaterialRequestDetailsRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [MaterialRequestDetailsComponent]
})
export class MaterialRequestDetailsModule { }
