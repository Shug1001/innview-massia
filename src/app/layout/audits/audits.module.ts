import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditsRoutingModule } from './audits-routing.module';
import { AuditsComponent } from './audits.component';
import { MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AuditsComponent],
  imports: [
    CommonModule,
    AuditsRoutingModule,
    MatFormFieldModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatInputModule,
    FormsModule
  ]
})
export class AuditsModule { }
