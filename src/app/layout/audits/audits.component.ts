import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';
import { AuditsService } from 'src/app/shared/services/audits.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';

@Component({
  selector: 'app-audits',
  templateUrl: './audits.component.html',
  styleUrls: ['./audits.component.scss']
})
export class AuditsComponent implements OnInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  _subscription: Subscription;
  audits: Array<any>;
  loaded: boolean;
  displayedColumns: string[] = ['timestamp', 'userName', 'log', 'productionLine', 'project'];
  dataSource: MatTableDataSource<AuditsData>;



  constructor(private auditsService: AuditsService, private dateUtilsService: DateUtilService) { }

  ngOnInit() {
    this.loaded = false;

    this._subscription = this.auditsService.getAudits().subscribe((audits) => {
      this.audits = audits;
      this.audits.reverse();
      this.loaded = true;
      this.dataSource = new MatTableDataSource(this.audits);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  toUserDate(date){
    return this.dateUtilsService.toUserSlachDate(date);
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

}

export interface AuditsData {
  log: string
  timestamp: number,
  link: string,
  projectId: string,
  productionLine: string,
  userName: string,
}
