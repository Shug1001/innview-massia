import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FramingStylesComponent } from './framing-styles.component';

describe('FramingStylesComponent', () => {
  let component: FramingStylesComponent;
  let fixture: ComponentFixture<FramingStylesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FramingStylesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FramingStylesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
