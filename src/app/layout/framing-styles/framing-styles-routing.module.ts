import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FramingStylesComponent } from './framing-styles.component';

const routes: Routes = [
  {
    path: '',
    component: FramingStylesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FramingStylesRoutingModule { }
