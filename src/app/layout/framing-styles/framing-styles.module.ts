import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FramingStylesRoutingModule } from './framing-styles-routing.module';
import { FramingStylesComponent } from './framing-styles.component';
import { MatAutocompleteModule, MatButtonModule, MatIconModule, MatInputModule, MatSelectModule, MatToolbarModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FramingStylesComponent],
  imports: [
    CommonModule,
    MatSelectModule,
    MatButtonModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    FramingStylesRoutingModule
  ]
})
export class FramingStylesModule { }
