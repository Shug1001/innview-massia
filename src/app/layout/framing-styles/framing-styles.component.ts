import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ProjectsService } from 'src/app/shared/services';
import { ProjectsAdditional } from 'src/app/shared/models/models';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-framing-styles',
  templateUrl: './framing-styles.component.html',
  styleUrls: ['./framing-styles.component.scss']
})
export class FramingStylesComponent implements OnInit {

  _subscription: Subscription;
  _subscription2: Subscription;
  allFramingStyles: object;
  framingStyleSets: object;
  framingStylegroups: Array<any>;
  group: string;
  framingStyle: string;
  ready: boolean;
  ready2: boolean;
  list: Array<any>;
  incrementer: number;
  id: string;
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  
  constructor(private projectsService: ProjectsService, private db: AngularFireDatabase, private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit() {

    this.incrementer = 0;
    this.id = this.route.parent.parent.snapshot.paramMap.get('id');

    this.ready = false;
    this._subscription = this.http.get<any>('https://innview-api.azurewebsites.net/api/Maintenance').subscribe(data => {
      this.allFramingStyles = data;
      this.framingStyleSets = {};
      this.framingStylegroups = [];
      data.forEach(element => {
        if(this.framingStyleSets[element['Panel_Group_Name']] === undefined){
          this.framingStyleSets[element['Panel_Group_Name']] = [];
          this.framingStylegroups.push(element['Panel_Group_Name']);
        }
        
        this.framingStyleSets[element['Panel_Group_Name']].push(element['Panel_Framing_Style']);
      });
      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
      this.ready = true;
    });

    let queryParams = new HttpParams();
    queryParams = queryParams.append("projKey",this.id);
    this.ready2 = false;
    this._subscription2 = this.http.get<any>('https://innview-api.azurewebsites.net/api/ProjectFramingData',{"params":queryParams}).subscribe(data => {
      this.list = data;
      this.ready2 = true;
    });


  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.framingStylegroups.filter(option => option.toLowerCase().includes(filterValue));
  }

  selectGroup(evt){
    this.group = evt.option.value;
  }

  selectFramingStyle(evt){
    this.framingStyle = evt;
  }

  save(){
    if(this.group && this.framingStyle){

      let found = false;

      this.list.forEach((item)=>{
        if(item.Panel_Framing_Style === this.framingStyle){
          found = true;
        }
      });

      if(!found){
      
        this.incrementer = this.list.length;

        let holder = {
          fb_id: this.id,
          RowNumber: this.incrementer,
          PanelGroup: this.group,
          PanelFramingStyle: this.framingStyle
        }

        this.incrementer++;


        return this.projectFramingData(holder)
        .then(()=>{
          let queryParams = new HttpParams();
          queryParams = queryParams.append("projKey",this.id);
          this._subscription2 = this.http.get<any>('https://innview-api.azurewebsites.net/api/ProjectFramingData',{"params":queryParams}).subscribe(data => {
            this.list = data;
            this.ready2 = true;
          });
        });

      }
      
    }
  }

  deleteFramingStyle(line){
    let holder = {
      fb_id: this.id,
      RowNumber: line
    }
    return this.projectFramingData(holder)
    .then(()=>{
      let queryParams = new HttpParams();
      queryParams = queryParams.append("projKey",this.id);
      this._subscription2 = this.http.get<any>('https://innview-api.azurewebsites.net/api/ProjectFramingData',{"params":queryParams}).subscribe(data => {
        this.list = data;
        this.ready2 = true;
      });
    });
  }

  projectFramingData(val){
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectFramingData',val).toPromise();
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
    this._subscription2.unsubscribe();
  }

}
