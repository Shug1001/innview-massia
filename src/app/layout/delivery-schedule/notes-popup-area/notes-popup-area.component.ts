import { Component, OnInit, Input } from '@angular/core';
import { DialogNoteAreaComponent } from './dialog-note-area/dialog-note-area.component';
import { MatDialog } from '@angular/material';
import { AreaExt } from 'src/app/shared/models/models';
import { Subscription } from 'rxjs';
import { AreasService } from 'src/app/shared/services';

@Component({
  selector: 'app-notes-popup-area',
  templateUrl: './notes-popup-area.component.html',
  styleUrls: ['./notes-popup-area.component.scss']
})
export class NotesPopupAreaComponent {

  @Input() area: AreaExt;

  mappAddons: object;
  additionalDataAddon: string;
  _subscription: Subscription;

  constructor(public dialog: MatDialog, private areasService: AreasService) {}

  ngOnInit(){
    this.mappAddons = {
      'Windows': 'W',
      'Cladding': 'C',
      'Windows & Cladding': 'W/C'
    };
    this.additionalDataAddon = '';
    this._subscription = this.areasService.getAdditionalAreaData2(this.area.$key).subscribe((snap) => {
        this.additionalDataAddon = snap !== null && snap.addons !== undefined ? snap.addons : '';
    });
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogNoteAreaComponent, {
      width: '300px',
      data: {area: this.area}
    });

    dialogRef.componentInstance.data = {area: this.area};

  }
}
