import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNoteAreaComponent } from './dialog-note-area.component';

xdescribe('DialogNoteComponent', () => {
  let component: DialogNoteAreaComponent;
  let fixture: ComponentFixture<DialogNoteAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogNoteAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNoteAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
