import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreasService } from 'src/app/shared/services';

@Component({
  selector: 'app-dialog-note-area',
  templateUrl: './dialog-note-area.component.html',
  styleUrls: ['./dialog-note-area.component.scss']
})
export class DialogNoteAreaComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogNoteAreaComponent>,
    public areasService: AreasService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveAreaNote(key, comments){
    const updateObj = {"comments": comments};
    return this.areasService.updateArea(key, updateObj)
            .then(()=>{
              this.dialogRef.close();
            });
  }

  saveComponentAddons(){
    const updateObj = {};
    updateObj["addons"] = this.data.additionalDataAddon;
    return this.areasService.updateAdditionalAreaData(this.data.area.$key, updateObj)
            .then(()=>{
              this.dialogRef.close();
            });
  }

}
