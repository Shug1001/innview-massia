import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeliveryScheduleComponent } from './delivery-schedule.component';

const routes: Routes = [
    {
        path: '',
        component: DeliveryScheduleComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DeliveryScheduleRoutingModule {}
