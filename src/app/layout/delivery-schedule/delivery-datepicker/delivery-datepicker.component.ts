import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatCalendar, MatDateFormats } from '@angular/material';
import { AreasService } from 'src/app/shared/services';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import {
  Component,
  Input,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
  ChangeDetectorRef,
  EventEmitter,
  Output
} from '@angular/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-delivery-datepicker',
  templateUrl: './delivery-datepicker.component.html',
  styleUrls: ['./delivery-datepicker.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    {provide: CustomDateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})

export class DeliveryDatepickerComponent implements OnInit, OnDestroy {

  @Input() deliveryDate;
  @Input() project;
  @Input() component;
  @Input() area;
  @Input() selected;

  @Output() triggerParentSubscription = new EventEmitter<boolean>();

  minDate: Date;
  bulkMove: boolean;
  includeSaturdays: boolean;

  _subscription: Subscription;
  _subscription2: Subscription;

  dateClass = BulkCustomHeaderComponent;

  constructor(
    private revisionsService: RevisionsService,
    private areasService: AreasService,
    private dateUtilService: DateUtilService) {
      this.minDate = new Date();
      this.bulkMove = false;
      this.includeSaturdays = false;
  }

  weekendFilter = (data) => {
    const day = new Date(data).getDay();
    // Prevent Saturday and Sunday from being selected.
    if (!this.component.project.includeSaturday) {
      return day !== 0 && day !== 6;
    } else {
      return day !== 0;
    }
  }

  ngOnInit() {
    this._subscription = this.areasService.bulkMoveToggle$.subscribe((toggle) => {
      this.bulkMove = toggle;
    });
    this._subscription2 = this.areasService.incSat$.subscribe((toggle) => {
      this.includeSaturdays = toggle;
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._subscription2.unsubscribe();
  }

  deliveryDateChange(type: string, event: MatDatepickerInputEvent<any>) {
    this.triggerParentSubscription.emit(false);
    if ( !this.bulkMove ) {
      if ( this.area === undefined ) {
        const saves = [];
        Object.keys(this.component.areas).forEach((areaKey) => {
          const area = this.component.areas[areaKey];
          const date = this.dateUtilService.toIsoDate(event.value._d);
          area.revisions[this.component.project.deliverySchedule.unpublished] = this.dateUtilService.toIsoDate(date);
          const updateObj = {'revisions':area.revisions};
          const promise = this.areasService.updateArea(area.$key, updateObj);
          saves.push(promise);
        });
        return Promise.all(saves)
        .then(() => {
          this.triggerParentSubscription.emit(true);
        });
      } else {
        const date = this.dateUtilService.toIsoDate(event.value._d);
        this.area.revisions[this.component.project.deliverySchedule.unpublished] = this.dateUtilService.toIsoDate(date);
        const updateObj = {'revisions': this.area.revisions};
        return this.areasService.updateArea(this.area.$key, updateObj)
        .then(() => {
          this.triggerParentSubscription.emit(true);
        });
      }
    } else {
      
      let diffDays = 0;
      if ( this.dateUtilService.validDate(this.deliveryDate)
      && this.dateUtilService.validDate(this.dateUtilService.toIsoDate(event.value._d))) {
        if (this.includeSaturdays) {
          diffDays = this.dateUtilService.daysBetweenWeekDaysAndSat(this.deliveryDate, this.dateUtilService.toIsoDate(event.value._d));
        } else {
          diffDays = this.dateUtilService.daysBetweenWeekDaysOnly(this.deliveryDate, this.dateUtilService.toIsoDate(event.value._d));
        }
      }
      return this.revisionsService.bulkMoveAreasPhases(this.component.project, this.includeSaturdays, diffDays, this.deliveryDate, this.selected)
      .then(() => {
        this.triggerParentSubscription.emit(true);
      });
    }
  }
}

@Component({
  selector: 'app-date-header',
  styles: [`
  .example-header {
    display: flex;
    align-items: center;
    padding: 0.5em;
  }

  .example-header-label {
    flex: 1;
    height: 1em;
    font-weight: 500;
    text-align: center;
  }

  .example-double-arrow .mat-icon {
    margin: -22%;
  }
  mat-button-toggle {
    height: 40px;
    font-size: 0.6vw;
  }
`],
  template: `
    <div>
    <div style="text-align:center;padding-top:2px">
    <mat-button-toggle
    [checked]="bulkMove"
    (change)="bulkMoveToggled($event.source.checked)">
      Bulk Move
    </mat-button-toggle>
    <mat-button-toggle
    [checked]="incSat"
    (change)="incSatToggled($event.source.checked)">
      Inc. Sat
    </mat-button-toggle>
    </div>
    <div style="text-align:center;">
      <button mat-icon-button class="example-double-arrow" (click)="previousClicked('year')">
      <mat-icon>keyboard_arrow_left</mat-icon>
      <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <button mat-icon-button (click)="previousClicked('month')">
        <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <span class="example-header-label">{{periodLabel}}</span>
      <button mat-icon-button (click)="nextClicked('month')">
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
      <button mat-icon-button class="example-double-arrow" (click)="nextClicked('year')">
        <mat-icon>keyboard_arrow_right</mat-icon>
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
    </div>
  </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class BulkCustomHeaderComponent implements OnDestroy {
  private _destroyed = new Subject<void>();
  bulkMove: boolean;
  incSat: boolean;

  constructor(
    private areasService: AreasService,
    private _calendar: MatCalendar<any>,
    private customDateAdapter: CustomDateAdapter,
    @Inject(MAT_DATE_FORMATS)
    private _dateFormats: MatDateFormats,
    cdr: ChangeDetectorRef) {
    this.bulkMove = false;
    _calendar.stateChanges
      .pipe(takeUntil(this._destroyed))
      .subscribe(() => cdr.markForCheck());
  }
  get periodLabel() {
    return this.customDateAdapter
      .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }
  bulkMoveToggled(toggle) {
    this.bulkMove = toggle;
    this.areasService.bulkMoveToggle(toggle);
  }
  incSatToggled(toggle) {
    this.incSat = toggle;
    this.areasService.incSat(toggle);
  }
  previousClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month'
    ? this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, -1)
    : this.customDateAdapter.addCalendarYears(this._calendar.activeDate, -1);
  }
  nextClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month'
     ? this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, 1)
     : this.customDateAdapter.addCalendarYears(this._calendar.activeDate, 1);
  }
  ngOnDestroy() {
    this.bulkMove = false;
    this.incSat = false;
    this._destroyed.next();
    this._destroyed.complete();
  }
}