import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { DeliveryScheduleComponent } from './delivery-schedule.component';
import { DeliveryScheduleRoutingModule } from './delivery-schedule-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeliveryDatepickerComponent, BulkCustomHeaderComponent } from './delivery-datepicker/delivery-datepicker.component';
import { NotesPopupComponent } from './notes-popup/notes-popup.component';
import { DialogNoteComponent } from './notes-popup/dialog-note/dialog-note.component';
import { NotesPopupAreaComponent } from './notes-popup-area/notes-popup-area.component';
import { DialogNoteAreaComponent } from './notes-popup-area/dialog-note-area/dialog-note-area.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HandoverConfirmedComponent } from './handover-confirmed/handover-confirmed.component';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SrdSelectModule } from 'src/app/shared/modules/srd-select/srd-select.module';
import {NgxPrintModule} from 'ngx-print';
import { AreaPopupModule } from 'src/app/shared/modules/area-popup/area-popup.module';
import { FileUploaderModule } from 'src/app/shared/modules/file-uploader/file-uploader.module';
import { RevitOptionsModule } from '../../shared/modules/revit-options/revit-options.module';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import {
    MatListModule,
    MatSortModule,
    MatDialogModule,
    MatCardModule,
    MatBadgeModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatButtonToggleModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatChipsModule,
    MatTableModule,
    MatGridListModule,
    MatTooltipModule,
    MatRadioModule,
    MatSelectModule,
    MatToolbarModule,
    MatExpansionModule,
    MatSnackBarModule } from '@angular/material';
import { ExpectedDateModule } from 'src/app/shared/modules/expected-date/expected-date.module';
import { JobsheetsModule } from 'src/app/shared/modules/jobsheets/jobsheets.module';
import { ExpectedDateComponent } from 'src/app/shared/modules/expected-date/expected-date.component';
import { ExpectedDateComponentModule } from 'src/app/shared/modules/expected-date-component/expected-date-component.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        DeliveryScheduleRoutingModule,
        MatSelectModule,
        MatDialogModule,
        MatTooltipModule,
        MatCardModule,
        MatBadgeModule,
        MatRadioModule,
        MatProgressBarModule,
        MatListModule,
        MatCheckboxModule,
        MatButtonModule,
        MatChipsModule,
        MatButtonToggleModule,
        MatIconModule,
        FormsModule,
        RevitOptionsModule,
        MatToolbarModule,
        MatInputModule,
        MatExpansionModule,
        MatButtonToggleModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatSortModule,
        MatListModule,
        MatGridListModule,
        MatSnackBarModule,
        MatTableModule,
        JobsheetsModule,
        MatFormFieldModule,
        PipesModule,
        ExpectedDateComponentModule,
        ExpectedDateModule,
        SrdSelectModule,
        NgxPrintModule,
        AreaPopupModule,
        FileUploaderModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    exports: [DeliveryScheduleComponent],
    declarations: [
        DeliveryScheduleComponent,
        BulkCustomHeaderComponent,
        DeliveryDatepickerComponent,
        HandoverConfirmedComponent,
        NotesPopupComponent,
        DialogNoteComponent,
        NotesPopupAreaComponent,
        DialogNoteAreaComponent,
        ConfirmationPopupDialogComponent],
    entryComponents: [DialogNoteComponent, BulkCustomHeaderComponent, DialogNoteAreaComponent, ConfirmationPopupDialogComponent]
})
export class DeliveryScheduleModule {}
