import { Component, OnInit } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { ExportService } from 'src/app/shared/services/export.service';
import { CustomDateAdapter } from '../../shared/helpers/custom-adapter-date';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MMM',
  },
  display: {
    dateInput: 'DD-MMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})


export class ExportComponent implements OnInit {

  dateStart: Date;
  dateEnd: Date;
  loaded: boolean;

  constructor(private exportService: ExportService) { }

  ngOnInit() {
    this.dateStart = new Date();
    this.dateEnd = new Date();
    this.dateStart.setMonth(this.dateStart.getMonth() - 3);
    this.loaded = true;
  }

  export(){
    this.loaded = false;
    return this.exportService.getData(this.dateStart, this.dateEnd)
    .then((returnData)=>{
      this.loaded = true;
      return this.exportService.exportAsExcelFile(returnData, 'panel');
    })
  }

}
