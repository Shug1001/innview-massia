import { Component, OnInit } from '@angular/core';
import Srd  from '../../../assets/srd/srd.json';
import { ForwardPlannerService } from 'src/app/shared/services/forward-planner.service.js';
import { DateUtilService } from 'src/app/shared/services/date-util.service.js';
import { Subscription, interval, Observable } from 'rxjs';
import { PerformanceService } from 'src/app/shared/services/performance.service.js';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.scss']
})
export class PerformanceComponent implements OnInit {

  _subscription: Subscription;
  _subscription2: Subscription;
  today: string;
  statsMapped: object;
  plansMapped: object;
  areasMapped: object;
  loaded: boolean;
  loaded2: boolean;
  now: Date;

  target: object;
  plannedM2: object;
  complete: object;
  completeM2: object;
  started: object;
  dayTarget: object;
  atRisk: object;
  counter: Observable<any>;

  constructor(
    private performanceService: PerformanceService, 
    private forwardPlannerService: ForwardPlannerService, 
    private dateUtilsService: DateUtilService) { 
      this.loaded = false;
      this.loaded2 = false;
      this.now = new Date();
      this.counter = interval(60000);
      this.today = this.dateUtilsService.todayAsIso();
      this.counter.subscribe((n) => {
        this.now = new Date();
      });
    }

  ngOnInit() {
    this.setUpSubscriptions();
  }

  ngOnDestroy(){
    this.unsubscribe();
  }

  setUpSubscriptions(){
    this.loaded = false;
    this.loaded2 = false;
    this.unsubscribe();
    let start = this.dateUtilsService.subtractWorkingDays(this.today, 15);
    let end = this.dateUtilsService.addWorkingDays(this.today, 15);
    this._subscription = this.forwardPlannerService.getPerformance(this.today, start, end).subscribe(([target, stats])=>{
      this.target = target;
      this.statsMapped = stats;
      this.loaded = true;
    });
    this._subscription2 = this.forwardPlannerService.getPlansAndAdd().subscribe((areas)=>{
      this.areasMapped = this.dateUtilsService.areaMapper(areas);
      this.plannedM2 = this.performanceService.getPlannedProductionTotal(this.areasMapped, this.today);
      this.atRisk = this.performanceService.getGreatestRisks(this.areasMapped);
      this.loaded2 = true;
    });
  }

  unsubscribe(){
    if(this._subscription !== undefined){
      this._subscription.unsubscribe();
    }
    if(this._subscription2 !== undefined){
      this._subscription2.unsubscribe();
    }
  }

  currentDateChange(date: string){
    this.today = date;
    this.setUpSubscriptions();
  }

  getLowestRisk(riskData){
    var currentLowest = 0;
    riskData.forEach(risk =>{
        if(risk <= currentLowest){
            currentLowest = risk;
        }
    })
    return currentLowest;
  }

  dayTargetColorIndicator(date, value, entry1, entry2, entry3){
    let colours = [];
    colours.push({"red-indicator": true},{"amber-indicator": true},{"green-indicator": true},{"white-indicator": true});
    let now = new Date(new Date().getTime());
    let endTime = new Date(new Date(date).setHours(15,0,0,0));

    if((now > endTime) && value != undefined){
      if (value >= entry3) {
        return "green-indicator";   
      } else if (value >= entry2) {
        return "amber-indicator";
      } else if (value <= entry1) {
        return "red-indicator";
      }
    }else{
      return "white-indicator";
    }   
  }

  colorIndicator(value, byNow){
    var colours = [];
    colours.push({"red-indicator": true},{"amber-indicator": true},{"green-indicator": true},{"purple-indicator": true},{"white-indicator": true});
    if (value > 0 && byNow != undefined) {
      var percent = (value * 100) / parseInt(byNow);
      if (percent > 1 && percent < 90) {
        return "red-indicator";
      } else if (percent >= 90 && percent < 100) {
        return "amber-indicator";
      } else if (percent >= 100 && percent < 105) {
        return "green-indicator";
      } else if (percent >= 105) {
        return "purple-indicator";
      } 
    }
  }




}
