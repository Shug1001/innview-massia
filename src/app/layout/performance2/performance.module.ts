import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule, MatCardModule,MatFormFieldModule, MatDatepickerModule, MatIconModule, MatInputModule, MatCheckboxModule } from '@angular/material';
import { PerformanceRoutingModule } from './performance-routing.module';
import { PerformanceComponent } from './performance.component';
import { byNowPipe } from './by-now.pipe';
import { CurrentDateComponent2 } from './current-date2/current-date.component2';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ForwardPlanner2Module } from '../forward-planner-component/forward-planner2.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
      CommonModule, 
      PerformanceRoutingModule, 
      MatGridListModule,
      MatCardModule,
      MatDatepickerModule,
      MatMomentDateModule,
      MatFormFieldModule,
      MatInputModule,
      MatIconModule,
      FormsModule,
      MatCheckboxModule,
      ForwardPlanner2Module
  ],
  declarations: [PerformanceComponent, byNowPipe, CurrentDateComponent2]
})
export class PerformanceModule { }
