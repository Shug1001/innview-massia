import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  project: any;
  projectAdditional: any;
  revisions: Array<any>;
  _subscription: Subscription;
  _subscription2: Subscription;
  srdComponents: object;
  loaded: boolean;
  totalRevisions: number;
  latestRevision: object;
  framingStyles:object;
  routeLinks: any[];
  activeLinkIndex = -1;


  constructor(private db: AngularFireDatabase, private route: ActivatedRoute, private router: Router, private revisionsService: RevisionsService) {
    this.loaded = false;
    this.totalRevisions = 0;
    this.routeLinks = [
      {
        label: 'Project Details',
        link: './project-details',
        index: 0
    }, {
        label: 'Delivery Schedule',
        link: './delivery-schedule',
        index: 1
    }, {
        label: 'Project Scope',
        link: './project-scope',
        index: 2
    }, {
        label: 'Framing Styles',
        link: './framing-styles',
        index: 3
    }, {
        label: 'Revisions',
        link: './project-revisions',
        index: 4
    }
    ];
    
  }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');

    this._subscription = this.revisionsService.getProjectRevisions(id).subscribe(([project, revisions]) => {
      
      this.project = project;

      if(project.deliverySchedule !== undefined && project.deliverySchedule.revisions !== undefined){
        this.totalRevisions = Object.keys(this.project.deliverySchedule.revisions).length;
      }

      this.project.$key = id;
      this.revisions = revisions;
      this.latestRevision = revisions[revisions.length-1];
      this.loaded = true;
    });

    this._subscription2 = this.revisionsService.changeRoute$.subscribe(route => {
      this.router.navigate(['project/'+id+'/'+route]);
    });

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._subscription2.unsubscribe();
  }

  routeToOldInnview(tab){
    if(tab.index === 3){
      window.location.href="https://innview-showcase.firebaseapp.com/newux.html#/projects/"+ this.project.id +"/revisions";
    }

  }

}
