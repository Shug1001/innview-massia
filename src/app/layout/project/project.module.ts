import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';
import { MatToolbarModule, MatCardModule, MatTabsModule, MatFormFieldModule, MatSelectModule, MatOptionModule } from '@angular/material';
import { DeliveryScheduleModule } from '../delivery-schedule/delivery-schedule.module';
import { ProjectDetailsModule } from '../project-details/project-details.module';
import { ProjectRevisionsModule } from '../project-revisions/project-revisions.module';
import { ProjectScopeModule } from '../project-scope/project-scope.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { AreaPopupModule } from 'src/app/shared/modules/area-popup/area-popup.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectEstimatesModule } from '../../shared/modules/project-estimates/project-estimates.module';

@NgModule({
  declarations: [ProjectComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatTabsModule,
    DeliveryScheduleModule,
    ProjectScopeModule,
    ProjectRevisionsModule,
    ProjectDetailsModule,
    PipesModule,
    AreaPopupModule,
    ProjectEstimatesModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class ProjectModule { }
