import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AreasService } from '../../../shared/services';
import { MatDialog } from '@angular/material';
import { DialogLineComponent } from './dialog-line/dialog-line.component';
import { AreaExt } from 'src/app/shared/models/models';

@Component({
  selector: 'app-select-line',
  templateUrl: './select-line.component.html',
  styleUrls: ['./select-line.component.scss']
})
export class SelectLineComponent implements OnInit {

  @Input() area: AreaExt;
  @Output() changeLine = new EventEmitter<boolean>();
  @Output() changeLine2 = new EventEmitter<boolean>();
  show: boolean;

  constructor(private areaService: AreasService, public dialog: MatDialog) {}

  ngOnInit(){
    this.show = true;
  }

  openDialogLine(): void {

    this.changeLine.emit(true);

    const dialogRef = this.dialog.open(DialogLineComponent, {
      width: '200px',
      data: {area: this.area}
    });

    dialogRef.componentInstance.data = {area: this.area};

    dialogRef.afterClosed().subscribe(result => {
      this.changeLine2.emit(true);
    });

  }
}
