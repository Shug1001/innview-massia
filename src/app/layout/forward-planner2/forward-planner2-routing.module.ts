import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForwardPlanner2Component } from './forward-planner2.component';

const routes: Routes = [
    {
        path: '',
        component: ForwardPlanner2Component
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ForwardPlanner2RoutingModule {}
