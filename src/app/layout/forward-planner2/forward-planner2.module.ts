import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCardModule, MatIconModule, MatInputModule, MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatButtonToggleModule, MatDialogModule, MatListModule, MatProgressBarModule, MatBadgeModule, MatSnackBarModule, MatRadioModule, MatTooltipModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { ForwardPlanner2RoutingModule } from './forward-planner2-routing.module';
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { ForwardPlanner2Component,resetCustomHeader } from './forward-planner2.component';
import { CapacityInputComponent } from './capacity-input/capacity-input.component';
import { HandoverConfirmedComponent } from './handover-confirmed/handover-confirmed.component';
import { PanelTooltipComponent } from './panel-tooltip/panel-tooltip.component';
import { DialogComponent } from './panel-tooltip/dialog/dialog.component';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { StartDatePickerComponent, ExampleHeader } from './start-date-picker/start-date-picker.component';
import { SelectLineComponent } from './select-line/select-line.component';
import { DialogLineComponent } from './select-line/dialog-line/dialog-line.component';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';
import { NotesPopupComponent } from './notes-popup/notes-popup.component';
import { JobsheetsModule } from 'src/app/shared/modules/jobsheets/jobsheets.module';
 

@NgModule({
    imports: [
        CommonModule,
        PipesModule,
        MatGridListModule,
        MatDialogModule,
        MatCardModule,
        MatBadgeModule,
        MatProgressBarModule,
        MatListModule,   
        MatCheckboxModule,
        MatButtonModule,
        MatIconModule,
        FormsModule,
        MatInputModule,
        MatTooltipModule,
        MatRadioModule,
        MatSnackBarModule,
        JobsheetsModule,
        MatButtonToggleModule,
        MatNativeDateModule, 
        MatDatepickerModule,
        AngularFireFunctionsModule,
        DataExportModule,
        ForwardPlanner2RoutingModule,
    ],
    declarations: [ForwardPlanner2Component,  NotesPopupComponent, resetCustomHeader, CapacityInputComponent, HandoverConfirmedComponent, PanelTooltipComponent, DialogLineComponent, DialogComponent, StartDatePickerComponent, SelectLineComponent, ExampleHeader],
    entryComponents: [DialogLineComponent, DialogComponent,resetCustomHeader, ExampleHeader]

})
export class ForwardPlanner2Module {}
