import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Subject, Subscription, SubscriptionLike } from 'rxjs';
import { AreasService, ForwardPlannerService } from 'src/app/shared/services';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import Srd  from '../../../assets/srd/srd.json';
import { Blotter, Plan, ExtendedArea } from 'src/app/shared/models/models';
import { DateAdapter, MatCalendar, MatDateFormats, MatDatepickerInputEvent, MatSnackBar, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';
import { first, takeUntil } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AngularFireDatabase } from '@angular/fire/database';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-forward-planner3',
  templateUrl: './forward-planner3.component.html',
  styleUrls: ['./forward-planner3.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }, 
    {provide: CustomDateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ],
})
export class ForwardPlanner3Component implements OnInit {

  _subscription: Subscription;
  areas: Array<any>;
  areasRaw: Array<any>;
  capacities: Array<any>;
  loaded: boolean;
  days: Array<any>;
  dayHeadings: Array<any>;
  lineCapacities: object;
  viewStart: string;
  toggleType: object;
  toggleCount: object;
  defaultProductionLine: string;
  productTypes: object;
  productTypeProductionLine: object;
  lineAvailablities: object;
  productionLines: object;
  defaultLineCapacity: object;
  blotter: Blotter;
  skipWeeks: number;
  areasSelection: object;
  plan: Plan;
  productionLineFilter: object;
  productionLineConverter: object;
  areaPlans: Array<any>;
  mappedPanelsComplete: object;
  areaPlansMapped: object;
  m2Type: string;
  exportAreas: Array<any>;
  disableExport:boolean;

  constructor(private dateUtilService: DateUtilService, private forwardPlannerService: ForwardPlannerService, private db: AngularFireDatabase, private areasService: AreasService, private _snackBar: MatSnackBar) { 
    this.days = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
    this.dayHeadings = this.getDayHeadings();
    this.lineCapacities = {};
    this.lineAvailablities = {};
    this.productionLines = Srd.productionSubLines;
    this.productTypes = Srd.productTypes;
    this.areas = [];
    this.disableExport = true;
    this.defaultProductionLine = "SIP";
    this.m2Type = "count";
    this.productionLineFilter = { SIP: true, SIP2: true, HSIP: true, IFAST: true, IFAST2: true, TF: true, TF2: true, CASS: true };
    this.productionLineConverter = { SIP: 'Auto Line 1', SIP2: 'Auto Line 2', HSIP: 'Auto Line 3', IFAST: 'Man, Line 1', IFAST2: 'Man. Line 2', TF: 'Man. Line 3', TF2: 'Man. Line 4', CASS: 'SIP Line' };
    this.toggleType = { actual: true, design: true, est: true };
    this.toggleCount = { action: true };
    this.areasSelection ={};
    this.defaultLineCapacity = this.dateUtilService.buildMap(this.productionLines, "defaultCapacity");
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
  }

  ngOnInit() {
    var today = new Date(); 
    this.getForwardPlan(today);
  }

  addEvent(type: string, event: MatDatepickerInputEvent<any>) {
    const date = this.dateUtilService.toIsoDate(event.value._d);
    this.getForwardPlan(date);
  }

  getForwardPlan(startDate) {

    this.loaded = false;

    var today = new Date();
    var start = new Date(startDate || today);
    if (start < today) {
      start = today;
    }

    // Plan displays from Monday of selected start date
    this.viewStart = this.dateUtilService.weekStart(start);
    this.skipWeeks = Math.ceil(this.dateUtilService.daysBetween(today, this.viewStart) / 7);

    this.plan = {
      viewStart: this.viewStart,
      current: this.skipWeeks == 0 ? this.dateUtilService.dayOfWeek(today) : -1,
      capacity: {},
      dailyM2Totals: {},
      lineM2Totals: {},
      areas: []
    };

    combineLatest(this.forwardPlannerService.getAreaProductionPlans(), this.forwardPlannerService.getActiveAreasPlanner(), this.forwardPlannerService.getCapacitiesFromToday()).subscribe(([plans, areas, capacities]) => {

      this.areaPlans = plans;
      this.areasRaw = areas;
      this.capacities = capacities;
      this.lineCapacities = this.dateUtilService.mapper(this.capacities);
      this.lineAvailablities = this.dateUtilService.mapper(this.capacities);
      this.areaPlansMapped = this.dateUtilService.mapper(this.areaPlans);

      this.buildExportData();
      this.initiatePlan();
      this.loaded = true;
      
    });
  }
  
  toggleLine(line){
    this.defaultProductionLine = line;
    this.initiatePlan();
  }

  removePriority(area){
    let updateObj = {
      sortKey: null
    }
    return this.areasService.updateArea(area.$key, updateObj);
  }

  initiatePlan(){

    this.areas = [];

    this.areasRaw.forEach((area) => {

      if(area._productionLine === this.defaultProductionLine){
        this.areas.push(area);
      }

    });

    this.blotter = this.createBlotter(this.plan, this.skipWeeks, 3);

    this.areas.sort((areaA, areaB) => {
      var a = this.areasSelection[areaA.$key] ? areaA._unpublishedDeliveryDate : areaA._deliveryDate;
      var b = this.areasSelection[areaB.$key] ? areaB._unpublishedDeliveryDate : areaB._deliveryDate;
      var r = a.localeCompare(b);
      return r;
      }
    );

    this.areas.forEach((area) => {
      if(area.sortKey !== undefined){
        area._sortKey = area.sortKey;
      }else{
        area._sortKey = 99;
      }
    });

    this.areas.sort((areaA, areaB) => {
      return (areaA._sortKey !== undefined && areaB._sortKey !== undefined) ? areaA._sortKey - areaB._sortKey : 0;
    });

    this.areas.map((area) => {
      if (this.areasSelection[area.$key] === undefined){
        this.areasSelection[area.$key] = false;
      }
      this.calculateAreaPlan(area, this.areasSelection[area.$key]);
    });

    let blotterAreas = this.getAreasFromBlotter(this.skipWeeks);
    this.plan.capacity = this.getCapacityFromBlotter(this.skipWeeks);
    var calculateM2 = this.calculateM2( blotterAreas );
    this.plan.lineM2Totals = calculateM2.line;
    this.plan.dailyM2Totals = calculateM2.total;

    this.plan.areas = blotterAreas;

  }

  savePlanner(){
    this._snackBar.open('Saving Planner...', 'Hide', {
      panelClass: ['innviewSnack']
    });
    this.forwardPlannerService.buildAreasPlan(this.plan).then((_)=>{
      this._snackBar.open('Planner Saved', 'Hide', {
        duration: 1500,
        panelClass: ['innviewSnack']
      });
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    const temp = this.areas[event.currentIndex];
    this.areas[event.currentIndex] = this.areas[event.previousIndex];
    this.areas[event.previousIndex] = temp;

    this.blotter = this.createBlotter(this.plan, this.skipWeeks, 3);

    this.areas[event.currentIndex]._sortKey = Number(event.currentIndex);
    this.areas[event.previousIndex]._sortKey = Number(event.previousIndex);

    let updates = {};

    updates['/areas/' + this.areas[event.currentIndex].$key + '/sortKey'] = Number(event.currentIndex);
    updates['/areas/' + this.areas[event.previousIndex].$key + '/sortKey'] = Number(event.previousIndex);

    this.areas.sort((areaA, areaB) => {
      var a = this.areasSelection[areaA.$key] ? areaA._unpublishedDeliveryDate : areaA._deliveryDate;
      var b = this.areasSelection[areaB.$key] ? areaB._unpublishedDeliveryDate : areaB._deliveryDate;
      var r = a.localeCompare(b);
      if (r === 0) {
        r = (areaA._sortKey !== undefined && areaB._sortKey !== undefined) ? areaA._sortKey - areaB._sortKey : 0;
      }
      return r;
      }
    );

    this.areas.sort((areaA, areaB) => {
      return (areaA._sortKey !== undefined && areaB._sortKey !== undefined) ? areaA._sortKey - areaB._sortKey : 0;
    });

    this.areas.map((area) => {
      if (this.areasSelection[area.$key] === undefined){
        this.areasSelection[area.$key] = false;
      }
      this.calculateAreaPlan(area, this.areasSelection[area.$key]);
    });

    let blotterAreas = this.getAreasFromBlotter(this.skipWeeks);
    this.plan.capacity = this.getCapacityFromBlotter(this.skipWeeks);
    var calculateM2 = this.calculateM2( blotterAreas );
    this.plan.lineM2Totals = calculateM2.line;
    this.plan.dailyM2Totals = calculateM2.total;

    this.plan.areas = blotterAreas;

    
    return this.db.database.ref().update(updates);

  }

  getAreasFromBlotter(skipWeeks) {
    Object.keys(this.blotter.areas).forEach((areaKey) => {
      let area = this.blotter.areas[areaKey];
      area._productionPlan.splice(0, 7 * skipWeeks);
    });

    return this.blotter.areas;
  }

  calculateM2( areas ) {

    var calculatedM2 = {
      "line": {},
      "total": {}
    };

    areas.map((area) => {
      area._productionPlan.map((data, buildDays) => {
        if(this.plan.capacity["m2"][area._productionLine] === undefined){
          this.plan.capacity["m2"][area._productionLine] = {};
        }

        if(this.plan.capacity["m2"][area._productionLine][buildDays] === undefined){
          this.plan.capacity["m2"][area._productionLine][buildDays] = 0;
        }

        if(this.plan.capacity["total"][buildDays] === undefined){
          this.plan.capacity["total"][buildDays] = 0;
        }

        this.plan.capacity["m2"][area._productionLine][buildDays] += data.m2;
        this.plan.capacity["total"][buildDays] += data.m2;
      });
    });

    return calculatedM2;
  }

  calculateAreaPlan(area, useUnpublishedDate){

    let today = new Date();

    delete area._productionStartDate;
    let productionLine = area._productionLine;
    let lineBlotter = this.blotter.productionLines[productionLine];
    if (!lineBlotter) {
      // Just drop area
      return false;
    }

    if (lineBlotter.nextStart < 0) {
      // No available capacity
      return false;
    }

    let productionPlan = [];

    for (let i = 0; i < this.blotter.planDays; i++) {
      if(productionPlan[i] === undefined){
        productionPlan[i] = {
          count: null,
          m2: null
        };
      }
    }

    let designHandoverDate = useUnpublishedDate ? area._unpublishedDesignHandoverDate : area._designHandoverDate;

    let buildDay = lineBlotter.nextStart;
    let buildDate = this.dateUtilService.plusDays(this.blotter.planStart, buildDay);  
        
    // Skip forward to design handover date
    while (buildDate < designHandoverDate) {
      buildDate = this.dateUtilService.plusDays(this.blotter.planStart, ++buildDay);
    }

    if (buildDay > this.blotter.planDays) {
      return false;
    }

    area._productionPlan = productionPlan;

    if (area._hasUnpublishedOnly && !useUnpublishedDate) {
      // Don't consume capacity, but potentially include in area list
      return buildDate >= this.blotter.viewStart;
    }

    if(productionPlan[buildDay] === undefined){
      productionPlan[buildDay] = {
        count: null,
        m2: null
      };
    }

    let panels = (area.actualPanels || area.numberOfPanels || area.estimatedPanels || 0) - (area.completedPanels || 0);

    if (panels == 0) {
      productionPlan[buildDay].count = 0;
      productionPlan[buildDay].m2 = 0;
      area._productionStartDate = buildDate;
      area._productionEndDate = null;
    }
    while (panels > 0) {
      // Extend planning capacity
      if (lineBlotter.available.length <= buildDay) {
        let capacity = this.getCapacity(productionLine, buildDate);
        lineBlotter.available[buildDay] = capacity;
      }

      let available = lineBlotter.available[buildDay];
      if (available) {
        if (!area._productionStartDate) {
          area._productionStartDate = buildDate;
        }

        let buildCount = Math.min(panels, available);

        panels -= buildCount;
        available -= buildCount;

        lineBlotter.available[buildDay] = available;
          if (buildDay < productionPlan.length) {

            let m2 = this.forwardPlannerService.avgM2(area, buildCount);

            productionPlan[buildDay].m2 = m2;
            productionPlan[buildDay].count = buildCount;
  
          }

        if (panels == 0) {
          area._productionEndDate = buildDate;
        }
      }

      if (!available) {
        while (buildDay < lineBlotter.available.length && !lineBlotter.available[buildDay]) {
          buildDay++;
        }
        buildDate = this.dateUtilService.plusDays(this.blotter.planStart, buildDay);
      }
    }

    lineBlotter.nextStart = buildDay < this.blotter.planDays ? buildDay : -1;

    let toDo = false;

    if (area._productionEndDate) {
      toDo = area._productionEndDate >= this.blotter.viewStart;
    }
    else {
      toDo = area._productionStartDate >= this.blotter.viewStart;
    }
    if (area._deliveryDate !== undefined) {
      if (toDo) {
        //if exists in array overright if not add
        const index = this.blotter.areas.indexOf(area);
        if (index === -1) {
          this.blotter.areas.push(area);
        } else {
          this.blotter.areas[index] = area;
        }
      }
    }
  }

  getDayHeadings() {
    var week = ["M", "T", "W", "T", "F", "S", "S" ];
    return week.concat(week, week);
  }

  startChange(save: boolean){

  }

  areaUserChange(area){
    let userChange = false;
    if(this.areaPlansMapped[area.$key] !== undefined && this.areaPlansMapped[area.$key].userChange !== undefined){
      userChange = this.areaPlansMapped[area.$key].userChange;
    }
    return userChange;
  }

  createBlotter(plan, skipWeeks, planWeeks){
    var today = new Date();
    var planStart = this.dateUtilService.weekStart(today);
    var planDays = 7 * (skipWeeks + planWeeks);

    this.blotter = {
      planStart: planStart,
      planDays: planDays,
      viewStart: plan.viewStart,
      current: plan.current,
      productionLines: {},
      areas: []
    };

    var nextStart = this.dateUtilService.dayOfWeek(today);
    Object.keys(this.defaultLineCapacity).forEach((productionLine) => {
      var lineBlotter = {
        capacity: [],
        m2:[],
        available: [],
        nextStart: nextStart
      };

      for (var i = 0; i < planDays; i++) {
        var buildDate = this.dateUtilService.plusDays(planStart, i);
        var capacity = this.getCapacity(productionLine, buildDate);
        let available = this.getAvailable(productionLine, capacity, buildDate, today);
        
        lineBlotter.m2.push(0);
        lineBlotter.capacity.push(capacity);
        lineBlotter.available.push(available);
      }

      this.blotter.productionLines[productionLine] = lineBlotter;
    });

    return this.blotter;
  }

  getCapacityFromBlotter(skipWeeks){
    var capacity = {
      count: {},
      m2: {},
      total: {}
    };
    Object.keys(this.blotter.productionLines).forEach(productionLine => {
      let lineBlotter = this.blotter.productionLines[productionLine];
      lineBlotter.capacity.splice(0, 7 * skipWeeks);
      capacity["count"][productionLine] = lineBlotter.capacity;
      capacity["m2"][productionLine] = lineBlotter.m2;
    });
    return capacity;
  }

  getCapacity(productionLine, date) {

    if(this.lineCapacities[productionLine] === undefined){
      return null;
    }
    
    var capacity = this.lineCapacities[productionLine][date];
    if (capacity !== undefined) {
      return capacity;
    }

    if (this.dateUtilService.isWeekDay(date)) {
      return this.defaultLineCapacity[productionLine];
    }
    else {
      return null;
    }
  }

  getAvailable(productionLine, capacity, buildDate, today) {
      
    var available;
    // var sum = 0;
    // var panelsCompleteOnDay;
      
    // panelsCompleteOnDay  = this.mappedPanelsComplete[productionLine];
    
    // if(panelsCompleteOnDay === undefined || isNaN(panelsCompleteOnDay)){
    //   panelsCompleteOnDay = 0;
    // }

    // if (panelsCompleteOnDay > capacity) {
    //   sum = capacity;
    // } else {
    //   sum = panelsCompleteOnDay;
    // }

    if (this.dateUtilService.isWeekDay(today) && buildDate === this.dateUtilService.toIsoDate(today)) {
      available = capacity;
    } else {
      available = capacity;
    }

    return available;
  }

  isSameAsSave(area){
    if(this.areaPlansMapped[area.$key] && this.areaPlansMapped[area.$key].plannedStart === area._productionStartDate){
      return false;
    } else {
      return true;
    }
  }
  

  buildExportData(){

    let exportAreas = [];
    let promises = [];
    let jobsheets = {};
    Object.keys(this.plan.areas).forEach((key) => {
      let area = this.plan.areas[key];
      promises.push(this.forwardPlannerService.getJobsheetsByAreaPromise(area.$key).then((res)=>{
        if(res.length){
          res.forEach((jobsheet) => {
            if (jobsheets[area.$key] === undefined){
              jobsheets[area.$key] = [];
            }
            jobsheets[area.$key].push(jobsheet.projectNumber);
          });
        }else{
          if (jobsheets[area.$key] === undefined){
            jobsheets[area.$key] = [];
          }
          jobsheets[area.$key].push("None");
        }
      }));
    });

    return Promise.all(promises)   
    .then(()=>{
      Object.keys(this.plan.areas).forEach((area) => {
        if(jobsheets[this.plan.areas[area].$key] && jobsheets[this.plan.areas[area].$key].length){
        jobsheets[this.plan.areas[area].$key].forEach((jobsheet) => {
          let count = this.plan.areas[area]._stats.count - this.plan.areas[area]._stats.completed;
          let m2: any = this.forwardPlannerService.existingM2(this.plan.areas[area]);
          let exportArea = {
            "Area" : this.plan.areas[area]._id,
            "Line" : this.plan.areas[area]._line,
            "Panel Qty": count,
            "Panel M2 (Existing)": m2,
            "Complete": this.plan.areas[area]._stats.completed, 
            "DFM": this.getDate(this.plan.areas[area]),
            "Risk": this.getRiskDays(this.plan.areas[area]),
            "Start Date": this.dateUtilService.toUserDate(this.plan.areas[area]._productionStartDate),
            "End Date": this.plan.areas[area]._productionEndDate === null ? "" : this.dateUtilService.toUserDate(this.plan.areas[area]._productionEndDate),
            "Delivery Date": this.dateUtilService.toUserDate(this.plan.areas[area]._deliveryDate),
            "Jobsheet": jobsheet
          }
          exportAreas.push(exportArea);
        });
        } else {
          let count = this.plan.areas[area]._stats.count - this.plan.areas[area]._stats.completed;
          let m2: any = this.forwardPlannerService.existingM2(this.plan.areas[area]);
          let exportArea = {
            "Area" : this.plan.areas[area]._id,
            "Line" : this.plan.areas[area]._line,
            "Panel Qty": count,
            "Panel M2 (Existing)": m2,
            "Complete": this.plan.areas[area]._stats.completed, 
            "DFM": this.getDate(this.plan.areas[area]),
            "Risk": this.getRiskDays(this.plan.areas[area]),
            "Start Date": this.dateUtilService.toUserDate(this.plan.areas[area]._productionStartDate),
            "End Date": this.plan.areas[area]._productionEndDate === null ? "" : this.dateUtilService.toUserDate(this.plan.areas[area]._productionEndDate),
            "Delivery Date": this.dateUtilService.toUserDate(this.plan.areas[area]._deliveryDate),
            "Jobsheet": ""
          }
          exportAreas.push(exportArea);
        }
      });
  
      this.exportAreas = exportAreas;

      this.disableExport = false;
    });

  
  }

  isMeterSquaredChecked(action){
    this.toggleCount = action;
    let type = "count";
    if(!this.toggleCount){
      type = "m2";
    }
    this.m2Type = type;
  }

  getBackground(area){
    let dt = this.getDateNormal(area);
    let dfm = new Date(dt);
    let productionStartDate = new Date(area._productionStartDate);
    let dec = dfm >= productionStartDate ? true : false;
    return dec;
  }

  getDateNormal(area){
    return area.additionalData !== undefined && area.additionalData.DFMDate !== undefined ? area.additionalData.DFMDate : area._designHandoverDate;
  }

  getDate(area){
    let date = area.additionalData !== undefined && area.additionalData.DFMDate !== undefined ? area.additionalData.DFMDate : area._designHandoverDate;
    return this.dateUtilService.toPlannerDate(date);
  }

  useUnpublishedOnly(area) {
    return area._hasUnpublishedOnly && this.useUnpublished(area);
  }

  useUnpublished(area) {
    return area._hasUnpublished && this.areasSelection[area.$key];
  }

  atRisk(area) {
    var riskDays = this.riskDays(area);
    return riskDays === undefined || riskDays < 0;
  }

  riskDays(area){
    if (this.useUnpublishedOnly(area)) {
      return null;
    }

    if (!area._productionEndDate || !area._riskDate) {
      return undefined;
    }

    let riskDate = this.useUnpublished(area) ? area._unpublishedRiskDate : area._riskDate;
    return this.dateUtilService.daysBetweenWeekDaysOnly(area._productionEndDate, riskDate);
  }

  getRiskDays(area) {
    let days = this.riskDays(area);
    if (days === undefined) {
      return "!";
    }
    if (days == null) {
      return "";
    }
    return days > 0 ? "+" + days : days.toString();
  }

  weekStart(n) {
    return this.dateUtilService.plusDays(this.plan.viewStart, 7 * n);
  }

  weekFinish(n) {
    return this.dateUtilService.plusDays(this.plan.viewStart, 7 * n + 6);
  }

}



@Component({
  selector: 'date-header',
  styles: [`
  .example-header {
    display: flex;
    align-items: center;
    padding: 0.5em;
  }

  .example-header-label {
    flex: 1;
    height: 1em;
    font-weight: 500;
    text-align: center;
  }

  .example-double-arrow .mat-icon {
    margin: -22%;
  }
`],
  template: `
    <div>
    <div style="text-align:center;">
      <button mat-icon-button  (click)="setToday()">Reset View</button>
    </div>
    <div style="text-align:center;">
      <button mat-icon-button class="example-double-arrow" (click)="previousClicked('year')">
      <mat-icon>keyboard_arrow_left</mat-icon>
      <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <button mat-icon-button (click)="previousClicked('month')">
        <mat-icon>keyboard_arrow_left</mat-icon>
      </button>
      <span class="example-header-label">{{periodLabel}}</span>
      <button mat-icon-button (click)="nextClicked('month')">
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
      <button mat-icon-button class="example-double-arrow" (click)="nextClicked('year')">
        <mat-icon>keyboard_arrow_right</mat-icon>
        <mat-icon>keyboard_arrow_right</mat-icon>
      </button>
    </div>
  </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class resetCustomHeader<D> implements OnDestroy {
  private _destroyed = new Subject<void>();

  constructor(private forwardPlannerService:ForwardPlannerService,private _calendar: MatCalendar<any>,private customDateAdapter:CustomDateAdapter,@Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats, cdr: ChangeDetectorRef) {
    _calendar.stateChanges
      .pipe(takeUntil(this._destroyed))
      .subscribe(() => cdr.markForCheck());
  }
  get periodLabel() {
    return this.customDateAdapter
      .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }
  setToday(){
    let today = new Date();
    this.forwardPlannerService.setDate(today);
  }
  previousClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month' ? this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, -1) : this.customDateAdapter.addCalendarYears(this._calendar.activeDate, -1);
  }
  
  nextClicked(mode: 'month' | 'year') {
    this._calendar.activeDate = mode === 'month' ?this.customDateAdapter.addCalendarMonths(this._calendar.activeDate, 1) : this.customDateAdapter.addCalendarYears(this._calendar.activeDate, 1);
  }  
  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }
}
