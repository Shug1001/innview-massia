import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForwardPlanner3Component } from './forward-planner3.component';

describe('ForwardPlanner3Component', () => {
  let component: ForwardPlanner3Component;
  let fixture: ComponentFixture<ForwardPlanner3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForwardPlanner3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForwardPlanner3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
