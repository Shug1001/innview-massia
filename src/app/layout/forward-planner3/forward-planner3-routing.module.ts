import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForwardPlanner3Component } from './forward-planner3.component';

const routes: Routes = [
  {
    path: '',
    component: ForwardPlanner3Component
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForwardPlanner3RoutingModule { }
