import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForwardPlanner3RoutingModule } from './forward-planner3-routing.module';
import { MatBadgeModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatProgressBarModule, MatRadioModule, MatSnackBarModule, MatTooltipModule } from '@angular/material';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { ExampleHeader, StartDatePickerComponent } from './start-date-picker/start-date-picker.component';
import { PanelTooltipComponent } from './panel-tooltip/panel-tooltip.component';
import { DialogComponent } from './panel-tooltip/dialog/dialog.component';
import { ForwardPlanner3Component } from '../forward-planner3/forward-planner3.component';
import { resetCustomHeader } from '../forward-planner3/forward-planner3.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CapacityInputComponent } from './capacity-input/capacity-input.component';
import { DialogLineComponent } from './select-line/dialog-line/dialog-line.component';
import { HandoverConfirmedComponent } from './handover-confirmed/handover-confirmed.component';
import { SelectLineComponent } from './select-line/select-line.component';
import { NotesPopupComponent } from './notes-popup/notes-popup.component';
import { JobsheetsModule } from 'src/app/shared/modules/jobsheets/jobsheets.module';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';


@NgModule({
  declarations: [ForwardPlanner3Component,  NotesPopupComponent, resetCustomHeader, CapacityInputComponent, HandoverConfirmedComponent, PanelTooltipComponent, DialogLineComponent, DialogComponent, StartDatePickerComponent, SelectLineComponent, ExampleHeader],
  entryComponents: [DialogLineComponent, DialogComponent,resetCustomHeader, ExampleHeader],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    PipesModule,
    DragDropModule,
    MatGridListModule,
    MatDialogModule,
    MatCardModule,
    MatBadgeModule,
    MatProgressBarModule,
    MatListModule,   
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatInputModule,
    MatTooltipModule,
    MatRadioModule,
    MatSnackBarModule,
    MatButtonToggleModule,
    MatNativeDateModule, 
    JobsheetsModule,
    MatDatepickerModule,
    DataExportModule,
    ForwardPlanner3RoutingModule
  ]
})
export class ForwardPlanner3Module { }
