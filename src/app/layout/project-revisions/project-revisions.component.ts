import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { AreasService, ProjectsService } from '../../shared/services'
import Srd  from '../../../assets/srd/srd.json'
import {animate, state, style, transition, trigger} from '@angular/animations';
import { combineLatest, Subscription } from 'rxjs';
import { ComponentsHolder, Project } from '../../shared/models/models';
import { DateUtilService } from '../../shared/services/date-util.service';
import { LoadPanelsService } from '../../shared/services/load-panels.service'
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';


@Component({
  selector: 'app-project-revisions',
  templateUrl: './project-revisions.component.html',
  styleUrls: ['./project-revisions.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*', display: 'block' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ProjectRevisionsComponent implements OnInit {
  

  @Input() valueChange;

  componentsMap: object;
  srdComponents: object;
  srdFloors: object;
  componentsHolder: object;
  loaded: boolean;
  togglePublishButton: boolean;
  accending: boolean;
  sortType: string;
  spans: object;
  spans2: object;
  toggleData: object;
  expandedComponent: object;
  filterValue: string;
  _subscriptionProjAndCom: Subscription;
  _subscriptionPro: Subscription;
  project: Project;
  projectKey: string;
  dataSource: MatTableDataSource<ComponentsHolder>;
  panelOpenState = false;
  areasGrouped: ComponentsHolder[];
  columnsToDisplay = ['sorter', 'expand', 'phase', 'componentWeek', 'floor', 'componentName', 'supplier', 'offload',	'latestRevision', 'deliveryDate', 'notes', 'delivered', 'active'];
  columnsToFilter = ['phase', 'componentWeek', 'floor', 'componentName', 'supplier'];
  sorterData: object;
  floorsMapped: object;
  id: string;
  designHandoverPeriod = Srd.defaultSettings.designHandoverPeriod;
  revisions: Array<any>;
  revisionsCounter: number;

  constructor(
    private loadPanelsService: LoadPanelsService,
    private route: ActivatedRoute, 
    private projectsService: ProjectsService,
    private dateUtilService: DateUtilService,
    private revisionsService: RevisionsService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private areasService: AreasService) {

    this.componentsMap = {};
    this.componentsHolder = {};
    this.srdComponents = Srd.components;
    this.srdFloors = Srd.floors;
    this.areasGrouped = [];
    this.spans = {};
    this.spans2 = {};
    this.togglePublishButton = false;
    this.filterValue = "";
    this.sortType = "deliveryDate";
    this.expandedComponent = {};
    this.loaded = false;
    this.floorsMapped = this.dateUtilService.buildMap(this.srdFloors, "order");
    this.dataSource = new MatTableDataSource(this.areasGrouped);

    this.dataSource.filterPredicate = (data: ComponentsHolder, filter: string) => {
      let found = false;
      if(filter === "toggle"){
        found = this.toggledLogic(data);
      }else{
        this.columnsToFilter.forEach((item) => {
          if(data[item] !== undefined && String(data[item]).toLowerCase().includes(filter)){
            found = true;
          }
        });
      }
      return found;
    };

    this.toggleData = {'delivered':false,'active':false};

    this.sorterData = {
      'phase':{
        'name':'Phase',
        'accending': true
      },
      'componentWeek':{
        'name':'Week Commencing',
        'accending': true
      },
      'floor':{
        'name':'Floor',
        'accending': true
      },
      'componentName':{
        'name':'Component',
        'accending': true
      },
      'supplier':{
        'name':'Supplier',
        'accending': true
      },
      'offload':{
        'name':'Offload Method',
        'accending': true
      },
      'deliveryDate':{
        'name':'Delivery Date',
        'accending': true
      },
      'latestRevision':{
        'name':'Latest Revision',
        'accending': true
      }
    };

  }

  ngOnDestroy(){
    this._subscriptionProjAndCom.unsubscribe();
    this._subscriptionPro.unsubscribe();
  }

  ngOnInit() {

    this.id = this.route.parent.parent.snapshot.paramMap.get('id');

    this._subscriptionPro = combineLatest(this.projectsService.getproject(this.id), this.revisionsService.getRevisionsForProject(this.id)).subscribe(([project, revisions]) => {
      this.revisions = revisions;
      this.revisionsCounter = revisions.length-1;
      this.project = project;
      this.project.$key = this.id;
    });

    this.areasSubscribe();
  }

  areasSubscribe(){
    this._subscriptionProjAndCom = this.areasService.getProjectAreasGroupedByComponent(this.id).subscribe((groupedComponents) => {
      
      this._snackBar.dismiss();
      this._snackBar.open('Update in progress!', 'Hide', {
        duration: 1500,
        panelClass: ['innviewSnack']
      });

      this.areasGrouped = [];
      this.spans = {};

      this.togglePublishButton = false;

      Object.keys(groupedComponents).forEach((componentKey, key) => {
        const component = groupedComponents[componentKey];
        this.showHidePromoteRevision(component);
        this.expandedComponent[componentKey] = false;
        this.spanRow(component, key); 
        this.areasGrouped.push(component);
      });

      this.dataSource.data = this.areasGrouped;

      this.whichWay();
 
      this.loaded = true;
      
    });
  }

  cycleLeft(){
    if(this.revisions.length > 3 && this.revisionsCounter >= 3){
      this.revisionsCounter--;
    }
  }

  cycleRight(){
    if(this.revisions.length > 3 && this.revisionsCounter <= this.revisions.length -2){
      this.revisionsCounter++;
    }  
  }

  getInitials(number){
    const nameString = this.revisions[this.revisionsCounter - number] ? this.revisions[this.revisionsCounter - number].deliveryManager : "";
    if(nameString === ""){
      return "";
    }
    const fullName = nameString.split(' ');
    const initials = fullName.shift().charAt(0) + fullName.pop().charAt(0);
    return "Issued By: " + initials.toUpperCase();
  }

  getRevisionDate(number){
    return this.revisions[this.revisionsCounter - number] ? this.revisions[this.revisionsCounter - number].createdDate : '';
  }

  getAreaRevision(number, area){
    return this.revisions[this.revisionsCounter - number] && area.revisions[this.revisions[this.revisionsCounter - number].$key] ? area.revisions[this.revisions[this.revisionsCounter - number].$key] : "";
  }

  getRevision(number, areas){
    return this.revisions[this.revisionsCounter - number] && areas[0].revisions[this.revisions[this.revisionsCounter - number].$key] ? areas[0].revisions[this.revisions[this.revisionsCounter - number].$key] : "";
  }

  triggerParentSubscription(trigger){
    if(trigger){
      this.areasSubscribe();
    }else{
      if(!this.sorterData[this.sortType].accending){
        this.changeSort(this.sortType);
      }
      this.togglePublishButton = false;
      this._snackBar.open('Processing a bulk shift!', 'Hide', {
        panelClass: ['innviewSnack']
      });
      this.areasUnsubscribe();
    }
  }

  areasUnsubscribe(){
    this._subscriptionProjAndCom.unsubscribe();
  }

  uploadFile(data){
    this.loadPanelsService.loadAreas(this.project, data)
    .then((result) => {
      const dialogRef = this.dialog.open(ConfirmationPopupDialogComponent, {
        width: '800px',
        data: {areasGrouped: this.areasGrouped, project: this.project, result: result}
      });
      dialogRef.componentInstance.data = {areasGrouped: this.areasGrouped, project: this.project, result: result};
    });
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.data = this.areasGrouped;
    filterValue = filterValue.trim().toLowerCase(); // Remove whitespace
    this.filterValue = filterValue;
    this.dataSource.filter = filterValue;
    this.whichWay();
  }

  notFiltered(){
    if(this.dataSource.filteredData.length === this.dataSource.data.length){
      return true;
    }else{
      return false;
    }
  }

  compareProperty(a, b) {
    return (a || b) ? (!a ? 1 : !b ? -1 : a.localeCompare(b)) : 0;
  }
  
  sorter(type){
    let holder = this.dataSource[type];
    if(this.sortType === "floor"){
      holder.sort((areaA, areaB) => {
        let a = this.floorsMapped[areaA.floor];
        let b = this.floorsMapped[areaB.floor];
        let r = a-b;
        if (r === 0) {
          a = this.dateUtilService.toIsoDate(areaA.deliveryDate);
          b = this.dateUtilService.toIsoDate(areaB.deliveryDate);
          r = a.localeCompare(b);
          return r;
        }
        return r;
      });
    }else{
      holder.sort((areaA, areaB) => {
        let a = areaA[this.sortType];
        let b = areaB[this.sortType];
        if(this.sortType === "deliveryDate" || this.sortType === "latestRevision"){
          a = this.dateUtilService.toIsoDate(areaA[this.sortType]);
          b = this.dateUtilService.toIsoDate(areaA[this.sortType]);
        }
        let r = this.compareProperty(a, b);
        if (r === 0) {
          a = this.dateUtilService.toIsoDate(areaA.deliveryDate);
          b = this.dateUtilService.toIsoDate(areaB.deliveryDate);
          r = a.localeCompare(b);
          return r;
        }
        return r;
      });
    }
    this.spans = {};
    holder.forEach((component, key) => {
      this.expandedComponent[component.componentName] = false;
      this.spanRow(component, key);  
    });
    this.dataSource.data = holder;
  }

  reverser(type){
    let holder = this.dataSource[type];
    holder.reverse();
    this.spans = {};
    holder.forEach((component, key) => {
      this.expandedComponent[component.componentName] = false;
      this.spanRow(component, key);  
    });
    this.dataSource.data = holder;
  }

  expandClick(row, expandedElement){
    console.log(JSON.stringify(row));
    // add remove row span for old tables
    this.expandedComponent[row.componentName] = !this.expandedComponent[row.componentName];
    if(this.expandedComponent[row.componentName]){
      this.spans[this.sortType][row[this.sortType]].count += row.areas.length;
    }else{
      this.spans[this.sortType][row[this.sortType]].count -= row.areas.length;
    }
  }

  spanRow(component, index) {
    this.columnsToDisplay.forEach((item) => {
      if(this.spans[item] === undefined){
        this.spans[item] = {};
      }
      if(this.spans[item][component[item]] === undefined){
        this.spans[item][component[item]] = {
          count:0,
          onRow:index
        };
      }
      //mat table increment twice
      this.spans[item][component[item]].count++;
      //this.spans[item][component[item]].count++;
    });
  }

  whichWay(){
    if(this.sorterData[this.sortType].accending){
      if(this.notFiltered()){
        this.sorter("data");
      }else{
        this.sorter("filteredData");
      }
    }else{
      if(this.notFiltered()){
        this.reverser("data");
      }else{
        this.reverser("filteredData");
      }
    }
  }

  changeSort(sort){
    if(this.sortType === sort){
      this.sorterData[this.sortType].accending = !this.sorterData[this.sortType].accending;
    }else{
      Object.keys(this.sorterData).forEach((key) => {
        this.sorterData[key].accending = true;
      });
      this.sortType = sort;
    }
    this.whichWay();
  }

  toggleChecks(type){
    this.filterValue = "";
    this.dataSource.data = this.areasGrouped;
    this.toggleData[type] = !this.toggleData[type];
    if(this.toggleData[type]){
      this.dataSource.filter = "toggle";
    }else{
      this.dataSource.filter = "";
    }
    this.whichWay();
  }

  toggledLogic(element){
    let decider = false;
    if ((this.toggleData['active'] && !element.active) || (this.toggleData['delivered'] && !element.delivered)){
      decider = true;
    }
    return decider
  }

  changeActive(area){
    area.active = !area.active;
    const updateObj = {"active": area.active};
    return this.areasService.updateArea(area.$key, updateObj);
  }

  changeShowHideForComponent(component) {
    var saves = [];
    component.areas.forEach((area)=>{
      component.active = !component.active;
      const updateObj = {"active": component.active};
      let promise = this.areasService.updateArea(area.$key, updateObj)
      saves.push(promise);
    });
    return Promise.all(saves);
  }

  publishRevision(){
    if(this.togglePublishButton){
      this.togglePublishButton = false;
      this.areasUnsubscribe();
      this._snackBar.open('Processing a revision!', 'Hide', {
        panelClass: ['innviewSnack']
      });
      this.revisionsService.promoteUnpublishedRevision(this.project)
      .then(()=>{
        this.areasSubscribe();
      })
      .catch((error) => {
        this._snackBar.open(error + " - Please let IT know!");
      });
    }
  }

  showHidePromoteRevision(component) {
    if (this.project.deliverySchedule !== undefined && this.project.deliverySchedule.revisions !== undefined) {
      Object.keys(component.areas).forEach((areaKey)=>{
        let area = component.areas[areaKey];
        if (area.revisions[this.project.deliverySchedule.published] !== area.revisions[this.project.deliverySchedule.unpublished]){
          this.togglePublishButton = true;
        }
      });
    }else{
      if (Object.keys(component.areas).length > 0){
        this.togglePublishButton = true;
      }
    }
  }

  isDate(date){
    return this.dateUtilService.validDate(date);
  }

  getHandoverDate(date){
    return this.dateUtilService.subtractWorkingDays(date, this.designHandoverPeriod);
  }

  resetDeliveryChanges(){
    if(!this.sorterData[this.sortType].accending){
      this.changeSort(this.sortType);
    }
    this.togglePublishButton = false;
    this.areasUnsubscribe();
    this._snackBar.open('Undoing date changes in red!', 'Hide', {
      panelClass: ['innviewSnack']
    });
    return this.areasService.resetDeliveryChanges(this.project)
    .then(()=>{
      this.areasSubscribe();
    });
  }

}


/**  Copyright 2019 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
