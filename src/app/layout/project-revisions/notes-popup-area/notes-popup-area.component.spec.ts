import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NotesPopupAreaComponent } from './notes-popup-area.component';

xdescribe('NotesPopupAreaComponent', () => {
  let component: NotesPopupAreaComponent;
  let fixture: ComponentFixture<NotesPopupAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesPopupAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesPopupAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
