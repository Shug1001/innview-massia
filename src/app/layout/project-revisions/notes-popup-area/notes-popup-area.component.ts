import { Component, OnInit, Input } from '@angular/core';
import { DialogNoteAreaComponent } from './dialog-note-area/dialog-note-area.component';
import { MatDialog } from '@angular/material';
import { AreaExt } from 'src/app/shared/models/models';

@Component({
  selector: 'app-notes-popup-area',
  templateUrl: './notes-popup-area.component.html',
  styleUrls: ['./notes-popup-area.component.scss']
})
export class NotesPopupAreaComponent {

  @Input() area: AreaExt;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogNoteAreaComponent, {
      width: '300px',
      data: {area: this.area}
    });

    dialogRef.componentInstance.data = {area: this.area};

  }
}
