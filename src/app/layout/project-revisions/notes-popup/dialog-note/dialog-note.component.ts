import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { ComponentsService } from 'src/app/shared/services';

@Component({
  selector: 'app-dialog-note',
  templateUrl: './dialog-note.component.html',
  styleUrls: ['./dialog-note.component.scss']
})
export class DialogNoteComponent {

  _subscription:Subscription;

  constructor(
    public dialogRef: MatDialogRef<DialogNoteComponent>,
    public componentsService: ComponentsService,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  isWalls(componentName){
    return componentName.includes('IFAST');
  }

  saveComponentNote(){
    const updateObj = {"comments": this.data.component.comments};
    return this.componentsService.updateComponent(this.data.component.componentId, updateObj)
            .then(()=>{
              this.dialogRef.close();
            });
  }


  saveComponentAddons(){
    const updateObj = {"addons": this.data.additionalDataAddon};
    return this.componentsService.saveComponentAdditional(this.data.component.componentId, updateObj)
            .then(()=>{
              this.dialogRef.close();
            });
  }

}
