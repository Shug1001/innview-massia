import { Component, OnInit, Input } from '@angular/core';
import { DialogNoteComponent } from './dialog-note/dialog-note.component';
import { MatDialog } from '@angular/material';
import { ComponentsService } from 'src/app/shared/services';
import { ComponentsHolder } from 'src/app/shared/models/models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-notes-popup',
  templateUrl: './notes-popup.component.html',
  styleUrls: ['./notes-popup.component.scss']
})
export class NotesPopupComponent implements OnInit{


  @Input() component: ComponentsHolder;
  _subscription:Subscription;
  additionalDataAddon: string;
  mappAddons: object;

  constructor(
    public componentsService: ComponentsService,
    public dialog: MatDialog) {}

  ngOnInit(){
    this.mappAddons = {
      'Windows': 'W',
      'Cladding': 'C',
      'Windows & Cladding': 'W/C'
    };
    this.additionalDataAddon = '';
    this._subscription = this.componentsService.getComponentAdditional(this.component.componentId).subscribe((snap) => {
        this.additionalDataAddon = snap !== null && snap.addons !== undefined ? snap.addons : '';
    });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogNoteComponent, {
      width: '500px',
      data: {component: this.component, additionalDataAddon: this.additionalDataAddon}
    });

    dialogRef.componentInstance.data = {component: this.component, additionalDataAddon: this.additionalDataAddon};

  }
}
