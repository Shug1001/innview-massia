import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog.component';

xdescribe('ConfirmationPopupDialogComponent', () => {
  let component: ConfirmationPopupDialogComponent;
  let fixture: ComponentFixture<ConfirmationPopupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationPopupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationPopupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
