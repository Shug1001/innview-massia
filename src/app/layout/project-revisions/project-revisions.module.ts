import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { ProjectRevisionsComponent } from './project-revisions.component';
import { ProjectRevisionsRoutingModule } from './project-revisions-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeliveryDatepickerComponent, BulkCustomHeaderComponent } from './delivery-datepicker/delivery-datepicker.component';
import { NotesPopupComponent } from './notes-popup/notes-popup.component';
import { DialogNoteComponent } from './notes-popup/dialog-note/dialog-note.component';
import { NotesPopupAreaComponent } from './notes-popup-area/notes-popup-area.component';
import { DialogNoteAreaComponent } from './notes-popup-area/dialog-note-area/dialog-note-area.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { SrdSelectModule } from 'src/app/shared/modules/srd-select/srd-select.module';
import {NgxPrintModule} from 'ngx-print';
import { AreaPopupModule } from 'src/app/shared/modules/area-popup/area-popup.module';
import { FileUploaderModule } from 'src/app/shared/modules/file-uploader/file-uploader.module';
import { RevitOptionsModule } from '../../shared/modules/revit-options/revit-options.module';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import {
    MatListModule,
    MatSortModule,
    MatDialogModule,
    MatCardModule,
    MatBadgeModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatButtonToggleModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatChipsModule,
    MatTableModule,
    MatGridListModule,
    MatTooltipModule,
    MatRadioModule,
    MatSelectModule,
    MatToolbarModule,
    MatExpansionModule,
    MatSnackBarModule } from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ProjectRevisionsRoutingModule,
        MatSelectModule,
        MatDialogModule,
        MatTooltipModule,
        MatCardModule,
        MatBadgeModule,
        MatRadioModule,
        MatProgressBarModule,
        MatListModule,
        MatCheckboxModule,
        MatButtonModule,
        MatChipsModule,
        MatButtonToggleModule,
        MatIconModule,
        FormsModule,
        RevitOptionsModule,
        MatToolbarModule,
        MatInputModule,
        MatExpansionModule,
        MatButtonToggleModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatSortModule,
        MatListModule,
        MatGridListModule,
        MatSnackBarModule,
        MatTableModule,
        MatFormFieldModule,
        PipesModule,
        SrdSelectModule,
        NgxPrintModule,
        AreaPopupModule,
        FileUploaderModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    exports: [ProjectRevisionsComponent],
    declarations: [
        ProjectRevisionsComponent,
        BulkCustomHeaderComponent,
        DeliveryDatepickerComponent,
        NotesPopupComponent,
        DialogNoteComponent,
        NotesPopupAreaComponent,
        DialogNoteAreaComponent,
        ConfirmationPopupDialogComponent],
    entryComponents: [DialogNoteComponent, BulkCustomHeaderComponent, DialogNoteAreaComponent, ConfirmationPopupDialogComponent]
})
export class ProjectRevisionsModule {}
