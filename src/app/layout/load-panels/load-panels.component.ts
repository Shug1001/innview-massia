import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Output, OnInit, ViewChild } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { LoadPanelsService } from 'src/app/shared/services/load-panels.service';
import { PanelsService } from 'src/app/shared/services/panels.service';
 
// const URL = '/api/';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-load-panels',
  templateUrl: './load-panels.component.html',
  styleUrls: ['./load-panels.component.scss']
})
export class LoadPanelsComponent implements OnInit {

  files: Array<any>;
  updateSummary: Object;
  filePanels: Array<any>;
  fileComponents: Object;
  fileAreas: Object;
  updateSummaryComplete: Boolean;
  fileErrors: Array<any>;
  saveInProgress: boolean;

  panels: Array<any>;
  components: Object;
  actualEqualNumberOfPanels: Object;
  errors: Array<any>;
  jobSheetCount: Number;
  selectData: Array<any>;
  panData: Array<any>;
  autoUpdateString: string;
  status: string;
  exportAreas: Array<any>;

  @Output() uploadEvent =  new EventEmitter();
  @ViewChild('fileInput') fileInput: any;

  constructor(private http: HttpClient, private loadPanelsService: LoadPanelsService, private panelsService: PanelsService) {}

  ngOnInit() {
    this.selectData = [];
    this.files = [];
    this.updateSummary = {};
    this.filePanels = [];
    this.fileComponents = {};
    this.fileAreas = {};
    this.fileErrors = [];
    this.saveInProgress = true;
    this.updateSummaryComplete = false;
    this.exportAreas = [];
    this.status = "ready";
    

    this.updateLists();

    this.autoUpdateString = "Initialising...";

    this.http.get<any>('https://innview-api.azurewebsites.net/api/MissingPanels').subscribe(data => {

      this.panData = data;

      this.autoUpdateString = this.panData.length + " panels to update";

      let exportAreas = [];
      

      data.forEach((pan) => {

        let exportArea = {
          "Panel Ref": "",
          "Type": "",
          "Framing Style": "",
          "Length": "",
          "Height": "",
          "Area": "",
          "Depth": "",
          "Stud Size": "",
          "Nailing": "",
          "Spandrel": "",
          "Doors": "",
          "Windows": "",
          "Pockets": "",
          "Weight": "",
          "Qty": ""
        }

        exportArea['Panel Ref'] = pan.Panel_ref;
        exportArea["Type"] = pan.Type;
        exportArea["Framing Style"] = pan.Panel_Framing_Style;
        exportArea["Length"] = pan.Length;
        exportArea["Height"] = pan.Height;
        exportArea["Area"] = pan.Area;
        exportArea["Depth"] = pan.Depth;
        exportArea["Stud Size"] = pan.Studsize;
        exportArea["Nailing"] = pan.Nailing;
        exportArea["Spandrel"] = pan.Spandrel;
        exportArea["Doors"] = pan.Doors;
        exportArea["Windows"] = pan.Windows;
        exportArea["Pockets"] = pan.Pockets;
        exportArea["Weight"] = pan.Weight;
        exportArea["Qty"] = pan.Qty;

        exportAreas.push(exportArea);
      });

      this.exportAreas = exportAreas;
      this.status = "ready";

    });

  }

  apiUpdate(){

    var index = this.files.push(1) -1;

    return this.loadPanelsService.loadApiPanels(this.panData)

    .then((result) => {

      this.filePanels[index] = result.panels || [];

      this.fileComponents = result.components || {};

      this.fileAreas = result.areas || {};

      this.fileErrors[index] = result.errors || [];

      this.saveInProgress = false;

      this.updateLists();

    })
    .catch((error) => {

      this.filePanels[index] = [];

      this.fileComponents = {};

      this.fileAreas = {};

      this.fileErrors[index] = [ "Failed to parse"];

      this.updateLists();

    });

  }

  

  removeFile(index) {
    this.files.splice(index, 1);
    this.filePanels.splice(index, 1);
    delete this.fileComponents;
    delete this.fileAreas;
    this.fileErrors.splice(index, 1);

    this.updateLists();
  }

  objKeys(obj){
    return Object.keys(obj);
  }

  reset(){
    this.selectData = [];
    this.files = [];
    this.updateSummary = {};
    this.filePanels = [];
    this.fileComponents = {};
    this.fileAreas = {};
    this.fileErrors = [];
    this.saveInProgress = true;
    this.updateSummaryComplete = false;

    this.updateLists();
  }

  updateLists() {
    this.panels  = [].concat.apply([], this.filePanels);
    this.components = this.fileComponents;
    this.actualEqualNumberOfPanels = this.actualEqualNumberOfPanel(this.fileAreas, this.panels);
    this.errors = [].concat.apply([], this.fileErrors);
    this.jobSheetCount = this.jobSheetCounts(this.panels);
  }

  jobSheetCounts(panels){
    var keys = [];
    panels.forEach(item => {
      var key = item["jobSheet"];
      if (keys.indexOf(key) === -1 && key !== undefined) {
        keys.push(key);
      }
    });
    return keys.length;
  }
  
  actualEqualNumberOfPanel(areas, panels){
    var areasHolder = {};
    panels.forEach(item => {
      var key = item["area"];
      if (areas[key] !== undefined) {
        var area = areas[key];
        var areaId = area.phase + "-" + area.floor + "-" + area.type;
        if (areasHolder[areaId] === undefined) {
          areasHolder[areaId] = {
            "numberOfPanels": 0,
            "actual": 0,
            "numberOfPanelsExist": false
          };
        }
        if (area.numberOfPanels !== undefined) {
          areasHolder[areaId].numberOfPanels = area.numberOfPanels;
          areasHolder[areaId].numberOfPanelsExist = true;
        }
        areasHolder[areaId].actual++;
      }else{
        this.saveInProgress = false;
      }
    });
    return areasHolder;
  }

  
  public uploader:FileUploader = new FileUploader({
    url: URL, 
    disableMultipart:true
    });
  public hasBaseDropZoneOver:boolean = false;

  fileObject: any;


  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e; 
  }

  allSelected(){
    var selected = true;
    if (this.components !== undefined){
      Object.keys(this.components).forEach(componentsKeys => {
        let components = this.components[componentsKeys];
        if (!components.selected){
          selected = false;
        }
      });
    }
    return selected;
  }

  save() {
    var selected = this.allSelected();
    if (selected) {
      this.saveInProgress = true;
      if (this.panels.length > 0) {
        this.panelsService.updatePanels(this.panels)
          .then((updateSummary) => {
            this.updateSummary = updateSummary;
            this.updateSummaryComplete = true;
            return this.loadPanelsService.averageMeterSquareOfDatum();
            // return this.loadPanelsService.incrimentDesignCount(this.panels);
          });
            
          
      }
    }
  }
     
  selectedComponet(area, components, index) {
    this.selectData = [];
    var selectData = this.selectData[index] = index.value;
    if (selectData !== undefined && selectData !== ""){
      components.selected = true;
      this.panels.forEach((panel) => {
        var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
        var idParts = PANEL_ID_REGEX.exec(panel.id);
        if (idParts) {
          var id = idParts[1] + "-" + idParts[3] + "-" + idParts[4] + "-" + panel.type;
          if(id === area){
            panel.area = selectData;
          }
        }
      });
      this.saveInProgress = false;
      this.actualEqualNumberOfPanels = this.actualEqualNumberOfPanel(this.fileAreas, this.panels);
    }else{
      components.selected = false;
      this.panels.forEach((panel) => {
        var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
        var idParts = PANEL_ID_REGEX.exec(panel.id);
        if (idParts) {
          var id = idParts[1] + "-" + idParts[3] + "-" + idParts[4] + "-" + panel.type;
          if(id === area){
            panel.area = "";
          }
        }
      });
      this.saveInProgress = true;
      this.actualEqualNumberOfPanels = this.actualEqualNumberOfPanel(this.fileAreas, this.panels);
    }
  } 

  public onFileSelected(event: EventEmitter<File[]>) {
    const file: File = event[0];

    console.log(file);
    this.uploadEvent.emit(file);
    this.fileInput.nativeElement.value = '';
    var index = this.files.push(file) -1;
    return this.loadPanelsService.loadPanels(file)
    .then((result) => {
      this.filePanels[index] = result.panels || [];
      this.fileComponents = result.components || {};
      this.fileAreas = result.areas || {};
      this.fileErrors[index] = result.errors || [];
      this.saveInProgress = false;
      this.updateLists();
    })
    .catch((error) => {
      this.filePanels[index] = [];
      this.fileComponents = {};
      this.fileAreas = {};
      this.fileErrors[index] = [ "Failed to parse '" + file.name + "'"];
      this.updateLists();
    });
  }
}