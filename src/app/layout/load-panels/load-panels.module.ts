import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadPanelsRoutingModule } from './load-panels-routing.module';
import { LoadPanelsComponent } from './load-panels.component';
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatIconModule, MatInputModule, MatListModule, MatSelectModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';

@NgModule({
  declarations: [LoadPanelsComponent],
  imports: [
    CommonModule,
    LoadPanelsRoutingModule,
    FileUploadModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatCardModule,
    DataExportModule,
    MatListModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})

  ]
})
export class LoadPanelsModule { }
