import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoadPanelsComponent } from './load-panels.component';

const routes: Routes = [
  {
    path: '',
    component: LoadPanelsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoadPanelsRoutingModule { }
