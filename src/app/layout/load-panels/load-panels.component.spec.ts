import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadPanelsComponent } from './load-panels.component';

describe('LoadPanelsComponent', () => {
  let component: LoadPanelsComponent;
  let fixture: ComponentFixture<LoadPanelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadPanelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPanelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
