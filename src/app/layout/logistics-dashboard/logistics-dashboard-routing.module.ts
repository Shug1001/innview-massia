import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogisticsDashboardComponent } from './logistics-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: LogisticsDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogisticsDashboardRoutingModule { }
