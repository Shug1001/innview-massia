import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule, MatSortModule, MatTableModule, MatPaginatorModule, MatCardModule, MatInputModule, MatFormFieldModule, MatIconModule, MatGridListModule, MatCheckboxModule, MatTooltipModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { DfmBufferModule } from 'src/app/shared/modules/dfm-buffer/dfm-buffer.module';
import { AdditionalDataModule } from 'src/app/shared/modules/additional-data/additional-data.module';
import { StackedGroupedChartModule } from 'src/app/shared/modules/stacked-grouped-chart/stacked-grouped-chart.module';
import { DesignEnteredModule } from 'src/app/shared/modules/design-entered/design-entered.module';
import { DatePickerModule } from 'src/app/shared/modules/date-picker/date-picker.module';
import { DashAreaSearchPopupComponent } from './dash-area-search-popup/dash-area-search-popup.component';
import { LogisticsDashboardRoutingModule } from './logistics-dashboard-routing.module';
import { LogisticsDashboardComponent } from './logistics-dashboard.component';
import { AddBookingModule } from 'src/app/shared/modules/add-booking/add-booking.module';
import { JobsheetsModule } from 'src/app/shared/modules/jobsheets/jobsheets.module';
import { MaterialRequestsModule } from 'src/app/shared/modules/material-requests/material-requests.module';

@NgModule({
  declarations: [LogisticsDashboardComponent, DashAreaSearchPopupComponent],
  entryComponents: [DashAreaSearchPopupComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatSortModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatGridListModule,
    MatTooltipModule,
    MatInputModule,
    DfmBufferModule,
    DatePickerModule,
    AdditionalDataModule,
    AddBookingModule,
    MaterialRequestsModule,
    JobsheetsModule,
    PipesModule,
    DesignEnteredModule,
    StackedGroupedChartModule,
    LogisticsDashboardRoutingModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class LogisticsDashboardModule { }
