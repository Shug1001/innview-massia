import { Component, OnInit } from '@angular/core';
import { Subscription, combineLatest } from 'rxjs';
import { DashboardsService } from 'src/app/shared/services/dashboards.service';
import { ForwardPlannerService } from 'src/app/shared/services/forward-planner.service';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material';
import { DashAreaSearchPopupComponent } from './dash-area-search-popup/dash-area-search-popup.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-logistics-dashboard',
  templateUrl: './logistics-dashboard.component.html',
  styleUrls: ['./logistics-dashboard.component.scss']
})
export class LogisticsDashboardComponent implements OnInit {

  selectDate: string;
  _subscription: Subscription;
  chartData: object;
  loaded: boolean;
  chartColors: object;
  buildAreas: object;
  bookings: object;
  true = true;
  false = false;


  constructor(public dialog: MatDialog, private route: ActivatedRoute, private forwardPlannerService: ForwardPlannerService, private dashboardsService: DashboardsService, private dateUtilsService: DateUtilService, private dateUtilService: DateUtilService) {
    this.loaded = false;
    this.selectDate = this.dateUtilsService.weekStart(new Date());
    this.chartColors = {
      booked: "#0000FF",
      dispatches: "#FF0000"
    };
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DashAreaSearchPopupComponent, {
      width: '350px',
      data: {areas: this.buildAreas}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.selectDate = result;
      this.getData();
    });
  }

  calculatePercentage(a, b) {
    if (b === 0) {
      return 0;
    }
    let ans = Math.round((a/b)*100);
    if(isNaN(ans)){
      return 0;
    }
    return ans;
  }

  round1(val){
    return this.dateUtilsService.round1(val);
  }

  objKeys(obj){
    return Object.keys(obj);
  }

  panelStat(area){
    let note = "";
    note += "Estimates: " + area.estimatedPanels || 0 + " \n ";
    note += "Design: " + area.numberOfPanels || 0 + " \n ";
    return note;
  }

  m2Stat(area){
    let note = "";
    note += "Estimates: " + area.estimatedArea || 0 + " \n ";
    note += "Design: " + area.areaOfPanels || 0 + " \n ";
    return note;
  }

  areasWithActuals(chartData, day){  
    let count = 0;
    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined && chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day] !== undefined){
      let areasArr  = chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day];
      if (areasArr !== undefined){
        areasArr.forEach((area) => {
          area.panStatus = false;
          if(area.actualPanels !== undefined && area.actualPanels > 0){
            area.panStatus = true;
            count++;
          }
        });
      }
    }
    return count;
  }

  getAreas(chartData, day){
    if(chartData.areasByDesignHandoverDate[chartData.weekStartReturn] !== undefined && chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day] !== undefined){
      return chartData.areasByDesignHandoverDate[chartData.weekStartReturn][day].length;
    }else{
      return 0;
    }
  }

  getRiskDays(plan){
    if (plan && plan.plannedFinish && plan.riskDate) {
      return this.dateUtilService.daysBetweenWeekDaysOnly(plan.plannedFinish, plan.riskDate);
    } else {
      return 0;
    }
  }

  dateChange(dt){
    this.selectDate = this.dateUtilsService.weekStart(new Date(dt));
    this.getData();
  }

  getData(){
    this.loaded = false;
    const daysOfWeeks = this.dashboardsService.getWeekDaysLogistics(this.selectDate);
    this.chartData = this.dashboardsService.getWeeksLogistics(this.selectDate, daysOfWeeks, this.buildAreas, 6, 21, this.bookings);
    this.loaded = true;
    console.log(this.chartData);
  }

  ngOnInit() {
    this.selectDate = this.route.snapshot.paramMap.get('id');
    this._subscription = combineLatest([this.forwardPlannerService.getPlansAndAddNoFilter(), this.dashboardsService.getAllBookings()]).subscribe(([buildAreas, bookings]) => {
      this.bookings = bookings;
      this.buildAreas = buildAreas;
      this.getData();
    });
  }

  getInitials(string){
    var names = string.split(' '),
    initials = names[0].substring(0, 1).toUpperCase();
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }



}
