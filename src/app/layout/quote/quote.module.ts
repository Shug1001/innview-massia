import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule, MatCardModule, MatTabsModule, MatFormFieldModule,MatSelectModule,MatOptionModule } from '@angular/material';
import { QuoteRoutingModule } from './quote-routing.module';
import { QuoteComponent } from './quote.component';

@NgModule({
  declarations: [QuoteComponent],
  imports: [
    CommonModule,
    QuoteRoutingModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatTabsModule,
  ]
})
export class QuoteModule { }
