import { NgModule } from '@angular/core';
import { QuoteComponent } from './quote.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: QuoteComponent,
      children: [
        { 
          path: '', 
          redirectTo: 'take-off', 
          pathMatch: 'full' },
        {
          path: 'take-off',
          loadChildren: '../take-off/take-off.module#TakeOffModule'
        }
    ],
  },{
    path: '**',
    redirectTo: 'take-off',
    pathMatch: 'full'
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuoteRoutingModule { }
