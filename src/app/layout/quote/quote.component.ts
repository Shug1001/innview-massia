import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import { ProjectsService } from 'src/app/shared/services/projects.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {

  quote: any;
  loaded: boolean;
  _subscription: Subscription;
  _subscription2: Subscription;
  totalRevisions: number;
  latestRevision: object;
  framingStyles:object;
  routeLinks: any[];
  activeLinkIndex = -1;


  constructor(private route: ActivatedRoute, private router: Router, private revisionsService: RevisionsService, private projectsService: ProjectsService) {
    this.loaded = false;
    this.routeLinks = [
      {
        label: 'Take Off',
        link: './take-off',
        index: 0
      }
    ]
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this._subscription = this.projectsService.getQuoteWithId(id).subscribe(quote => {
      this.quote = quote;
      this.quote.$key = id;
      this.loaded = true;
    });

    this._subscription2 = this.revisionsService.changeRoute$.subscribe(route => {
      this.router.navigate(['quote/'+id+'/'+route]);
    });
  }

  returnQuote(title){
    return title.split("-")[0];
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._subscription2.unsubscribe();
  }


}
