import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevProgressRoutingModule } from './dev-progress-routing.module';
import { DevProgressComponent } from './dev-progress.component';

@NgModule({
  declarations: [DevProgressComponent],
  imports: [
    CommonModule,
    DevProgressRoutingModule
  ]
})
export class DevProgressModule { }
