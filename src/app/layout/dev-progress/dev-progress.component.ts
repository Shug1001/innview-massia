import { Component, OnInit } from '@angular/core';
import { ForwardPlannerService } from 'src/app/shared/services';

@Component({
  selector: 'app-dev-progress',
  templateUrl: './dev-progress.component.html',
  styleUrls: ['./dev-progress.component.scss']
})
export class DevProgressComponent implements OnInit {

  areas: Array<any>;
  revitCount: number;

  constructor(private plannerService: ForwardPlannerService) { }

  ngOnInit() {
    this.revitCount = 0;
    this.plannerService.getActiveAreas().subscribe(areas => {
      this.areas = areas;
      areas.forEach(area => {
        if(area.revitUpload){
          this.revitCount++;
        }
      });
    });
  }

}
