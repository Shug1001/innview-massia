import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DevProgressComponent } from './dev-progress.component';

const routes: Routes = [
  {
    path: '',
    component: DevProgressComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevProgressRoutingModule { }
