import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploaderModule } from '../../shared/modules/file-uploader/file-uploader.module';
import { DataExportModule } from '../../shared/modules/data-export/data-export.module';
import { ProjectDetailsRoutingModule } from './project-details-routing.module';
import { ProjectDetailsComponent } from './project-details.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SrdSelectModule } from 'src/app/shared/modules/srd-select/srd-select.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AssociatedUsersSelectModule } from 'src/app/shared/modules/associated-users-select/associated-users-select.module';
import { NotificationSelectModule } from 'src/app/shared/modules/notification-select/notification-select.module';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import { ProjectEstimatesModule } from 'src/app/shared/modules/project-estimates/project-estimates.module';
import { DimensionComponent } from './dimension/dimension.component';
import { AreaEstimatesComponent } from './area-estimates/area-estimates.component';
import {
  MatCardModule,
  MatToolbarModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatCheckboxModule,
  MatButtonModule,
  MatIconModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatListModule
} from '@angular/material';

@NgModule({
  declarations: [ProjectDetailsComponent, DimensionComponent, AreaEstimatesComponent, ConfirmationPopupDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatListModule,
    MatInputModule,
    MatProgressBarModule,
    MatButtonModule,
    MatCheckboxModule,
    SrdSelectModule,
    MatSnackBarModule,
    NotificationSelectModule,
    AssociatedUsersSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatIconModule,
    FileUploaderModule,
    DataExportModule,
    MatToolbarModule,
    ProjectDetailsRoutingModule,
    ProjectEstimatesModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  exports: [ProjectDetailsComponent],
  entryComponents: [ConfirmationPopupDialogComponent, DimensionComponent, AreaEstimatesComponent]
})
export class ProjectDetailsModule { }
