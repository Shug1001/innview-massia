import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProjectDetailsComponent } from '../project-details.component';
import { DateUtilService } from '../../../shared//services/date-util.service';
import { Subscription, combineLatest } from 'rxjs';
import { CreatedComponents } from '../../../shared/models/models';
import { ProjectsService } from '../../../shared/services/projects.service';
import Srd  from '../../../../assets/srd/srd.json';

@Component({
  selector: 'app-area-estimates',
  templateUrl: './area-estimates.component.html',
  styleUrls: ['./area-estimates.component.scss']
})
export class AreaEstimatesComponent implements OnInit {

  srdFloors = Srd.floors;
  floorSeqMap: object;
  quoteHolder: {};
  status: string;
  quotesList: Array<any>;
  subTypeObj: {};
  toggleEstimates: boolean;
  text: string;
  titles: Array<any>;
  titlesTable: Array<any>;
  errorTitles: Array<any>;
  statusData: object;
  ready: CreatedComponents;
  statusSubscription: Subscription;
  _subscription: Subscription;
  quotes: Array<any>;
  loaded: boolean;
  isQuotes: boolean;
  areaEstimates: Array<any>;


  constructor(private projectsService: ProjectsService, private dateUtilsService: DateUtilService, public dialogRef: MatDialogRef<ProjectDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.mapFloor();
      this.areaEstimates = [];
      this.isQuotes = false;
      this.loaded = false;
      this.subTypeObj= {
        "DAT_Walls": function(type) {
          let subType = "No Type";
          if (type.indexOf("iSIP") !== -1) {
            subType = "External";
          } else if (type.indexOf("TF") !== -1) {
            subType = "Internal";
          } else if (type.indexOf("iFAST") !== -1) {
            subType = "IFAST";
          }
          return subType;
        },
        "DAT_Floors": function(type) {
          let subType = "No Type";
          if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1) {
            subType = "Floor";
          } else if (type.indexOf("Concrete")  !== -1) {
            subType = "Floor";
          }
          return subType;
        },
        "DAT_Roofs": function(type) {
          let subType = "No Type";
          if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1 || type.indexOf("Cassette") !== -1) {
            subType = "Roof";
          } else if (type.indexOf("Cassette") === -1) {
            subType = "Roof";
          }
          return subType;
        }
      };
      
    }

    subTypeObj2(type) {
      let subType = "No Product Type";
      if (type.indexOf("iSIP") !== -1) {
        subType = "iSIP";
      } else if (type.indexOf("iFAST") !== -1) {
        subType = "iFAST";
      } else if (type.indexOf("Closed TF") !== -1) {
        subType = "CTF";
      } else if (type.indexOf("TF")  !== -1) {
        subType = "TF";
      } else if (type.indexOf("245D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("300D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("350D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("400D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("450D") !== -1) {
        subType = "I-Joist";
      } else if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1) {
        subType = "Timber Floors";
      } else if (type.indexOf("Concrete")  !== -1) {
        subType = "Concrete Floors";
      } else if (type.indexOf("TC") !== -1 || type.indexOf("FS") !== -1 || type.indexOf("Posi") !== -1 || type.indexOf("Cassette") !== -1) {
        subType = "Timber Cassette Roofs";
      } else if (type.indexOf("Cassette") === -1) {
        subType = "Timber Frame Roofs";
      }
      return subType;
    }

    mapFloor(){
      this.floorSeqMap = {};
      Object.keys(this.srdFloors).forEach(key => {
        if(this.floorSeqMap[key] === undefined){
          this.floorSeqMap[key] = this.srdFloors[key].seq;
        }
      });
    }
    
    ngOnInit(){

      this._subscription = combineLatest(this.projectsService.getQuotesList(), this.projectsService.getQuoteDataByLatest(this.data.project.$key)).subscribe(([quotesList, quotes]) => {
        
        this.quoteHolder = {};
        this.quotesList = quotesList;
        this.quotes = quotes;

        this.getAreaEstimates();

      });

    }

    getAreaEstimates(){
 
      if(this.quotes.length){

        this.areaEstimates = [];
        let groupQuotes = {};

        this.quotes.forEach(quote => {
          if(this.subTypeObj[quote.sheetName] !== undefined){
            quote.subType = this.subTypeObj[quote.sheetName](quote.type);
            //quote.subType2 = this.subTypeObj2(quote.type);
            if(quote.subType !== "No Type"){
              if(groupQuotes[quote.type] === undefined){
                groupQuotes[quote.type] = {};
              }
              if(groupQuotes[quote.type][quote.level] === undefined){
                groupQuotes[quote.type][quote.level] = {};
              }
              if(groupQuotes[quote.type][quote.level][quote.phase] === undefined){
                groupQuotes[quote.type][quote.level][quote.phase] = {
                  type: "",  
                  phase: "",
                  level: "",
                  gross:0
                };
              }
              groupQuotes[quote.type][quote.level][quote.phase].type = quote.type;
              groupQuotes[quote.type][quote.level][quote.phase].phase = quote.phase;
              groupQuotes[quote.type][quote.level][quote.phase].level = quote.level;
              groupQuotes[quote.type][quote.level][quote.phase].gross += quote.areaGross;
            }
          }
        });
        for (let type in groupQuotes) {
          for (let level in groupQuotes[type]) {
            for (let phase in groupQuotes[type][level]) {
              let quote = groupQuotes[type][level][phase];
              quote.gross = this.round2(groupQuotes[type][level][phase].gross)
              this.areaEstimates.push(quote);
            }
          }
        }
        this.areaEstimates.sort((areaA, areaB) => {

          var o1 = areaA.phase.toLowerCase();
          var o2 = areaB.phase.toLowerCase();
        
          var p1 = this.floorSeqMap[areaA.level];
          var p2 = this.floorSeqMap[areaB.level];
        
          if (o1 < o2) return -1;
          if (o1 > o2) return 1;
          if (p1 < p2) return -1;
          if (p1 > p2) return 1;
          return 0;
        });
        this.loaded = true; 
      }else{
        this.loaded = true;
      }
    }

    round2(data){
      return this.dateUtilsService.round2(data);
    }

    getAvg(data) {
      const total = data.reduce((acc, c) => acc + c, 0);
      return total / data.length;
    }

    removeRevit(str){
      if(str.indexOf("Revit ") !== -1){
        return str.replace("Revit ", "");
      }
    }
  
    getObjectKeys(data){
      return Object.keys(data);
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    ngOnDestroy(){
      this._subscription.unsubscribe();
    }
  

}