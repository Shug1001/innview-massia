import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProjectDetailsComponent } from '../project-details.component';
import { LoadPanelsService } from '../../../shared/services/load-panels.service';

@Component({
  selector: 'app-dimension',
  templateUrl: './dimension.component.html',
  styleUrls: ['./dimension.component.scss']
})
export class DimensionComponent {

  dimensionData: object;
  showUpload: boolean;
  showButtons: boolean;
  showResults: boolean;
  allResults: object;
  result: object;
  errors: [];
  status: string;

  constructor(
    private loadPanelsService: LoadPanelsService,
    public dialogRef: MatDialogRef<ProjectDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.dimensionData = {
        "floor":{
          "width":2400,
          "length":5000
        },
        "roof":{
          "width":2400,
          "length":9000
        }
      };
      this.status;
      this.showUpload = true;
      this.showButtons = false;
      this.showResults = false;
    }

  uploadFile(data){
    this.showUpload = false;
    this.loadPanelsService.loadQuotes(data, this.dimensionData)
    .then((result) => {
      this.allResults = result;
      if(result.errors.length){
        this.errors = result.errors;
      }else{
        this.result = result.revitData;
        this.showButtons = true;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  showQuoteData(){
    this.showResults = !this.showResults;
  }

  saveQuoteData(){
    this.status = "Saving...";
    this.showButtons = false;
    this.showResults = false;
    return this.loadPanelsService.saveQuotes(this.allResults)
    .then((result) => {
      this.status = "Saved!";
    })
    .catch((error) => {
      this.status = "Error: Contact developer!";
    });
  }

  theKeys(data){
    return Object.keys(data);
  }

  populateDimensions(val, type, size){
    
    this.dimensionData[type][size] = parseInt(val);

    let keys = Object.keys(this.dimensionData);
    let score = 0;

    keys.forEach(type => {
      if(this.dimensionData[type]["width"] > 0 && this.dimensionData[type]["length"] > 0){
        score += 2;
      }
    });

    if(score === 4){
      this.showUpload = true;
    }else{
      this.showUpload = false;
    }

  }
}
