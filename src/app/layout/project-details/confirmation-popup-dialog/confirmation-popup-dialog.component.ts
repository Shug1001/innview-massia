import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProjectDetailsComponent } from '../project-details.component';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { ProjectsService } from 'src/app/shared/services';
import { RevisionsService } from 'src/app/shared/services/revisions.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmation-popup-dialog',
  templateUrl: './confirmation-popup-dialog.component.html',
  styleUrls: ['./confirmation-popup-dialog.component.scss']
})
export class ConfirmationPopupDialogComponent{

  text: string;
  buttonClick: boolean;
  buttonClose: boolean;

  constructor(
    public dialogRef: MatDialogRef<ProjectDetailsComponent>,
    private revisionsService: RevisionsService,
    private projectsService: ProjectsService,
    private dateUtilService: DateUtilService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data) {
      let whichDate;
      this.buttonClick = false;
      this.buttonClose = false;
      if (!this.data.siteStarted){
        whichDate = "site start date";
      }else{
        whichDate = "next delivery date";
      }
      if(this.data.changeData.includeSaturday){
        this.text = "The " + whichDate + " has changed from " + this.dateUtilService.toUserDate(this.data.changeData.currentDate) + " to " + this.dateUtilService.toUserDate(this.data.changeData.changedDate) + " all dates have changed by " + this.data.changeData.diffDays + " working days, including Saturday. Please select an option below:";
      }else{
        this.text = "The " + whichDate + " has changed from " + this.dateUtilService.toUserDate(this.data.changeData.currentDate) + " to " + this.dateUtilService.toUserDate(this.data.changeData.changedDate) + " all dates have changed by " + this.data.changeData.diffDays + " working days. Please select an option below:";
      }
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  createRevision(){
    this.buttonClick = true;
    let projKey = this.data.project.$key;
    this.text = "Please wait - processing bulk move...";
    this.revisionsService.bulkMoveAreas(this.data.project, this.data.updateObj.includeSaturday, this.data.changeData.diffDays, this.data.changeData.currentDate)
    .then(()=>{
      this.text = "Please wait - updating additional data...";
      return this.projectsService.updateProjectAdditional(projKey, this.data.updateObjAdditional);
    })
    .then(()=>{
      this.text = "Please wait - updating project data...";
      return this.projectsService.updateProject(this.data.project.$key, this.data.updateObj);
    })
    .then(()=>{
      this.buttonClose = true;
      this.text = "Bulk move complete.";
      this.dialogRef.close();
      this.revisionsService.changeRoute("delivery-schedule");
    })  
    .catch((error) => {
      this.text = error + " - Please let IT know!";
    })
    
    
  }
}
