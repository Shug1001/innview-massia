import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Project, ProjectsAdditional } from 'src/app/shared/models/models';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { ProjectsService, AreasService } from 'src/app/shared/services';
import { MatDialog, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { ConfirmationPopupDialogComponent } from './confirmation-popup-dialog/confirmation-popup-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DimensionComponent } from './dimension/dimension.component';
import { AreaEstimatesComponent } from './area-estimates/area-estimates.component';
import { CustomDateAdapter } from 'src/app/shared/helpers/custom-adapter-date';
import {MatSnackBar} from '@angular/material/snack-bar';
import { AuditsService } from 'src/app/shared/services/audits.service';
import { HttpClient } from '@angular/common/http';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/M/YYYY',
  },
  display: {
    dateInput: 'DD/M/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ProjectDetailsComponent implements OnInit {

  projectControl: FormGroup;
  additionalControl: FormGroup;
  quoteControl: FormGroup;
  minDate = new Date();
  project: Project;
  areas: Array<any>;
  projectAdditional: ProjectsAdditional;
  originalSiteStart: string;
  nextDeliveryDate: string;
  siteStarted: boolean;
  quoteHolder: object;
  siteStartDateIsValid: boolean;
  nextDeliveryDateAfterToday: boolean;
  userKeys: Array<any>;
  submitted: boolean;
  quotes: Array<any>;
  _subscription: Subscription;
  loaded: boolean;
  quoteData: object;
  options: string[];
  quoteId: string;
  filteredOptions: Observable<string[]>;
  showEstimates: boolean;
  estimatedData: object;
  id: string;
  gatewayDate: string;
  siteStart: string;

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private fb: FormBuilder,
    private dateUtilService: DateUtilService,
    private projectsService: ProjectsService,
    private areasService: AreasService,
    private auditsService: AuditsService) {

    this.userKeys = [];
    this.submitted = false;
    this.showEstimates = false;
    this.estimatedData = {};
    this.gatewayDate = this.dateUtilService.toIsoDate(new Date());

  }

  ngOnInit() {

    this.id = this.route.parent.parent.snapshot.paramMap.get('id');

    this._subscription = this.areasService.getProjectsAreasAdditional(this.id).subscribe(([project, projectAdditional, areas, quotes]) => {
      this.project = project;
      this.areas = areas;
      this.quotes = quotes;
      this.projectAdditional = projectAdditional;
      this.getOriginalDates();
      this.setUpForm();
      this.loaded = true;
    });

  }

  private _filter(value: string): string[] {
    if (value !== undefined && value !== ''){
      const filterValue = value['quote'].toLowerCase();
      return this.options.filter((option) => {
        if (option.toLowerCase().indexOf(filterValue) === 0 && option.toLowerCase().indexOf('q') !== 0){
          return true;
        }
      });
    }
  }

  getKeys(obj){
    if(obj.gatewayDates){
      return Object.keys(obj.gatewayDates); 
    }    
  }

  toHumanDate(date){
    return this.dateUtilService.toUserSlachDate(date);
  }

  isLoginToken(): boolean {
    const field = this.projectControl.get('id');
    return field.hasError('idAvailable');
  }

  getOriginalDates(){
    this.originalSiteStart = this.project.siteStart;
    this.nextDeliveryDate = this.project.nextDeliveryDate;

    let nextDeliveryDate = '9999-99-99';
    const todaysDate = this.dateUtilService.todayAsIso();
    let earliestDeliveryDate = '9999-99-99';
    const revisionRef = this.project.deliverySchedule.published;

    this.areas.forEach(areaSnap => {
      const area = areaSnap.payload.val();
      if (area.revisions[revisionRef] !== undefined && area.revisions[revisionRef] < earliestDeliveryDate) {
        earliestDeliveryDate = area.revisions[revisionRef];
      }
      if (area.revisions[revisionRef] !== undefined &&
        area.revisions[revisionRef] < nextDeliveryDate &&
        area.revisions[revisionRef] > todaysDate) {
        nextDeliveryDate = area.revisions[revisionRef];
      }
    });

    if(nextDeliveryDate !== '9999-99-99'){
      this.project.nextDeliveryDate = nextDeliveryDate;
    }

    this.siteStart = "";

    if(earliestDeliveryDate !== '9999-99-99'){
      this.project.siteStart = earliestDeliveryDate;
      this.siteStart = earliestDeliveryDate;
    }
  }

  estimatesData(data){
    this.showEstimates = true;
    this.estimatedData = data.areaEstimates;
    this.quoteData = data.quotesList;
  }

  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 49 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }

  estimatesPopup(): void{
    const dialogRef = this.dialog.open(AreaEstimatesComponent, {
      width: '800px',
      height: '500px',
      data: {
          project: this.project
        }
    });
    dialogRef.componentInstance.data = {project: this.project};
  }

  dimensionsPopup(){
    this.dialog.open(DimensionComponent);
  }

  setUpForm(){

      this.quoteId = '';
      this.options = [];
      this.quoteHolder = {};
      this.options.push('No Match');
      this.quotes.forEach(quote => {
        if (this.quoteHolder[quote.qid] === undefined){
          this.quoteHolder[quote.qid] = quote;
        }
        if (quote.project === this.project.$key){
          this.quoteId = quote.qid;
        } else {
          if(quote.project === ''){
            this.options.push(quote.qid);
          }
        }
      });

      this.quoteControl = this.fb.group({
        'quote': new FormControl(this.quoteId)
      });

      this.filteredOptions = this.quoteControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );

      if(this.project !== undefined){
        if (this.project.siteStart !== undefined) {
          if (this.project.siteStart < this.dateUtilService.todayAsIso()) {
            this.siteStarted = true;
          }
        }
        if (this.project.nextDeliveryDate !== undefined) {
          if (this.project.nextDeliveryDate > this.dateUtilService.todayAsIso()) {
            this.nextDeliveryDateAfterToday = true;
          }
        }
      }

      if(this.project.notifyUsers !== undefined){
        Object.keys(this.project.notifyUsers).forEach((userKey)=>{
          this.userKeys.push(userKey);
        });
      }
      this.projectControl = this.fb.group({
        'name': new FormControl({value: this.project.name, disabled: true}, [Validators.required]),
        'id': new FormControl({value: this.project.id, disabled: true}, [Validators.required, Validators.pattern('^\d\d\d$')]),
        'client': new FormControl(this.project.client, [Validators.required]),
        'datumType': new FormControl(this.project.datumType, [Validators.required]),
        'chainOfCustody': new FormControl(this.project.chainOfCustody),
        'siteAccess': new FormControl(this.project.siteAccess),
        'siteAddress': new FormControl(this.project.siteAddress),
        'postCode': new FormControl(this.project.postCode),
        'weekDuration': new FormControl(this.project.weekDuration),
        'siteStart2': new FormControl({value: this.project.siteStart, disabled: true}, [Validators.required]),
        'siteStart': new FormControl({value: this.project.siteStart, disabled: this.siteStarted}, [Validators.required]),
        'siteCompletion': new FormControl(this.project.siteCompletion),
        'plannedStart': new FormControl(this.project.plannedStart),
        'plannedCompletion': new FormControl(this.project.plannedCompletion),
        'nextDeliveryDate': new FormControl({value: this.project.nextDeliveryDate ?
          this.project.nextDeliveryDate : 
          this.project.siteStart, disabled: !this.siteStarted}, [Validators.required]),
        'includeSaturday': new FormControl(this.project.includeSaturday),
        'specialRequirements': new FormControl(this.project.specialRequirements),
        'estimatedAreas': this.fb.group({
          'Int': new FormControl(this.project.estimatedAreas.Int ? this.project.estimatedAreas.Int : 0, [Validators.required]),
          'Ext': new FormControl(this.project.estimatedAreas.Ext ? this.project.estimatedAreas.Ext : 0, [Validators.required]),
          'Floor': new FormControl(this.project.estimatedAreas.Floor ? this.project.estimatedAreas.Floor : 0, [Validators.required]),
          'Roof': new FormControl(this.project.estimatedAreas.Roof ? this.project.estimatedAreas.Roof : 0, [Validators.required]),
          'ExtF': new FormControl(this.project.estimatedAreas.ExtF ? this.project.estimatedAreas.ExtF : 0, [Validators.required])
        }),
        'gateways': this.fb.group({
          'G5Planned': new FormControl(this.project.gateways && this.project.gateways.G5Planned ? this.project.gateways.G5Planned : null),
          'G5Actual': new FormControl(this.project.gateways && this.project.gateways.G5Actual ? this.project.gateways.G5Actual: null),
          'G6Planned': new FormControl(this.project.gateways && this.project.gateways.G6Planned ? this.project.gateways.G6Planned: null),
          'G6Actual': new FormControl(this.project.gateways && this.project.gateways.G6Actual ? this.project.gateways.G6Actual: null),
          'G7aPlanned': new FormControl(this.project.gateways && this.project.gateways.G7aPlanned ? this.project.gateways.G7aPlanned: null),
          'G7aActual': new FormControl(this.project.gateways && this.project.gateways.G7aActual ? this.project.gateways.G7bActual: null),
          'G7bPlanned': new FormControl(this.project.gateways && this.project.gateways.G7bPlanned ? this.project.gateways.G7bPlanned: null),
          'G7bActual': new FormControl(this.project.gateways && this.project.gateways.G7bActual ? this.project.gateways.G7bActual: null),
          'G8Planned': new FormControl(this.project.gateways && this.project.gateways.G8Planned ? this.project.gateways.G8Planned: null),
          'G8Actual': new FormControl(this.project.gateways && this.project.gateways.G8Actual ? this.project.gateways.G8Actual: null),
          'G9Planned': new FormControl(this.project.gateways && this.project.gateways.G9Planned ? this.project.gateways.G9Planned: null),
          'G9Actual': new FormControl(this.project.gateways && this.project.gateways.G9Actual ? this.project.gateways.G9Actual: null)
        }),
        'steel': new FormControl(this.project.steel),
        'gifa': new FormControl(this.project.gifa),
        'delivery-manager': new FormControl(this.project.deliveryManager),
        'notifyUsers': new FormControl(this.userKeys),
        'gateway': new FormControl(this.project.gateway)
      });

      if(this.projectAdditional){
        this.additionalControl = this.fb.group({
          'site-ops': new FormControl(this.projectAdditional.siteOps),
          'designer': new FormControl(this.projectAdditional.designer),
          'quantity-surveyor':new FormControl(this.projectAdditional.qs),
          'engineer': new FormControl(this.projectAdditional.engineer),
          'engineer2': new FormControl(this.projectAdditional.engineer2),
          'team': new FormControl(this.projectAdditional.team),
          'installer': new FormControl(this.projectAdditional.installer),
        });
      } else {
        this.additionalControl = this.fb.group({
          'site-ops': new FormControl(),
          'designer': new FormControl(),
          'quantity-surveyor':new FormControl(),
          'engineer': new FormControl(),
          'engineer2': new FormControl(),
          'team': new FormControl(),
          'installer': new FormControl()
        });
      }

  }

  changeGateway(evt){
    let updateObj = {
      gateway: evt.value
    }
    return this.projectsService.updateProject(this.project.$key, updateObj);
  }

  isDisabled(){
    if ((this.projectControl.valid
      && (this.quoteControl.dirty
        || this.projectControl.dirty
        || (this.additionalControl
          && this.additionalControl.dirty)))
          && !this.submitted) {
      return false;
    } else {
      //this.findInvalidControls();
      return true;
    }
  }

  findInvalidControls() {
    const invalid = [];
    const controls = this.projectControl.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  projectData(val){
    return this.http.post<any>('https://innview-api.azurewebsites.net/api/ProjectData',val).toPromise();
  }

  onSubmit(): void {
    this.submitted = true;
    let updateObj = this.projectControl.value;
    updateObj = this.projectsService.convertFormToDatabaseStruct(updateObj);

    updateObj.siteCompletion = updateObj.siteCompletion !== null ? this.dateUtilService.toIsoDate(updateObj.siteCompletion) : null;
    updateObj.plannedCompletion = updateObj.plannedCompletion !== null ? this.dateUtilService.toIsoDate(updateObj.plannedCompletion) : null;
    updateObj.plannedStart = updateObj.plannedStart !== null ? this.dateUtilService.toIsoDate(updateObj.plannedStart) : null;

    if(updateObj.gateways !== null){
      if(updateObj.gateways.G5Planned !== null){
        updateObj.gateways.G5Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G5Planned);
      }
      if(updateObj.gateways.G5Actual !== null){
        updateObj.gateways.G5Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G5Actual);
      }
      if(updateObj.gateways.G6Planned !== null){
        updateObj.gateways.G6Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G6Planned);
      }
      if(updateObj.gateways.G6Actual !== null){
        updateObj.gateways.G6Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G6Actual);
      }
      if(updateObj.gateways.G7aPlanned !== null){
        updateObj.gateways.G7aPlanned = this.dateUtilService.toIsoDate(updateObj.gateways.G7aPlanned);
      }
      if(updateObj.gateways.G7aActual !== null){
        updateObj.gateways.G7aActual = this.dateUtilService.toIsoDate(updateObj.gateways.G7aActual);
      }
      if(updateObj.gateways.G7bPlanned !== null){
        updateObj.gateways.G7bPlanned = this.dateUtilService.toIsoDate(updateObj.gateways.G7bPlanned);
      }
      if(updateObj.gateways.G7bActual !== null){
        updateObj.gateways.G7bActual = this.dateUtilService.toIsoDate(updateObj.gateways.G7bActual);
      }
      if(updateObj.gateways.G8Planned !== null){
        updateObj.gateways.G8Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G8Planned);
      }
      if(updateObj.gateways.G8Actual !== null){
        updateObj.gateways.G8Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G8Actual);
      }
      if(updateObj.gateways.G9Planned !== null){
        updateObj.gateways.G9Planned = this.dateUtilService.toIsoDate(updateObj.gateways.G9Planned);
      }
      if(updateObj.gateways.G9Actual !== null){
        updateObj.gateways.G9Actual = this.dateUtilService.toIsoDate(updateObj.gateways.G9Actual);
      }
    }

    let updateObjAdditional = this.additionalControl.value;
    updateObjAdditional = this.projectsService.convertToAdditional(updateObjAdditional);

    const updateQuote = this.quoteControl.value;

    const changeData = this.areasService.getdaysDiff(updateObj, this.originalSiteStart, this.nextDeliveryDate, this.siteStarted);
    if (changeData.diffDays !== 0 && this.project.deliverySchedule !== undefined && this.project.deliverySchedule.published !== undefined) {
      const dialogRef = this.dialog.open(ConfirmationPopupDialogComponent, {
        width: '800px',
        data: {project: this.project, updateObj: updateObj, updateObjAdditional: updateObjAdditional, changeData: changeData}
      });
      dialogRef.componentInstance.data = {
        project: this.project,
        updateObj: updateObj,
        updateObjAdditional:
        updateObjAdditional,
        changeData: changeData
      };
    } else{
      this.projectsService.updateProjectAdditional(this.project.$key, updateObjAdditional)
      .then(()=>{
        return this.projectsService.updateProject(this.project.$key, updateObj);
      })
      .then(() => {
        if(updateQuote.quote === '' && this.quoteId === '') {
          return Promise.resolve();
        } else {
          const quoteKey = updateQuote.quote === 'No Match' ?
          this.quoteHolder[this.quoteId].$key :
          this.quoteHolder[updateQuote.quote].$key;
          const projectKey = updateQuote.quote === 'No Match' ? '' : this.project.$key;
          return this.projectsService.updateQuote(projectKey, quoteKey);
        }
      })
      .then(()=>{
        let log = "Updated project";
        return this.auditsService.log(log, "", this.project.id, "");
      })
      .then(() => {
        this._snackBar.open('Project Saved!', 'close', {
          duration: 2000,
        });
        this.submitted = false;
      });
    }
  }
}
