import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StackingRoutingModule } from './stacking-routing.module';
import { StackingComponent } from './stacking.component';
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {NgxPrintModule} from 'ngx-print';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { DataExportModule } from 'src/app/shared/modules/data-export/data-export.module';

@NgModule({
  declarations: [StackingComponent, DeleteDialogComponent],
  imports: [
    CommonModule,
    StackingRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    DataExportModule,
    MatGridListModule,
    DragDropModule,
    NgxPrintModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  entryComponents: [DeleteDialogComponent]
})
export class StackingModule { }
