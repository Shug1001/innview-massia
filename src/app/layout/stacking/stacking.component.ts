import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AreasService, AuthService, ProjectsService } from 'src/app/shared/services';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import Srd  from '../../../assets/srd/srd.json';
import { Project, JobsheetDetail } from '../../shared/models/models';
import {MatDialog} from '@angular/material';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

import {
  CdkDragStart,
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { PanelsService } from 'src/app/shared/services/panels.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { DateUtilService } from 'src/app/shared/services/date-util.service';
import { ReferenceDataService } from 'src/app/shared/services/reference-data.service';
import { C } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-stacking',
  templateUrl: './stacking.component.html',
  styleUrls: ['./stacking.component.scss']
})
export class StackingComponent implements OnInit {

  _subscription: Subscription;
  _subscription2: Subscription;
  _subscription3: Subscription;
  projects: Array<any>;
  projectIds: Array<any>;
  projectMapped: Object;
  areas: Array<any>;
  jobsheets: Array<any>;
  areasIds: Array<any>;
  jobSheetsMapped: Object;
  areasMapped: Object;
  panels: Array<any>;
  panelIds: Array<any>;
  panelsMapped: Object;
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  myControl2 = new FormControl();
  filteredOptions2: Observable<string[]>;
  myControl3 = new FormControl();
  filteredOptions3: Observable<string[]>;
  largestPanelLength: Number;
  largestPanelHeight: Number;
  loaded: boolean;
  jobsheetNumber: string;
  area: string;
  project: Project;
  saveDisabled: boolean;
  jobsheetIds: Array<any>;
  update: boolean;
  productTypeProductionLine: object;
  productTypes: object;
  users: Array<any>;
  user: string;
  jobsheet: JobsheetDetail;
  revertLength: boolean;
  revertHeight: boolean;
  revertDepth: boolean;
  revertOpening: boolean;
  revertPanel: boolean;
  exportDisabled: boolean;
  exportData: Array<any>;
  panelKeyMapped: object;

  constructor(
    private dateUtilService: DateUtilService,
    private authService: AuthService,
    private projectsService: ProjectsService,
    private db: AngularFireDatabase,
    private areasService: AreasService,
    private panelService: PanelsService,
    private referenceDataService: ReferenceDataService,
    public dialog: MatDialog
    ) { }

  openDeleteDialog(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if(result === 'delete'){
        return this.delete();
      }
    });
  }

  ngOnInit() {

    this.revertLength = false;
    this.revertHeight = false;
    this.revertDepth = false;
    this.revertOpening = false;
    this.revertPanel = false;
    this.exportDisabled = true;
    this.exportData = [];

    this.productTypes = Srd.productTypes;
    this.productTypeProductionLine = this.dateUtilService.buildMap(this.productTypes, "productionLine");
    this.saveDisabled = true;
    this.loaded = false;
    this.update = true;
    this.jobsheetNumber = "Creating new jobsheet";
    this._subscription = combineLatest(this.referenceDataService.getUsers(), this.projectsService.getActiveProjectsList()).subscribe(([usersData, projects]) => {
      this.users = usersData;
      this.projects = projects;
      this.projectIds = [];
      this.projectMapped = {};

      this.users.forEach((user)=>{
        if(user.$key === this.authService.currentUserId){
          this.user = user.name;
        }
      });

      projects.forEach((project)=>{
        this.projectIds.push(project.id);
        if(this.projectMapped[project.id] === undefined){
          this.projectMapped[project.id] = project;
        }
      });

      this.projectIds.sort();

      this.loaded = true;

      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    });

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.projectIds.filter(projectIds => projectIds.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionProjectChange(event){
    console.log('onProjectChange', this.projectMapped[event.option.value].$key);
    this._subscription2 = this.areasService.getAreasForProject(this.projectMapped[event.option.value].$key).subscribe(areas=>{
      this.project = this.projectMapped[event.option.value];
      this.areas = areas;
      this.areasIds = [];
      this.areasMapped = {};

      areas.forEach((areaSnap)=>{
        let area = areaSnap.payload.val();
        let areaId = area.phase + "-" + area.floor + "-" + area.type;
        this.areasIds.push(areaId);
        if(this.areasMapped[areaId] === undefined){
          this.areasMapped[areaId] = areaSnap.key;
        }
      });

      this.areasIds.sort();

      this.filteredOptions2 = this.myControl2.valueChanges.pipe(
        startWith(''),
        map(value => this._filter2(value))
      );
    });

  }

  private _filter2(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.areasIds.filter(areaId => areaId.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filter3(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.jobsheetIds.filter(jobsheetId => jobsheetId.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionAreaChange(event){
    console.log('onAreaChange', this.areasMapped[event.option.value]);
    this._subscription3 = combineLatest([this.panelService.getPanelsForArea(this.areasMapped[event.option.value]),this.panelService.getJobsheetsForArea(this.areasMapped[event.option.value])]).subscribe(([panels, jobsheets]) => {
      this.area = this.areasMapped[event.option.value];
      this.panels = panels;
      this.jobsheets = jobsheets;
      this.panelIds = [];
      this.jobsheetIds = [];
      this.jobSheetsMapped = {};
      this.panelKeyMapped = {};
      this.panelsMapped = {};
      this.largestPanelLength = 0;
      this.largestPanelHeight = 0;
      this.myControl.disable();
      this.myControl2.disable();

      this.jobsheets.sort(function(a, b){return a.projectNumber - b.projectNumber});

      this.jobsheets.forEach((jobsheet)=>{
        this.jobsheetIds.push(jobsheet.projectNumber);
        if(this.jobSheetsMapped[jobsheet.projectNumber] === undefined){
          this.jobSheetsMapped[jobsheet.projectNumber] = jobsheet;
        }
      });

      this.filteredOptions3 = this.myControl3.valueChanges.pipe(
        startWith(''),
        map(value => this._filter3(value))
      );

      this.panels.forEach((panel)=>{

        this.panelIds.push(panel.id);
        if(this.panelKeyMapped[panel.$key] === undefined){
          this.panelKeyMapped[panel.$key] = panel.id;
        }
        if(this.panelsMapped[panel.id] === undefined){
          this.panelsMapped[panel.id] = panel;
          this.panelsMapped[panel.id].flip = false;
          if(this.panelsMapped[panel.id].dimensions.length > this.largestPanelLength){
            this.largestPanelLength = this.panelsMapped[panel.id].dimensions.length;
          }

          if(this.panelsMapped[panel.id].dimensions.width > this.largestPanelHeight){
            this.largestPanelHeight = this.panelsMapped[panel.id].dimensions.height;
          }
          
        }
        
      });

      this.panelIds.sort();

    });
  }

  opening(panel){
    let returnVal = "No";
    let opening = panel.additionalInfo.doors || 0 + panel.additionalInfo.windows || 0;
    if(opening){
      returnVal = "Yes";
    }
    return returnVal;
  }

  rotate(panel){
    let length = panel.dimensions.length;
    let height = panel.dimensions.height;
    panel.dimensions.length = height;
    panel.dimensions.height = length;
    panel.flip = !panel.flip;

    this.panels.forEach((panel)=>{
      if(this.panelsMapped[panel.id].dimensions.length > this.largestPanelLength){
        this.largestPanelLength = this.panelsMapped[panel.id].dimensions.length;
      }

      if(this.panelsMapped[panel.id].dimensions.width > this.largestPanelHeight){
        this.largestPanelHeight = this.panelsMapped[panel.id].dimensions.height;
      }
    });
  }

  buildExportData(){

    let exportData = [];

    let panelData = {};

    if(this.jobsheet !== undefined){

      for(let i = 0; i<this.done8.length;i++){
        let panel = this.done8[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 1,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done7.length;i++){
        let panel = this.done7[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 2,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done6.length;i++){
        let panel = this.done6[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 3,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done5.length;i++){
        let panel = this.done5[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 4,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done4.length;i++){
        let panel = this.done4[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 5,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done3.length;i++){
        let panel = this.done3[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 6,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done2.length;i++){
        let panel = this.done2[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 7,
            "pos": i+1
          }
        }
      }

      for(let i = 0; i<this.done1.length;i++){
        let panel = this.done1[i];
        if(panelData[panel] === undefined){
          panelData[panel] = {
            "level": 8,
            "pos": i+1
          }
        }
      }

      Object.keys(this.jobsheet.panels).forEach(panel => {

        let jobsheetData = {
          "Jobsheet No.": this.jobsheet.projectNumber,
          "Panel": this.panelKeyMapped[panel],
          "Level": panelData[this.panelKeyMapped[panel]].level,
          "Pos": panelData[this.panelKeyMapped[panel]].pos,
        }

        exportData.push(jobsheetData);

      });

      exportData.sort((a, b) => {
          var r = a.Level - b.Level;
          if (r === 0) {
            r = a.Pos - b.Pos;
          }
          return r;
        }
      );

      this.exportData = exportData;
    }
  }

  onSelectionJobsheetChange(event){
    console.log('onAreaChange', this.jobSheetsMapped[event.option.value]);
    this.clear();
    this.exportDisabled = false;
    this.update = false;

    this.jobsheetNumber = event.option.value;

    let jobsheet = this.jobSheetsMapped[this.jobsheetNumber];

    this.jobsheet = this.jobSheetsMapped[this.jobsheetNumber];

    if(jobsheet.done1 !== undefined){
      this.done1 = Object.keys(jobsheet.done1);
    }
    if(jobsheet.done2 !== undefined){
      this.done2 = Object.keys(jobsheet.done2);
    }
    if(jobsheet.done3 !== undefined){
      this.done3 = Object.keys(jobsheet.done3);
    }
    if(jobsheet.done4 !== undefined){
      this.done4 = Object.keys(jobsheet.done4);
    }
    if(jobsheet.done5 !== undefined){
      this.done5 = Object.keys(jobsheet.done5);
    }
    if(jobsheet.done6 !== undefined){
      this.done6 = Object.keys(jobsheet.done6);
    }
    if(jobsheet.done7 !== undefined){
      this.done7 = Object.keys(jobsheet.done7);
    }
    if(jobsheet.done8 !== undefined){
      this.done8 = Object.keys(jobsheet.done8);
    }
    
    this.panelIds.forEach(done => {
      if(jobsheet.panels && jobsheet.panels[this.panelsMapped[done].$key]){
        this.rotate(this.panelsMapped[done]);
      }
    });

    this.alldone = this.done1.concat(this.done2, this.done3, this.done4, this.done5, this.done6, this.done7, this.done8);

    this.buildExportData();
  }

  done1 = [];
  done2 = [];
  done3 = [];
  done4 = [];
  done5 = [];
  done6 = [];
  done7 = [];
  done8 = [];
  alldone = [];

  drop(event: CdkDragDrop<string[]>) {

    this.saveDisabled = false;
    
    let sumArr = [];
    event.container.data.forEach(done => {
      let length = this.panelsMapped[done].dimensions.length || 0;
      sumArr.push(length);
    });
    let length = 0;
    if(sumArr.length){
       length = sumArr.reduce((a, b) => a + b, 0);
    }

    length = length + this.panelsMapped[event.previousContainer.data[event.previousIndex]].dimensions.length || 0;

    if(length > this.largestPanelLength){
      return;
    }

    // If current element has ".selected"
    if(event.item.element.nativeElement.classList.contains("selected")){
      this.multiDrag.dropListDropped(event);
    }
    // If dont have ".selected" (normal case)
    else{  
      if (event.previousContainer === event.container) {
        moveItemInArray(
          event.container.data,
          event.previousIndex,
          event.currentIndex
        );
      } else {
        transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex
        );
      }
    }
    this.alldone = this.done1.concat(this.done2, this.done3, this.done4, this.done5, this.done6, this.done7, this.done8);
  }

  disectJobsheet(jobsheet, type){
    if(jobsheet !== undefined){
      let disectArr = jobsheet.split("-");
      if(type === "project"){
        return disectArr[1];
      } else if(type === "phase"){
        return disectArr[2];
      }else if(type === "floor"){
        return disectArr[3];
      }
    }
  }

  deletePanel(panel, stack, stackName){
    const index = stack.indexOf(panel.id);
    if (index > -1) { // only splice array when item is found
      stack.splice(index, 1); // 2nd parameter means remove one item only
    }
    this.panelIds.push(panel.id);
    this.panelIds.sort();
    this.alldone = this.done1.concat(this.done2, this.done3, this.done4, this.done5, this.done6, this.done7, this.done8);
    if(!this.update){
      let jobsheet = Object.assign({}, this.jobSheetsMapped[this.jobsheetNumber]);
      let jobsheetKey = jobsheet.$key;
      delete jobsheet[stackName][panel.id];
      delete jobsheet.panels[panel.$key];
      delete jobsheet.$key;
      return this.db.list("/jobsheetDetails").set(jobsheetKey, jobsheet)
      .then(() => {
        return this.panelService.updatepanel(panel.$key, {jobSheetNo: null});
      });
    }
  }

  delete(){
    let jobsheet = this.jobSheetsMapped[this.jobsheetNumber];
    return this.db.list("/jobsheetDetails").set(jobsheet.$key, {})
    .then(() => {
      let updatePanels = [];
      this.alldone.forEach(done => {
        updatePanels.push(this.panelService.updatepanel(this.panelsMapped[done].$key, {jobSheetNo: null}));
      });
      return Promise.all(updatePanels);
    })
    .then(()=>{
      this.clear();
    });
  }

  calculateStats(type){

    let total = 0;  

    if(type === "weight"){

      let maxArr = [];
      this.alldone.forEach(done => {
        let weight = this.panelsMapped[done].dimensions.weight || 0;
        maxArr.push(weight);
      });
      if(maxArr.length){
        total = maxArr.reduce((a, b) => a + b, 0);
      }

    }

    if(type === "area"){

      let maxArr = [];
      this.alldone.forEach(done => {
        let area = this.panelsMapped[done].dimensions.area || 0;
        maxArr.push(area);
      });
      if(maxArr.length){
        total = maxArr.reduce((a, b) => a + b, 0);
        total = Math.round(total * 10) / 10
      }

    }
    if(type === "height"){

      let maxArr = [];
      this.alldone.forEach(done => {
        let height = this.panelsMapped[done].dimensions.height || 0;
        maxArr.push(height);
      });
      if(maxArr.length){
        total = Math.max(...maxArr);
      }

    }
    if(type === "length"){

      let maxArr = [];

      let sumArr1 = [];
      this.done1.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr1.push(length);
      });
      let length1 = 0;
      if(sumArr1.length){
         length1 = sumArr1.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length1);

      let sumArr2 = [];
      this.done2.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr2.push(length);
      });
      let length2 = 0;
      if(sumArr2.length){
         length2 = sumArr2.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length2);

      let sumArr3 = [];
      this.done3.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr3.push(length);
      });
      let length3 = 0;
      if(sumArr3.length){
         length3 = sumArr3.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length3);

      let sumArr4 = [];
      this.done4.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr4.push(length);
      });
      let length4 = 0;
      if(sumArr4.length){
         length4 = sumArr4.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length4);

      let sumArr5 = [];
      this.done5.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr5.push(length);
      });
      let length5 = 0;
      if(sumArr5.length){
         length5 = sumArr5.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length5);

      let sumArr6 = [];
      this.done6.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr6.push(length);
      });
      let length6 = 0;
      if(sumArr6.length){
         length6 = sumArr6.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length6);

      let sumArr7 = [];
      this.done7.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr7.push(length);
      });
      let length7 = 0;
      if(sumArr7.length){
         length7 = sumArr7.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length7);

      let sumArr8 = [];
      this.done8.forEach(done => {
        let length = this.panelsMapped[done].dimensions.length || 0;
        sumArr8.push(length);
      });
      let length8 = 0;
      if(sumArr8.length){
         length8 = sumArr8.reduce((a, b) => a + b, 0);
      }
      maxArr.push(length8);

      total = Math.max(...maxArr);

    }
    if(type === "depth"){
      let maxArr1 = [];
      this.done1.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr1.push(depth);
      });
      let depth1 = 0;
      if(maxArr1.length){
         depth1 = Math.max(...maxArr1);
      }

      let maxArr2 = [];
      this.done2.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr2.push(depth);
      });
      let depth2 = 0;
      if(maxArr2.length){
         depth2 = Math.max(...maxArr2);
      }

      let maxArr3 = [];
      this.done3.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr3.push(depth);
      });
      let depth3 = 0;
      if(maxArr3.length){
         depth3 = Math.max(...maxArr3);
      }

      let maxArr4 = [];
      this.done4.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr4.push(depth);
      });
      let depth4 = 0;
      if(maxArr4.length){
         depth4 = Math.max(...maxArr4);
      }

      let maxArr5 = [];
      this.done5.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr5.push(depth);
      });
      let depth5 = 0;
      if(maxArr5.length){
         depth5 = Math.max(...maxArr5);
      }

      let maxArr6 = [];
      this.done6.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr6.push(depth);
      });
      let depth6 = 0;
      if(maxArr6.length){
         depth6 = Math.max(...maxArr6);
      }

      let maxArr7 = [];
      this.done7.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr7.push(depth);
      });
      let depth7 = 0;
      if(maxArr7.length){
         depth7 = Math.max(...maxArr7);
      }

      let maxArr8 = [];
      this.done8.forEach(done => {
        let depth = this.panelsMapped[done].dimensions.width !== undefined ? this.panelsMapped[done].dimensions.width : this.panelsMapped[done].dimensions.depth || 0;
        maxArr8.push(depth);
      });
      let depth8 = 0;
      if(maxArr8.length){
         depth8 = Math.max(...maxArr8);
      }

      total = depth1 + depth2 + depth3 + depth4 + depth5 + depth6 + depth7 + depth8;
    }
    return total;
  }

  percentage(partialValue, totalValue) {
    return (100 * partialValue) / totalValue;
  } 

  save(){

    let jobsheetArray = this.panels[0].id.split("-");
    let jobsheetNumber = this.padLeadingZeros(1, 3);
    if (this.jobsheets.length){
      jobsheetNumber = this.jobsheets[this.jobsheets.length-1].projectNumber.split("-");
      jobsheetNumber = this.padLeadingZeros(parseInt(jobsheetNumber[jobsheetNumber.length-1], 10) + 1, 3);
    }
    let jobsheet = {
      projectNumber: "JS-"+jobsheetArray[0]+"-"+jobsheetArray[2]+"-"+jobsheetArray[3]+"-"+jobsheetArray[4]+"-"+jobsheetNumber,
      panels: {},
      area: this.area,
      createdBy: this.user,
      done1: {},
      done2: {},
      done3: {},
      done4: {},
      done5: {},
      done6: {},
      done7: {},
      done8: {},
      timestamps: {
        created: this.authService.serverTime(),
        modified: this.authService.serverTime()
      }
    }

    this.done1.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done1[done] === undefined){
        jobsheet.done1[done] = true;
      }
    });

    this.done2.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done2[done] === undefined){
        jobsheet.done2[done] = true;
      }
    });

    this.done3.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done3[done] === undefined){
        jobsheet.done3[done] = true;
      }
    });

    this.done4.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done4[done] === undefined){
        jobsheet.done4[done] = true;
      }
    });

    this.done5.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done5[done] === undefined){
        jobsheet.done5[done] = true;
      }
    });

    this.done6.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done6[done] === undefined){
        jobsheet.done6[done] = true;
      }
    });

    this.done7.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done7[done] === undefined){
        jobsheet.done7[done] = true;
      }
    });

    this.done8.forEach(done => {
      if(jobsheet.panels[this.panelsMapped[done].$key] === undefined){
        jobsheet.panels[this.panelsMapped[done].$key] = this.panelsMapped[done].flip;
      }
      if(jobsheet.done8[done] === undefined){
        jobsheet.done8[done] = true;
      }
    });

    console.log(JSON.stringify(jobsheet));
    return this.db.list("/jobsheetDetails").push(jobsheet)
    .then(()=>{
      let updatePanels = [];
      this.alldone.forEach(done => {
        updatePanels.push(this.panelService.updatepanel(this.panelsMapped[done].$key, {jobSheetNo: jobsheet.projectNumber}));
      });
      return Promise.all(updatePanels);
    })
    .then(()=>{
      this.clear();
    });
  }

  printDetails() {  
    document.getElementById('hidden_div').style.display='block'
  } 

  padLeadingZeros(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  getColour(floor){
    if(floor === "GF"){
      return "red";
    }else if(floor === "1F"){
      return "blue";
    }else if(floor === "2F"){
      return "green";
    }else if(floor === "3F"){
      return "pink";
    }else if(floor === "RF"){
      return "orange";
    }else{
      return "black";
    }
  }

  sort(type){

    if(this.panelIds){
      if(type === "length"){
        this.panelIds = this.panelIds.sort((a,b) => this.panelsMapped[a].dimensions.length - this.panelsMapped[b].dimensions.length);
        if(this.revertLength){
          this.panelIds.reverse();
        }
        this.revertLength = !this.revertLength;
      }
      if(type === "height"){ 
        this.panelIds = this.panelIds.sort((a,b) => this.panelsMapped[a].dimensions.height - this.panelsMapped[b].dimensions.height);
        if(this.revertHeight){
          this.panelIds.reverse();
        }
        this.revertHeight = !this.revertHeight;
      }
      if(type === "depth"){    
        this.panelIds = this.panelIds.sort((a,b) => {
          a = this.panelsMapped[a].dimensions.width !== undefined ? this.panelsMapped[a].dimensions.width : this.panelsMapped[a].dimensions.depth || 0;
          b = this.panelsMapped[b].dimensions.width !== undefined ? this.panelsMapped[b].dimensions.width : this.panelsMapped[b].dimensions.depth || 0;
          return a - b;
        });
        if(this.revertDepth){
          this.panelIds.reverse();
        }
        this.revertDepth = !this.revertDepth;
      }
      if(type === "opening"){
        this.panelIds = this.panelIds.sort((a,b) => {
          a = this.opening(this.panelsMapped[a]);
          b = this.opening(this.panelsMapped[b]);
          return a.localeCompare(b);
        });
        if(this.revertOpening){
          this.panelIds.reverse();
        }
        this.revertOpening = !this.revertOpening;
      }
      if(type === "panel"){
        this.panelIds = this.panelIds.sort();
        if(this.revertPanel){
          this.panelIds.reverse();
        }
        this.revertPanel = !this.revertPanel;
      } 
    }
  }

  clear(){

    this.myControl.enable();
    this.myControl2.enable();

    this.exportDisabled = true;
    this.exportData = [];

    this.panelIds = [];
    this.panelsMapped = {};

    if(this.panels){

      this.panels.forEach((panel)=>{

        this.panelIds.push(panel.id);
        if(this.panelsMapped[panel.id] === undefined){
          this.panelsMapped[panel.id] = panel;
          this.panelsMapped[panel.id].flip = false;
          if(this.panelsMapped[panel.id].dimensions.length > this.largestPanelLength){
            this.largestPanelLength = this.panelsMapped[panel.id].dimensions.length;
          }

          if(this.panelsMapped[panel.id].dimensions.width > this.largestPanelHeight){
            this.largestPanelHeight = this.panelsMapped[panel.id].dimensions.height;
          }
          
        }
        
      });

    }

    this.panelIds.sort();
    this.done1 = [];
    this.done2 = [];
    this.done3 = [];
    this.done4 = [];
    this.done5 = [];
    this.done6 = [];
    this.done7 = [];
    this.done8 = [];
    this.alldone = [];
    this.saveDisabled = true;
    this.update = true;
    this.jobsheetNumber = "Creating new jobsheet";
  }

  ngOnDestroy(){
    if(this._subscription){
      this._subscription.unsubscribe();
    }
    if(this._subscription2){
      this._subscription2.unsubscribe();
    }
    if(this._subscription3){
      this._subscription3.unsubscribe();
    }
  }

  	// Multi Select
	multiSelect = { // Put ".selected" on elements when clicking after longPress or Ctrl+Click
		// Initial Variables
		longPressTime: 500, // in ms unit
		verifyLongPress: 0,
		multiSelect: false,
		verifyDragStarted: false,
		ctrlMode: false,
		firstContainer: null as unknown as HTMLElement,

		selectDrag(el: HTMLElement) {
			while (!el.classList.contains("cdk-drag")) {
				el = el.parentElement as HTMLElement;
			}
			return el;
		},

		mouseDown(e: Event) {
			let target = this.selectDrag(e.target as HTMLElement);
			let ctrlKey = (e as KeyboardEvent).ctrlKey;

			if (this.multiSelect) { // If multiSelect is enabled

				/* The responsibility for removing only the first ".selected" has to be with mouseDown and not with mouseUp.
				   if not you can't add the first one */

				// Remove
				let allSelected = document.querySelectorAll(".selected").length;
				if (allSelected == 1 && target.classList.contains("selected") && (this.ctrlMode ? ctrlKey : true)) { // If only have one ".selected" and it was clicked
					target.classList.remove("selected", "last");  // remove ".selected" and ".last"
					this.multiSelect = false; // turns off multiSelect
				}
			}

			else { // If multiSelect is disabled
				// Do this
				let addSelected = () => {
					this.multiSelect = true; // enable multiSelect
					this.firstContainer = target.parentElement as HTMLElement; //saves the container of the first selected element
					target.classList.add("selected", "last"); // and add ".selected" and ".last" to the current element clicked
				}

				// If using CTRL
				if (ctrlKey) {
					this.ctrlMode = true;
					addSelected();
				};

				// If using longPress
				this.verifyLongPress = <any>setTimeout(() => { // If there is a LongPress
					this.ctrlMode = false;
					addSelected();
				}, this.longPressTime); // after "longPressTime"(ms)
			}
		},

		mouseUp(e: Event) {
			clearTimeout(this.verifyLongPress); // cancel LongPress

			if (this.multiSelect && !this.verifyDragStarted) { // If multiSelect is enabled AND not start DragStarted
				let target = this.selectDrag(e.target as HTMLElement);
				let allSelected = document.querySelectorAll(".selected");
				let ctrlKey = (e as KeyboardEvent).ctrlKey;
				let last = document.querySelector(".last");

				// If use Shift
				if (last && (e as KeyboardEvent).shiftKey) {
					// take range informations
					let containerLast = Array.from(last.parentElement!.children);
					let lastIndex = containerLast.indexOf(last);
					let currIndex = containerLast.indexOf(target);
					let max = Math.max(lastIndex, currIndex);
					let min = Math.min(lastIndex, currIndex);

					// toggle .selected in the range
					for (let i = min; i <= max; i++) {
						if (i != lastIndex) { // Does not toggle the penult element clicked
							containerLast[i].classList.toggle("selected");
						}
					}

					// put .last if last clicked was selected at end
					if (target.classList.contains("selected")) {
						last && last.classList.remove("last"); // remove .last from penult element clicked
						target.classList.add("last"); // and add ".last" to the current element
					}
				}

				//If don't use shift
				else {
					// To remove from selection
					/* responsibility to remove from selection assigned to mouseUp */
					if (target.classList.contains("selected") && allSelected.length > 1 && (this.ctrlMode ? ctrlKey : true)) { // If the clicked element already has ".selected" AND If you have more than 1 (not to remove the first one added)
						target.classList.remove("selected"); // remove ".selected"
						target.classList.remove("last"); // remove ".last"
					}

					// To add to selection
					else { // if the clicked element does not have the ".selected"
						if (this.firstContainer == target.parentElement && (this.ctrlMode ? ctrlKey : true)) { //if the item click is made within the same container
							last && last.classList.remove("last"); // remove .last from penult element clicked
							target.classList.add("selected", "last"); // add ".selected" and ".last"
						}
						else if (this.ctrlMode ? ctrlKey : true) { // if in different container, and with ctrl (if ctrl)
							allSelected.forEach((el) => { // remove all selected from last container
								el.classList.remove("selected", "hide", "last");
							});
							this.firstContainer = target.parentElement as HTMLElement; //saves the container of the new selected element
							target.classList.add("selected", "last"); // and add ".selected" to the element clicked in the new container
						}
					}

				}
			}
		},

		dragStarted() {
			this.verifyDragStarted = true; // shows to mouseDown and mouseUp that Drag started
			clearTimeout(this.verifyLongPress); // cancel longPress
		},

		dragEnded() {
			this.verifyDragStarted = false; // show mouseDown and mouseUp that Drag is over
		},

		dropListDropped(e: CdkDragDrop<string[]>) {
			let el = e.item.element.nativeElement;
			if (el.classList.contains("selected")) { // the dragged element was of the "selected" class
				this.multiSelect = false; // disable multiSelect
			}
		},

	}

	// Multi Drag
	multiDrag = { // Adjusts clicked items that have ".selected" to organize together
		// Initial Variables
		dragList: [""], // has the value of the selected items in sequence from listData
		dragListCopy: [""], // a copy of the listData, but with the selected elements marked with "DragErase" to delete later
		dragErase: Symbol("DragErase") as any, // a symbol to have unique value when deleting

		dragStarted(e: CdkDragStart) {
			if (e.source.element.nativeElement.classList.contains("selected")) { // If the dragged element has ".selected"
				//prepare
				let listData = e.source.dropContainer.data; // get list data value
				this.dragList = []; // reset the dragList
				this.dragListCopy = [...listData]; // copy listData into variable
				let DOMdragEl = e.source.element.nativeElement; // dragged element
				let DOMcontainer = Array.from(DOMdragEl.parentElement!.children); // container where all draggable elements are
				let DOMdragElIndex = DOMcontainer.indexOf(DOMdragEl); // index of the dragged element
				let allSelected = document.querySelectorAll(".selected"); // get all the ".selected"

				// Goes through all ".selected"
				allSelected.forEach((eli) => {
					// get index of current element
					let CurrDOMelIndexi = DOMcontainer.indexOf(eli);

					// Add listData of current ".selected" to dragList
					this.dragList.push(listData[CurrDOMelIndexi]);

					// Replaces current position in dragListCopy with "DragErase" (to erase exact position later)
					this.dragListCopy[CurrDOMelIndexi] = this.dragErase;

					// Put opacity effect (by CSS class ".hide") on elements (after starting Drag)
					if (DOMdragElIndex !== CurrDOMelIndexi) {
						eli.classList.add("hide");
					}
				});

			}
		},

		dropListDropped(e: CdkDragDrop<string[]>) {

			if (e.previousContainer === e.container) { // If in the same container

				let posAdjust = e.previousIndex < e.currentIndex ? 1 : 0; // Adjusts the placement position
				this.dragListCopy.splice(e.currentIndex + posAdjust, 0, ...this.dragList); // put elements in dragListCopy
				this.dragListCopy = this.dragListCopy.filter((el) => (el !== this.dragErase)); // remove the "DragErase" from the list

				// Pass item by item to final list
				for (let i = 0; i < e.container.data.length; i++) {
					e.container.data[i] = this.dragListCopy[i];
				}

			}

			else { // If in different containers

				// remove the "DragErase" from the list
				this.dragListCopy = this.dragListCopy.filter((el) => (el !== this.dragErase));

				// Pass item by item to initial list
				for (let i = 0; i < e.previousContainer.data.length; i++) {
					e.previousContainer.data[i] = this.dragListCopy[i];
				}
				for (let i = 0; i < this.dragList.length; i++) {
					e.previousContainer.data.pop();
				}


				let otherListCopy = [...e.container.data]; // list of new container
				otherListCopy.splice(e.currentIndex, 0, ...this.dragList); // put elements in otherListCopy

				// Pass item by item to final list
				for (let i = 0; i < otherListCopy.length; i++) {
					e.container.data[i] = otherListCopy[i];
				}

			}

			// Remove ".hide"
			let allHidden = document.querySelectorAll(".hide");
			allHidden.forEach((el) => {
				el.classList.remove("hide");
			});
			// Remove ".selected" after 300ms
			setTimeout(() => {
				let allSelected = document.querySelectorAll(".selected");
				allSelected.forEach((el) => {
					el.classList.remove("selected", "last");
				});
			}, 300);


			this.dragListCopy = []; // reset the dragListCopy
			this.dragList = []; // reset the dragList
		},

	}


}
