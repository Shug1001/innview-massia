import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIssuedComponent } from './design-issued.component';

xdescribe('DesignIssuedComponent', () => {
  let component: DesignIssuedComponent;
  let fixture: ComponentFixture<DesignIssuedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignIssuedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIssuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
