import { Component, OnInit } from '@angular/core';
import { ForwardPlannerService, AreasService } from 'src/app/shared/services';
import { Subscription, combineLatest } from 'rxjs';
import { designIssued } from 'src/app/shared/models/models';
import { DateUtilService } from 'src/app/shared/services/date-util.service';

@Component({
  selector: 'app-design-issued',
  templateUrl: './design-issued.component.html',
  styleUrls: ['./design-issued.component.scss']
})
export class DesignIssuedComponent implements OnInit {

  _subscription: Subscription;
  _subscription2: Subscription;
  statsMapped: object;
  checkedDFMRange: object;
  designIssued: object;

  loaded: boolean;

  constructor(
    private areasService: AreasService,
    private forwardPlannerService: ForwardPlannerService) {
    this.loaded = false;
  }

  ngOnInit() {

    this._subscription = combineLatest(this.areasService.checkedCount(), this.forwardPlannerService.getDesignIssued()).subscribe(([checkedDFMRange, designIssued])=>{
      this.checkedDFMRange = checkedDFMRange;
      this.designIssued = designIssued;
      this.loaded = true;
    });

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
