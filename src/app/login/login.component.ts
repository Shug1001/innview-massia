import { Component, AfterViewInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [ AuthService ]
})
export class LoginComponent implements AfterViewInit {

    public password: string;
    public email: string;

    constructor(@Inject('AuthService') private authService, private router: Router) {}

    ngAfterViewInit() {
        const matCardTitle = <HTMLElement> document.querySelector('.mat-card-title');
        matCardTitle.setAttribute('style', 'text-align: center; font-size: 32px; font-weight: bold');
        matCardTitle.innerHTML = 'Innview';

        const matCard = <HTMLElement> document.querySelector('.mat-tab-body-wrapper');
        matCard.setAttribute('style', 'min-width:500px');
    }

    // onLogin() {
    //     this.authService.emailLogin(this.email, this.password)
    //     .then((value) => {
    //         if(value){
    //             localStorage.setItem('isLoggedin', 'true');
    //             this.router.url;
    //             return this.router.navigate(['/projects']);
    //         }else{
    //             return null;
    //         }

    //     })
    //     .catch(error => console.log(error));
    // }

    printUser(event) {
        console.log(event);
        localStorage.setItem('isLoggedin', 'true');
        return this.router.navigate(['/projects']);
    }
 
    printError(event) {
        console.error(event);
    }
}
